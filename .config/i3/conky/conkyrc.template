conky.config = {
	alignment = 'top_left',
	background = true,
	cpu_avg_samples = 2,
	default_color = '#F6DFBB',
	default_outline_color = '#111111',
	default_shade_color = 'black',
    disable_auto_reload = true,
	double_buffer = true,
	draw_borders = false,
	draw_graph_borders = true,
	draw_outline = false,
	draw_shades = false,
	font = 'Dejavu:size=08',
	gap_x = 0,
	gap_y = 0,
    maximum_width = ___width___ ,
    minimum_width = ___width___ ,
    net_avg_samples = 2,
	no_buffers = true,
    out_to_console = false,
    out_to_ncurses = false,
    out_to_stderr = false,
    out_to_x = true,
	override_utf8_locale = true,
	own_window = true,
	own_window_argb_value = 50,
	own_window_argb_visual = true,
	own_window_colour = e3e3e3,
	own_window_class = 'Conky',
	own_window_hints = 'below,sticky,undecorated,skip_taskbar,skip_pager',	
	own_window_transparent = false,
	own_window_type = 'desktop',
    show_graph_range = true,
    show_graph_scale = true,
    stippled_borders = 0,
    top_cpu_separate = true,
	total_run_times = 0,
	update_interval = 2.0,
    uppercase = false,
    use_spacer = 'none',
	use_xft = true,
}

conky.text = [[\
${alignc}${font Fantasque Sans Mono:pixelsize=62}${time %H:%M}${font}
${alignc}${execbar 3,150 "echo "$(date +%S)* 10/6" | bc"}
${alignc}${font Dejavu:pixelsize=21}${time %d} ${time %B} ${time %Y}${font}
${voffset -10}${font Liberation Mono:pixelsize=14}${execpi 3600 printf "\\n"; cal | sed ";s/^/   /g;s@ $(date +%d | sed 's/^0*//') @[$(date +%d | sed 's/^0*//')]@;1d"}${font}
${voffset -10}${if_existing .todo}${goto 150} todo: ${exec grep 'status=""' ~/.todo | wc -l}${endif}
${offset 7}${font Comfortaa:pixelsize=22}$USER@${nodename}${font}
${voffset 7}${goto 15}${exec lscpu | grep 'Model name:' | sed "s/(TM)//g;s/(R)//g;s/CPU //g;s/Model name://;s/^ *//"}
${goto 15}Kernel: ${kernel} ${goto 170}Caps:${goto 204}${key_caps_lock}
${goto 15}Uptime: ${uptime} ${goto 170}Num:${goto 204}${key_num_lock}
${goto 10}${font Roboto:pixelsize=24}${exec ~/.config/i3/i3-list-workspaces.sh}${font}

${goto 3}${top pid 1}${goto 52}${top name 1}${goto 160}${top cpu 1} ${top mem 1}
${goto 3}${top pid 2}${goto 52}${top name 2}${goto 160}${top cpu 2} ${top mem 2}
${goto 3}${top pid 3}${goto 52}${top name 3}${goto 160}${top cpu 3} ${top mem 3}
${goto 3}${top pid 4}${goto 52}${top name 4}${goto 160}${top cpu 4} ${top mem 4}

# 	swapbar           (height)      Bar that shows amount of swap in use     
# 	swap                            Amount of swap in use                    
# 	swapmax                         Total amount of swap                     
# 	swapperc                        Percentage of swap in use        

${color}${if_match ${cpu /}>=90}${color ff0000}${endif}${offset 7}CPU ${voffset -23}${goto 46}${cpugraph 30,100}${goto 150}${voffset 5}${freq_g}GHz${voffset 14}${goto 150}${cpu}%${color}${if_match ${acpitemp}>=90}${color ff0000}${endif}${goto 185}${acpitemp}°C${color}${voffset -25}
___CORES___
${offset 7}RAM${goto 46}${membar 10,100}${goto 150}${mem} (${memperc}%)
${offset 7}DISK${goto 46}${fs_bar 10,100 /home}${goto 150}${fs_free /home} free
${offset 7}SWAP${goto 46}${swapbar 10,100}${goto 150}${swap} (${swapperc}%)
___BAT___
___NET___
${voffset -65}
]]
