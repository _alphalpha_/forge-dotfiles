#!/bin/sh

active_ws="$(i3-msg -t get_workspaces | jq | awk -F '"' '/true/{print a;exit};{a=$4}')"
active_win="$(xprop -root _NET_ACTIVE_WINDOW | awk '{print $NF}')"

case "$active_ws" in
  1) xprop -root -format WS1 8s -set WS1 "$active_win";;
  2) xprop -root -format WS2 8s -set WS2 "$active_win";;
  3) xprop -root -format WS3 8s -set WS3 "$active_win";;
  4) xprop -root -format WS4 8s -set WS4 "$active_win";;
  5) xprop -root -format WS5 8s -set WS5 "$active_win";;
  6) xprop -root -format WS6 8s -set WS6 "$active_win";;
  7) xprop -root -format WS7 8s -set WS7 "$active_win";;
  8) xprop -root -format WS8 8s -set WS8 "$active_win";;
  9) xprop -root -format WS9 8s -set WS9 "$active_win";;
  0) xprop -root -format WS0 8s -set WS0 "$active_win";;
esac

case "$1" in
  1) i3-msg "workspace 1";  wmctrl -ia "$(xprop -root WS1 | awk -F '"' '{print $2}')";;
  2) i3-msg "workspace 2";  wmctrl -ia "$(xprop -root WS2 | awk -F '"' '{print $2}')";;
  3) i3-msg "workspace 3";  wmctrl -ia "$(xprop -root WS3 | awk -F '"' '{print $2}')";;
  4) i3-msg "workspace 4";  wmctrl -ia "$(xprop -root WS4 | awk -F '"' '{print $2}')";;
  5) i3-msg "workspace 5";  wmctrl -ia "$(xprop -root WS5 | awk -F '"' '{print $2}')";;
  6) i3-msg "workspace 6";  wmctrl -ia "$(xprop -root WS6 | awk -F '"' '{print $2}')";;
  7) i3-msg "workspace 7";  wmctrl -ia "$(xprop -root WS7 | awk -F '"' '{print $2}')";;
  8) i3-msg "workspace 8";  wmctrl -ia "$(xprop -root WS8 | awk -F '"' '{print $2}')";;
  9) i3-msg "workspace 9";  wmctrl -ia "$(xprop -root WS9 | awk -F '"' '{print $2}')";;
  0) i3-msg "workspace 10"; wmctrl -ia "$(xprop -root WS0 | awk -F '"' '{print $2}')";;
esac

exit 0
