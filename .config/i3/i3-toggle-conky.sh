#!/bin/sh
i3_dir="${XDG_CONFIG_HOME}/i3"
win="$(xdotool getactivewindow)"
$i3_dir/i3-conky.sh
$i3_dir/autostart.sh toggle-tray
sleep 0.5
xdotool windowactivate $win
xdotool windowfocus $win
exit 0
