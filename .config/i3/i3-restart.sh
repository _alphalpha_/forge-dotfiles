#!/bin/sh
i3_dir="${XDG_CONFIG_HOME}/i3"
"${i3_dir}/autostart.sh" stop
win="$(xdotool getwindowfocus)"
xprop -root -format CONKY_PID 8s -remove CONKY_PID
xprop -root -format VOLICON_PID 8s -remove VOLICON_PID
xprop -root -format BATICON_PID 8s -remove BATICON_PID
xprop -root -format TRAY_PID 8s -remove TRAY_PID
i3-msg restart

sleep 1
#xdotool windowfocus $win
xdotool windowactivate $win
#xdo -a "$win"
exit 0
