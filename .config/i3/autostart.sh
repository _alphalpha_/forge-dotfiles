#!/bin/sh

i3_dir="${XDG_CONFIG_HOME}/i3"
"${SCRIPTS}/wallpaper.sh" --restore &

picom_start() {
  if [ ! "$(pidof picom)" ]; then
    xprop -root -format PICOM_PID 8s -remove PICOM_PID
  fi
  if [ ! "$(xprop -root PICOM_PID | awk -F '"' '{print $2}')" ]; then
    picom -f --config "${XDG_CONFIG_HOME}/picom/picom.conf" &
    xprop -root -format PICOM_PID 8s -set PICOM_PID "$!"
  fi
}

picom_stop() {
  picom_pid="$(xprop -root PICOM_PID | awk -F '"' '{print $2}')"
  if [ "$picom_pid" ]; then
    kill "$picom_pid"
    xprop -root -format PICOM_PID 8s -remove PICOM_PID
  fi
}

tray_start() {
  y_pos="$(($(xdpyinfo | awk '/dimensions/{split($2,v,"x");print v[2]}') -26))"
  bg="#3e3e3e"

  if [ ! "$(pidof stalonetray)" ]; then
    xprop -root -format TRAY_PID 8s -remove TRAY_PID
  fi
  if [ ! "$(xprop -root TRAY_PID | awk -F '"' '{print $2}')" ]; then
    stalonetray --geometry 8x1+1+$y_pos --no-shrink -bg $bg --icon-size 28 \
                --window-strut bottom --window-type utility --window-layer top -t -p &
    xprop -root -format TRAY_PID 8s -set TRAY_PID "$!"
  fi
}

tray_stop(){
  tray_pid="$(xprop -root TRAY_PID | awk -F '"' '{print $2}')"
  if [ "$tray_pid" ]; then
    kill "$tray_pid"
    xprop -root -format TRAY_PID 8s -remove TRAY_PID
  fi
}

baticon_start() {
  if [ ! "$(pidof cbatticon)" ]; then
    xprop -root -format BATICON_PID 8s -remove BATICON_PID
  fi
  if [ ! "$(xprop -root BATICON_PID | awk -F '"' '{print $2}')" ]; then
    cbatticon &
    xprop -root -format BATICON_PID 8s -set BATICON_PID "$!"
  fi
}

baticon_stop(){
  baticon_pid="$(xprop -root BATICON_PID | awk -F '"' '{print $2}')"
  if [ "$baticon_pid" ]; then
    kill "$baticon_pid"
    xprop -root -format BATICON_PID 8s -remove BATICON_PID
  fi
}

volicon_start() {
  if [ ! "$(pidof volumeicon)" ]; then
    xprop -root -format VOLICON_PID 8s -remove VOLICON_PID
  fi
  if [ ! "$(xprop -root VOLICON_PID | awk -F '"' '{print $2}')" ]; then
    volumeicon &
    xprop -root -format VOLICON_PID 8s -set VOLICON_PID "$!"
  fi
}

volicon_stop(){
  volicon_pid="$(xprop -root VOLICON_PID | awk -F '"' '{print $2}')"
  if [ "$volicon_pid" ]; then
    kill "$volicon_pid"
    xprop -root -format VOLICON_PID 8s -remove VOLICON_PID
  fi
}

batalarm_start() {
  if [ ! "$(ps -Af | awk '/battery-alarm.sh/ && !/awk/ {print $2}')" ]; then
    xprop -root -format BATALARM_PID 8s -remove BATALARM_PID
  fi
  if [ ! "$(xprop -root BATALARM_PID | awk -F '"' '{print $2}')" ]; then
    "${SCRIPTS}/battery-alarm.sh" &
    xprop -root -format BATALARM_PID 8s -set BATALARM_PID "$!"
  fi
}

batalarm_stop(){
  BATALARM_PID="$(xprop -root BATALARM_PID | awk -F '"' '{print $2}')"
  if [ "$BATALARM_PID" ]; then
    kill "$BATALARM_PID"
    xprop -root -format BATALARM_PID 8s -remove BATALARM_PID
  fi
}


if [ "$1" = 'stop' ]; then
  picom_stop
  volicon_stop
  baticon_stop
  batalarm_stop
  tray_stop
  eval "${i3_dir}/i3-conky.sh" off
elif [ "$1" = 'toggle-tray' ]; then
  tray_pid="$(xprop -root TRAY_PID | awk -F '"' '{print $2}')"
  if [ "$tray_pid" ]; then
    kill "$tray_pid"
    xprop -root -format TRAY_PID 8s -remove TRAY_PID
  else
    tray_start &
  fi
else
  picom_start &
  "${i3_dir}/i3-conky.sh" on &
  tray_start &
  find /sys/class/power_supply/*B* && batalarm_start &
  find /sys/class/power_supply/*B* && baticon_start &
  volicon_start &
fi

exit 0
