#!/bin/sh
active_win="$(xprop -root _NET_ACTIVE_WINDOW | awk '{print $NF}')"
case "$1" in
  1) i3-msg "move container to workspace 1" \
     && xprop -root -format WS1 8s -set WS1 "$active_win";;
  2) i3-msg "move container to workspace 2" \
     && xprop -root -format WS2 8s -set WS2 "$active_win";;
  3) i3-msg "move container to workspace 3" \
     && xprop -root -format WS3 8s -set WS3 "$active_win";;
  4) i3-msg "move container to workspace 4" \
     && xprop -root -format WS4 8s -set WS4 "$active_win";;
  5) i3-msg "move container to workspace 5" \
     && xprop -root -format WS5 8s -set WS5 "$active_win";;
  6) i3-msg "move container to workspace 6" \
     && xprop -root -format WS6 8s -set WS6 "$active_win";;
  7) i3-msg "move container to workspace 7" \
     && xprop -root -format WS7 8s -set WS7 "$active_win";;
  8) i3-msg "move container to workspace 8" \
     && xprop -root -format WS8 8s -set WS8 "$active_win";;
  9) i3-msg "move container to workspace 9" \
     && xprop -root -format WS9 8s -set WS9 "$active_win";;
  0) i3-msg "move container to workspace 0" \
     && xprop -root -format WS0 8s -set WS0 "$active_win";;
esac
exit 0
