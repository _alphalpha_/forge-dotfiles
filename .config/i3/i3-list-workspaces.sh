#!/bin/sh

print_list(){
  i3-msg -t get_workspaces \
  | jq \
  | awk '/name|visible|focused/{print $1,$2}' \
  | tr -d '[:punct:]'
}

focus="$(print_list | grep 'focused true' -B2 | awk '/name/{print $2}')"

if [ "$1" = '-c' ] || [ "$1" = '--color' ]; then
  print_list | \
  awk -v focus="$focus" '
    /name/{if ($2==focus){printf "\033[33m%s \033[0m",$2}else printf "%s ",$2}
    END{printf "\n"
  }'
else
  print_list | \
  awk -v focus="$focus" '
    /name/{if ($2==focus){printf "[%s] ",$2}else printf "%s ",$2}
    END{printf "\n"
  }'
fi

exit 0
