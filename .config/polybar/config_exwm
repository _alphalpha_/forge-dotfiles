;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

; ┏━╸┏━┓╻  ┏━┓┏━┓┏━┓
; ┃  ┃ ┃┃  ┃ ┃┣┳┛┗━┓
; ┗━╸┗━┛┗━╸┗━┛╹┗╸┗━┛
[colors]
	background = ${xrdb:color0:#222}
;	background = #26001A
	background-alt = #56304A
	foreground = ${xrdb:color3:#222}
;	foreground = #dfdfdf
	foreground-alt = ${xrdb:color1:#222}
;	foreground-alt = #dc6434
	primary = ${xrdb:color3:#222}
	secondary = #e60053
	alert = #bd2c40

; ┏━┓┏━╸╺┳╸╺┳╸╻┏┓╻┏━╸┏━┓
; ┗━┓┣╸  ┃  ┃ ┃┃┗┫┃╺┓┗━┓
; ┗━┛┗━╸ ╹  ╹ ╹╹ ╹┗━┛┗━┛
[settings]
	screenchange-reload = true
;	compositing-background = xor
;	compositing-background = screen
;	compositing-foreground = source
;	compositing-border = over
;	pseudo-transparency = false

; ┏┓ ┏━┓┏━┓   ╻   ┏━┓   ┏━┓┏━┓╻  ╻ ╻┏┓ ┏━┓┏━┓
; ┣┻┓┣━┫┣┳┛  ┏┛  ┃╺━┫   ┣━┛┃ ┃┃  ┗┳┛┣┻┓┣━┫┣┳┛
; ┗━┛╹ ╹╹┗╸  ╹   ╹┗━┛╺━╸╹  ┗━┛┗━╸ ╹ ┗━┛╹ ╹╹┗╸
[bar/i3]
;	monitor = ${env:MONITOR:HDMI-1}
	width = 100%
	height = 26
	bottom = true
;	offset-x = 1%
;	offset-y = 1%
;	radius = 12.0
	fixed-center = false
	background = ${colors.background}
	foreground = ${colors.foreground}
	line-size = 2
	line-color = #f00
	border-size = 0
	bordr-color = #00000000
	padding-left = 2
	padding-right = 2
	module-margin-left = 1
	module-margin-right = 3
	font-0 = roboto:pixelsize=12
	font-1 = unifont:fontformat=truetype:size=8:antialias=false;0
	font-2 = comfortaa:pixelsize=10;1
;	tray-background = #0063ff
	tray-detached = false
;	tray-maxsize = 12
;	tray-offset-x = 0
;	tray-offset-y = 0
;	tray-offset-z = 0
	tray-padding = 2
	tray-position = right
;	tray-scale = 1.15
;	tray-transparent = true
	wm-restack = i3
;	override-redirect = true
;	scroll-up = i3wm-wsnext
;	scroll-down = i3wm-wsprev
	cursor-click = pointer
	cursor-scroll = ns-resize

;==========================================================
;				┏┳┓┏━┓╺┳┓╻ ╻╻  ┏━╸┏━┓
;				┃┃┃┃ ┃ ┃┃┃ ┃┃  ┣╸ ┗━┓
;				╹ ╹┗━┛╺┻┛┗━┛┗━╸┗━╸┗━┛
;==========================================================
	modules-left = time date xwindow
;	modules-center =
	modules-right = temperature filesystem cpu memory battery

; ╻ ╻╻ ╻╻┏┓╻╺┳┓┏━┓╻ ╻
; ┏╋┛┃╻┃┃┃┗┫ ┃┃┃ ┃┃╻┃
; ╹ ╹┗┻┛╹╹ ╹╺┻┛┗━┛┗┻┛
[module/xwindow]
	type = internal/xwindow
	label = %title:0:55:...%


; ┏━╸╻╻  ┏━╸┏━┓╻ ╻┏━┓╺┳╸┏━╸┏┳┓
; ┣╸ ┃┃  ┣╸ ┗━┓┗┳┛┗━┓ ┃ ┣╸ ┃┃┃
; ╹  ╹┗━╸┗━╸┗━┛ ╹ ┗━┛ ╹ ┗━╸╹ ╹
[module/filesystem]
	type = custom/script
	exec = df -h / | awk '{if ($6 =="/") print $5}' 
	format-prefix = "DISK: "
	interval = 25
	click-left = [ "$(pidof qdirstat)" ] && killall qdirstat || qdirstat / &

; ┏━╸┏━┓╻ ╻
; ┃  ┣━┛┃ ┃
; ┗━╸╹  ┗━┛
[module/cpu]
	type = internal/cpu
	interval = 2
	format-prefix = "CPU:"
	format-prefix-foreground = ${colors.foreground}
	;format-underline = #ffffff
	label = %percentage:2%%

; ┏┳┓┏━╸┏┳┓┏━┓┏━┓╻ ╻
; ┃┃┃┣╸ ┃┃┃┃ ┃┣┳┛┗┳┛
; ╹ ╹┗━╸╹ ╹┗━┛╹┗╸ ╹ 
[module/memory]
	type = internal/memory
	interval = 2
	format-prefix = "MEM:"
	format-prefix-foreground = ${colors.foreground}
	;format-underline = #ffffff
	label = %percentage_used%%

; ╺┳┓┏━┓╺┳╸┏━╸
;  ┃┃┣━┫ ┃ ┣╸ 
; ╺┻┛╹ ╹ ╹ ┗━╸
[module/date]
	type = custom/script
	exec = date +%d.%m.%Y
	format-prefix = "DATE: "
	label = "%output%"
	click-left = [ "$(pidof kalendar)" ] && killall kalendar || kalendar &
	interval = 1

; ╺┳╸╻┏┳┓┏━╸
;  ┃ ┃┃┃┃┣╸ 
;  ╹ ╹╹ ╹┗━╸
[module/time]
	type = custom/script
	exec = date +%H:%M:%S
	format-prefix = "TIME: "
	label = "%output%"
	click-left = [ "$(pidof qtclock)" ] && killall qtclock || qtclock &
	interval = 1

; ╺┳╸┏━╸┏┳┓┏━┓┏━╸┏━┓┏━┓╺┳╸╻ ╻┏━┓┏━╸
;  ┃ ┣╸ ┃┃┃┣━┛┣╸ ┣┳┛┣━┫ ┃ ┃ ┃┣┳┛┣╸ 
;  ╹ ┗━╸╹ ╹╹  ┗━╸╹┗╸╹ ╹ ╹ ┗━┛╹┗╸┗━╸
[module/temperature]
	type = internal/temperature
	thermal-zone = 0
	warn-temperature = 70
	format-prefix = "CORE: "
;	format = <ramp> <label>
	format = <label>
;	format-underline = #ffffff
	format-warn-prefix = "CORE: "
;	format-warn = <ramp> <label-warn>
	format-warn = <label-warn>
;	format-warn-underline = ${self.format-underline}
	label = %temperature-c%
	label-warn = %temperature-c%
	label-warn-foreground = ${colors.secondary}

; ┏┓ ┏━┓╺┳╸╺┳╸┏━╸┏━┓╻ ╻
; ┣┻┓┣━┫ ┃  ┃ ┣╸ ┣┳┛┗┳┛
; ┗━┛╹ ╹ ╹  ╹ ┗━╸╹┗╸ ╹ 
[module/battery]
	type = internal/battery
	battery = BAT0
	adapter = AC
	full-at = 98
	format-charging-prefix = "PWR:"
	format-charging = <animation-charging> <label-charging>
;	format-charging-underline = #ffb52a
	format-discharging-prefix = "PWR:"
	format-discharging = <animation-discharging> <label-discharging>
;	format-discharging-underline = ${self.format-charging-underline}	
	format-full-prefix = "PWR:"
	format-full-prefix-foreground = ${colors.foreground}
;	format-full-underline = ${self.format-charging-underline}
	ramp-capacity-0 = 
	ramp-capacity-1 = 
	ramp-capacity-2 = 
	ramp-capacity-foreground = ${colors.foreground}
	animation-charging-0 = 
	animation-charging-1 = 
	animation-charging-2 = 
	animation-charging-foreground = ${colors.foreground}
	animation-charging-framerate = 750
	animation-discharging-0 = 
	animation-discharging-1 = 
	animation-discharging-2 = 
	animation-discharging-foreground = ${colors.foreground}
	animation-discharging-framerate = 750
