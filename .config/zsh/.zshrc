# Michaels
# ▀▀█ █▀▀ █ █   █▀▀ █▀█ █▀█ █▀▀ ▀█▀ █▀▀ #
# ▄▀  ▀▀█ █▀█   █   █ █ █ █ █▀▀  █  █ █ #
# ▀▀▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀ #
source "$HOME/.env"

autoload -U colors && colors
autoload -U compinit && compinit
autoload -U vcs_info && vcs_info
autoload promptinit && promptinit
zmodload zsh/complist
zstyle ':completion:*:*:*:*:*' menu select
zstyle ':completion:*' matcher-list 'r:|=*' 'l:|=* r:|=*'
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
unsetopt beep
HISTFILE="$HOME/.cache/zshistory"
HISTSIZE=100000
SAVEHIST=100000
#setopt appendhistory
setopt inc_append_history
setopt share_history

# █▀▀ █ █ █▀█ █▀▀ ▀█▀ ▀█▀ █▀█ █▀█ █▀▀ #
# █▀▀ █ █ █ █ █    █   █  █ █ █ █ ▀▀█ #
# ▀   ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀ #
[ -f "$SCRIPTS/functions/pretty-curl.func" ] && source "$SCRIPTS/functions/pretty-curl.func"
[ -f "$SCRIPTS/functions/update_all.func" ] && source "$SCRIPTS/functions/update_all.func"
[ -f "$SCRIPTS/functions/main_menu.func" ] && source "$SCRIPTS/functions/main_menu.func"
[ -f "$SCRIPTS/functions/extract.func" ] && source "$SCRIPTS/functions/extract.func"
[ -f "$SCRIPTS/functions/switch_dir.func" ] && source "$SCRIPTS/functions/switch_dir.func"
[ -f "$SCRIPTS/functions/qemu.func" ] && source "$SCRIPTS/functions/qemu.func"
[ -f "$SCRIPTS/functions/search_things.func" ] && source "$SCRIPTS/functions/search_things.func"
[ -f "$SCRIPTS/functions/check_file.func" ] && source "$SCRIPTS/functions/check_file.func"
[ -f "$SCRIPTS/functions/figlet_demo.func" ] && source "$SCRIPTS/functions/figlet_demo.func"
[ -f "$SCRIPTS/functions/wm_name.func" ] && source "$SCRIPTS/functions/wm_name.func"
[ -f "$SCRIPTS/functions/dzen_msg.func" ] && source "$SCRIPTS/functions/dzen_msg.func"

print_alert() {
[ "$1" = "-c" ] && MSG='# ███' || MSG=' ███'
echo -e "\n$MSG\n$MSG\n$MSG\n$MSG\n\n$MSG\n"
}

change_prompt_color() {
if [ "$PS1" = "$PS1_def" ]; then
PS1="$PS1_alt"
else
PS1="$PS1_def"
fi
}

# █▀█ █▀▄ █▀█ █▄ ▄█ █▀█ ▀█▀ #
# █▀▀ █▀▄ █ █ █ ▀ █ █▀▀  █  #
# ▀   ▀ ▀ ▀▀▀ ▀   ▀ ▀    ▀  #
#PS1="%D{%H:%M} [%B%F{yellow} %~/%f%k%b]"

PS1_def="┌─[%B%F{yellow}%~/%f%k%b]-%D{%H:%M}-[%F{yellow}%B%n%b%f@%F{yellow}%B%M%b%f]
└>%{$reset_color%}"
PS1_alt="%{$fg[blue]%}┌─[%B%F{yellow}%~/%f%k%b%{$fg[blue]%}]-%D{%H:%M}-[%F{yellow}%B%n%b%f%{$fg[blue]%}@%F{yellow}%B%M%b%f%{$fg[blue]%}]
└>%{$reset_color%}"
PS1="$PS1_def"
PS2='\`%_> '
PS3='?# '
PS4='+%N:%i:%_> '

if [[ -z "$WM" ]]; then
#  PS1="[%n@%M] %~/%f%k%b "
PS1="┌─[%B%F{yellow}%~/%f%k%b]-%D{%H:%M}-[%F{yellow}%B%n%b%f@%F{yellow}%B%M%b%f]
└> "

  PS2="\`%_> "
#  RPROMPT="%D{%H:%M}"
fi


# █▀█ █   ▀█▀ █▀█ █▀▀ █▀▀ █▀▀ #
# █▀█ █    █  █▀█ ▀▀█ █▀▀ ▀▀█ #
# ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀ ▀▀▀ ▀▀▀ #
[ -f "$ZDOTDIR"/aliases ] && source $ZDOTDIR/aliases


# █ █ █▀▀ █ █ █▀▀ #
# █▀▄ █▀▀  █  ▀▀█ #
# ▀ ▀ ▀▀▀  ▀  ▀▀▀ #
source $ZDOTDIR/keys


fpath+=${ZDOTDIR:-~}/.zsh_functions
fpath+=${ZDOTDIR:-~}/.zsh_functions

#[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh




n ()
{   #https://github.com/jarun/nnn/blob/547508aa78ead66a4299423dfef95b24b0895bee/misc/quitcd/quitcd.bash_sh_zsh
    # Block nesting of nnn in subshells
    [ "${NNNLVL:-0}" -eq 0 ] || {
        echo "nnn is already running"
        return
    }

    # The behaviour is set to cd on quit (nnn checks if NNN_TMPFILE is set)
    # If NNN_TMPFILE is set to a custom path, it must be exported for nnn to
    # see. To cd on quit only on ^G, remove the "export" and make sure not to
    # use a custom path, i.e. set NNN_TMPFILE *exactly* as follows:
    #      NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"
    export NNN_TMPFILE="${XDG_CONFIG_HOME:-$HOME/.config}/nnn/.lastd"

    # Unmask ^Q (, ^V etc.) (if required, see `stty -a`) to Quit nnn
    # stty start undef
    # stty stop undef
    # stty lwrap undef
    # stty lnext undef

    # The command builtin allows one to alias nnn to n, if desired, without
    # making an infinitely recursive alias
    command nnn "$@"

    [ ! -f "$NNN_TMPFILE" ] || {
        . "$NNN_TMPFILE"
        rm -f "$NNN_TMPFILE" > /dev/null
    }
}

nnn_cd()                                                                                                   
{
    if ! [ -z "$NNN_PIPE" ]; then
        printf "%s\0" "0c${PWD}" > "${NNN_PIPE}" !&
    fi  
}

trap nnn_cd EXIT

source "$SCRIPTS/startx.sh"
