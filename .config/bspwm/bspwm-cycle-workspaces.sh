#!/bin/sh

max="$(bspc query -D -d .occupied --names | awk 'END{print $1;exit}')"
focus="$(bspc query -D -d .active --names)"

if [ "$1" = '-b' ]; then
  if [ "$focus" = "1" ]; then
    bspc desktop -f "$max"
  else
    bspc desktop -f prev
  fi
else
  if [ "$focus" = "$max" ]; then
    bspc desktop -f 1  
  else
    bspc desktop -f next
  fi
fi
exit 0
