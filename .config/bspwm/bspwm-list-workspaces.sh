#!/bin/sh

focus="$(bspc query -D -d .active --names)"
if [ "$1" = '-c' ] || [ "$1" = '--color' ]; then
  bspc query -D -d .occupied --names | \
  awk -v focus="$focus" '
    {if ($1==focus){printf "\033[33m%s \033[0m",$1}else printf "%s ",$1}
    END{printf "\n"}'
else
  bspc query -D -d .occupied --names | \
  awk -v focus="$focus" '
    {if ($1==focus){printf "[%s] ",$1}else printf "%s ",$1}
    END{printf "\n"}'
fi

exit 0
