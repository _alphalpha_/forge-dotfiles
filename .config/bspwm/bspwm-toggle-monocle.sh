#!/bin/sh

table="$(bspc wm -d | jq \
         | awk '/          "name"|"layout": "monocle"/{if (/name/) {printf "\n%s ",$2} else {printf "%s",$2} }'\
         | tr -d '[:punct:]' | grep .)"


active="$(bspc query -D -d .active --names)"

active_state="$(echo "$table" | awk -v a="$active" '{if ($1 == a){print $2}}')"

if [ "$active_state" = 'monocle' ]; then
  bspc desktop -l tiled
else
  bspc desktop -l monocle
fi

#tint_window="$(xdotool search --class "Tint2")"
#if [ "$tint_window" ]; then
#  xdo hide "$tint_window"
#fi

#  xdo show "$tint_window"
#fi
