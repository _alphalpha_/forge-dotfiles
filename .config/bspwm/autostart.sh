#! /bin/sh

bspwm_dir="${XDG_CONFIG_HOME}/bspwm"
"${SCRIPTS}/wallpaper.sh" --restore &
#$HOME/.config/polybar/launch.sh &

sxhkd_start() {
  if [ ! "$(pidof sxhkd)" ]; then
    xprop -root -format SXHKD_PID 8s -remove SXHKD_PID
  fi
  if [ ! "$(xprop -root SXHKD_PID | awk -F '"' '{print $2}')" ]; then
    sxhkd -m 1 -c "${bspwm_dir}/sxhkdrc" &
    xprop -root -format SXHKD_PID 8s -set SXHKD_PID "$!"
  fi
}

sxhkd_stop() {
  sxhkd_pid="$(xprop -root SXHKD_PID | awk -F '"' '{print $2}')"
  if [ "$sxhkd_pid" ]; then
    kill "$sxhkd_pid"
    xprop -root -format SXHKD_PID 8s -remove SXHKD_PID
  fi
}

tint_start() {
  if [ ! "$(pidof tint)" ]; then
    xprop -root -format TINT_PID 8s -remove TINT_PID
  fi
  if [ ! "$(xprop -root TINT_PID | awk -F '"' '{print $2}')" ]; then
    tint2 &
    xprop -root -format TINT_PID 8s -set TINT_PID "$!"
  fi
}

tint_stop() {
  tint_pid="$(xprop -root TINT_PID | awk -F '"' '{print $2}')"
  if [ "$tint_pid" ]; then
    kill "$tint_pid"
    xprop -root -format TINT_PID 8s -remove TINT_PID
  fi
}


picom_start() {
  if [ ! "$(pidof picom)" ]; then
    xprop -root -format PICOM_PID 8s -remove PICOM_PID
  fi
  if [ ! "$(xprop -root PICOM_PID | awk -F '"' '{print $2}')" ]; then
    picom -f --config "${XDG_CONFIG_HOME}/picom/picom.conf" &
    xprop -root -format PICOM_PID 8s -set PICOM_PID "$!"
  fi
}

picom_stop() {
  picom_pid="$(xprop -root PICOM_PID | awk -F '"' '{print $2}')"
  if [ "$picom_pid" ]; then
    kill "$picom_pid"
    xprop -root -format PICOM_PID 8s -remove PICOM_PID
  fi
}

baticon_start() {
  if [ ! "$(pidof cbatticon)" ]; then
    xprop -root -format BATICON_PID 8s -remove BATICON_PID
  fi
  if [ ! "$(xprop -root BATICON_PID | awk -F '"' '{print $2}')" ]; then
    cbatticon &
    xprop -root -format BATICON_PID 8s -set BATICON_PID "$!"
  fi
}

baticon_stop(){
  baticon_pid="$(xprop -root BATICON_PID | awk -F '"' '{print $2}')"
  if [ "$baticon_pid" ]; then
    kill "$baticon_pid"
    xprop -root -format BATICON_PID 8s -remove BATICON_PID
  fi
}

volicon_start() {
  if [ ! "$(pidof volumeicon)" ]; then
    xprop -root -format VOLICON_PID 8s -remove VOLICON_PID
  fi
  if [ ! "$(xprop -root VOLICON_PID | awk -F '"' '{print $2}')" ]; then
    volumeicon &
    xprop -root -format VOLICON_PID 8s -set VOLICON_PID "$!"
  fi
}

volicon_stop(){
  volicon_pid="$(xprop -root VOLICON_PID | awk -F '"' '{print $2}')"
  if [ "$volicon_pid" ]; then
    kill "$volicon_pid"
    xprop -root -format VOLICON_PID 8s -remove VOLICON_PID
  fi
}

batalarm_start() {
  if [ ! "$(ps -Af | awk '/battery-alarm.sh/ && !/awk/ {print $2}')" ]; then
    xprop -root -format BATALARM_PID 8s -remove BATALARM_PID
  fi
  if [ ! "$(xprop -root BATALARM_PID | awk -F '"' '{print $2}')" ]; then
    "${SCRIPTS}/battery-alarm.sh" &
    xprop -root -format BATALARM_PID 8s -set BATALARM_PID "$!"
  fi
}

batalarm_stop(){
  BATALARM_PID="$(xprop -root BATALARM_PID | awk -F '"' '{print $2}')"
  if [ "$BATALARM_PID" ]; then
    kill "$BATALARM_PID"
    xprop -root -format BATALARM_PID 8s -remove BATALARM_PID
  fi
}

if [ "$1" = 'stop' ]; then
  picom_stop &
  sxhkd_stop &
  tint_stop &
  volicon_stop &
  baticon_stop &
  batalarm_stop &
  "${bspwm_dir}/bspwm-conky.sh" off &
else
  picom_start &
  "${bspwm_dir}/bspwm-conky.sh" on &
  tint_start &
  sxhkd_start &
  find /sys/class/power_supply/*B* && batalarm_start &
  find /sys/class/power_supply/*B* && baticon_start &
  volicon_start &
  if [ -d /run/live ]; then
    "$SCRIPTS"/yad/yad-welcome.sh &
  fi
fi

exit 0
