#!/bin/sh
jwm_config_dir="$HOME/.config/jwm"

# load jwm theme
if [ ! -f "$jwm_config_dir"/theme ] && [ ! -L "$jwm_config_dir"/theme ] \
|| [ ! -e "$(readlink .config/jwm/theme)" ]; then
  "$jwm_config_dir/scripts/load_theme.sh"
fi

# load Xresources
if [ -f "$HOME/.Xresources" ]; then
  xrdb -merge "$HOME/.Xresources" &
fi

case $(cat "$jwm_config_dir"/themes/.active) in
  '98') color='#C0C0C0';;
  'dmg') color='#9BBC0F';;
  'classic') color='#333333';;
  'deep') color='#13305B';;
  'neoaqua') color='#494949';;
  'neoroyal') color='#494949';;
  'protogrey') color='#333333';;
  'protowhite') color='#777777';;
  'jungle') color='#222233';;
  'legendary') color='#736876';;
  'protoplasmatic') color='#300A24';;
  *) color='#BE3B3E';;
esac

# conky
"$jwm_config_dir/scripts/jwm_conky.sh"

# picom 
if [ ! "$(pidof picom)" ]; then
  xprop -root -format PICOM_PID 8s -remove PICOM_PID
fi
if [ ! "$(xprop -root PICOM_PID | awk -F '"' '{print $2}')" ]; then
  picom -f --config "${XDG_CONFIG_HOME}/picom/picom.conf" &
  xprop -root -format PICOM_PID 8s -set PICOM_PID "$!"
fi

# pcmanfm icons on desktop
if [ ! "$(xprop -root PCMAN_PID | awk -F '"' '{print $2}')" ]; then
  pcmanfm --desktop &
  pcman_pid="$(ps x -o "%p " -o "args=ARGS" | awk '/[ ]{4}?[0-9][0-9]{5}? pcmanfm --desktop/ && !/awk/{print $1}')"
  xprop -root -format PCMAN_PID 8s -set PCMAN_PID "$pcman_pid"
#  pcmanfm -w .local/wallpaper
fi

# tray
if [ ! "$(pidof stalonetray)" ]; then
  xprop -root -format TRAY_PID 8s -remove TRAY_PID
fi
if [ ! "$(xprop -root TRAY_PID | awk -F '"' '{print $2}')" ]; then
  stalonetray -c "$jwm_config_dir"/stalonetrayrc --background "$color" &
  xprop -root -format TRAY_PID 8s -set TRAY_PID "$!"
fi

# volume tray icon
if [ ! "$(pidof volumeicon)" ]; then
  xprop -root -format VOLICON_PID 8s -remove VOLICON_PID
fi
if [ ! "$(xprop -root VOLICON_PID | awk -F '"' '{print $2}')" ]; then
  volumeicon &
  xprop -root -format VOLICON_PID 8s -set VOLICON_PID "$!"
fi

# battery tray icon
if find /sys/class/power_supply/*B*; then
  if [ ! "$(pidof cbatticon)" ]; then
    xprop -root -format BATICON_PID 8s -remove BATICON_PID
  fi
  if [ ! "$(xprop -root BATICON_PID | awk -F '"' '{print $2}')" ]; then
    cbatticon &
    xprop -root -format BATICON_PID 8s -set BATICON_PID "$!"
  fi
  
  # battery alarm
  if [ ! "$(ps -Af | awk '/battery-alarm.sh/ && !/awk/ {print $2}')" ]; then
    xprop -root -format BATALARM_PID 8s -remove BATALARM_PID
  fi
  if [ ! "$(xprop -root BATALARM_PID | awk -F '"' '{print $2}')" ]; then
    "${SCRIPTS}/battery-alarm.sh" &
    xprop -root -format BATALARM_PID 8s -set BATALARM_PID "$!"
  fi 
fi

# Live System Welcome Window
if [ -d /run/live ]; then
  "$SCRIPTS"/yad/yad-welcome.sh &
  sleep 0
fi

# load touchscreen calibrator if touchscreen is detected
#if [ ! "$(xinput_calibrator --list)" = "No calibratable devices found." ]; then
#  x11-touchscreen-calibrator &
#fi

exit 0
