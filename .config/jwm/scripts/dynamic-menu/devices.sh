#!/bin/sh
# Dynamic Device Menu for JWM
# written by Michael
for DEVICE in $(lsblk -dno NAME,SUBSYSTEMS | awk '/(usb|mmc)/ {print $1}'); do
  MENU="$MENU <Menu label=\"$DEVICE\">"
  for PARTITION in $(lsblk -nro NAME /dev/${DEVICE}| grep -vw "$DEVICE" | sort); do
	MOUNTPOINT="$(lsblk /dev/$PARTITION -no MOUNTPOINT)"
	if [ "$MOUNTPOINT" ]; then
	  SUBMENU_PARTITION="
	  <Menu label=\"$PARTITION\">
	  <Program label=\"$MOUNTPOINT\"> $FILEMANAGER $MOUNTPOINT </Program>
      <Separator/>
	  <Program label=\"Unmount\">pumount /dev/$PARTITION</Program>
      <Separator/>
		  <Program label=\"File Manager\"> $FILEMANAGER $MOUNTPOINT </Program>
		  <Program label=\"QDirStat\"> qdirstat $MOUNTPOINT </Program>
	  </Menu>"
	else
	SUBMENU_PARTITION="
	<Menu label=\"$PARTITION\">
	  <Program label=\"Mount\">pmount /dev/$PARTITION && $FILEMANAGER /media/$PARTITION </Program>
	</Menu>"
	fi
	MENU="$MENU $SUBMENU_PARTITION"
  done
  MENU="$MENU </Menu>"
done
echo "<JWM>$MENU</JWM>"
exit 0
