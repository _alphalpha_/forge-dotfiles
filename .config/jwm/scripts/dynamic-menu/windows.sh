#!/bin/sh

window_stack="$(wmctrl -lxp | awk -v na="N/A" '
{ id = $1
  name = sprintf("%s %s %s %s",$6,$7,$8,$9)
  if ( $4 ~ na ) { class = name }
  else 
  { split($4,a,".")
    if ( a[2] ) { class = a[2] }
    else if ( a[1] ) { class = a[1] }
    else { class = "?" }
  }
  if ( $4 ~ "qutebrowser" )
  { cmd = "ps --no-headers $(pidof \".venv/bin/python3\")| grep \"qutebrowser\" | grep -v \"pts/\" | head | cut -b 1-5 | tr -d \" \""
    cmd | getline pid
    class = "qutebrowser"
  }
  else { pid = $3 }
  if ( ! class) { class = $4 }
  if ( pid == 0 ) { pid = "     " }
  if (( $4 != "Conky.Conky" ) && ( name !~ "pcmandesktop")) {printf "%s %s %s\n",id,pid,class}
}'
)"

echo "<JWM>"

if echo "$window_stack" | grep -i "$FILEMANAGER"; then
  echo "<Menu label=\"$FILEMANAGER\">"
  echo "$window_stack" | grep -i "$FILEMANAGER" | while read win pid class
  do
    class="$(wmctrl -l -x | awk -v win="$win" ' $1 ~ win {for(i=5;i<=NF;i++) { printf $i" "; print ""}}')"
    echo "<Program label=\"$class\"> xdotool windowactivate \"$win\"</Program>"
  done
  echo "</Menu>"
fi

if echo "$window_stack" | grep -i "$GUI_EDITOR"; then
  echo "<Menu label=\"$GUI_EDITOR\">"
  echo "$window_stack" | grep -i "$GUI_EDITOR" | while read win pid class
  do
    class="$(wmctrl -l -x | awk -v win="$win" ' $1 ~ win {for(i=5;i<=NF;i++) { printf $i" "; print ""}}')"
    echo "<Program label=\"$class\"> xdotool windowactivate \"$win\"</Program>"
  done
  echo "</Menu>"
fi

if echo "$window_stack" | grep -i "Alacritty"; then
  echo "<Menu label=\"Alacritty\">"
  echo "$window_stack" | grep -i "Alacritty" | while read win pid class
  do
    class="$(wmctrl -l -x | awk -v win="$win" ' $1 ~ win {for(i=5;i<=NF;i++) { printf $i" "; print ""}}')"
    echo "<Program label=\"$class\"> xdotool windowactivate \"$win\"</Program>"
  done
  echo "</Menu>"
fi

echo "$window_stack" | grep -vi -P "$FILEMANAGER|$GUI_EDITOR|Alacritty" | while read win pid class
do
  echo "<Program label=\"$class\"> xdotool windowactivate \"$win\"</Program>"
done

echo "</JWM>"
exit
