#!/bin/sh

xrandr | awk 'BEGIN{printf "<JWM>\n"}
{ if (1 == /^   [0-90-90-9]/)
 { 
  split($1,v,"x")
  printf "<Program label=\"%s * %s\">xrandr -s %s</Program>\n",v[1],v[2],$1
 }
}
END{printf "</JWM>\n"}'

