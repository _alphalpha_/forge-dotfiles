#!/bin/sh
id=$(xinput | awk '/[Tt]ouch ?[Pp]ad/ {id=substr($6,4,6);print id}')
if [ "$id" ]; then
  echo "<JWM>"
  echo "<Program label=\"Enable\">xinput --enable $id</Program>"
  echo "<Program label=\"Disable\">xinput --disable $id</Program>"
  echo "</JWM>"
fi
