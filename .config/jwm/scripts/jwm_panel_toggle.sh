#!/bin/sh
if grep 'panel' "$HOME"/.config/jwm/jwmrc | grep -w '<!--'; then
  sed -i s'*<!-- <Include>.config/jwm/panel</Include> -->*<Include>.config/jwm/panel</Include>*' "$HOME"/.config/jwm/jwmrc
  "$HOME"/.config/jwm/scripts/jwm_restart.sh
else
  sed -i s'*<Include>.config/jwm/panel</Include>*<!-- <Include>.config/jwm/panel</Include> -->*' "$HOME"/.config/jwm/jwmrc
  killall stalonetray
  jwm -restart
fi

