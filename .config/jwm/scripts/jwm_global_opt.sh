#!/bin/sh
case "$1" in
  '--default')  opt='groups-global/default';;
  '--maximize') opt='groups-global/maximize';;
  '--tiled')    opt='groups-global/tiled';;
  *) echo "$0: options can be: --default --maximize --tiled"; exit;;
esac

groups_file="$HOME/.config/jwm/groups"
if grep 'groups-global/maximize' "$groups_file"; then
  sed -i "s:groups-global/maximize:$opt:" "$groups_file" \
  && "$HOME"/.config/jwm/scripts/jwm_restart.sh
elif grep 'groups-global/tiled' "$groups_file"; then
  sed -i "s:groups-global/tiled:$opt:" "$groups_file" \
  && "$HOME"/.config/jwm/scripts/jwm_restart.sh
elif grep 'groups-global/default' "$groups_file"; then
  sed -i "s:groups-global/default:$opt:" "$groups_file" \
  && "$HOME"/.config/jwm/scripts/jwm_restart.sh
fi
exit 0
