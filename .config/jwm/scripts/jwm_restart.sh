#!/bin/sh
"$HOME"/.config/jwm/scripts/jwm_conky.sh stop

pcman_pid="$(xprop -root PCMAN_PID | awk -F '"' '{print $2}')"
if [ "$pcman_pid" ]; then
  kill "$pcman_pid"
  xprop -root -format PCMAN_PID 8s -remove PCMAN_PID
fi

tray_pid="$(xprop -root TRAY_PID | awk -F '"' '{print $2}')"
if [ "$tray_pid" ]; then
  kill "$tray_pid"
  xprop -root -format TRAY_PID 8s -remove TRAY_PID
fi


win="$(xdotool getwindowfocus)"
jwm -restart
xdotool windowfocus "$win"
xdotool windowactivate "$win"

"$HOME"/.config/jwm/autostart.sh

[ "$(acpi -b)" ] && pidof cbatticon || cbatticon
pidof volumeicon || volumeicon

#"$HOME"/.config/jwm/scripts/jwm_conky.sh &

#pidof -a stalonetray || "$HOME"/.config/jwm/scripts/start_stalonetray.sh &
#wmctrl -a pcmandesktop || { pcmanfm --desktop && pcmanfm -w .local/wallpaper &; wmctrl -r "pcmanfm" -N "pcmandesktop" & }
exit 0
