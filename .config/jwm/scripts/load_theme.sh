#!/bin/sh
JWM_DIR="$HOME/.config/jwm"
if [ -d "$JWM_DIR" ] ; then
  THEME="$(cat "$JWM_DIR"/themes/.active)"
  if [ -n "$THEME" ]; then
    if [ -L "$JWM_DIR"/theme ] || [ -f "$JWM_DIR"/theme ]; then
      rm "$JWM_DIR"/theme
    fi
    ln -s "$JWM_DIR"/themes/"$THEME" "$JWM_DIR"/theme
    "$JWM_DIR"/scripts/jwm_restart.sh
  fi
fi
exit 0
