#!/bin/sh
sed -i "s/$(grep 'default_color' .config/conkyrc)/	default_color = '#111111',/;s/$(grep 'draw_outline' .config/conkyrc)/	draw_outline = false,/" .config/conkyrc && killall conky && conky -c .config/conkyrc
exit 0
