#!/bin/sh
COLOR="$(yad --title="Set Wallpaper" --color)"
OLD_STRING="$(grep 'default_outline_color' .config/conkyrc)"
NEW_STRING="	default_outline_color = '$COLOR',"
[ - z "$COLOR" ] || [ -z "$OLD_STRING" ] || -z [ "$NEW_STRING" ] && exit 1
sed -i "s/$OLD_STRING/$NEW_STRING/"\
 .config/conkyrc && killall conky && conky -b -c .config/conkyrc
exit 0
