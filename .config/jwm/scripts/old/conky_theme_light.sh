#!/bin/sh
sed -i "s/$(grep 'default_color' .config/conkyrc)/	default_color = '#F6DFBB',/\
;s/$(grep 'draw_outline' .config/conkyrc)/	draw_outline = true,/\
;s/$(grep 'default_outline_color' .config/conkyrc)/	default_outline_color = '#111111',/"\
 .config/conkyrc && killall conky && conky -b -c .config/conkyrc
exit 0
