#!/bin/sh
if [ "$(grep 'valign="bottom"' .config/jwm/panel)" ]; then
  sed -i 's/valign="bottom"/valign="top"/' .config/jwm/panel && jwm -restart
fi
exit 0
