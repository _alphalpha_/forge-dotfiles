#!/bin/sh
sed -i "s/$(grep '	alignment =' .config/conkyrc)/	alignment = 'top_left',/" \
 .config/conkyrc && killall conky && conky -b -c .config/conkyrc
exit 0
