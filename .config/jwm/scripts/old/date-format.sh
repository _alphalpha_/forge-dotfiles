#!/bin/sh
case $1 in
  -DMY) NEW_FORMAT='<Clock format="%H:%M:%S %d.%m.%Y">';;
  -YMD) NEW_FORMAT='<Clock format="%H:%M:%S %Y-%m-%d">';;
  -MDY) NEW_FORMAT='<Clock format="%H:%M:%S %m/%d/%Y">';;
  *)  exit 1;;
esac
OLD_FORMAT="$(grep -oE '<Clock format=.?{20}>' .config/jwm/panel)"
if [ "$OLD_FORMAT" != "$NEW_FORMAT" ]; then
  sed -i "s|$OLD_FORMAT|$NEW_FORMAT|" .config/jwm/panel && jwm -restart
fi
exit 0
