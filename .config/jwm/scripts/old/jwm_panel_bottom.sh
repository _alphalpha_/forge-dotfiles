#!/bin/sh
if [ "$(grep 'valign="top"' .config/jwm/panel)" ]; then
  sed -i 's/valign="top"/valign="bottom"/' .config/jwm/panel && jwm -restart
fi
exit 0
