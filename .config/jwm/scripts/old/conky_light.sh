#!/bin/sh
sed -i "s/$(grep 'default_color' .config/conkyrc)/	default_color = '#F6DFBB',/;s/$(grep 'draw_outline' .config/conkyrc)/	draw_outline = false,/" .config/conkyrc && killall conky && conky -c .config/conkyrc
exit 0
