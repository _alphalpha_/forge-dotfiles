#!/bin/sh
sed -i "s/$(grep '	draw_outline =' .config/conkyrc)/	draw_outline = true,/"\
 .config/conkyrc && killall conky && conky -b -c .config/conkyrc
exit 0
