#!/bin/sh

sysdir="$(find /sys/devices/platform/i8042 -name name | xargs grep -Fl TrackPoint | sed 's/\/input\/input[0-9]*\/name$//')"

if [ ! -f "$sysdir/sensitivity" ] || [ ! -f "$sysdir/speed" ]; then
  exit 0
fi

# min 0 --> 250 max
printf "change trackpoint sensitivity (%s)\\n" "$(cat "$sysdir/sensitivity")"
printf "enter a value from 0 to 250\\n"
read sens

printf "change trackpoint speed (%s)\\n" "$(cat "$sysdir/speed")"
printf "enter a value from 0 to 250\\n"
read speed

if [ "$sens" -ge 0 ] && [ "$sens" -le 250 ]; then
  echo "$sens" | sudo tee "$sysdir/sensitivity"
else
  printf "invalid sensitivity: %s\\n" "$sens"
fi

if [ "$speed" -ge 0 ] && [ "$speed" -le 250 ]; then
  echo "$speed" | sudo tee "$sysdir/speed"
else
  printf "invalid speed: %s\\n" "$speed"
fi

exit 0
