#!/bin/sh
sudo dpkg-reconfigure keyboard-configuration

# Load the correct keyboard configuration from /etc/default/keyboard
XKBMODEL="$(awk -F\" '/XKBMODEL/ {print $2}' /etc/default/keyboard)"
XKBLAYOUT="$(awk -F\" '/XKBLAYOUT/ {print $2}' /etc/default/keyboard)"
XKBVARIANT="$(awk -F\" '/XKBVARIANT/ {print $2}' /etc/default/keyboard)"
XKBOPTIONS="$(awk -F\" '/XKBOPTIONS/ {print $2}' /etc/default/keyboard)"

[ "$XKBMODEL" ] && XKBMODEL="-model $XKBMODEL"
[ "$XKBLAYOUT" ] && XKBLAYOUT="-layout $XKBLAYOUT"
[ "$XKBVARIANT" ] && XKBVARIANT="-variant $XKBVARIANT"
if [ "$XKBOPTIONS" ]; then
  if [ -z "$(echo "$XKBOPTIONS" | grep ",")" ]; then
    XKBOPTIONS="-option $XKBOPTIONS"
  else
    XKBOPTIONS="$(echo "$XKBOPTIONS" | sed 's/^/-option /;s/,/ -option /g')"
  fi
fi
[ "$XKBMODEL" ] && [ "$XKBLAYOUT" ] && setxkbmap ${XKBMODEL} ${XKBLAYOUT} ${XKBVARIANT} ${XKBOPTIONS}
exit 0
