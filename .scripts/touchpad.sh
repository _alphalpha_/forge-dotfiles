#!/bin/sh
id=$(xinput | awk '/[Tt]ouch ?[Pp]ad/ {id=substr($6,4,6);print id}')
device_status=$(xinput list-props "$id" | awk '/Device Enabled/{print $4}')
if [ "$id" ] && [ "$device_status" ]; then
  if [ "$device_status" = "0" ]; then
    xinput --enable "$id"
  elif [ "$device_status" = "1" ]; then
    xinput --disable "$id"
  fi
fi
exit 0
