#!/bin/sh
IMAGE_MAIN="gnome-shutdown"
IMAGE_OK="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
IMAGE_CANCEL="/usr/share/icons/Numix/24/actions/dialog-close.svg"

ACTION=$(yad --width 300 --entry --title "System Logout" \
    --image="$IMAGE_MAIN" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "Choose action:" \
    --entry-text \
    "Power Off" "Reboot" "Lock" "Suspend" "Hibernate" "Logout")
[ -z "$ACTION" ] && exit 0

case $ACTION in
    'Power Off') CMD="sudo -A -p 'Enter password to shutdown:' /sbin/poweroff";;
    'Reboot') CMD="sudo -A -p 'Enter password to reboot:' /sbin/reboot";;
    'Lock') CMD="/usr/bin/i3lock-fancy";;
    'Suspend') CMD="sudo -A -p 'Enter password to suspend:' /bin/sh -c 'echo disk > /sys/power/state'";;
    'Logout')
             case $WM in
               i3) CMD="i3-msg exit; export WM=''";;
               jwm) CMD="jwm -exit; export WM=''";;
               bspwm) CMD="bspc quit; export WM=''";;
               spectrwm) CMD="killall spectrwm; export WM=''";;
               openbox) CMD="openbox --exit; export WM=''";;
               exwm) CMD="kill $(ps -aux | awk '/emacs/ && /desktop.el/ && !/awk/ {print $2}')";;
               *) exit 1 ;;
             esac ;;
	'Hibernate') CMD="sudo -A -p 'Enter password to hibernate:' pm-suspend";;
    *) exit 1 ;;
esac

eval exec "$CMD"
exit 0
