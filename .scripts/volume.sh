#!/bin/sh
case "$1" in
  'up') amixer -- sset Master playback 10+ | awk -F"[][]" '/dB/ { print $2 }' | dzen2 -w 83 -x 417 -h 48 -p 1;;
  'down') amixer -- sset Master playback 10- | awk -F"[][]" '/dB/ { print $2 }' | dzen2 -w 83 -x 417 -h 48 -p 1;;
  'mute') amixer -- sset Master toggle | awk '/dB/ { print $NF }' | dzen2 -w 83 -x 417 -h 48 -p 1;;
  'mutemic') amixer -- sset Capture toggle | awk '/dB/ { print $NF }' | dzen2 -w 83 -x 417 -h 48 -p 1;;
esac
exit 0
