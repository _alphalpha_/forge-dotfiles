#!/bin/sh
# perform various actions on open windows
# written by Michael Thorberger

. "$SCRIPTS"/functions/awk-menu.func  || exit 1
. "$SCRIPTS"/functions/draw-line.func || exit 1

sleep 0.01
term_width="$(tput cols || stty size | cut -d ' ' -f2)"

win_menu(){
  awk_menu_keys="rxst"
  awk_menu "$(wmctrl -l -x | awk -v w="$term_width" -F "\t" '!/window_menu.sh/ {print substr($0,1,w)}')"
  win="$(echo "$awk_menu_output" | awk '{print $1}')"
  name="$(echo "$awk_menu_output" | awk '{print $3}')"
  pid=$(xdotool getwindowpid "$win")

  case "$awk_menu_key_press" in
  'r') win_menu;;
  'x') kill "$pid" && sleep .1
       line="$awk_menu_line_number"
       win_menu;;
  's') if [ "$(ps --no-headers -o stat "$pid")" = "TLl" ]; then 
         kill -CONT "$pid"
       elif [ "$(ps --no-headers -o stat "$pid")" = "RLl" ]; then 
         kill -TSTP "$pid"
       fi;;    
  't') box_height=3
       box_color="${bg_clr3}"
       draw_box
       cursor_move "4" "$((term_height/2))"
       printf "${bg_clr3}${clr4}Enter a new title for %s: ${txt_underlined}" "$name"
       read -r newname
       printf "${clr_end}"
       cursor_move "0" "$((term_height/2+2))"
       xdotool set_window --name "$newname" "$win"
       line="$awk_menu_line_number"
       win_menu;;
  *)   xdotool windowactivate "$win" || exit 1;;
  esac
}
win_menu
exit 0
