#!/bin/sh

# Plumber script to perform various actions on urls

# If no input parameter is provided, use clipboard
if [ "$1" ]; then
  IN="$1"
else
  IN=$(xclip -o -selection clipboard)
fi
echo "$IN"

# check youtube-dl or yt-dlp
if [ -f /usr/local/bin/yt-dlp ] || [ -f /usr/bin/yt-dlp ] || [ -f $HOME/.local/bin/yt-dlp ]; then
  YTD=yt-dlp
elif [ -f /usr/local/bin/youtube-dl ] || [ -f /usr/bin/youtube-dl ] || [ -f $HOME/.local/bin/youtube-dl ]; then
  YTD=youtube-dl
fi

# Yad Icons
IMAGE_OK="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
IMAGE_CANCEL="/usr/share/icons/Numix/24/actions/dialog-close.svg"

# test if $IN is a valid URL
if [ "$(echo "$IN" | grep -E "(([A-Za-z]{3,9})://)?([-;:&=\+\$,\w]+@{1})?(([-A-Za-z0-9]+\.)+[A-Za-z]{2,3})(:\d+)?((/[-\+~%/\.\w]+)?/?([&?][-\+=&;%@\.\w]+)?(\#[\w]+)?)?")" ]; then

# youtube
  if [ "$(echo "$IN" | grep -E "youtube.com|youtu.be")" ]; then
    ACTION=$(yad --width 300 --entry --title "YAD" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "$IN" \
    --entry-text \
    "Play in MPV (HD)" "Play in MPV (sd)" "Play in MPV (Select Format)" "Play in MPV" "Download with $YTD (HD)" "Download with $YTD (sd)" "Download with $YTD (Select Format)" "Add to youtube-dl-script" "Open in browser" "Show")

# rumble
  elif [ "$(echo "$IN" | grep -v "embed" | grep -E "rumble.com")" ]; then
    #IN="$(curl "$IN" | xargs -d ',' -n 1 | grep embedUrl | sed 's/"//g;s/embedUrl://')"
    ACTION=$(yad --width 300 --entry --title "YAD" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "$IN" \
    --entry-text \
    "Play in MPV" "Play in MPV (Select Format)" "Download with $YTD (Select Format)" "Add to youtube-dl-script" "Open in browser" "Show")

# other video sites
  elif [ "$(echo "$IN" | grep -E "twitch.tv|vimeo.com|bitchute.com|odysee.com|lbry.tv|streamable.com|dailymotion.com|veoh.com|.mp4$|.webm$")" ]; then
    ACTION=$(yad --width 300 --entry --title "YAD" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "$IN" \
    --entry-text \
    "Play in MPV" "Play in MPV (Select Format)" "Download with $YTD" "Download with $YTD (Select Format)" "Add to youtube-dl-script" "Open in browser" "Show")

# soundcloud
  elif [ "$(echo "$IN" | grep -E "soundcloud.com")" ]; then
    ACTION=$(yad --width 300 --entry --title "YAD" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "$IN" \
    --entry-text \
    "Download with $YTD" "Add to youtube-dl-script" "Open in browser" "Show")

# git
  elif [ "$(echo "$IN" | grep -E "\.git$|git\.|github\.com|gitlab\.com|savannah\.gnu\.org")" ]; then
    ACTION=$(yad --width 300 --entry --title "YAD" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "$IN" \
    --entry-text \
    "Open in browser" "clone" "Save as PNG" "Show")

# everything else
  else
    ACTION=$(yad --width 300 --entry --title "YAD" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "$IN" \
    --entry-text \
    "Open in browser" "Download with wget" "Save as PNG" "Show")
  fi

# Actions
  case "$ACTION" in
  
    "Play in MPV")
      mpv "$IN";;
       
    "Play in MPV (HD)")
      mpv --ytdl-format=22 "$IN" || mpv "$IN";;
      
    "Play in MPV (sd)")
      mpv --ytdl-format=18 "$IN" || mpv "$IN";;
     
    "Play in MPV (Select Format)")
      FORMATS=$($YTD "$IN" -F | grep "^format code" -A 20 | grep -vE "video only|audio only" | awk 'NR!=1 {print $1,$2,$3}')
      ACTION=$(yad --width 300 --entry --title "Select Format" \
      --image="$IMAGE_MAIN" \
      --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
      --text "Select Format:" \
      --entry-text $(echo "$FORMATS" | sed 's/ /__/g'))
      FORMAT="$(echo "$ACTION" | cut -d '_' -f 1)"
      [ -z "$ACTION" ] && exit 0
      mpv --ytdl-format="$FORMAT" "$IN";;

    "Convert to MP3")
      $TERMINAL -e $YTD -x --audio-format=mp3 "$IN";;
		
    "Download with $YTD")
      $TERMINAL -e $YTD "$IN";;
      
    "Download with $YTD (HD)")
      $TERMINAL -e $YTD -f best "$IN";;
      
    "Download with $YTD (sd)")
      $TERMINAL -e $YTD -f 18 "$IN";;
      
    "Download with $YTD (Select Format)")
		FORMATS=$(youtube-dl "$IN" -F | grep "^format code" -A 20 | grep -vE "video only|audio only" | awk 'NR!=1 {print $1,$2,$3}')
    	ACTION=$(yad --width 300 --entry --title "Select Format" \
    	--image="$IMAGE_MAIN" \
    	--button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    	--text "Select Format:" \
    	--entry-text $(echo "$FORMATS" | sed 's/ /__/g'))
	    FORMAT="$(echo "$ACTION" | cut -d '_' -f 1)"
		[ -z "$ACTION" ] && exit 0
		$TERMINAL -e "$YTD -f $FORMAT $IN";;
		
    "Add to youtube-dl-script")
      echo "$IN" >> "$HOME/.cache/ytdl_script_list.tmp";;
      
    "Open in browser")
      $BROWSER "$IN";;
      
	"clone")
	  NAME="$(echo "$IN" | awk -F/ '{print $NF; exit}' | awk -F.  '{print $1}')"
	  if [ "$(echo "$NAME" | grep "^#")" ]; then
	    NAME="$(echo "$IN" | awk -F/ '{print $(NF-1)}'| awk -F.  '{print $1}')"
	  fi
	  OUT="$(yad --width=500 --center --title="git clone $IN to" --entry --editable --entry-text="$HOME/$NAME")"
	  $TERMINAL -e git clone "$IN $OUT";;
	  
	"Download with wget")
	 "$TERMINAL" -e sh -c "wget -c $IN ; printf '\\npress enter to close this window'; read -r ok";;
	 
	"Save as PNG") OUT="$(yad --text "Enter filename:" --entry)"
                   [ ! "$(echo "$OUT" | grep -iE ".png$")" ] && OUT="$OUT".png
                   while [ -f "$OUT" ]; do
                     OUT="$(yad --text "$OUT already exists\nEnter filename:" --entry)"
                     [ ! "$(echo "$OUT" | grep -iE ".png$")" ] && OUT="$OUT".png
                   done
                   E=0
                   wkhtmltoimage --stop-slow-scripts --quality 98 -f png "$IN" "$OUT".tmp || E=1

                   #img_width="$(identify "$OUT".tmp | awk '{print $3}' | cut -d x -f1)"
                   #img_height="$(identify "$OUT".tmp | awk '{print $3}' | cut -d x -f2)"
                   
                   img_width="$(exiftool -r "$OUT".tmp | awk '/Image Size/ {print $NF}' | cut -d x -f1)"
                   img_height="$(exiftool -r "$OUT".tmp | awk '/Image Size/ {print $NF}' | cut -d x -f2)"
                   
                   img_height_total="$((img_height+50))"
                   box_border="$((img_width-2))"
                   
                   convert "$OUT".tmp -gravity NorthWest -splice 0x44 \
                    -stroke LightSlateGray -strokewidth 3 -fill LightSteelBlue -draw "rectangle 1,1 $box_border,48" \
                    -stroke none -fill black \
                    -font /usr/share/fonts/opentype/bebas-neue/BebasNeue-Regular.otf \
                    -pointsize 24  -annotate +5+5 "$(echo "$IN" | sed 's/^https\?:\/\///' | sed 's/^www\.//')" \
                    -pointsize 18 -annotate +5+30 "$(date "+%a %d %B %Y %H:%M:%S %Z")" \
                    "$OUT" || E=1
                   pngcrush "$OUT" || E=1
	               exiftool -overwrite_original_in_place -Title="$IN"  "$OUT" || E=1
	               exiftool -overwrite_original_in_place -ImageSize="$img_widthx$img_height_total" "$OUT"
	               if [ "$E" = "0" ]; then
                     yad --width="300" --fixed --center \
                       --title="Save as PNG" \
                       --buttons-layout="center" \
                       --text=" saved $(pwd)/$OUT" \
                       --button="Open:xdg-open $OUT" --button="Ok:0"
	               else
                     yad --width="300" --fixed --center \
                       --title="Save as PNG" \
                       --buttons-layout="center" \
                       --text=" Error" \
                       --button="Ok:0"              
	               fi
	               if [ -f "$OUT.tmp" ]; then
	                 rm "$OUT.tmp"
	               fi
	               ;;
	               
	"Save as PDF") OUT="$(yad --text "Enter filename:" --entry)"
	               [ ! "$(echo "$OUT" | grep -iE ".pdf$")" ] && OUT="$OUT".pdf
                   while [ -f "$OUT" ]; do
                     OUT="$(yad --text "$OUT already exists\nEnter filename:" --entry)"
                     [ ! "$(echo "$OUT" | grep -iE ".pdf$")" ] && OUT="$OUT".pdf
                   done
	               wkhtmltopdf "$IN" "$OUT"
	               exiftool -overwrite_original_in_place -Title="$IN" "$OUT";;
	               
	"Show")
      echo "$IN" | dzen2 -p 5 -x 1 -y 1 -h 52 -bg "#DC6434" -fg "#201010" -fn Bebas\ Neue-35 -xs 0;;
	
    *) exit 0
  esac
fi

exit 0
