#!/bin/sh
. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func || exit 1

# default wallpaper directory
DIR="/usr/share/backgrounds"

# a sysmlink will be created here
LINK="$HOME/.local/wallpaper"

# this file will be created and stores information about the active wallpaper ($OPT)
CONF="$HOME/.local/wallpaper.conf"


pcmanfmdesktop="$(ps -ax | awk '/pcmanfm --desktop/&&!/awk/{print $1}')"



if [ -f "$1" ]; then
  WP="$1"
elif [ "$1" = "-r" ] || [ "$1" = "--restore" ]; then
  if [ -f "$CONF" ]; then
    OPT="$(cat "$CONF")"
  else
    OPT="--zoom"
  fi
  if [ -L "$LINK" ]; then
    LINK="$(readlink "$LINK")"
  fi
  xwallpaper "$OPT" "$LINK"
  exit 0
elif [ -d "$1" ]; then
  DIR="$1"
  LIST="$(find "$DIR" -maxdepth 1 -type f \( -iname "*.png" -o -iname "*.jpg" \) | head -n 1000)"
  WP="$(awk_menu "$LIST")"  
else
  awk_menu_tmsg="$(text_box "Select a file..."))"
  WP="$(awk_menu "$(find "$DIR" -type f)")"
fi

case "$2" in
  -c|--center|-f|--full|-m|--maximize) OPT="--maximize";;
  -s|--stretch) OPT="--stretch";;
  -t|--tile|--tiled) OPT="--tile";;
  -z|--zoom|*) OPT="--zoom";;
esac

if [ -f "$WP" ]; then
  if [ "$(file -b --mime-type "$WP" | cut -d '/' -f 1)" = "image" ]; then
    [ -L "$LINK" ] || [ -f "$LINK" ] && rm "$LINK"
    if [ "$(file -b --mime-type "$WP" | cut -d '/' -f 2)" = "png" ]; then
      ln -s "$WP" "$LINK"
      LINK="$WP"
    else
      convert "$WP" "$LINK"
    fi
  else
    printf "Error. %s is not an image file." "$WP"
    exit 1
  fi

  if [ "$pcmanfmdesktop" ]; then
    case "$2" in
      -c|--center|-f|--full|-m|--maximize) pcmanfm --wallpaper-mode=center &;;
      -s|--stretch) pcmanfm --wallpaper-mode=stretch &;;
      -t|--tile|--tiled) pcmanfm --wallpaper-mode=tile &;;
      -z|--zoom|*) pcmanfm --wallpaper-mode=crop &;;
    esac
    pcmanfm -w "$LINK" &
  else
    xwallpaper "$OPT" "$LINK" &
  fi
  echo "$OPT" > "$CONF"
fi
exit 0
