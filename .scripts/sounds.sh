#!/bin/sh

snd_dir="/usr/share/sounds"

snd_ok="$snd_dir/482653__joao-janz__bonus-points-1-1.wav"
snd_error="$snd_dir/504853__joao-janz__vintage-error-alert-1-1.wav"
snd_fail="$snd_dir/175409__kirbydx__wah-wah-sad-trombone.wav"
snd_tada="$snd_dir/376318__jimhancock__tada.wav"
snd_notification="$snd_dir/416791__enlian75__select-sound-thing.wav"
snd_alarm="$snd_dir/25031__sirplus__alarm-fatal.wav"
snd_login_default="$snd_dir/Login640367.wav"
snd_brass="$snd_dir/350889__tdmdddddd__blaring-brass.wav"
snd_gong="$snd_dir/209940__veiler__fade-in-hi-thirsk-gong-better-32-bit.wav"
snd_kotsuzumi="$snd_dir/330563__andre-onate__kotsuzumi-roll-at-126.wav"
snd_guzheng="$snd_dir/396868__pufermufin__lovely-chinese-guzheng-plucked.wav"
snd_horse="$snd_dir/394831__uepcomanam2011__cavall.wav"
snd_login="$snd_kotsuzumi"

snd_f1="$snd_dir/350428__benjaminharveydesign__trumpet-fanfare.wav"
snd_f2="$snd_dir/175409__kirbydx__wah-wah-sad-trombone.wav"
snd_f3="$snd_dir/191718__adriann__drumroll.wav"
snd_f4="$snd_dir/277022__sandermotions__applause-1.wav"
snd_f5="$snd_gong"
snd_f6="$snd_kotsuzumi"
snd_f7="$snd_guzheng"
snd_f8="$snd_dir/364807__rutgermuller__japan-koto-my-amateur-improv-session-1.wav"
snd_f9="$snd_horse"
snd_f10="$snd_tada"
snd_f11="$snd_dir/520998__saviraz__boxing-bell-signals.wav"
snd_f12="$snd_dir/209578__zott820__cash-register-purchase.wav"

play_sound() {
  if [ -f "$snd" ]; then
    aplay "$snd"
  fi
}

case "$1" in
  '--login'|'-login'|'-l') snd="$snd_login"; play_sound;;
  '--ok'|'-ok'|'-o') snd="$snd_ok"; play_sound;;
  '--error'|'-error'|'-e') snd="$snd_error"; play_sound;;
  '--fail'|'-fail'|'-f') snd="$snd_fail"; play_sound;;
  '--tada'|'-tada'|'-t') snd="$snd_tada"; play_sound;;
  '--notify'|'-notify'|'-n') snd="$snd_notification"; play_sound;;
  '--alarm'|'-alarm'|'-a') snd="$snd_alarm"; play_sound;;
  *'.wav') snd="$1"; play_sound;;
  'f'|'F1') snd="$snd_f1"; play_sound;;
  'f2'|'F2') snd="$snd_f2"; play_sound;;
  'f3'|'F3') snd="$snd_f3"; play_sound;;
  'f4'|'F4') snd="$snd_f4"; play_sound;;
  'f5'|'F5') snd="$snd_f5"; play_sound;;
  'f6'|'F6') snd="$snd_f6"; play_sound;;
  'f7'|'F7') snd="$snd_f7"; play_sound;;
  'f8'|'F8') snd="$snd_f8"; play_sound;;
  'f9'|'F9') snd="$snd_f9"; play_sound;;
  'f10'|'F10') snd="$snd_f10"; play_sound;;
  'f11'|'F11') snd="$snd_f11"; play_sound;;
  'f11'|'F12') snd="$snd_f12"; play_sound;;
esac

exit 0
