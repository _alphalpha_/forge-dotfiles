#!/bin/sh
i="$(echo "$0" | awk -F '/' '{print $NF}')"

case "$1" in
  'l'|'-l'|'left'|'--left') xrandr_opt="left"; xsetwacom_opt="ccw";;
  'r'|'-r'|'right'|'--right') xrandr_opt="right"; xsetwacom_opt="cw";;
  'n'|'-n'|'normal'|'--normal') xrandr_opt="normal"; xsetwacom_opt="none";;
  'i'|'-i'|'u'|'-u'|'inverted'|'--inverted') xrandr_opt="inverted"; xsetwacom_opt="half";;
  *) echo "Usage: $i -l to rotate screen left           (or --left)
       $i -r to rotate screen right          (or --right)
       $i -u to rotate screen upside down    (or --inverted)
       $i -n to rotate screen back to normal (or --normal)"
     exit 0;;
esac

# this will rotate the primary display
display_name="$(xrandr | awk '/primary/ {print $1}')"
xrandr --output "$display_name" --rotate "$xrandr_opt"

# this will check for wacom devices:
if xsetwacom --list devices | grep "id:"; then
  for wacom_dev in $(xsetwacom --list devices | grep -Eo "id: [0-9]{2}?"| cut -d ' ' -f2); do
    xsetwacom --set "$wacom_dev" Rotate "$xsetwacom_opt"
  done
fi

exit 0
