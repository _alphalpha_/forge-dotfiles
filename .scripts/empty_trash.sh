#!/bin/sh
trash_d="$HOME/.local/share/Trash"
if [ -d "$trash_d" ]; then
  find "$trash_d" -type l -delete
  find "$trash_d" -type f -delete
  find "$trash_d" -type p -delete
  for d in $(find "$trash_d" -type d -empty | awk '{ print length, $0 }' | sort -nr | cut -d" " -f2-); do
    rmdir "$d"
  done
else
  printf "Error: %s not found.\\n" "$trash_d"
  exit 1
fi
exit 0
