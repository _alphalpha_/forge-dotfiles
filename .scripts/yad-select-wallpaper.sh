#!/bin/sh
#by Michael
WALLPAPER="$(find /usr/share/backgrounds -iname "*.png" -type f |\
yad --width 500 --height 600 --list --title "Search Results" --text "Select a wallpaper" --column "Files" |\
cut -d '|' -f1)"
echo "$WALLPAPER"
ln -fs "$WALLPAPER" ~/.local/wallpaper.png
xwallpaper --zoom ~/.local/wallpaper.png
