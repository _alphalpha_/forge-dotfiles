#! /bin/bash
# -*- mode: sh -*-

if [ "$1" ]; then
  case "$1" in
  -c) T="1";;
  -m) T="2";;
  -d) T="3";;
  -p) T="4";;
  -k) T="5";;
  -s) T="6";;
  esac
  shift
else
  T="1"
fi

YAD_OPTIONS="--window-icon='dialog-information' --name=IxSysinfo"

KEY=$RANDOM

function show_mod_info {
    TXT="\\n<span face='Monospace'>$(modinfo $1 | sed 's/&/\&amp;/g;s/</\&lt;/g;s/>/\&gt;/g')</span>"
    yad --title=$"Module information" --button="yad-close" --width=500 \
        --image="application-x-addon" --text="$TXT"
}
export -f show_mod_info

# CPU tab
lscpu | sed -r "s/:[ ]*/\n/" |\
    yad --plug=$KEY --tabnum=1 --text=$"CPU information" \
        --list --no-selection --column=$"Parameter" --column=$"Value" &

# Memory tab
sed -r "s/:[ ]*/\n/" /proc/meminfo |\
    yad --plug=$KEY --tabnum=2 --text=$"Memory usage information" \
        --list --no-selection --column=$"Parameter" --column=$"Value" &

# Harddrive tab
df -T | grep -vwE 'tmpfs|devtmpfs' | tail -n +2 | awk '{printf "%s\n%s\n%s\n%s\n%s\n%s\n", $1,$7, $2, $3, $4, $6}' |\
yad --plug=$KEY --tabnum=3 --title="Disk space usage" --width=800 --height=450 --text="Disk space usage" \
    --list --no-selection --column="Device" --column="Mountpoint" --column="Type" \
    --column="Total:sz" --column="Free:sz" --column="Usage:bar" &

# PCI tab
lspci -vmm | sed 's/\&/\&amp;/g' | grep -E "^(Slot|Class|Vendor|Device|Rev):" | cut -f2 |\
    yad --plug=$KEY --tabnum=4 --text=$"PCI bus devices" \
        --list --no-selection --column=$"ID" --column=$"Class" \
        --column=$"Vendor" --column=$"Device" --column=$"Rev" &

# Modules tab
awk '{printf "%s\n%s\n%s\n", $1, $3, $4}' /proc/modules | sed "s/[,-]$//" |\
    yad --plug=$KEY --tabnum=5 --text=$"Loaded kernel modules" --image-on-top \
        --list --dclick-action='bash -c "show_mod_info %s"' \
        --column=$"Name" --column=$"Used" --column=$"Depends" &

# Battery tab
( acpi -i ; acpi -a ) | sed -r "s/:[ ]*/\n/" | yad --plug=$KEY --tabnum=6 \
    --text=$"Battery state" --list --no-selection \
    --column=$"Device" --column=$"Details" &

# Sensors tab
SENSORS=($(sensors | grep -E '^[^:]+$'))
sid=1
cid=1

for s in ${SENSORS[@]}; do
    echo -e "s$sid\n<b>$s</b>\n"
    sensors -A "$s" | tail -n +2 | while read ln; do
        [[ $ln == "" ]] && continue
        echo "$cid:s$sid"
        echo $ln | sed -r 's/:[ ]+/\n/'
        ((cid++))
    done
    ((sid++))
done | yad --plug=$KEY --tabnum=7 --text=$"Temperature sensors information" \
    --list --tree --tree-expanded --no-selection --column=$"Sensor" --column=$"Value" &

# main dialog
TXT=$"<b>Hardware system information</b>\\n\\n"
TXT+=$"\\tOS: $(lsb_release -ds) on $(hostname)\\n"
TXT+=$"\\tKernel: $(uname -sr)\\n\\n"
TXT+="\\t<i>$(uptime)</i>"

yad --notebook --width=750 --height=550 --title=$"System info" --text="$TXT" --button="yad-close" \
    --key=$KEY --tab=$"CPU" --tab=$"Memory" --tab=$"Disks" --tab=$"PCI" --tab=$"Modules" \
    --tab=$"Battery" --tab=$"Sensors" --active-tab=${1:-"$T"}

exit 0
