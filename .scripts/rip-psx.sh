#!/bin/sh
# Script to rip Playstation 1 games (for PS2 games just use dd)
# by Michael Thorberer

. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func || exit 1

# Output files are created here:
game_dir="$HOME/PSX"

# Check dependencies
if [ ! -f /usr/bin/cdrdao ] || [ ! -f /usr/bin/bchunk ]; then
  printf "Error: Missing dependencies. Exiting Script.\\n"
  printf "Please make sure cdrdao and bchunk are installed.\\n"
  exit 1
fi

# Check user input
if [ "$1" ]; then
  game="$1"
else
  printf "ERROR: Invalid usage.\\nExample: rip-psx.sh 'filename'\\nwhere filename is the desired name, minus extensions.\\n"
  exit 1
fi

# Menu
awk_menu_tmsg="$(text_box "Select output format:")"
input_text="iso
bin/cue
toc
Exit Script"
format="$(awk_menu "$input_text")"

# Check dir
if [ ! -d "$game_dir" ]; then
  mkdir -p "$game_dir"
fi
cur_dir="$(pwd)"
cd "$game_dir" || exit 1

# Lets go
cdrdao read-cd --read-raw --datafile "$game".bin --device /dev/sr0 --driver generic-mmc-raw "$game".toc

# Convert
case "$format" in
  "bin/cue") toc2cue "$game".toc "$game".cue 
       if [ -f "$game".toc ]; then rm "$game".toc ; fi ;;
  "iso") toc2cue "$game".toc "$game".cue && bchunk -r "$game".bin "$game".cue "$game"
       if [ -f "$game"01.iso ]; then mv "$game"01.iso "$game".iso ; fi
       if [ -f "$game".bin ]; then rm "$game".bin ; fi
       if [ -f "$game".cue ]; then rm "$game".cue ; fi 
       if [ -f "$game".toc ]; then rm "$game".toc ; fi ;;
esac

cd "$cur_dir" || exit 1
printf "Done. Files have been created in %s\\n" "$game_dir"
exit 0
