#!/bin/sh
if [ "$(wmctrl -l | awk '($4 == "Scratchpad-Terminal"){print}')" ] ; then
  if [ "$(xdotool getactivewindow getwindowname)" = 'Scratchpad-Terminal' ]; then
    $TERMINAL
  else
    xdotool windowactivate --sync $(xdotool search --name "Scratchpad-Terminal") \
    && xdotool windowfocus  $(xdotool search --name "Scratchpad-Terminal") 
  fi
else
  $TERMINAL -T "Scratchpad-Terminal"
fi
