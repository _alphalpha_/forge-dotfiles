#!/bin/sh

# script to swap some fn keys around

# xmodmap can show the codes
# xmodmap -pk | awk '/XF86ScreenSaver/ {print $1}'
# xmodmap -pk | awk '/XF86Battery/ {print $1}'

xmodmap -e "keycode 160 = XF86Battery"
xmodmap -e "keycode 244 = XF86ScreenSaver"

exit 0
