#!/bin/sh
img_main="gnome-shutdown"
img_ok="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
img_cancel="/usr/share/icons/Numix/24/actions/dialog-close.svg"

yad_action="$(\
  yad --title "System Power" --width 220 --window-icon="$img_main"\
    --entry --entry-text "Power Off" "Reboot" "Lock" "Hibernate" "Logout"\
    --button="OK!$img_ok:0" --button="Cancel!$img_cancel:1"\
    --buttons-layout="spread" --borders="8" --center --on-top)"

case "$yad_action" in
  'Power Off') "$SCRIPTS"/power.sh poweroff;;
     'Reboot') "$SCRIPTS"/power.sh reboot;;
       'Lock') "$SCRIPTS"/power.sh lock;;
     'Logout') "$SCRIPTS"/power.sh logout;;
  'Hibernate') "$SCRIPTS"/power.sh hibernate;;
            *) exit 1;;
esac
exit 0
