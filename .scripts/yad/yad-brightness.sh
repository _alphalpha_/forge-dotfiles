#!/bin/sh
sudo brightnessctl s "$(yad --title='Brightness' --width=420 --scale --min-value=0 --max-value=100  --value="$(brightnessctl | grep -Eo "[0-9]?{3}%" | tr -d '%')" \
 --mark=10:10 --mark=20:20  --mark=30:30 --mark=40:40 --mark=50:50 --mark=60:60 --mark=70:70 --mark=80:80 --mark=90:90 --mark=100:100  --button=OK --button=Exit)%"
exit 0
