#!/bin/sh
icon_ok="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
icon_cancel="/usr/share/icons/Numix/24/actions/dialog-close.svg"

yad_output="$(\
yad \
--text 'select startup sound: ' \
--entry --entry-text 'default' 'brass' 'gong' 'kotsuzumi' 'guzheng' 'horse' 'other' 'none' \
)"
[ -z "$yad_output" ] && exit 0

current_sound="$(grep -m1 '^snd_login=' "$SCRIPTS/sounds.sh")"

case "$yad_output" in
  'default') sed -i "s:$current_sound:snd_login=\"\$snd_login_default\":" "$SCRIPTS/sounds.sh";;
  'brass') sed -i "s:$current_sound:snd_login=\"\$snd_brass\":" "$SCRIPTS/sounds.sh";;
  'gong') sed -i "s:$current_sound:snd_login=\"\$snd_gong\":" "$SCRIPTS/sounds.sh";;
  'kotsuzumi') sed -i "s:$current_sound:snd_login=\"\$snd_kotsuzumi\":" "$SCRIPTS/sounds.sh";;
  'guzheng') sed -i "s:$current_sound:snd_login=\"\$snd_guzheng\":" "$SCRIPTS/sounds.sh";;
  'horse') sed -i "s:$current_sound:snd_login=\"\$snd_horse\":" "$SCRIPTS/sounds.sh";;
  'none') sed -i "s:$current_sound:snd_login=\"\":" "$SCRIPTS/sounds.sh";;
  'other') other='other';;
esac

if [ -z "$other" ] || [ "$other" != 'other' ]; then
  exit 0
else
  yad_output="$(\
  yad \
  --form --field="Select wav file:":FL /usr/share/sounds/\  \
  --file-filter="*.wav *.WAV" \
  )"

  other="$(echo "$yad_output" | awk -F '|' '{print $1}')"
  if [ -f "$other" ]; then
    sed -i "s:$current_sound:snd_login=$other:" "$SCRIPTS/sounds.sh"
  fi
fi

exit 0
