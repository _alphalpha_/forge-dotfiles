#!/bin/ksh
#!/bin/bash
# by Michael Thorberger

KEY=$RANDOM
tn=0
yad_tabs=''
for license in $(find /usr/share/common-licenses -type f | sort); do
  tn="$((tn+1))"
  name=$(echo "$license" | awk -F '/' '{print $NF}')
  yad_tabs="$yad_tabs --tab=$name"
  echo "$tn $name $license"
  cat "$license" | yad --plug="$KEY" --tabnum="$tn" --listen --text-info  &
done 
$(yad --center --text="Here are some common open source licenses:" --notebook --key="$KEY" $yad_tabs --title="Licenses" --button="Exit:0" --height=700 --width=768)    
exit 0
