#!/bin/sh
# generate a plain colored wallpaer
# by Michael
FILE="$HOME/.local/wallpaper.png"
# use yad to select color value
COLOR="$(yad --title="Set Wallpaper" --color --init-color=#D76438)"
# use imagemagick to create the png file
if [ "$COLOR" ]; then
  [ -e "$FILE" ] && rm "$FILE"
  convert -size $(xdpyinfo | awk '/dimensions/ {print $2}') canvas:$COLOR "$FILE"
# use xwallpaper to set the wallpaper
  if ps -ax | grep -v 'grep' | grep -w 'pcmanfm --desktop' ; then
    pcmanfm --wallpaper-mode=crop &
    pcmanfm -w "$FILE" &
  else
    xwallpaper --zoom "$FILE"
  fi
fi
exit 0
