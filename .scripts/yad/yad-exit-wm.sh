#!/bin/sh

case "$WM" in
  berry) exit_cmd="berryc quit";;
  bspwm) exit_cmd="bspc quit";;
  i3) exit_cmd="i3-msg exit";;
  JWM) exit_cmd="jwm -exit";;
  spectrwm) exit_cmd="killall spectrwm";;
esac

IMAGE_OK="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
IMAGE_CANCEL="/usr/share/icons/Numix/24/actions/dialog-close.svg"
yad --width="300" --fixed --center \
  --title="Exit $wm" \
  --buttons-layout="center" \
  --text=" Do you really want to exit $wm? \n This will end your X session. " \
  --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
&& eval "$exit_cmd"
exit 0
