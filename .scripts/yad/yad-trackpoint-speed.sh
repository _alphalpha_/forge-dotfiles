#!/bin/sh

sysdir="$(find /sys/devices/platform/i8042 -name name | xargs grep -Fl TrackPoint | sed 's/\/input\/input[0-9]*\/name$//')"

if [ ! -f "$sysdir/speed" ]; then
  exit 1
fi

yad --title='Trackpoint speed' --width=420 --scale --min-value=0 --max-value=250  --value="$(cat "$sysdir/speed")" \
 --mark=50:50 --mark=100:100  --mark=150:150 --mark=200:200 --mark=250:250  --button=OK --button=Exit | sudo tee "$sysdir/speed"

exit 0
