#!/bin/sh

#curl https://pkgmaster.devuan.org/mirror_list.txt \

cat ml.txt \
| awk '/FQDN:/ {print $2};
       /BaseURL:/ {print $2};
       /Bandwidth:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)}; printf "\n"};
       /Rate:/{print $2};
       /Country:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)};printf "\n"};
       /CountryCode:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)}; printf "\n"};
       /Protocols:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)}; printf "\n"};
       /Active:/ {print $2};
       /DNSRR:/ {print $2};
       /DNSRRCC:/ {print $2};' \
| yad --width="$(($(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 1) -20))" \
      --height="$(($(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 2) -20))" \
      --list --no-selection --column="FQDN" --column="BaseURL" --column="Bandwidth" \
      --column="Rate" --column="Country" --column="CountryCode" --column="Protocols" \
      --column="Active" --column="DNSRR" --column="DNSRRCC"




OUT="$(yad --text "Enter filename:" --entry)"
                   [ ! "$(echo "$OUT" | grep -iE ".png$")" ] && OUT="$OUT".png
                   while [ -f "$OUT" ]; do
                     OUT="$(yad --text "$OUT already exists\nEnter filename:" --entry)"
                     [ ! "$(echo "$OUT" | grep -iE ".png$")" ] && OUT="$OUT".png
                   done
                   E=0
                   wkhtmltoimage --stop-slow-scripts --quality 98 -f png "$IN" "$OUT".tmp || E=1

                   #img_width="$(identify "$OUT".tmp | awk '{print $3}' | cut -d x -f1)"
                   #img_height="$(identify "$OUT".tmp | awk '{print $3}' | cut -d x -f2)"
                   
                   img_width="$(exiftool -r "$OUT".tmp | awk '/Image Size/ {print $NF}' | cut -d x -f1)"
                   img_height="$(exiftool -r "$OUT".tmp | awk '/Image Size/ {print $NF}' | cut -d x -f2)"
                   
                   img_height_total="$((img_height+50))"
                   box_border="$((img_width-2))"
                   
                   convert "$OUT".tmp -gravity NorthWest -splice 0x44 \
                    -stroke LightSlateGray -strokewidth 3 -fill LightSteelBlue -draw "rectangle 1,1 $box_border,48" \
                    -stroke none -fill black \
                    -font /usr/share/fonts/opentype/bebas-neue/BebasNeue-Regular.otf \
                    -pointsize 24  -annotate +5+5 "$(echo "$IN" | sed 's/^https\?:\/\///' | sed 's/^www\.//')" \
                    -pointsize 18 -annotate +5+30 "$(date "+%a %d %B %Y %H:%M:%S %Z")" \
                    "$OUT" || E=1
                   pngcrush "$OUT" || E=1
	               exiftool -overwrite_original_in_place -Title="$IN"  "$OUT" || E=1
	               exiftool -overwrite_original_in_place -ImageSize="$img_widthx$img_height_total" "$OUT"
	               if [ "$E" = "0" ]; then
                     yad --width="300" --fixed --center \
                       --title="Save as PNG" \
                       --buttons-layout="center" \
                       --text=" saved $(pwd)/$OUT" \
                       --button="Open:xdg-open $OUT" --button="Ok:0"
	               else
                     yad --width="300" --fixed --center \
                       --title="Save as PNG" \
                       --buttons-layout="center" \
                       --text=" Error" \
                       --button="Ok:0"              
	               fi
	               if [ -f "$OUT.tmp" ]; then
	                 rm "$OUT.tmp"
	               fi


	"Save as PDF") OUT="$(yad --text "Enter filename:" --entry)"
	               [ ! "$(echo "$OUT" | grep -iE ".pdf$")" ] && OUT="$OUT".pdf
                   while [ -f "$OUT" ]; do
                     OUT="$(yad --text "$OUT already exists\nEnter filename:" --entry)"
                     [ ! "$(echo "$OUT" | grep -iE ".pdf$")" ] && OUT="$OUT".pdf
                   done
	               wkhtmltopdf "$IN" "$OUT"
	               exiftool -overwrite_original_in_place -Title="$IN" "$OUT";;
