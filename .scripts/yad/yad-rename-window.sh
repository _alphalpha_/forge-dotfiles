#!/bin/sh
id=$(xprop | awk '/_NET_WM_PID\(CARDINAL\)/ {print $3}')
window="$(wmctrl -lpx | awk -v id="$id" '$3 == id {print $1}')"
newname="$(yad --class="Yad" \
    --title="Rename Window" \
    --text="Enter a new name:" \
    --entry)"
wmctrl -r "$window" -T "$newname" 
exit 0
