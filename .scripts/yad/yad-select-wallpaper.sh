#!/bin/sh
pape="$(find /usr/share/backgrounds -iname "*.png" -type f |\
yad --width 500 --height 600 --list --title "Search Results" --text "Select a wallpaper" --column "Files" |\
cut -d '|' -f1)"
if [ "$pape" ]; then
  ln -fs "$pape" ~/.local/wallpaper.png
  xwallpaper --zoom ~/.local/wallpaper.png
fi
exit 0
