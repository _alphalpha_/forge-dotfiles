#!/bin/sh
IMAGE_MAIN="gnome-shutdown"
IMAGE_OK="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
IMAGE_CANCEL="/usr/share/icons/Numix/24/actions/dialog-close.svg"

ACTION=$(yad --width 300 --entry --title "System Logout" \
    --image="$IMAGE_MAIN" \
    --button="OK!$IMAGE_OK:0" --button="Cancel!$IMAGE_CANCEL:1" \
    --text "Choose action:" \
    --entry-text \
    "Power Off" "Reboot" "Lock" "Hibernate" "Logout")
[ -z "$ACTION" ] && exit 0

case $ACTION in
  'Power Off') sudo -A -p 'Enter password to shutdown:' /sbin/poweroff;;
     'Reboot') sudo -A -p 'Enter password to reboot:' /sbin/reboot;;
       'Lock') i3lock-fancy;;
     'Logout') case $WM in
                 i3) i3-msg exit; export WM='';;
                 JWM) jwm -exit; export WM='';;
                 bspwm) bspc quit; export WM='';;
                 spectrwm) killall spectrwm; export WM='';;
                 openbox) openbox --exit; export WM='';;
                 fvwm|NsCDE) killall fvwm3; export WM='';;
                 berry) berryc quit; export WM='';;
                 hypr) killall Hypr; export WM='';;
                 *) exit 1 ;;
               esac;;
	'Hibernate') i3lock-fancy &
                 sleep 3
                 sudo  pm-suspend;;
    *) exit 1 ;;
esac
exit 0
