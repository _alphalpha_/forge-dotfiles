#!/bin/sh
# written by Michael Thorberger
# Licence: GPL-3
# This is free software with no warrantees. Use at your own risk.

color_theme_default() {
  clr1='#0e2f44' # dark blue
  clr2='#33aaee' # sky blue
  clr3='#336699' # flat blue
  clr4='#fafaea' # white
  clr6='#ea0000' # red
  clr7='#fec30c' # yellow
  clr8='#daa520' # orange
}

color_theme_alert() {
  clr1='#d20000' # red
  clr2='#780000' # other red 
  clr3='#b92211' # dark red
  clr4='#fafaea' # white
  clr6='#ea0000' # red
  clr7='#fec30c' # yellow
  clr8='#daa520' # orange
}

wm="$(xprop -id $(xprop -root -notype _NET_SUPPORTING_WM_CHECK \
      | awk '{print $NF}') -f _NET_WM_NAME 8t \
      | awk -F\" '/WM_NAME/{print $2}')"
case "$wm" in
  'berry') autostart_file="$HOME/.config/berry/autostart.sh";;
  'bspwm') autostart_file="$HOME/.config/bspwm/autostart.sh";;
  'i3') autostart_file="$HOME/.config/i3/autostart.sh";;
  'JWM') autostart_file="$HOME/.config/jwm/autostart.sh";;
  'spectrwm') autostart_file="$HOME/.config/spectrwm/autostart.sh";;
esac


svg_checkbox_0='<svg width="32px" height="32px" viewBox="0 0 32 32">
<path fill="#11E3BE" fill-rule="nonzero" d="M5.75,3 L18.25,3 C19.7687831,3 21,4.23121694 21,5.75 L21,18.25 C21,19.7687831 19.7687831,21 18.25,21 L5.75,21 C4.23121694,21 3,19.7687831 3,18.25 L3,5.75 C3,4.23121694 4.23121694,3 5.75,3 Z M5.75,4.5 C5.05964406,4.5 4.5,5.05964406 4.5,5.75 L4.5,18.25 C4.5,18.9403559 5.05964406,19.5 5.75,19.5 L18.25,19.5 C18.9403559,19.5 19.5,18.9403559 19.5,18.25 L19.5,5.75 C19.5,5.05964406 18.9403559,4.5 18.25,4.5 L5.75,4.5 Z"></path>
</svg>'

svg_checkbox_1='<svg width="32px" height="32px" viewBox="0 0 32 32">
<path fill="#11E3BE" fill-rule="nonzero" d="M18.25,3 C19.7687831,3 21,4.23121694 21,5.75 L21,18.25 C21,19.7687831 19.7687831,21 18.25,21 L5.75,21 C4.23121694,21 3,19.7687831 3,18.25 L3,5.75 C3,4.23121694 4.23121694,3 5.75,3 L18.25,3 Z M18.25,4.5 L5.75,4.5 C5.05964406,4.5 4.5,5.05964406 4.5,5.75 L4.5,18.25 C4.5,18.9403559 5.05964406,19.5 5.75,19.5 L18.25,19.5 C18.9403559,19.5 19.5,18.9403559 19.5,18.25 L19.5,5.75 C19.5,5.05964406 18.9403559,4.5 18.25,4.5 Z M10,14.4393398 L16.4696699,7.96966991 C16.7625631,7.6767767 17.2374369,7.6767767 17.5303301,7.96966991 C17.7965966,8.23593648 17.8208027,8.65260016 17.6029482,8.94621165 L17.5303301,9.03033009 L10.5303301,16.0303301 C10.2640635,16.2965966 9.84739984,16.3208027 9.55378835,16.1029482 L9.46966991,16.0303301 L6.46966991,13.0303301 C6.1767767,12.7374369 6.1767767,12.2625631 6.46966991,11.9696699 C6.73593648,11.7034034 7.15260016,11.6791973 7.44621165,11.8970518 L7.53033009,11.9696699 L10,14.4393398 L16.4696699,7.96966991 L10,14.4393398 Z">
</svg>'

svg_line='<svg width="700px" height="4px" viewBox="0 0 700 4">
<path style="color:#daa520;fill-opacity:1;stroke:#daa520;fill:#daa520;stroke-opacity:1" d="m 0,0 v 1 H 700 v -1 z" />
</svg>'

check_autostart(){
autostart="$(awk  '
{ if ($0 ~ /yad-welcome.sh/)
  { x = substr($0,1,1)
    if ( x == "#" ) { printf "0";exit}
    else {printf "1";exit}
  }
}' "$autostart_file")"

if [ "$autostart" = 0 ]; then
  autorun_icon="$svg_checkbox_0"
elif [ "$autostart" = 1 ]; then
  autorun_icon="$svg_checkbox_1"
fi
}
check_autostart

load_kbd(){
# Load the correct keyboard configuration from /etc/default/keyboard
KBD_MODEL="$(grep XKBMODEL /etc/default/keyboard | cut -d\" -f 2)"
KBD_LAYOUT="$(grep XKBLAYOUT /etc/default/keyboard | cut -d\" -f 2)"
KBD_VARIANT="$(grep XKBVARIANT /etc/default/keyboard | cut -d\" -f 2)"
KBD_OPTIONS="$(grep XKBOPTIONS /etc/default/keyboard | cut -d\" -f 2)"
[ "$KBD_MODEL" ] && KBD_MODEL="-model $KBD_MODEL"
[ "$KBD_LAYOUT" ] && KBD_LAYOUT="-layout $KBD_LAYOUT"
[ "$KBD_VARIANT" ] && KBD_VARIANT="-variant $KBD_VARIANT"
if [ "$KBD_OPTIONS" ]; then
  if [ -z "$(echo "$KBD_OPTIONS" | grep ",")" ]; then
    KBD_OPTIONS="-option $KBD_OPTIONS"
  else
    KBD_OPTIONS="$(echo "$KBD_OPTIONS" | sed 's/^/-option /;s/,/ -option /g')"
  fi
fi
[ "$KBD_MODEL" ] && [ "$KBD_LAYOUT" ] && setxkbmap ${KBD_MODEL} ${KBD_LAYOUT} ${KBD_VRIANT} ${KBD_OPTIONS}
}

if [ "$1" = "i" ] && [ ! -e /lib/live/mount/rootfs/filesystem.squashfs ] ; then
  page='install-warning'
elif [ "$1" = "install" ] && [ ! -e /lib/live/mount/rootfs/filesystem.squashfs ] ; then
  page='install-warning'
else
  page="$1"  
fi

case "$page" in
  'a'|'about')     option='about'
                   color_theme_default
                   tab_active_about='style="border-color: #fec30c"'
                   bot_btn='<br><br><br><br>__________________________________________________________________________________________________________________<br><br><a href="about_about" class="bl" >About this application</a><br><br>'
                   unset tab_active_readme
                   unset tab_active_welcome
                   unset tab_active_install
                   ;;
  'r'|'readme')    option='readme'
                   color_theme_default
                   tab_active_readme='style="border-color: #fec30c"'
                   bot_btn='爪丨匚卄卂乇ㄥ<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
                           <details>
                           <summary><span class="big" style="font-size:50%;">Do not click this</span></summary>
                           <a href="v3" class="smolbtn">distraction</a>
                           </details>
                           <br><br>'
                   unset tab_active_welcome
                   unset tab_active_about
                   unset tab_active_install
                   ;;
  'i'|'install'|'no-warn')
                   option='install'
                   color_theme_default
                   tab_active_install='style="border-color: #fec30c"'
                   unset tab_active_welcome
                   unset tab_active_about
                   unset tab_active_readme
                   ;;
  'install-warning') 
                   option='install-warning'
                   color_theme_alert
                   unset tab_active_welcome
                   unset tab_active_about
                   unset tab_active_readme
                   ;;
  'p'|'post-install')
                   option='post-install'
                   color_theme_default
                   unset tab_active_welcome
                   unset tab_active_about
                   unset tab_active_readme
                   unset tab_active_install
                   ;;
  'w'|'welcome'|*) option='welcome'
                   color_theme_default
                   tab_active_welcome='style="border-color: #fec30c"'
                   #bot_btn='<table style="width:100%;font-size: 100%;"><tr> <th style="width:10%;font-size: 78%"><a href="autorun" style="color: #11E3BE;border-style:none">卂ㄩㄒㄖ尺ㄩ几'"$autorun_icon"'</a> </th> <th style="width:70%"><a href="refresh" class="button btn" >Refresh<br>this data</a></th> <th style="width:15%"></th> </tr></table>'
                   bot_btn='<table style="width:100%;font-size: 100%;"><tr> <th style="width:10%;font-size: 78%"></th> <th style="width:70%"><a href="refresh" class="button btn" >Refresh<br>this data</a></th> <th style="width:15%"></th> </tr></table>'
                   unset tab_active_about
                   unset tab_active_readme
                   unset tab_active_install
                   ;;
esac

gen_sys_info() {
  kbd_layout="$(awk -F\" '/XKBLAYOUT/ {print $2}' /etc/default/keyboard)"
  screen_res="$(xrandr | awk '/*/ {print $1}')"
  master_vol="$(amixer sget Master | grep -P -o "1?[0-9]?[0-9]%")"
  locale="$(awk -F '=' '{print $2}' /etc/default/locale | cut -d\. -f1)"
  timezone="$(cat /etc/timezone)"
}
gen_sys_info

firmware_dir="/firmware"
btn_style='style="font-size: 100%; width: 200px; background-color: $clr3; color: $clr4; text-align: center;"'
btn_style_smol='style="font-size: 100%; width: 100px; background-color: $clr3; color: $clr4; text-align: center;"'
btn_set_kbd="<a href='kbd' class='button' $btn_style>$kbd_layout</a>"
btn_set_loc="<a href='lang' class='button' $btn_style>$locale</a>"
btn_set_tz="<a href='tz' class='button' $btn_style>$timezone</a>"
btn_set_res="<a href='res' class='button' $btn_style_smol>$screen_res</a>"
btn_set_bright="<a href='bright' class='button' $btn_style_smol>brightness</a>"
btn_set_vol="<a href='vol' class='button' $btn_style>$master_vol</a>"
btn_set_mrr='<a href="mirrorlist" class="button" style="font-size: 100%; width: 200px; background-color: $clr3; color: $clr4; text-align: center; ">/etc/apt/sources.list</a>'
head_msg=""

gen_net_info() {
printf '<br><table style="text-align: left;">'
for nic in $(ip a | awk -F ': ' '/^[0-9]/ && $2 != "lo" && !/wwan/ {print $2}'); do
  if echo $nic | grep -q -E "wlp|wlan"; then
    btn_set_net=$(cat <<EOF
<a href="$nic" class="button" $btn_style_smol>set</a></td><td style="widht: 10px; margin: 1px;">
<a href="wifimenu" class="button" $btn_style_smol >TUI</a>
EOF
)
  else
    btn_set_net=$(cat <<EOF
<a href="$nic" class="button" $btn_style_smol >set</a></td><td style="widht: 10px; margin: 1px;">
<a href="ceni" class="button" $btn_style_smol >TUI</a>
EOF
)
  fi 
  if ip a s "$nic" | grep -q -m1 -Eo 'inet ([0-9]*\.){3}[0-9]*'; then
    ip="$(ip a s "$nic" | grep -m1 -Eo 'inet ([0-9]*\.){3}[0-9]*' | cut -d ' '  -f2)"
  else
    ip="0.0.0.0"
  fi
  printf "<tr> <td style=\"width: 280px\">%s</td> <td style=\"width: 280px\">%s</td> <td style=\"width: 180px\">%s</td> <td></td> </tr>" "$nic" "$ip" "$btn_set_net"
done
printf "</table>"
}

if [ -e /lib/live/mount/rootfs/filesystem.squashfs ]; then
  html_buttons=$(cat <<EOF
<a href="welcome" class="button btn" ${tab_active_welcome}>Welcome</a>
<a href="about"   class="button btn" ${tab_active_about}>About</a>
<a href="readme"  class="button btn" ${tab_active_readme}>Readme</a>
<a href="install" class="button btn" ${tab_active_install}>Install</a>
EOF
)
else
  html_buttons=$(cat <<EOF
<a href="welcome" class="button btn" ${tab_active_welcome}>Welcome</a>
<a href="about"   class="button btn" ${tab_active_about}>About</a>
<a href="readme"  class="button btn" ${tab_active_readme}>Readme</a>
<a href="post-install" class="button btn" ${tab_active_install}>Post-Install</a>
EOF
)
fi

launch_parser(){
while [ "$#" -gt 0 ]; do
  echo "<a href=\"$1\">"$1"</a>"
  shift
done
}

firmware_table(){
wifi_cards="$(lspci | grep -iE "network|wifi" | grep -iv "ethernet" | cut -d\: -f3-)"
if [ -d "$firmware_dir" ]; then
  printf "<details>"
  printf "<summary><span class=\"big\"> Firmware</span></summary>"
  if [ "$wifi_cards" ]; then
    printf "<p>This machine is rocking:<br>$wifi_cards</p>"
  fi
  printf "<p>Click on a package to install<br>"
  printf "<table style=\" border: 1px solid; border-collapse: collapse;text-align: left;\">"
  printf "<th style=\" border: 1px solid; border-collapse: collapse;text-align: left;\">pkg</th> <th>status</th>"
#<tr> <td></td> <td></td> </tr>

  for pkg in $(find "$firmware_dir" -not  \( -name "firmware" \) -printf %f\\n | sort | awk -F '_' '{print $1}'); do
    printf "<tr > <td style=\" border: 1px solid; border-collapse: collapse;text-align: left;\"><a href=\"pkg_%s\" class=\"smolbtn\">%s</a></td>" "$pkg" "$pkg"
    if [ "$(dpkg -L "$pkg" 2>/dev/null)" ]; then
      printf "<td>installed</td>"
    else
      printf "<td></td>"
    fi
    printf "</tr>"
  done

  printf "</table></p></details>"
fi
}

gen_html_body(){
default_wm="$(awk -F'=' '/default_start=/{print $2}' "$SCRIPTS/startx.sh" | tr -d '[:punct:]')"
case $option in
  'post-install') body=$(cat <<EOF
<h1>Post-Install</h1>
<div><br><br>
<p>
<table style="width: 40%; text-align: center;">
<tr><td>Default WM</td>
<td><a href='set_wm' class='button' $btn_style>$default_wm</a></td></tr>
<tr><td>Startup Sound</td>
<td><a href='set_sound' class='button' $btn_style>Sound</a></td></tr>
<tr><td>Wallpaper</td>
<td><a href='wallp' class='button' $btn_style>Wallpaper</a></td></tr>
<tr><td>Time</td>
<td><a href='set_time' class='button' $btn_style>$(date +%H:%M)</a></td></tr>
<tr><td>Date</td>
<td><a href='set_date' class='button' $btn_style>$(date +"%d %b %Y")</a></td></tr>
</table>
</p>
</body>
EOF
);;
  'about') body=$(cat <<EOF
<h1>About!</h1>
<div><br><br>
<p>Forge Version 2024.03<br>
Code Name: Cold Penguin
</p>
$svg_line
<p><a href="https://dev1galaxy.org/viewtopic.php?id=4579">forge</a> started 2019 as a fork of an <b><i>experimental</b> <u>no dbus</u> release</i> of <a href="https://www.ibiblio.org/refracta/">refracta</a></p>
<p>which is build on <a href="https://www.devuan.org/os/explore">devuan</a>, a <u>systemd free</u> fork of <a href="https://www.debian.org/intro/why_debian">debian</a></p>
<p>special thanks also to <a href="https://www.gnu.org/">gnu</a> and <a href="https://www.kernel.org/">linux</a></p><br>
$svg_line
<p>this is my personal take on a free operating system,<br>
it includes a very wide range of software<br>
and everything is set up and ready to go</p>
<p>i made these iso's mainly for myself,<br>
because i wanted something that i can quickly deploy across all my old computers</p>
<p>i made this available, in case someone else might enjoy it</p>
<p>maybe you can learn a thing or two (i certainly did)</p>
$svg_line
<p>you will not be able to install applications that depend on dbus<br>
so no kde, no gnome, no xfce, no lxde, no tde etc. => <b>window manager only!</b> <br>
<b>most stuff works just fine</b><br>
also <u>no pulseaudio</u> and <u>no avahi</u>, sorry Leonard</p>
$svg_line
<p>this system is <b>experimental</b><br><br>
<b><u>no warranty</u></b> no nothing!<br>
i am not responsible for any damages or data loss<br>
that you may or may not experience by using or not using this software<br>
this is a independent hobbiest project, i am not a professinal programmer<br>
</p>
$svg_line
<p><table style="width: 60%; text-align: center;">
<tr> <td><b>Browsers:</b></td> <td>$(launch_parser qutebrowser) $(launch_parser palemoon) $(launch_parser lynx) ...</td> </tr>
<tr> <td><b>Office:</b></td>   <td>$(launch_parser abiword) $(launch_parser gnumeric) $(launch_parser speedcrunch) $(launch_parser scribus) $(launch_parser sozi) $(launch_parser heimer) ...</td> </tr>
<tr> <td><b>Graphics:</b></td> <td>$(launch_parser azpainter) $(launch_parser gimp) $(launch_parser inkscape) $(launch_parser blender) $(launch_parser darktable) ...</td> </tr>
<tr> <td><b>Audio:</b></td>    <td>$(launch_parser alsa) $(launch_parser jack) $(launch_parser audacious) $(launch_parser deadbeef) $(launch_parser ardour) $(launch_parser carla) $(launch_parser cardinal) ...</td> </tr>
<tr> <td><b>Video:</b></td>    <td>$(launch_parser mpv) $(launch_parser smplayer) $(launch_parser ffmpeg) $(launch_parser shotcut) $(launch_parser obs) $(launch_parser handbrake) $(launch_parser kodi) ...</td> </tr>
<tr> <td><b>Emulation:</b></td><td>$(launch_parser qemu) $(launch_parser dosbox-x) $(launch_parser dolphin) $(launch_parser bsnes+) $(launch_parser mupuen64+) $(launch_parser retroarch) ...</td> </tr>
<tr> <td><b>System:</b></td>   <td>$(launch_parser pcmanfm), $(launch_parser featherpad), $(launch_parser alacritty), $(launch_parser i3), $(launch_parser jwm), $(launch_parser openrc) ...</td> </tr>
<tr> <td></td> <td><u><b>... and a lot more!</b></u></td> </tr> </table></p>
$svg_line
<h2>Special Thanks to</h2>
<br>
<b>fsmithred</b> for the refracta<br><br>
<b>the Devuan Team</b> for devuan<br><br>
<b>Larry De Coste</b> for feedback and testing<br><br>
<b>Florian Bruhin</b> for qutebrowser<br><br>
<b>Hong Jen Yee</b> for pcmanfm<br><br>
<b>Tsu Jan</b> for featherpad, qps and kvantum<br><br>
<b>Joe Wingbermuehle</b> for jwm<br><br>
<b>Michael Stapelberg</b> for i3<br><br>
<b>Bastien Dejean</b> for bspwm, sxhkd and xdo<br><br>
<b>Hui-Jun Chen</b> for fm.awk, wich is also the base of awk_menu<br><br>
<b>Calvin Kent McNabb</b> for JWMKit, the basis of my wallpaper application<br><br>
<b>Azel</b>, honorable motif programmer, for azpainter <br><br>
<b>falkTX</b> and <b>kx.studio</b> for all the amazing audio software<br><br>
<b>Rui Nuno Capela</b> for qtractor and his midi tool<br><br>
<b>Fabrice Bellard</b> for qemu and ffmpeg<br><br>
<a href="https://freesound.org/">freesound.org</a> for free sound files<br><br>
<b>all the other developers</b> that i cannot name all here,<br>
 who made all this software available<br><br>
the <a href="https://dev1galaxy.org/">dev1galaxy forum</a> and <a href="https://sourceforge.net/projects/forge-os/">sourceforge</a><br>
and everyone who helped making this possible<br><br></p>
</div>
<br><br>
爪丨匚卄卂乇ㄥ
</body>
EOF
);;

  'readme') body=$(cat <<EOF
  <h1>Readme!</h1><br><br><br>

<details>
<summary><span class="big">News</span></summary>

<p style="text-align: left; width: 58%;">
This is the last release!<br>
While i am discontinueing this project,<br>
i will probably still work on some of the scripts,<br>
but there will be no more isos.
</p>

<p style="text-align: left; width: 58%;">
The last iso was pretty rough, to say the least.<br>
I really hope that i could fix most issues this time.<br>
Since i knew this would be the final version,<br>
i wanted to deliver a little more than just a quick hotfix.<br><br>
I got extra hacky this time,<br>
and gave i3 and bspwm a major revamp.<br>
No more vertical bars! (but you can get it back with mod+F7)<br>
Conky is the new kid in town, and bspwm also has tint2!<br>
Please notice that it can appear as if bspwm is frozen,<br>
if it starts or reloads. Just click on the desktop!<br>
In i3 it can happen that conky gets focus afer a reload,<br>
just click any other window. (also mod+F4 to hide conky)
</p>

<p style="text-align: left; width: 58%;">
The Install button above will turn into Post-Install<br>
and provide some basic options.
</p>

<p style="text-align: left; width: 58%;">
Also i made some less anoying wallpapers<br>
and installed Firefox as a third browser.
<p>
<br>
$svg_line
</details>

<details>
  <summary><span class="big">Notes</span></summary>
  <p> You can run this script by pressing <b>Super + F1</b></p>
  <p> Qutebrowser will fail to start if you chaged the username during the installation<br>
  to fix this, run: <b><a href="qutebrowser-reinstall">update_qutebrowser --reinstall</a></b>.<br></p>
  <p>If you want to create your own ISO file of the system, you can use the <b><a href="snapshot">snapshot script</a></b></p>
  <p>For feedback or questions, see this thread at the <a href="https://dev1galaxy.org/viewtopic.php?id=4579">dev1galaxy forum</a></p>
$svg_line
</details>

$(firmware_table)
  
<details>
  <summary><span class="big">Updates</span></summary>
  <p>Before you do any updates, you probably want to modify $btn_set_mrr <br><br>
  Here is the official list of <a href="mirrors" class="smolbtn">devuan mirrors</a><br></p>
  
  <p> To update the system, you can run the command: <b><a href="update_all">update_all</a></b>,<br>
  this will do a regular apt update and also run scripts<br>
  to upgrade some software that is not in the official repository<br><br>
  <b><a href="Somepackages">Some packages</a></b> are build from source, they will not get updates<br>
  unless you rebuild them. There is a <b><a href="build-recipes">build-recipes</a></b> directory<br>
  inside $SCRIPTS<br>
  (not all recipes are actually installed, <br>
  and some are probably pretty dated <br>
  but the essential stuff works)<br>
  this should help you to rebuild said packages<br></p>
  <p>you could even go ahead and build proprietary stuff <br>like google-earth and a really dated makemkv alpha<br>
  just dont blindly run random scrpts that you dont need</p>
  <p>eventually i will post updates for scripts and configs at <a href="https://gitlab.com/_alphalpha_/forge-dotfiles">gitlab</a></p>
$svg_line
</details>

<details>
<summary><span class="big">the Xorg situation</span></summary>
<p>since devuan 5 daedalus, the xserver is inderectly depending on dbus<br>
my solution was to pin certain xorg packages to chimaera<br>
this means these packages will not get any updates</p>
<p>it may be possible to set the mirrors back to chimaera and just update affected packages<br>
maybe the nix package manager can be a solution for this</p>
$svg_line
</details>

<details>
  <summary><span class="big">Firewall</span></summary>
  <p> The default firewall configuration is set to DROP, only certain ports are allowed.<br>
  To check the firewall settings, see <a href="iptable">/etc/iptables.conf</a> or run <b>sudo iptables --list</b>.<br>
  To apply changes run 'sudo iptables-restore < /etc/iptables.conf'.<br><br>
  yes, i still use iptables, i did not have a look at nftables just yet<br>
  feel free to change this to your liking<br></p>
$svg_line
</details>
<br><br><br>

</div>
</body>
EOF
);;

  'install-warning')
  body=$(cat <<EOF
<h1>Warning!</h1>
<div></div>

<div>
<br><br>
<p style="color: $clr2 ; font-size: 200%;">
<b>The script has detected that you are not running a live session.<br>
Do not continue unless you know what you are doing!</b>
</p>
<table><tr> <th></th> <th><a href="back" class="button btn" style="color: $clr2;">Back</a></th> <th><a href="no-warn" class="button btn" style="color: $clr2;">Continue</a></th> </tr></table>
</div>
<br><br>
爪丨匚卄卂乇ㄥ
</body>
EOF
);;

  'install')
  body=$(cat <<EOF
<h1>Install</h1>
<br><br>
<div>
<br>
<details>
<summary><span class="big">Instructions for a simple installation:</span></summary>
create a boot partition (100-200MB)<br>
create a root partition<br>
encrypt root if you want<br>
change username and hostname<br>
adjust swap size to your liking<br>
let it rip<br><br>
if you do not understand an option, leave the default value<br>
</details>
<br>
<details>
<summary><span class="big">Notes:</span></summary>
if you are using i3, press Super+Shift+Space to unfloat this window<br><br>
<br>
if you dont like my custom installer,<br>
you can always fall back to the 
<a href="original-refracta">original refracta installer</a><br><br>
</details>

<br>$svg_line<br><br>
<a href="installer" class="button btn" style="color: $clr2;">installer</a>
<br>

</body>
EOF
);;

  'welcome'|*) body=$(cat <<EOF
<h1>Welcome!</h1>
<br>
<div style="background: $clr1; width: 100%; border: none; border-width: 0px;">
<table style="text-align: left; width: 700px";>
<th width="180px"></th><th width="200px"></th><th width="400px"></th>
<tr height="2px"><td></td><td><b>Keyboard layout</b>:</td><td>$btn_set_kbd</td></tr>
<tr><td></td><td><b>System Language</b>:</td><td>$btn_set_loc</td></tr>
<tr><td></td><td><b>Timezone</b>:</td><td>$btn_set_tz</td></tr>
<tr><td></td><td><b>Screen resolution</b>:</td><td>$btn_set_res $btn_set_bright</td></tr>
<tr><td></td><td><b>Volume</b>:</td><td>$btn_set_vol</td></tr>
</table>
$(gen_net_info)</p>
</div>
</body>
EOF
);;
esac
}

gen_html() {
UI=$(cat <<EOF
<!DOCTYPE html>
<html>
<style>
body {width: 98% ;background-color:$clr1; color:$clr4; text-align: center;}
h1   {color:$clr2; font-family: keine; font-family: Bebas\ Neue; font-size: 180px;; margin: 1px;  line-height: 70px; text-align: center}
h3   {color:$clr2; font-family: basic title font; font-size: 400%;}
p    {text-align:center; color: $clr4; font-family: roboto; font-size: 130%;}
br   {display: block; margin: 5px;}
tr   {border-bottom: 1px solid #ddd;}
td   {height: 1px;}
ul   {list-style-type: disk; list-style-position: inside;}
table	  {width:500px;border-radius: 0px;border-color $clr3;border-width: 1px;border-collapse: separate; border-spacing: 4px;}
a:link    {color:$clr2; background-color:$clr1; text-decoration:none; border-style:solid; border-color: $clr3; border-width: 1px;}
a:hover   {color:$clr2; background-color:$clr1; text-decoration:none; border-color: $clr7; }
a:visited {color:orange ; background-color:transparent; text-decoration:none}
a:active  {color:$clr8; background-color:transparent; text-decoration:bold}
.button   {font-size: 12px; background-color: $clr1; border: none; border-radius: 8px;color: $clr1; text-decoration: none; display: inline-block; margin: 4px 2px; cursor: pointer;}
.inputbox {width:115%; border-radius: 2px;border: none;}
select    {-webkit-appearance: none; -moz-appearance: none; appearance: none; background-color: $clr6; color:$clr4; border-radius: 21px;border: none;}

.big {color: $clr2 ; font-size: 200%;}

.btn {
  border: black;
  padding: 15px 32px;
  text-align: center;
  width: 120px;
  border-style:none;border-radius: 8px;border-collapse: separate; border-spacing: 40px 1px;text-align: center;
  font-size: 16px;
}

.smolbtn {
.button   {
font-size: 12px; background-color: $clr1; border: none; border-radius: 8px;color: $clr1; text-align: center; text-decoration: none; display: inline-block; margin: 4px 2px; cursor: pointer;}
font-size: 100%; width: 270px; 
color: $clr4; background-color:$clr1;
text-align: center;
}
</style> 

<body><center> 
<div style="position: fixed; background: $clr3; opacity: 1; width: 100%; border: none; border-width: 0px; top: 0;  right: 0;  height: 100px;">
<br>
$html_buttons
<br>
<b>$head_msg</b>
</div>
<br><br><br><br><br><br><br><br>
$body 
${bot_btn}
</html>
EOF
)
}

gen_sys_info
gen_net_info
gen_html_body
gen_html

yad_net(){
action="$(sudo --stdin yad --quoted-output --center --borders=30 \
        --text="Interface: $nic" \
        --form \
        --field="UI (Ceni)":btn "$TERMINAL -e /usr/sbin/Ceni" \
        --field="ifdown":btn "sudo ifdown $nic" \
        --field="ifup":btn "sudo ifup $nic" \
        --field="dhcp":btn "$TERMINAL -e sudo dhclient -v -i $nic" \
        --field="config":btn "sudo featherpad /etc/network/interfaces")"
echo "$action"
}


toggle_autorun(){
awk  '
{ if ($0 ~ /yad-welcome.sh/)
  { x = substr($0,1,1)
    if ( x == "#" )
      { x = substr($0,2,length)
      print x}
    else
      {printf "#%s\n",$0}
  }
  else {print}
}' "$autostart_file" > "$autostart_file".tmp \
&& rm "$autostart_file" \
&& mv "$autostart_file".tmp "$autostart_file"
}

kill_pid(){
for pid in $(xdotool search --name "yad-welcome" getwindowpid %@); do
  if ps "$pid" | grep -q 'yad --title=yad-welcome'; then
    kill "$pid"
  fi
done
}

echo "$UI" | stdbuf -eL -oL yad --title="yad-welcome" \
                    --window-icon="/usr/share/icons/Numix-Zafiro-Remix/48/apps/devuan.svg" \
                    --width=976 --height=700 \
                    --html --scroll \
                    --button="Applications:echo apps" \
                    --button="Files:echo files" \
                    --button="Terminal:echo term" \
                    --button="Devices:echo devices" \
                    --button="Processes:echo proc" \
                    --button="About this pc:echo hardw" \
                    --button="Licenses:echo lice" \
                    --button="yad-close" \
                    --print-uri \
| while read -r ans; do
   if echo "$ans" | grep -q -E "^enp|^eth|^wlp|^wlan"; then
     nic=$ans
     ans=net 
   fi
   case "$ans" in
     'autorun') kill_pid; toggle_autorun; exec $0 w ; exit 0;;
     'about') kill_pid; exec $0 a ; exit 0;;
     'readme') kill_pid; exec $0 r ; exit 0;;
     'install') kill_pid; exec $0 i ; exit 0;;
     'installer') alacritty -T 'Installer' -e /usr/bin/zsh -c "xdotool search --name 'Installer' windowsize 90% 90% ; sudo -A $SCRIPTS/refracta-tools.sh -i" & ;;
     'no-warn') kill_pid; exec $0 'no-warn' ; exit 0;;
     'post-install') kill_pid; exec $0 p ; exit 0;;
     'set_wm') "$SCRIPTS/yad/yad-select-wm.sh";;
     'set_sound') "$SCRIPTS/yad/yad-select-sound.sh";;
     'set_time') alacritty -T 'Set Hardware Time' -e sudo "$SCRIPTS/refracta-tools.sh" --set-time;;
     'set_date') alacritty -T 'Set Hardware Time' -e sudo "$SCRIPTS/refracta-tools.sh" --set-date;;
     'original-refracta') alacritty -T 'Installer' -e /usr/bin/zsh -c "xdotool search --name 'Installer' windowsize 90% 90% ; sudo -A refractainstaller" & ;;
     'welcome'|'back') kill_pid; exec $0 w ; exit 0;;
     'refresh') gen_sys_info; gen_net_info
              gen_html_body; gen_html
                kill_pid
                case "$option" in
                  'about')  exec $0 a ; exit 0;;
                  'readme')  exec $0 r ; exit 0;;
                  'welcome')  exec $0 w ; exit 0;;
                  'install') exec $0 i; exit 0;;
                  'post-install') exec $0 p; exit 0;;
                esac;;
     'kbd') alacritty -T 'Terminal Window' -e /usr/bin/zsh -c "sudo dpkg-reconfigure keyboard-configuration"
            load_kbd;;
     'lang') alacritty -T 'Terminal Window' -e /usr/bin/zsh -c "sudo dpkg-reconfigure locales" ;xdotool search --name "Terminal Window" windowactivate %@ windowfocus %@;;
     'tz') alacritty -T 'Terminal Window' -e /usr/bin/zsh -c "sudo dpkg-reconfigure tzdata" ;xdotool search --name "Terminal Window" windowactivate %@ windowfocus %@;;
     'vol') qasmixer &;;
     'res') arandr &;;
     'bright') $SCRIPTS/yad/yad-brightness.sh &;;
     'wifimenu') $TERMINAL -e $SCRIPTS/wifi_menu.sh &;;
     'net') #if echo "$nic" | grep -E "enp|eth"; then
              $SCRIPTS/yad/yad-pw-dialog.sh | yad_net  &
            #elif echo "$nic" | grep -E "wlp|wlan"; then
            # $SCRIPTS/yad/yad-pw-dialog.sh | yad_net &
            #fi
            ;;
     'Somepackages') pcmanfm /usr/local/bin &;;
     'build-recipes') pcmanfm $SCRIPTS/build-recipes &;;
     'ceni') $TERMINAL -e sudo -A /usr/sbin/Ceni ;;
     'deadbeef') "$SCRIPTS/launcher/launch-deadbeef.sh" &;;
     'snapshot') alacritty -T 'Snapshot' -e /usr/bin/zsh -c "xdotool search --name 'Snapshot' windowsize 90% 90% ; sudo -A $SCRIPTS/refracta-tools.sh -i" & ;;
     'wallp') $SCRIPTS/python/xwallpapergui.py &;;
     'apps')  rofi -show drun &;;
     'hardw') hardinfo ; xdotool search --name "System Information" windowactivate %@ windowfocus %@;;
     'devices')alacritty -T 'Terminal Window' -e /usr/bin/zsh -c "$SCRIPTS/mount_menu.sh" ;xdotool search --name "Terminal Window" windowactivate %@ windowfocus %@;;
     'term') alacritty -T 'Terminal Window' -e /usr/bin/zsh -c 'neofetch ; zsh' ;xdotool search --name "Terminal Window" windowactivate %@ windowfocus %@;;
     'files') pcmanfm &;;
     'proc') qps &;;
     'lice') $SCRIPTS/yad/yad-licenses.sh &;;
     'zshrc') featherpad ~/.config/zsh/.zshrc;;
     'bashrc') featherpad ~/.bashrc;;
     'about_about') yad --about &;;
     'mirrors') sudo $SCRIPTS/yad/yad-devuan-mirrors.sh &;;
     'mirrorlist') sudo featherpad /etc/apt/sources.list &;;
     'iptable') sudo featherpad /etc/iptables.conf &;;
     'v3') mpv "https://www.tiktok.com/@stekil_the_cat/video/7118256980141100314" || yad --text="sorry no internet" &;;
     'style') aplay /usr/share/sounds/330563__andre-onate__kotsuzumi-roll-at-126.wav &;;
     'qutebrowser') $SCRIPTS/launcher/launch-qutebrowser.sh &;;
     'qutebrowser-reinstall') $TERMINAL -e /usr/bin/zsh -c "$SCRIPTS/updater/update_qutebrowser.sh --reinstall" &;;
     'update_all') alacritty -T 'Terminal Window' -e /usr/bin/zsh -c '. $SCRIPTS/functions/update_all.func; update_all ; zsh' ;xdotool search --name "Terminal Window" windowactivate %@ windowfocus %@;;
     'palemoon') $SCRIPTS/palemoon.sh &;;
     'firefox') exec firefox-esr &;;
     'lynx') exec alacritty -T 'Terminal Window' -e /usr/bin/zsh -c lynx &;;
     'alsa') exec alsamixer &;;
     'jack') exec qjackctl &;;
     'cardinal');;
     'ffmpeg');;
     'mpv');;
     'smplayer');;
     'shotcut') $SCRIPTS/launcher/launch-shotcut.sh;;
     'qemu') $SCRIPTS/qemu_menu.sh &;;
     'i3'|'jwm'|'openrc'|'mupen64+');;
     'abiword'|'gnumeric'|'speedcrunch'|'scribus'|'sozi'|'heimer'|'azpainter'|'gimp'|'inkscape'|'kodi'|'blender'|'darktable'|'ardour'|'carla'|'obs'|'handbrake'|'dosbox-x'|'dolphin'|'bsnes+'|'pcmanfm'|'featherpad') exec '$ans' &;;
     'show_awk_menu') featherpad $SCRIPTS/functions/awk-menu.func &;;
     'retroarch') $SCRIPTS/launcher/launch-retroarch.sh &;;
            #yad --text="sorry, this button is only there for the audio/visual effect and does not serve any other purpose!" &;;
     pkg_*) pkg="$(find "$firmware_dir" -iname "$(echo "$ans" | sed 's/^pkg_//' | tail -n1)*.deb")"
            if [ -f "$pkg"  ]; then

              if echo "$pkg" | grep "firmware-ralink"; then
                pkg="$(find "$firmware_dir" -iname "firmware-misc-nonfree*.deb" | tail -n1) $pkg"
              fi

              if echo "$pkg" | grep "firmware-misc-nonfree"; then
                pkg="$(find "$firmware_dir" -iname "firmware-linux-nonfree*.deb" | tail -n1) $pkg"
              fi

              xterm -e /usr/bin/zsh -c "sudo dpkg -i $pkg && printf 'Done.\\nYou probably need to reboot.\\n'; zsh" &
            fi;;
     *)  if echo "$ans" | grep -E "(([A-Za-z]{3,9})://)?([-;:&=\+\$,\w]+@{1})?(([-A-Za-z0-9]+\.)+[A-Za-z]{2,3})(:\d+)?((/[-\+~%/\.\w]+)?/?([&?][-\+=&;%@\.\w]+)?(#[\w]+)?)?"; then
           $BROWSER "$ans" &
         else
           echo "$ans"
         fi;;
   esac
done
