#!/bin/sh
# by Michael Thorberger

if [ ! "$(id -u)" -eq "0" ] && [ "$(whoami)" != "root" ]; then
  printf "\\n\\tYou need to be root :)\\n"
  exit 0
fi

IMAGE_EDIT="/usr/share/icons/Numix/24/actions/edit-paste.svg"
IMAGE_OK="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
IMAGE_CANCEL="/usr/share/icons/Numix/24/actions/dialog-close.svg"

sel="$(curl https://pkgmaster.devuan.org/mirror_list.txt \
| awk '/FQDN:/ {print $2};
       /BaseURL:/ {print $2};
       /Bandwidth:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)}; printf "\n"};
       /Rate:/{print $2};
       /Country:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)};printf "\n"};
       /CountryCode:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)}; printf "\n"};
       /Protocols:/{ for(i=2; i<=NF; i++) {printf("%s ", $i)}; printf "\n"};
       /Active:/ {print $2};
       /DNSRR:/ {print $2};
       /DNSRRCC:/ {print $2};' \
| yad --width="$(($(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 1) -70))" \
      --height="$(($(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 2) -70))" \
      --text="<span foreground='#000000'><b><big>Doubleclick on a mirror in your location to add it to the sources list</big></b></span> (/etc/apt/sources.list)" \
      --list --column="FQDN" --column="BaseURL" --column="Bandwidth" \
      --column="Rate" --column="Country" --column="CountryCode" --column="Protocols" \
      --column="Active" --column="DNSRR" --column="DNSRRCC" \
| awk -F '|' '{print $2}'
)"

if [ ! "$sel" ]; then
  yad --text="could not read https://pkgmaster.devuan.org/mirror_list.txt"
  exit 1
fi

opt="$(yad --width=580 --selectable-labels --form --columns=1 \
           --text="<span foreground='#000000'><big><b>$sel</b> \n</big></span> 
<span>The following text will be added to /etc/apt/sources.list</span>

<span>deb https://$sel/merged daedalus main
deb https://$sel/merged daedalus-updates main
deb https://$sel/merged daedalus-security main
deb https://$sel/merged daedalus-backports main
</span>" \
           --column=0 --field="enable contrib":CHK TRUE --field="enable non-free":CHK TRUE  )"

if [ ! "$opt" ]; then
  exit 0
else
  opt1="$(echo "$opt" | cut -d '|' -f 1)"
  if [ "$opt1" = "TRUE" ]; then
    opt1="contrib"
  else
    unset opt1
  fi

  opt2="$(echo "$opt" | cut -d '|' -f 2)"
  if [ "$opt2" = "TRUE" ]; then
    opt2="non-free"
  else
    unset opt2
  fi 

  sudo cat >> "/etc/apt/sources.list" <<EOF | tr -s ' '
# $sel
deb https://$sel/merged daedalus main ${opt1} ${opt2}
deb https://$sel/merged daedalus-updates main ${opt1} ${opt2}
deb https://$sel/merged daedalus-security main ${opt1} ${opt2}
deb https://$sel/merged daedalus-backports main ${opt1} ${opt2}

EOF

  ${GUI_EDITOR} /etc/apt/sources.list
fi

exit 0
