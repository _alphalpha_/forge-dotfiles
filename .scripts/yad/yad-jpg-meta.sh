#! /bin/sh
# view jpg metadata with yad and exiftool
# by Michael Thorberger
# License WTFPL V2 http://www.wtfpl.net/about/

if ! command -v exiftool; then
  printf "Error: could not find exiftool\\n" && exit 1
fi

read screen_width screen_height <<EOF
$(xdpyinfo | awk '/dimensions/{split($2,res,"x"); print res[1],res[2]}')
EOF
yad_width="$((screen_width * 80 / 100))"
yad_height="$((screen_height * 80 / 100))"

[ "$1" ] && IN="$1"
while [ -z "$IN" ] && [ ! "$(file -b --mime-type "$IN")" = "image/jpg" ]; do
  IN="$(yad --title=Select File --width="$yad_width" --height="$yad_height" --file)"
  [ -z "$IN" ] && exit 0
done

exiftool -r "$IN" |\
  yad --title="JPG Metadata" --text-info --width="$yad_width" --height="$yad_height" 
exit 0
