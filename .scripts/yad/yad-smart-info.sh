#!/bin/bash
# by Michael Thorberger

KEY=$RANDOM

unset TL
TN=1
for device in $(sudo smartctl --scan | awk '{print $1}'); do
  A="$(($(sudo smartctl -a "$device" | grep -n 'ID#' | cut -d\: -f1)+1))"
  B="$(sudo smartctl -a "$device" | grep -nE "^[0-9 ]?{2}[0-9]" | tail -n1 | cut -d\: -f1)"
  n=1
  while [ 1 ];do
    n=$((n+1))
    LINE=$((A+n))
    if [ "$(sudo smartctl -a "$device" | awk "NR == $LINE" | grep -E "^[0-9 ]?{2}[0-9]")" ]; then
      B="$(sudo smartctl -a "$device" | awk "NR == $LINE" | grep -nE "^[0-9 ]?{2}[0-9]" | tail -n1 | cut -d\: -f1)"
    else
      B="$LINE"
      break
    fi
  done

MODEL="Model:    $(sudo smartctl -a "$device" | awk '/^Device Model:/ {print}' | cut -d\: -f2)"
SERIAL="Serial Nr:     $(sudo smartctl -a "$device" | awk '/^Serial Number:/ {print $NF}')"
FIRMWARE="Firmware:    $(sudo smartctl -a "$device" | awk '/^Firmware Version:/ {print $NF}')"
CAPACITY="Capacity: $(sudo smartctl -a "$device" | awk '/^User Capacity:/ {print}' | cut -d\: -f2)"
#RESULT="Overall Result: $(sudo smartctl -a "$device" | awk '/^SMART overall-health self-assessment test result:/ {print $NF}')"
TXT="$MODEL\\n$SERIAL\\n$FIRMWARE\\n$CAPACITY"
  sudo smartctl -a "$device" | awk "NR >= $A && NR <= $B"|\
  awk '{printf "%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n%s\n", $1, $2, $3, $4, $5, $6, $7, $8, $9, $10}'|\
    yad --plug=$KEY --tabnum="$TN" --text=$"$TXT" \
        --list --no-selection --column=$"ID#" --column=$"ATTRIBUTE_NAME" --column=$"FLAG" \
        --column=$"VALUE" --column=$"WORST" --column=$"THRESH" --column=$"TYPE"  --column=$"UPDATED"\
        --column=$"WHEN_FAILED"  --column=$"RAW_VALUE" &
  TN=$((TN+1))
  TL="$TL --tab=$device"
done

yad --notebook --width=1000 --height=600 --title=$"System info" --text="<b>S.M.A.R.T Values</b>" --button="yad-close" \
    --key=$KEY $TL --active-tab=${1:-1}

exit 0
