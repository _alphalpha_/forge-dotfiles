#!/bin/sh
icon_ok="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
icon_cancel="/usr/share/icons/Numix/24/actions/dialog-close.svg"

yad_output=$(yad --width 300 --entry --title 'Select WM' \
    --button="OK!$icon-ok:0" --button="Cancel!$icon-cancel:1" \
    --text 'Default start: ' \
    --entry-text 'menu' 'shell' 'JWM' 'i3' 'bspwm' 'NsCDE')
[ -z "$yad_output" ] && exit 0

current_wm="$(grep 'default_start=' "$SCRIPTS/startx.sh")"

case $yad_output in
  'menu')  sed -i "s/$current_wm/default_start='menu'/" "$SCRIPTS/startx.sh";;
  'shell') sed -i "s/$current_wm/default_start='shell'/" "$SCRIPTS/startx.sh";;
  'JWM')   sed -i "s/$current_wm/default_start='JWM'/" "$SCRIPTS/startx.sh";;
  'i3')    sed -i "s/$current_wm/default_start='i3'/" "$SCRIPTS/startx.sh";;
  'bspwm') sed -i "s/$current_wm/default_start='bspwm'/" "$SCRIPTS/startx.sh";;
  'NsCDE') sed -i "s/$current_wm/default_start='NsCDE'/" "$SCRIPTS/startx.sh";;
esac
exit 0
