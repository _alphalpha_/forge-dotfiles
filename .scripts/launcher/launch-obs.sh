#!/bin/sh

GLSL="$(glxinfo | grep 'OpenGL shading language version' | awk '{print $6}')"
if [ "$(echo "$GLSL" | cut -d '.' -f 1)" -gt 3 ] || \
 [ "$(echo "$GLSL" | cut -d '.' -f 1)" -eq 3 ] && [ "$(echo "$GLSL" | cut -d '.' -f 2 | cut -b 1)" -ge 3 ]; then
  obs ${@} &
else
  LIBGL_ALWAYS_SOFTWARE=1 obs ${@} &
fi
exit 0
