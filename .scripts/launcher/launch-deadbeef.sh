#!/bin/sh
DEADBEEF="$HOME/.local/opt/deadbeef/deadbeef"
if [ -f "$1" ]; then
  "$DEADBEEF" --queue "$1"
elif [ "$1" ]; then
  "$DEADBEEF" ${*}
else
  "$DEADBEEF"
fi
exit 0
