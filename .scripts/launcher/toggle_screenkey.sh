#!/bin/sh
screenkey_pid="$(ps -ax | awk '/\/bin\/screenkey/')"
if [ "$screenkey_pid" ]; then
  killall screenkey
else
  screenkey &
fi
exit 0
