#!/bin/sh
BASE_DIR="$HOME/.local/opt/asbru-cm"
APP_IMAGE="$(find "$BASE_DIR" -type f -iname "*.AppImage" -printf "%f\n" | tail -n1)"

if [ -f "$BASE_DIR"/"$APP_IMAGE" ]; then
  if [ ! -x "$BASE_DIR"/"$APP_IMAGE" ]; then
    chmod +x "$BASE_DIR"/"$APP_IMAGE"
  fi
  "$BASE_DIR"/"$APP_IMAGE"
else
  echo "Error, could not find appimage."
  if [ -x "$SCRIPTS"/sounds.sh ]; then
    "$SCRIPTS"/sounds.sh --error
  fi
  exit 1
fi
exit 0

