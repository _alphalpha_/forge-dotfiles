#!/bin/sh
dir="$HOME/.local/opt/RetroArch"
appimage="$(find "$dir" -type f -iname "*.AppImage" -printf "%f\n" | tail -n1)"

if [ -f "$dir"/"$appimage" ]; then
  if [ ! -x "$dir"/"$appimage" ]; then
    chmod +x "$dir"/"$appimage"
  fi
  "$dir"/"$appimage"
else
  echo "Error, could not find appimage."
  if [ -x "$SCRIPTS"/sounds.sh ]; then
    "$SCRIPTS"/sounds.sh --error
  fi
  exit 1
fi
exit 0

