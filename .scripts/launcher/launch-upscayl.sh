#!/bin/sh
BASE_DIR="$HOME/.local/opt/upscayl"
APP_IMAGE="$(find "$BASE_DIR" -type f -iname "*.AppImage" -printf "%f\n" | tail -n1)"

if [ -f "$1" ]; then
  FILE="$1"
fi

if [ -f "$BASE_DIR"/"$APP_IMAGE" ]; then
  "$BASE_DIR"/"$APP_IMAGE" "${FILE}"
else
  echo "Error, could not find appimage."
  if [ -x "$SCRIPTS"/sounds.sh ]; then
    "$SCRIPTS"/sounds.sh --error
  fi
  exit 1
fi
exit 0

