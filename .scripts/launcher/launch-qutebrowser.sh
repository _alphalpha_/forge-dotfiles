#!/bin/sh
STARTPAGE="file:///var/www/html/startpage.html"

check_firejail(){
  if [ -f /usr/bin/firejail ]; then
    sandbox="firejail --machine-id --profile=/etc/firejail/qutebrowser-m.profile"
    if [ ! -f /etc/firejail/qutebrowser-m.profile ]; then
      echo "Could not find /etc/firejail/qutebrowser-m.profile"
      echo "You need to create a firejail profile or use this script with the '--no-firejail' option"
      echo "You can run $SCRIPTS/qutebrowser-firejail-conf.sh with sudo to generate a profile"
      exit 1
    fi
  fi
}

clear_cache(){
  file_list="$(cat <<EOF
$HOME/.cache/qutebrowser
$HOME/.local/share/qutebrowser/sessions
$HOME/.local/share/qutebrowser/webengine
EOF
)"

  for x in $(echo "$file_list" | xargs -n1); do
    if [ -e "$x" ]; then
      rm -rdfv "$x"
    fi
  done
}

launch_qutebrowser(){
# most of this shenanigans is to assosiate the pid of the python process to the new window

# get old pids and window ids
  unset old_qb_pids
  unset old_qb_windows
  if [ "$(ps -aux | awk '/.venv\/bin\/python3 -m qutebrowser/ && !/awk/ &&!/firejail/ {print $2}' \
        | awk 'END{print NR}')" -gt 0 ]; then
    old_qb_pids="$(ps -aux | awk '/.venv\/bin\/python3 -m qutebrowser/ && !/awk/ &&!/firejail/ {print $2}')"
    old_qb_windows="$(wmctrl -l | awk '{if ($NF == "qutebrowser") {print $1}}')"
  fi

# start qutebrowser
  eval "${sandbox} .venv/bin/python3 -m qutebrowser $STARTPAGE" &

# get new window id
# has to sleep until the window has spawned
  qb_window="$(wmctrl -l | awk '{if ($NF == "qutebrowser") {print $1}}')"
  sleep_count="0"
  while [ ! "$qb_window" ] || [ "$qb_window" = "$old_qb_windows" ] && [ "$sleep_count" -le "10" ]; do
    sleep 1 && sleep_count="$((sleep_count+1))"
    qb_window="$(wmctrl -l | awk '{if ($NF == "qutebrowser") {print $1}}')"
  done
  if [ "$old_qb_windows" ]; then
    for w in $(echo "$old_qb_windows" | xargs -n1); do
      qb_window="$(echo "$qb_window" | grep -v "$w")"
    done
  fi

# get new pid
  qb_pid="$(ps -aux | awk '/.venv\/bin\/python3 -m qutebrowser/ && !/awk/ &&!/firejail/ {print $2}')"
  if [ "$old_qb_pids" ]; then
    for p in $(echo "$old_qb_pids" | xargs -n1); do
      qb_pid="$(echo "$qb_pid" | grep -v "$p")"
    done
  fi

# pass pid to window id
  xprop -id "$qb_window" -format QB_PID 8s -set QB_PID "$qb_pid"
}

# NOTE: Script starts here

if [ -d "$HOME/Downloads" ]; then
  mkdir "$HOME/Downloads"
fi

regex_url="(([A-Za-z]{3,9})://)?([-;:&=\+\$,\w]+@{1})?(([-A-Za-z0-9]+\.)+[A-Za-z]{2,3})(:\d+)?((/[-\+~%/\.\w]+)?/?([&?][-\+=&;%@\.\w]+)?(#[\w]+)?)?"

while [ -n "$1" ]; do
  case "$1" in
    --clear)       clear_cache
                   ;;
    --no-firejail) USE_FIREJAIL="0"
                   ;;
    --url|*)       if echo "$1" | grep -E "$regex_url"; then
                     STARTPAGE="$1"
                   fi
                   ;;
  esac
  shift
done

if [ "$USE_FIREJAIL" != "0" ]; then
  check_firejail
fi

launch_qutebrowser
exit 0
