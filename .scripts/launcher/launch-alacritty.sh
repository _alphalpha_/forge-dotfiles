#!/bin/sh

GLSL="$(glxinfo | awk '/OpenGL shading language version/{print $6}')"
if [ "$(echo "$GLSL" | cut -d '.' -f 1)" -gt 3 ] || \
 [ "$(echo "$GLSL" | cut -d '.' -f 1)" -eq 3 ] && [ "$(echo "$GLSL" | cut -d '.' -f 2 | cut -b 1)" -ge 3 ]; then
  alacritty "${@}" &
else
  export LIBGL_ALWAYS_SOFTWARE=1
  alacritty "${@}" &
fi
exit 0
