#!/bin/sh
# connect to wifi networks with a menu
# version 0.6
# by Michael Thorberger
# License WTFPL V2 http://www.wtfpl.net/about/

##########################################################
# options:
save_passwords="1"
# save_passwords can be 1 or 0
# hashed passwords and network information
# will be stored in /etc/wpa_supplicant.conf

wpa_pass_file="/tmp/wpa_wifi.$$"
# wpa_pass_file is used if save_passwords is 0
# this file will store your passwords for wpa_supplicant
# it will get deleted after usage

EDITOR="$EDITOR"
##########################################################
wifi_status="$(wifi | cut -b 13-)"
resolvconf='/etc/resolv.conf'
netifaces='/etc/network/interfaces'
wpaconf='/etc/wpa_supplicant.conf'
if [ ! -f "$wpaconf" ]; then
  sudo touch "$wpaconf"
fi

if [ ! "$EDITOR" ]; then
  EDITOR="nano"
fi

#shellcheck disable=SC1091,SC2034
. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func
. "$SCRIPTS"/functions/text-center.func
. "$SCRIPTS"/functions/colors.func
. "$SCRIPTS"/functions/cursor.func || exit 1

text_box_opt="-c clr4 -t $(echo "${clr7}$0${clr_end}" | sed "s:$HOME:~:") -tpos 10"

#awk_menu_bmsg="Keys: h=help  r=reload "

awk_menu_help="$(text_box $text_box_opt "$(echo "\\nPess h to go back\\n")" )
$(echo "\\n Keys:\\n
   ${clr7}j${clr_end} - ${clr2}move up${clr_end}
   ${clr7}k${clr_end} - ${clr2}move down${clr_end}
   ${clr7}l${clr_end} - ${clr2}mount/unmount${clr_end}

   ${clr7}r${clr_end} - ${clr2}reload${clr_end}
   ${clr7}e${clr_end} - ${clr2}edit $wpaconf${clr_end}
   ${clr7}h${clr_end} - ${clr2}show this help${clr_end}
   ${clr7}/${clr_end} - ${clr2}find string${clr_end}
   ${clr7}q${clr_end} - ${clr2}quit${clr_end}")"

clear

get_iface(){
# find wireless interfaces with the 'ip' command
if [ "$(ip a | awk '/^[0-9]: w/ {print $2}' | wc -l)" = 1 ]; then
  iface="$(ip a | awk '/^[0-9]: w/ {print $2}' | tr -d '[:punct:]')"
elif [ "$(ip a | awk '/^[0-9]: w/ {print $2}' | wc -l)" -gt 1 ]; then
  awk_menu_tmsg="$(text_box $text_box_opt "${clr7}select nic${clr_end}")"
  input_text="${clr4}$(ip a | awk '/^[0-9]: w/ {print $2}' | tr -d '[:punct:]')${clr_end}"
  iface="$(awk_menu "$input_text" | strip_color)"
fi

if [ ! "$iface" ]; then
  printf "%s ERROR no wireless interface found.\\n" "$0"
  exit 0
fi

# bring up wifi interface
sudo ifconfig "$iface" up
}

connection_check(){
  unset net_name
  unset net_ap
  unset net_chan
  unset net_freq
  unset net_prot
  unset net_mode
  if sudo iwgetid "$iface" >/dev/null ; then
    net_name="$(sudo iwgetid -r)"
    net_ap="$(sudo iwgetid -a "$iface" | awk '{for (i = 4; i <= NF; i++){printf "%s ",$i}; printf "\n"}')"
    net_freq="$(sudo iwgetid -f "$iface" | awk -F ':' '{print $2}' | tr -d ' ')"
#    net_chan="$(sudo iwgetid -c "$iface" | awk '{for (i = 2; i <= NF; i++){printf "%s ",$i}; printf "\n"}')"
#    net_prot="$(sudo iwgetid -p "$iface" | awk '{for (i = 2; i <= NF; i++){printf "%s ",$i}; printf "\n"}')"
#    net_mode="$(sudo iwgetid -m "$iface" | awk '{for (i = 2; i <= NF; i++){printf "%s ",$i}; printf "\n"}')"
  fi
}

wifi_toggle(){
  if [ "$wifi_status" = 'on' ]; then
    wifi off
  else
    wifi on
  fi
  wifi_status="$(wifi | cut -b 13-)"
}

connect_wifi(){
  ssid="$(echo "$selection"| awk -F\" '{print $2}')"
  bssid=$(echo "$selection"| grep  -P -o "[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}:[0-9A-F]{2}" | head -n1)


#TEST
echo "net_name: $net_name"
echo "net_ap:   $net_ap"
echo "ssid:     $ssid"
echo "bssid     $bssid"
echo "Press Enter to continue."
read -r a && unset a

  if [ "$save_passwords" = "1" ]; then
    if grep -v '#' "$wpaconf" | grep "$bssid"; then
      echo "already there !"
      sleep 1
      sudo wpa_supplicant -B -i "$iface" -c "$wpaconf"
      sudo dhclient -i "$iface" -v
    else
    printf "Enter password for %s: " "$ssid"
    stty -echo
    read -r psk
    stty echo
    printf "\n"

    sudo wpa_passphrase "$ssid" "$psk" | grep -v '#' \
      | sed "s/ssid/bssid=$bssid\n\tssid/;s/}/\tpriority=7\n}/" >> "$wpaconf"
    fi

    sudo wpa_supplicant -B -i "$iface" -c "$wpaconf"
    sudo dhclient -i "$iface" -v

  elif [ "$save_passwords" = "0" ]; then
    printf "Enter password for %s: " "$ssid"
    stty -echo
    read -r psk
    stty echo
    printf "\n"

    if [ "$(pidof wpa_supplicant)" ]; then
      sudo killall wpa_supplicant
    fi
    wpa_passphrase "$ssid" "$psk" | sed "s/#psk=\"$psk\"/bssid=$bssid/" > "$wpa_pass_file"
    unset psk
    sudo wpa_supplicant -B -i "$iface" -c "$wpa_pass_file"
    sudo dhclient -i "$iface" -v
    rm "$wpa_pass_file"

  else
    echo "error"
    exit 1
  fi
}

get_term_info(){
  term_width="$(tput cols || stty size | cut -d ' ' -f2)"
  term_height="$(tput lines || stty size | cut -d ' ' -f1)"
}

print_scanning(){
  get_term_info
  echo "$head"
  cursor_save
  cursor_move "1" "$((term_height-6))"
  spacing_pre="$((term_width/2-13))"
  spacing_post="$((term_width/2-12+term_width%2))"
  printf "${clr4}${txt_special}"
  printf "\\n%${term_width}s" | sed 's/ /━/g'
  printf "\\n%${spacing_pre}s┏━┓┏━╸┏━┓┏┓╻┏┓╻╻┏┓╻┏━╸   %${spacing_post}s"
  printf "\\n%${spacing_pre}s┗━┓┃  ┣━┫┃┗┫┃┗┫┃┃┗┫┃╺┓   %${spacing_post}s"
  printf "\\n%${spacing_pre}s┗━┛┗━╸╹ ╹╹ ╹╹ ╹╹╹ ╹┗━┛╹╹╹%${spacing_post}s"
  printf "\\n%${term_width}s" | sed 's/ /━/g'
  printf "${clr_end}"
  cursor_restore
}

wifi_menu(){
  case "$wifi_status" in
               'on') w_msg="${clr2}$wifi_status${clr_end}";;
   'off (software)') w_msg="${clr1}$wifi_status${clr_end}";;
                  *) w_msg="${bg_clr1}${clr0}$wifi_status${clr_end}";;
  esac
  connection_check
  if [ "$net_name" ]; then
    head="${clr4}$iface ${clr7}connected:${clr2} $net_name  $net_ap${clr_end}"
  else
    head="${clr7}select network${clr_end}"
  fi
  echo "interface = $iface"
  awk_menu_tmsg="$(text_box $text_box_opt "$head")"
  # start menu
  input_text="  ${clr4}>${clr7} wifi = $w_msg
  ${clr4}>${clr7} select interface [${clr4} ${txt_b}$iface ${clr7}]${clr_end}
  ${clr4}>${clr7} scan
  ${clr4}>${clr7} start wpa_supplicant
  ${clr4}>${clr7} stop wpa_supplicant
  ${clr4}>${clr7} get ip
  ${clr4}>${clr7} edit $wpaconf
  ${clr4}>${clr7} edit $resolvconf
  ${clr4}>${clr7} edit $netifaces
  ${clr4}>${clr7} ${clr1}exit${clr_end}"
  awk_menu -q "$input_text"
   case "$(echo "$awk_menu_output" | strip_color)" in
      "> iface = $iface") get_iface;;
      '> scan') wifi_scan;;
      "> edit $wpaconf") sudo ${EDITOR} "$wpaconf";;
      "> edit $resolvconf") sudo ${EDITOR} "$resolvconf";;
      "> edit $netifaces") sudo ${EDITOR} "$netifaces";;
      '> get ip') sudo dhclient -i "$iface" -v -r && sudo dhclient -i "$iface" -v;;
      '> start wpa_supplicant') sudo wpa_supplicant -B -i "$iface" -c "$wpaconf";;
      '> stop wpa_supplicant') sudo killall wpa_supplicant;;
      "> wifi ="*) wifi_toggle;;
      "> exit") exit 0;;
    esac
}

wifi_scan(){
  print_scanning
  input_text="${clr4}$(sudo iwlist "$iface" scan \
  | awk '/Cell [0-9]|ESSID:|Frequency:|Quality=/ { ORS=" "
    gsub(/Cell [0-9][0-9]?[0-9]? - Address: /,"\n",$0)
    gsub(/^[ \t]+|[ \t]+$|Frequency:|Quality=/,"",$0)
    gsub(/ GH?z? \(Channel [0-9][0-9]?[0-9]?\)/,"GHz",$0)
    gsub(/ Signal level=/,"",$0)
    gsub(/ dBm/,"dB",$0)
    gsub(/ESSID:"/,"\"",$0)
    gsub(/^[ \t]$/,"\"",$0)
    print
    }
    END {printf "\n> Reload\n< Back"}' \
     | grep .)"
  sleep 1
  awk_menu_keys="req"
  awk_menu_tmsg="$(text_box $text_box_opt "$head")"
  awk_menu -q "$input_text"

  if [ ! "$awk_menu_output" ]; then
    exit 0
  else
    selection="$awk_menu_output"
  fi

  if [ "$awk_menu_key_press" ]; then
    case "$awk_menu_key_press" in
      r) wifi_scan;;
      e) sudo ${EDITOR} "$wpaconf";;
      q) exit 0;;
    esac
  elif echo "$awk_menu_output" | grep -q 'GHz'; then
    connect_wifi
    connection_check
  else
    case "$awk_menu_output" in
      "> Reload") wifi_scan;;
    esac
  fi

}

#net_ap
#net_chan
#net_freq
#net_prot
#net_mode
get_iface
while [ "$input_text" != "> Exit" ] || [ "$awk_menu_key_press" = 'q' ] ; do
  wifi_menu
done
exit 0
