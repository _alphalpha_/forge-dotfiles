#!/bin/sh
# https://qutebrowser.org/doc/install.html#tox

# make sure to have the dependencies
# sudo apt install python3-venv asciidoc

. "$SCRIPTS"/functions/colors.func

echo -n "${clr5}Searching updates for qutebrowser...${clr_end} "

# path were qutebrowser will be extracted
qutebrowser_dir="$HOME/.local/opt/qutebrowser"

# qutebrowser start command
QUTEBROWSER_CMD="$HOME/.venv/bin/python3 -m qutebrowser"

git_url="https://github.com/qutebrowser/qutebrowser"

# Connection test
curl --connect-timeout 3 -s "$git_url" 1>/dev/null \
  || { echo "Error: could not connect to $git_url"; exit 1; }

# get the latest release version number from github
latest_version="$(wget --spider "$git_url/releases/latest"  2>&1 \
                | grep -m1 -o -E "$git_url/releases/tag/[vV]?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
                | awk -F '/' '{ printf $NF; exit}')"

install_func() {
  echo "${txt_bold}found new version: ${clr4}$latest_version${txt_normal}"
  [ ! -d "$qutebrowser_dir" ] && mkdir -pv "$qutebrowser_dir"
  wget -c https://github.com/qutebrowser/qutebrowser/archive/"$latest_version".tar.gz
  tar xvzfC "$latest_version".tar.gz "$qutebrowser_dir" --strip-components=1 
  [ -f "$latest_version".tar.gz ] && rm "$latest_version".tar.gz
  python3 "$qutebrowser_dir"/scripts/mkvenv.py
  python3 "$qutebrowser_dir"/scripts/asciidoc2html.py
  if [ ! -f /usr/local/share/icons/qutebrowser.svg ]; then
   gen_icon
  fi
  if [ ! -f /usr/local/share/applications/qutebrowser.desktop ]; then
    sudo echo "[Desktop Entry]
Name=Qutebrowser
Comment=Browse the World Wide Web
GenericName=Web Browser
Exec=$SCRIPTS/launcher/launch-qutebrowser.sh %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=qutebrowser.svg
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;" \
    | sudo tee /usr/local/share/applications/qutebrowser.desktop
  fi
  if [ "$(readlink ~/.config/qutebrowser/plumber.sh)" != "$HOME/.scripts/plumber.sh" ] ; then
    if [ -f "$HOME/.scripts/plumber.sh" ]; then
      if [ -L "$HOME/.config/qutebrowser/plumber.sh" ]; then
        rm "$HOME/.config/qutebrowser/plumber.sh"
      fi
      ln -s "$HOME/.scripts/plumber.sh" "$HOME/.config/qutebrowser/plumber.sh"
    fi
  fi    
}

uninstall_func() {
  [ -d "$HOME/.cache/qutebrowser" ] && rm -rdv "$HOME/.cache/qutebrowser"
  [ -d "$HOME/.cache/pip" ] && rm -rdv "$HOME/.cache/pip"
  [ -d "$HOME/.venv" ] && rm -rdv "$HOME/.venv"
  [ -d "$HOME/.local/opt/qutebrowser" ] && rm -rdv "$HOME/.local/opt/qutebrowser"
  [ -d "$HOME/.local/share/qutebrowser" ] && rm -rdv "$HOME/.local/share/qutebrowser"
}

reinstall_func() {
  [ -d "$HOME/.cache/pip" ] && rm -rdv "$HOME/.cache/pip"
  [ -d "$HOME/.venv" ] && rm -rdv "$HOME/.venv"
  [ -d "$HOME/.local/opt/qutebrowser" ] && rm -rdv "$HOME/.local/opt/qutebrowser"
  [ -d "$HOME/.local/share/qutebrowser/sessions" ] && rm -rdv "$HOME/.local/share/qutebrowser/sessions"
  [ -d "$HOME/.local/share/qutebrowser/webengine" ] && rm -rdv "$HOME/.local/share/qutebrowser/webengine"
  install_func
}

update_func() {
# get the currently installed version number
if [ -f .venv/bin/qutebrowser ]; then
  local_version="$($QUTEBROWSER_CMD -V 2>/dev/null | grep -e ^"qutebrowser v" | cut -d ' ' -f 2 )"
else
  local_version="0.0.0"
fi

# compare version numbers
if [ -z "$local_version" ] || [ -z "$latest_version" ]; then
  echo "${txt_bold}Error. Could not determine the version of qutebrowser.${txt_normal}"
else
  if [ "$local_version" = "$latest_version" ] \
  && [ $(echo "$local_version\\n$latest_version" | sort -V | tail -n1) = "$local_version" ]; then
    echo "${clr2}${txt_bold}$local_version is up to date.${clr_end}"
  elif [ $(echo "$local_version\\n$latest_version" | sort -V | tail -n1) = "$latest_version" ]; then
    install_func
  fi
fi
}

gen_icon(){
icon='<svg height="70mm" viewBox="0 0 70 70" width="70mm" xmlns="http://www.w3.org/2000/svg">
<g stroke-width=".104205"><ellipse cx="34.674946" cy="35.12365" fill="#cee5fd" rx="33.866669" ry="33.866486"/>
<path d="m44.317879 8.8636412-8.93192 5.3272788v13.625225l8.93192-5.32728zm-15.52697 1.1918458c-8.189858-.04496-23.704479 2.320295-23.773202 19.883028-.08336 21.30511 15.8826 16.44101 19.501202 14.29096 6.95768-4.10866 14.12594-8.45549 20.69225-12.30272 3.57588-2.09172 10.27153-3.45348 10.27153 6.37157s-7.27007 12.87145-11.16481 12.66092v-16.59283l-8.93192 5.27152v18.93561c.48109.0987 2.3018.33833 4.8089.3521 8.18987.045 23.7045-2.3203 23.77322-19.88303.0834-21.30511-15.8826-16.44101-19.50122-14.29096-6.95768 4.10867-14.12593 8.4555-20.69223 12.30272-3.575883 2.09172-10.271531 3.45348-10.271531-6.37157s7.270068-12.87145 11.164791-12.66092v16.59283l8.93193-5.27152v-18.935609c-.48108-.09868-2.3018-.338333-4.80891-.352099zm4.80891 31.109498-8.93193 5.32728v13.62522l8.93193-5.32727z" fill="#0a396e"/>
</g>
</svg>'
echo "$icon" | sudo tee /usr/local/share/icons/qutebrowser.svg
}

cur_dir="$(pwd)"
cd "$HOME"

case "$1" in
  --uninstall)	uninstall_func;;
  --reinstall)	reinstall_func;;
  --install)	install_func;;
  --update|*)	update_func;;
esac

cd "$cur_dir"
exit 0
