#!/bin/sh
# script to download / install / update the palemoon browser
# by Michael
# Script Version 3.2

pm_dir="$HOME/.local/opt/palemoon"
tmp_dir="$HOME/.local/tmp"
mirror="eu"        # mirror can be "eu" or "us"
gtk_version="gtk3" # gtk_version can be "gtk2" or "gtk3"
do_not_ask=1       # if 1 then there will be no question before removing old files

#shellcheck disable=SC1091,SC2034
. "$SCRIPTS"/functions/text-center.func || exit 1
. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func || exit 1
. "$SCRIPTS"/functions/colors.func

echo -n "${clr5}Searching updates for Palemoon... ${clr_end}"

# Connection test
curl --connect-timeout 3 -s "https://linux.palemoon.org/" 1>/dev/null \
  || { printf "Error: could not connect to 'https://linux.palemoon.org/'\\n"; exit 1; } 

# get version numbers
url="$(curl "https://repo.palemoon.org/MoonchildProductions/Pale-Moon/tags" -s |  awk -F\" '/MoonchildProductions\/Pale-Moon\/releases\/tag\/.*_Release/{ print $4 ; exit}')"
latest_version="$(echo "$url" | awk -F/ 'gsub("_Release","",$0) {print $NF}')"

if [ -z "$latest_version" ]; then
  echo "${txt_bold}Error. Could not determine the version of palemoon.${txt_normal}"
  exit 1
fi

if [ -x "$pm_dir/palemoon-bin" ]; then
  local_version="$("$pm_dir"/palemoon-bin -v | awk '{print $5}')"
else
  local_version="0"
fi

# compare version numbers
if [ "$local_version" = "$latest_version" ] \
&& [ $(echo "$local_version\\n$latest_version" | sort -V | tail -n1) = "$local_version" ] \
&& [ "$1" != '--reinstall' ] && [ "$1" != '--force' ]; then
  echo "${clr2}${txt_bold}$local_version is up to date.${clr_end}"
  exit 0
elif [ $(echo "$local_version\\n$latest_version" | sort -V | tail -n1) = "$latest_version" ]; then
  echo "${txt_bold}found new version: ${clr4}$latest_version${txt_normal}"

  # Download
  if [ ! -d "$tmp_dir" ]; then
    mkdir -vp "$tmp_dir"
  fi
#  dl_url="$(wget --spider "https://www.palemoon.org/download.php?mirror=$mirror&bits=64&type=linux${gtk_version}" 2>&1 \
#          | awk '/https/ && /\.tar/ {print $3}')"
#  file_name="$(echo "$dl_url" | awk -F '/' '{print $NF}')"

  dl_url="https://www.palemoon.org/download.php?mirror=$mirror&bits=64&type=linux$gtk_version"
  file_name="palemoon-$latest_version.linux-x86_64-$gtk_version.tar.xz"

  echo "${clr5}Downloading ${clr4}${txt_underlined}$dl_url${clr_end}"
  if [ -x "$SCRIPTS"/curlbar.bash ]; then
    "$SCRIPTS"/curlbar.bash -L -o "$tmp_dir/$file_name" -C - "$dl_url"
  else
    curl -L -o "$tmp_dir/$file_name" -C - "$dl_url"
  fi

  # compare checksum
  checksum="$(curl "https://www.palemoon.org/download.shtml" | grep "$file_name" -A1 | awk 'NR==2 {print $1}' | cut -b 1-64)"

  if [ "$checksum" = "$(sha256sum "$tmp_dir/$file_name" | cut -b 1-64)" ]; then
    echo "${txt_bold}checksum OK!${txt_normal}"
  else
    echo "${txt_bold}checksum error!${txt_normal}"
    [ -f "$file_name" ] && rm "$tmp_dir/$file_name"
    exit 1
  fi

  # stop palemoon if it is currently running
  if [ -n "$(pidof palemoon-bin)" ]; then
    awk_menu_tmsg="${clr3}$(text_box "$(text_center "Palemoon appears to be currently running.")")${clr_end}"
    selection="$(awk_menu "$(text_center "shut it down
    exit")")"
    if [ "$selection" = "shut it down" ]; then
      killall palemoon-bin
    else
      echo "exiting."
      exit 0
    fi
  fi

  # Delete old files?
  if [ -d "$pm_dir" ]; then
    if [ "$do_not_ask" = "1" ]; then
      rm -rfd "$pm_dir"
    else
      echo "${txt_bold}$pm_dir already exists, delete? ${clr2}[Y/n]${txt_normal}"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case $ans in
        [Yy]) rm -rdvf "$pm_dir";;
        [Nn]|*) exit 0;;
      esac
    fi
  fi

  # extracting
  echo "Extracting $file_name to $tmp_dir..."
  if [ ! -d "$pm_dir" ]; then
    mkdir -p "$pm_dir"
  fi
  if echo "$file_name" | grep '.tar.bz2$'; then
    tar_opt="xjf"
  elif echo "$file_name" | grep '.tar.xz$'; then
    tar_opt="xJf"
  fi
  if tar -tf "$tmp_dir/$file_name" | grep '^palemoon/'; then
    tar_opt_2="--strip-components=1"
  fi
  tar "$tar_opt" "$tmp_dir/$file_name" "${tar_opt_2}" -C "$pm_dir"

  # create launcher
  if [ ! -f /usr/local/share/applications/palemoon.desktop ]; then
    sudo echo "[Desktop Entry]
Name=PaleMoon
Comment=Browse the World Wide Web
GenericName=Web Browser
Exec=$SCRIPTS/palemoon.sh %u
Terminal=false
X-MultipleArgs=false
Type=Application
Icon=$pm_dir/browser/icons/mozicon128.png
Categories=Network;WebBrowser;
MimeType=text/html;text/xml;application/xhtml+xml;application/xml;application/vnd.mozilla.xul+xml;application/rss+xml;application/rdf+xml;image/gif;image/jpeg;image/png;x-scheme-handler/http;x-scheme-handler/https;" \
    | sudo tee /usr/local/share/applications/palemoon.desktop
  fi

  # cleanup
  if [ -f "$tmp_dir/$file_name" ]; then
    if [ "$do_not_ask" = "1" ]; then
      rm -v "$tmp_dir/$file_name"
    else
      printf "Installation complete. Remove %s? [Y/n] \\n" "$tmp_dir/$file_name"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case $ans in
        [Yy]) rm -v "$tmp_dir/$file_name";;
        [Nn]*) return;;
      esac
      if [ -d "$tmp_dir" ] && [ "$(find "$tmp_dir" -maxdepth 0 -empty)" ]; then
        rmdir "$tmp_dir"
      fi
    fi
  fi

  # generate a config for firejail if it is installed
  if [ -f /usr/bin/firejail ]; then
    sandbox="firejail --machine-id --profile=/etc/firejail/palemoon-bin.profile"
    if [ ! -f /etc/firejail/palemoon-bin.profile ]; then
    sudo echo '
# Firejail profile for palemoon
# This file is overwritten after every install/update

# Persistent local customizations
#include palemoon.local
# Persistent global definitions
include globals.local

noblacklist ${HOME}/.cache/moonchild productions/pale moon/
noblacklist ${HOME}/.cache/
noblacklist ${HOME}/.moonchild productions/pale moon/
noblacklist ${HOME}/.local/opt/palemoon/
noblacklist ${HOME}/.config/palemoon/
noblacklist /var/www/html/startpage.html

mkdir ${HOME}/.cache/moonchild productions/pale moon
mkdir ${HOME}/.moonchild productions

whitelist ${HOME}/.cache/moonchild productions/pale moon/
whitelist ${HOME}/.cache/
whitelist ${HOME}/.moonchild productions/
whitelist ${HOME}/.local/opt/palemoon/
whitelist ${HOME}/.config/palemoon/
whitelist /var/www/html/startpage.html

read-only /var/www/html/startpage.html


# Palemoon can use the full firejail seccomp filter (unlike firefox >= 60)
ignore seccomp.drop
seccomp

#private-bin palemoon
# private-etc must first be enabled in firefox-common.profile
#private-etc palemoon
#private-opt palemoon

# Redirect
#include firefox-common.profile


########################################################

noblacklist ${HOME}/.pki
noblacklist ${HOME}/.local/share/pki

include disable-common.inc
include disable-devel.inc
include disable-interpreters.inc
include disable-programs.inc

mkdir ${HOME}/.pki
mkdir ${HOME}/.local/share/pki
whitelist ${DOWNLOADS}
whitelist ${HOME}/.pki
whitelist ${HOME}/.local/share/pki
include whitelist-common.inc
include whitelist-var-common.inc

apparmor
#caps.drop all
# machine-id breaks pulse audio; it should work fine in setups where sound is not required
#machine-id
netfilter
# Breaks Gnome connector and KDE Connect
# Also seems to break Ubuntu titlebar menu
# Also breaks enigmail apparently?
# During a stream on Plasma it prevents the mechanism to temporarily bypass the power management, i.e. to keep the screen on
# Therefore disable if you use that
nodbus
nodvd
# nogroups breaks sound
#nogroups
nonewprivs
noroot
notv
?BROWSER_DISABLE_U2F: nou2f
protocol unix,inet,inet6,netlink
seccomp.drop @clock,@cpu-emulation,@debug,@module,@obsolete,@raw-io,@reboot,@resources,@swap,acct,add_key,bpf,fanotify_init,io_cancel,io_destroy,io_getevents,io_setup,io_submit,ioprio_set,kcmp,keyctl,mount,name_to_handle_at,nfsservctl,ni_syscall,open_by_handle_at,personality,pivot_root,process_vm_readv,ptrace,remap_file_pages,request_key,setdomainname,sethostname,syslog,umount,umount2,userfaultfd,vhangup,vmsplice
shell none
#disable tracelog, it breaks or causes major issues with many firefox based browsers, see github issue #1930
#tracelog

disable-mnt
private-dev
# private-etc below works fine on most distributions. There are some problems on CentOS.
#private-etc ca-certificates,ssl,machine-id,dconf,selinux,passwd,group,hostname,hosts,localtime,nsswitch.conf,resolv.conf,xdg,gtk-2.0,gtk-3.0,X11,pango,fonts,mime.types,mailcap,asound.conf,pulse,pki,crypto-policies,ld.so.cache
private-tmp

# breaks DRM binaries
#noexec ${HOME}
noexec /tmp
' > /etc/firejail/palemoon-bin.profile || echo "${txt_bold}error: could not create firejail profile.${txt_normal}"
    fi
  fi
fi
exit 0
