#!/bin/sh

#https://github.com/mltframework/shotcut/releases/latest

url="https://github.com/mltframework/shotcut"
AppImageDir="$HOME/.local/opt/shotcut"
if [ ! -d "$AppImageDir" ]; then
  mkdir -vp "$AppImageDir"
fi

. "$SCRIPTS"/functions/colors.func
echo -n "${clr5}Searching updates for Shotcut...${clr_end} "
latest_version="$(wget --spider "$url/releases/latest" 2>&1 \
           | grep -m1 -o -E "$url/releases/tag/v?[0-9]?{3}\.[0-9]?{3}\.[0-9]?{3}" \
           | awk -F '/' '{print $NF}')"
local_file="$(find "$AppImageDir" -type f -iname "*.AppImage" -printf "%f\n" | tail -n 1)"
local_version="$(echo "$local_file" | sed 's/Shotcut-//;s/.AppImage//')"
new_file="Shotcut-$latest_version.AppImage"
dl_url="https://github.com$(curl -s https://github.com/mltframework/shotcut/releases | grep -E -m1 "AppImage" | cut -d\" -f 2)"

if [ "$local_version" = "$latest_version" ]; then
  echo "${clr2}${txt_bold}$local_version is up to date${clr_end}"
else
  echo "${txt_bold}found new version: ${clr4}$latest_version${txt_normal}"
  if [ -x "$SCRIPTS"/curlbar.bash ]; then
    "$SCRIPTS"/curlbar.bash -L -o "$AppImageDir/$new_file" -C - "$dl_url"
  else
    curl -L -o "$AppImageDir/$new_file" -C - "$dl_url"
  fi
  if [ -f "$AppImageDir"/"$local_file" ]; then
    rm "$AppImageDir"/"$local_file"
  fi
fi

if [ ! "$(stat -c '%a' "$AppImageDir/$new_file")" = 755 ]; then
  chmod 755 "$AppImageDir/$new_file"
fi
exit 0
