#!/bin/sh
# script to install/update deadbeef

. "$SCRIPTS"/functions/colors.func

dl_url="$(curl -s 'https://deadbeef.sourceforge.io/download.html' | awk -v FS=\" '/linux/ && /https/ && /bz2/ { print $2; exit }')"
[ -z "$dl_url" ] && exit 1
echo -n "${clr5}Searching updates for deadbeef... ${clr_end}"

# install path
install_path="$HOME/.local/opt"


# get latest version number
latest_version="$(echo -n "$dl_url" | grep -E -o "[0-9]?{3}\.[0-9]?{3}\.[0-9]?{3}" 2>/dev/null| head -n1)"

# get installed version number
if [ -f "$install_path"/deadbeef/deadbeef ]; then
  local_version="$("$install_path"/deadbeef/deadbeef --version 2>/dev/null | awk '{print $2}')"
else
  local_version="0"
fi

# compare versions
if [ "$latest_version" ] && [ "$latest_version" = "$local_version" ]; then
  echo "${clr2}${txt_bold}$local_version is up to date.${clr_end}"
  exit 0
elif [ "$local_version" = "0" ]; then
  echo "${clr4}Downloading $dl_url${clr_end}"
else
  echo "${txt_bold}found new version: ${clr4}$latest_version${txt_normal}"
fi

# download
[ -d "$install_path"/deadbeef/ ] && rm -rdf "$install_path"/deadbeef/
[ -d "$install_path" ] || mkdir -p "$install_path"

if [ -x "$SCRIPTS"/curlbar.bash ]; then
  "$SCRIPTS"/curlbar.bash -L -o "$install_path"/deadbeef-latest.tar.bz2 -C - "$dl_url"
else
  curl -L -o "$install_path"/deadbeef-latest.tar.bz2 -C - "$dl_url"
fi

mkdir "$install_path"/deadbeef/

tar xvjf "$install_path"/deadbeef-latest.tar.bz2 --strip-components 1 -C "$install_path"/deadbeef/

if [ -f "$install_path"/deadbeef-latest.tar.bz2 ]; then
  rm "$install_path"/deadbeef-latest.tar.bz2
fi

exit 0
