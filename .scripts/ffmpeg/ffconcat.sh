#!/bin/sh

ffconcat_help() {
  printf "  this script can combine multiple clips
  of the same codec into an output file
 
  specify n inputs and 1 output
  Example: ffconcat clip1.mkv clip2.mkv clip3.mkv out.mkv
  
  Optional '-an' to disable audio and '-vn' to disable video
  Example: ffconcat -an clip1.mk1 clip2.mkv out.mkv
"
}

if [ ! "$1" ] || [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
  ffconcat_help
  exit 0
fi

output_file="$(echo "$@" | awk '{print $NF}')"
if [ -f "$output_file.list" ]; then
  rm "$output_file.list"
fi

n='0'
while [ "$#" -gt 1 ]; do
  n="$((n+1))"
  case "$1" in
    '-an') a='-an';;
    '-vn') v='-vn';;
        *) if [ -f "$1" ] ; then
             echo "file '$(pwd)/$1'" >> "$output_file.list"
           fi;;
  esac
  shift
done

ffmpeg -f concat -safe 0 -i "$output_file.list"\
       -c copy ${a} ${v} "$output_file"
exit 0
