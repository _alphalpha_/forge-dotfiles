#!/bin/sh
device="/dev/$(lsblk -dno VENDOR,NAME | awk '$1 == "Retrode" { print $2 }')"
mntdir="$(mktemp -d /tmp/retrode.XXXX)"
outdir="$HOME/Retrode-Dump"

dev_check() {
  if [ -b "$device" ]; then
    is_detected=1
  else
    is_detected=0
  fi
}

mnt_check() {
  if [ -z "$(lsblk "$device" -no MOUNTPOINT)" ]; then 
    is_mounted=0
  else
    is_mounted=1
  fi
}

do_mount() {
  if [ "$is_mounted" = "0" ]; then
    printf "mounting %s to %s\\n" "$device" "$mntdir"
    sudo mount "$device" "$mntdir"
  fi
}

do_copy() {
  if [ ! -d "$outdir" ]; then
    mkdir -p "$outdir" || exit 1
  fi
  for f in "$mntdir"/*; do
    if [ ! -f "$outdir"/"$(echo "$f" | awk -F '/' '{print $NF}')" ]; then
      cp -v "$f" "$outdir"
    fi    
  done
}

do_unmount() {
  if [ "$is_mounted" = "1" ]; then
    sudo umount "$device"
  fi
}

mnt_check
dev_check
if [ "$is_detected" = "0" ]; then
  printf "No Retrode device detected.\\n"
  exit 0
else
  printf "Found Retrode: %s\\n" "$device"
fi
if [ "$is_detected" = "1" ] && [ "$is_mounted" = "0" ]; then
  do_mount && do_copy
fi
if [ "$is_detected" = "1" ] && [ "$is_mounted" = "1" ]; then
  do_unmount && rm -rd "$mntdir"
fi
exit 0
