#!/bin/sh
. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func || exit 1

awk_menu_tmsg="$(text_box "Select an option  (to enable/disable a service permanently, use the rc-update command")"
input_text="start a service
stop a service
exit"

option="$1"
if [ -z "$option" ]; then
  option="$(awk_menu "$input_text" | awk '{print $1}')"
fi

case "$option" in
	"start") select_service="$(awk_menu "$(rc-status -s | grep 'stopped' | awk '{print $1}' | sort)\\nExit")"
			 if [ -z "$select_service" ] || [ "$select_service" = "Exit" ]; then
			   exit 0
			 fi
			 sudo service "$select_service" start;;
	 "stop") select_service="$(awk_menu "$(rc-status -s | grep 'started' | awk '{print $1}' | sort)\\nExit")"
             if [ -z "$select_service" ] || [ "$select_service" = "Exit" ]; then
			   exit 0
			 fi
			 sudo service "$select_service" stop;;
	*) exit 0;;
esac
exit 0
