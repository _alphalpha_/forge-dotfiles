#!/usr/bin/python3
import gi, os, subprocess
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, GdkPixbuf
import xml.etree.ElementTree as ET

# XWallpaperGUI - by alphalpha - is a fork of jwmkit_wallpaper from Calvin Kent McNabb's JWM Kit.


# this is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 2,
# as published by the Free Software Foundation.
#
# JWM Kit is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with JWM Kit.  If not, see <https://www.gnu.org/licenses/>.


class MainWindow(Gtk.Window):

    def __init__(self):
        Gtk.Window.__init__(self, title="XWallpaperGUI")
        self.home = os.path.expanduser('~')
        self.set_border_width(15)
        self.Image_dir = "/usr/share/backgrounds"
        self.Folder_list = os.listdir(self.Image_dir)
        self.Image_folder = []
        self.make_imagelist(self.Folder_list)
        self.display_mode = "stretch"
        self.selected_img, self.image, self.pixs = None, None, None
        self.mainbox = Gtk.Grid()
        self.mainbox.set_row_spacing(12)
        self.add(self.mainbox)
        self.scroll_window = Gtk.ScrolledWindow()
        self.scroll_window.set_min_content_width(800)
        self.scroll_window.set_min_content_height(450)
        self.box1 = Gtk.Grid()
        self.mainbox.attach(self.scroll_window, 1, 1, 1, 1)
        self.mainbox.attach(self.box1, 1, 2, 1 ,1)
        self.image_box = Gtk.Grid()
        self.scroll_window.add(self.image_box)
        self.status_image = Gtk.Image()
        self.status_button = Gtk.ModelButton(image=self.status_image)
        self.status_button.set_property("height-request", 1)
        self.box1.attach(self.status_button, 1, 7, 1, 1)

        self.folder_button = Gtk.Button(label="Browse", image=Gtk.Image(stock=Gtk.STOCK_DIRECTORY))
        self.folder_button.set_property("width-request", 120)
        self.folder_button.connect("clicked", self.pick_folder)
        self.folder_button.set_border_width(2)
        self.box1.attach(self.folder_button, 8, 8, 1, 1)

        self.scale = Gtk.RadioButton.new_with_label_from_widget(None, "Zoom")
        self.scale.set_border_width(1)
        self.scale.connect("toggled", self.on_button_toggled, "zoom")
        self.box1.attach(self.scale, 4, 7, 1, 1)

        self.stretch = Gtk.RadioButton.new_with_label_from_widget(self.scale, "Center")
        self.stretch.set_border_width(1)
        self.stretch.connect("toggled", self.on_button_toggled, "center")
        self.box1.attach(self.stretch, 5, 7, 1, 1)

        self.stretch = Gtk.RadioButton.new_with_label_from_widget(self.scale, "Stretch")
        self.stretch.set_border_width(1)
        self.stretch.connect("toggled", self.on_button_toggled, "stretch")
        self.box1.attach(self.stretch, 6, 7, 1, 1)

        self.tiled = Gtk.RadioButton.new_with_label_from_widget(self.scale, "Tiled")
        self.tiled.set_border_width(1)
        self.tiled.connect("toggled", self.on_button_toggled, "tiled")
        self.box1.attach(self.tiled, 7, 7, 1, 1)

        self.apply_button = Gtk.Button(label="Apply", image=Gtk.Image(stock=Gtk.STOCK_APPLY))
        self.apply_button.set_border_width(2)
        self.apply_button.connect("clicked", self.apply_clicked)
        self.box1.attach(self.apply_button, 7, 8, 1, 1)

        self.cancel_button = Gtk.Button(label="Exit", image=Gtk.Image(stock=Gtk.STOCK_QUIT))
        self.cancel_button.set_border_width(2)
        self.cancel_button.set_property("width-request", 80)
        self.cancel_button.connect("clicked", Gtk.main_quit)
        cancel_info_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        cancel_info_box.pack_start(self.cancel_button, True, True, 0)
        self.box1.attach(cancel_info_box, 10, 8, 1, 1)

        self.display_image_butt()

    def display_image_butt(self, imagey=0, imagex=0):
        for imag in self.Image_folder:
            self.image = Gtk.Image()

            try:
                pix = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.Image_dir + "/" + imag, 250, -1,
                                                          preserve_aspect_ratio=True)
                self.image.set_from_pixbuf(pix)
                self.image.set_size_request(260,160)
                img_button = Gtk.ModelButton(image=self.image)
                img_button.connect("clicked", self.img_clicked, imag)
                self.image_box.attach(img_button, imagey, int(imagex / 3), 1, 1)
                self.image_box.show_all()
                imagey += 1
                imagex += 1
                if imagey == 3:
                    imagey = 0
            except gi.repository.GLib.Error:
                print(imag + ' is corrupt or unsupported file.')

    def img_clicked(self, widget, imag):
        if self.display_mode not in ("gradient", "solid"):
            self.selected_img = imag
            self.pixs = GdkPixbuf.Pixbuf.new_from_file_at_scale(self.Image_dir + "/" + self.selected_img, 110, 85,
                                                                preserve_aspect_ratio=True)
            self.status_image.set_from_pixbuf(self.pixs)

    def on_button_toggled(self, button, name):
        self.display_mode = name
        self.status_image.set_from_pixbuf(self.pixs)
        if name == "center":
            self.display_mode = "--maximize"
        elif name == "tiled":
            self.display_mode = "--tiled"        
        elif name == "zoom":
            self.display_mode = "--zoom"
        elif name == "stretch":
            self.display_mode = "--stretch"
            
    def apply_clicked(self, widget):
       cmd = os.path.expanduser(".scripts/wallpaper.sh"), '/'.join([self.Image_dir, self.selected_img]), ' '.join([self.display_mode])
       subprocess.run(cmd)


    def pick_folder(self, widget):
        dialog = Gtk.FileChooserDialog(title="Folder Selection", parent=self, action=Gtk.FileChooserAction.SELECT_FOLDER)
        dialog.add_buttons(Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OPEN, Gtk.ResponseType.OK)
        dialog.set_current_folder(self.home)
        dialog.set_show_hidden(False)
        dialog.set_default_size(650, 400)
        response = dialog.run()
        if response == Gtk.ResponseType.OK:
            self.image_box.remove_column(0)
            self.image_box.remove_column(0)
            self.image_box.remove_column(0)
            self.Image_dir = dialog.get_filename()
            self.Folder_list = os.listdir(self.Image_dir)
            self.make_imagelist(self.Folder_list)
            self.status_image.set_from_pixbuf()
            self.display_image_butt()
        dialog.destroy()

    def make_imagelist(self, image_dir):
        self.Image_folder = []
        for image in image_dir:
            image_types = ('jpg', 'png', 'gif', 'jpeg', 'xpm', 'bmp')
            for im_type in image_types:
                if image.lower().endswith(im_type):
                    self.Image_folder.append(image)

window = MainWindow()
window.connect("delete-event", Gtk.main_quit)
window.set_position(Gtk.WindowPosition.CENTER)
window.set_resizable(False)
window.show_all()
Gtk.main()
