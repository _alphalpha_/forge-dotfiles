#!/bin/sh

FIRST_LINE=$(($(sudo dmidecode | grep 'Handle 0x0000' -n | cut -d ':' -f 1)+1))
LAST_LINE=$(($(sudo dmidecode | grep 'Handle 0x0001' -n | cut -d ':' -f 1)-2))
sudo dmidecode | sed -n "$FIRST_LINE,$LAST_LINE"p
