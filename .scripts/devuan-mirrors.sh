#!/bin/sh
. "$SCRIPTS"/functions/awk-menu.func || exit 1

country="$(awk_menu "$(curl https://pkgmaster.devuan.org/mirror_list.txt \
| awk '{if(/Country:/) print $0}' | sort -u )")"

awk_menu "$(curl https://pkgmaster.devuan.org/mirror_list.txt \
| awk -v country="$country" '{if(/\n/ && /HTTPS/ && $0~country) printf "\n%s",$0}' RS="" \
| awk '{ gsub(/FQDN:/, "\nFQDN:"); print }'
)"
