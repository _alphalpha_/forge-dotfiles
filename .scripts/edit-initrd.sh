#!/bin/sh
extract_dir="$HOME/.local/tmp/extracted_initrd"

#shellcheck disable=SC1091,SC2034
[ -z "$SCRIPTS" ] && SCRIPTS="$HOME/.scripts"
. "$SCRIPTS"/func_awk-menu.sh
. "$SCRIPTS"/func_text_box.sh
. "$SCRIPTS"/func_text_center.sh

select_initrd() {
awk_menu_tmsg="$(text_box "$(text_center "select file to extract")")"
input_text="$(find /boot -maxdepth 1 -type f -iname "initrd*")
 > Search file (fzf)
 > Exit Script"

selection="$(awk_menu "$input_text")"
case "$selection" in
  "> Search file (fzf)")  selection="$(fzf)";;
  "> Exit Script") exit 0;;
esac
if [ -f "$selection" ]; then
  initrd_file="$selection"
elif [ -z "$selection" ]; then
  exit 0
else
  exit 1
fi
}

extract_initrd() {
if [ -d "$extract_dir" ]; then
  mkdir -vp "$extract_dir"
fi
compression=$(file -L "$initrd_file" | egrep -o 'gzip compressed|XZ compressed|cpio archive')
case "$compression" in
  'gzip compressed') printf "%s is gzip compressed\\n" "$initrd_file"
                     zcat "$initrd_file" | cpio -d -i -m -D "$extract_dir";;
  'XZ compressed') printf "%s is XZ compressed\\n" "$initrd_file"
                   xzcat "$initrd_file" | cpio -d -i -m -D "$extract_dir";;
  'cpio archive') printf "%s is a cpio archive\\n" "$initrd_file";;
  *) printf "unknown file type: %s" "$initrd_file"; exit 1;;
esac
}

remove_cryptroot() {
  if [ -f "$extract_dir"/conf/conf.d/cryptroot ]; then
    rm -vf "$extract_dir"/conf/conf.d/cryptroot
  fi
  if [ -f cryptroot ]; then
    rm -vf "$extract_dir"/cryptroot
  fi
}

remove_resume() {
if [ -f "$extract_dir"/conf/conf.d/resume ]; then
  rm -vf "$extract_dir"/conf/conf.d/resume
elif [ -f "$extract_dir"/conf/conf.d/zz-resume-auto ]; then
  rm -vf "$extract_dir"/conf/conf.d/zz-resume-auto
fi
}

rebuild_initrd() {
#if echo "$initrd_file" | grep '/'; then
initrd_name="$(echo "$initrd_file" | awk -F '/' '{print $NF}')"
cur_dir="$(pwd)"
cd "$extract_dir" || exit 1
awk_menu_tmsg="$(text_box "select compression")"
input_text=" > gzip;; > xz;; > cpio;; > Exit Script;;"
selection="$(awk_menu "$input_text")"
case "$selection" in
  "> gzip") find . -print0 | cpio -0 -H newc -o | gzip -c > "$extract_dir/$initrd_name" || exit 1;;
  "> xz") find . | cpio -o -H newc | xz --check=crc32 --x86 --lzma2=dict=512KiB > "$extract_dir/$initrd_name" || exit 1;;
  "> cpio") find . | cpio -o -H newc > "$extract_dir/$initrd_name" || exit 1;;
  "> Exit Script") exit 0;;
  *) if [ -z "$selection" ]; then
       exit 0
     else 
       printf "error"; exit 1
     fi;;
esac

if [ -f "$extract_dir/$initrd_name" ]; then
  awk_menu_tmsg="$(text_box "replace $initrd_file;; with $extract_dir/$initrd_name ?")"
  input_text=" > yes;; > no;; > Exit Script;;"
  selection="$(awk_menu "$input_text")"
  case "$selection" in
    "> yes") mv -v "$initrd_file" "$initrd_file$(date +%Y%m%d%H%M%S)"
             cp -v "$extract_dir/$initrd_name" "$initrd_file";;
    "> no") ;;
    "> Exit Script") exit 0;;
    *) printf "error"; exit 1;;
  esac
fi
}

clean_extract_dir() {
rm -vrd "$extract_dir/*" "$extract_dir"
}

menu1() {
awk_menu_tmsg="$(text_box "$(text_center "files have been to extract to $extract_dir/$initrd_name")")"
input_text="
> remove cryptroot
> remove resume
> rebuild
> clean $extract_dir
> exit script"

selection="$(awk_menu "$input_text")"
case "$selection" in
  "> remove cryptroot") remove_cryptroot; menu1;;
  "> remove resume") remove_resume; menu1;;
  "> rebuild") rebuild_initrd; menu1;;
  "> clean $extract_dir") clean_extract_dir; menu1;;
  "> exit script") exit 0;;
  *) if [ -z "$selection" ]; then
       exit 0
     else 
       printf "error"; exit 1
     fi;;
esac
}

select_initrd
extract_initrd
menu1
exit 0
