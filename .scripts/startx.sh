#!/bin/sh
default_start='menu'
if expr "$(tty)" : "/dev/tty" >>/dev/null; then
  case "$default_start" in
    'i3') export WM='i3'; startx && logout;;
    'JWM') export WM='JWM'; startx && logout;;
    'bspwm') export WM='bspwm'; startx && logout;;
    'NsCDE') export WM='NsCDE'; startx && logout;;
    'shell') ;;
    'menu'|*) main_menu;;
  esac    
fi
