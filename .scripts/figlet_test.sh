#!/bin/sh
if [ "$1" ]; then
  txt="$1"
else
  txt="demo"
fi

term_width="$(tput cols || stty size | cut -d ' ' -f2)"
for f in $(ls -1A /usr/share/figlet/*.*lf | cut -d\/ -f 5 | cut -d\. -f 1); do 
  printf "%s:\\n" "$f"
  figlet -f $f $txt
  printf "%${term_width}s\\n" | sed 's/ /━/g'
done
