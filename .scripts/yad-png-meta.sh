#! /bin/sh

RES_X="$(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 1)"
RES_Y="$(xdpyinfo | awk '/dimension/ {print $2;}' | cut -d 'x' -f 2)"
YAD_WIDTH="$(echo "$RES_X * 0.8" | bc | cut -d '.' -f 1)"
YAD_HEIGHT="$(echo "$RES_Y * 0.8" | bc | cut -d '.' -f 1)"

[ "$1" ] && IN="$1"
while [ -z "$IN" ] && [ ! "$(file -b --mime-type "$IN")" = "image/png" ]; do
  IN="$(yad --title=Select File --width="$YAD_WIDTH" --height="$YAD_HEIGHT" --file)"
  [ -z "$IN" ] && exit 0
  [ ! "$(file -b --mime-type "$IN")" = "image/png" ] && IN=""
done

#pngmeta --all "$IN" |\
exiftool -r "$IN" |\
yad --title="PNG Metadata" --width="$YAD_WIDTH" --height="$YAD_HEIGHT" --text-info
exit 0
