#!/bin/sh

. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func || exit 1

while [ "$#" -gt 0 ]; do
  if [ -f "$1" ]; then
    FILE="$1"
  elif [ "$(lsblk -dnpo NAME | grep -w "$1")" ]; then
    DEV="$1"
  elif [ "$1" = "tc" ] || [ "$1" = "truecrypt" ] || [ "$1" = "tcrypt" ]; then
    TYPE="tcrypt"
  elif [ "$1" = "luks" ]; then
    TYPE="luks"
  fi
  shift
done


if [ -z "$DEV" ]; then
  awk_menu_tmsg="$(text_box "Select a device..."))"
  DEV="$(awk_menu "$(lsblk -nlpo NAME,MOUNTPOINT | awk '{ if ($2 =="") print $1}' \
	| grep -vw "$(lsblk -nlpo PKNAME | xargs -n1)")\\nCancel")"
  if [ "$DEV" = "Cancel" ] || [ -z "$DEV" ]; then
    exit 0
  fi
fi

if [ -z "$TYPE" ]; then
  awk_menu_tmsg="$(text_box "Select an option..."))"
  CRYPT="$(awk_menu "LUKS\\nTrueCrypt\\nVeraCrypt\\nCancel")"
    if [ "$CRYPT" = "Cancel" ] || [ -z "$CRYPT" ]; then
      exit 0
    elif [ "$CRYPT" = "LUKS" ]; then
      TYPE="luks"
    elif [ "$CRYPT" = "TrueCrypt" ] || [ "$CRYPT" = "VeraCrypt" ]; then
      TYPE="tcrypt"
    fi
fi

if [ -z "$FILE" ]; then
  KEY="$(awk_menu "Use Keyfile\\nEnter Key Manually")"
  if [ "$KEY" = "Use Keyfile" ]; then
    printf "Enter path to key file: "
    read -r FILE
    [ -f "$FILE" ] || exit 1
  fi
fi

printf "Enter a name for this device: "
read -r NAME

if [ -f "$FILE" ]; then
  cat "$FILE" | sudo cryptsetup open "$DEV" --type "$TYPE" "$NAME"
else
  sudo cryptsetup open "$DEV" --type "$TYPE" "$NAME"
fi

DIR="$HOME/Media/$NAME"
if [ -d "$DIR" ] && [ ! "$(find "$DIR" -maxdepth 0 -type d -empty)" ]; then
  DIR="$DIR_$(tr -dc A-Za-z0-9 </dev/urandom | head -c 8)"
fi

if [ ! -d "$DIR" ]; then
  mkdir -p "$DIR"
fi

sudo mount /dev/mapper/"$NAME" "$DIR"
exit 0
