#!/bin/sh
# NOTE: for screenshot of an area pass a '-a' flag
filename="$(date +%Y_%m_%d_%H-%M-%S)"
format="png"
quality="98"

if [ "$(xdg-user-dir PICTURES)" != "$HOME" ]; then
  dir="$(xdg-user-dir PICTURES)/Screenshots/"
else
  dir="$HOME/Screenshots/"
fi

if [ ! -d "$dir" ]; then
  mkdir -p "$dir"
fi

if [ -f "${dir}${filename}.${format}" ]; then
  i=0
  oldfilename="$filename"
  while [ -f "${dir}${filename}.${format}" ]; do
    if [ "$i" -lt "1000" ]; then
      i="$((i+1))" 
      i="$(printf "%03d" "$i")"
      filename="$(printf "%s_%s" "$oldfilename" "$i")"
    else
      exit 1
    fi
  done
fi

if [ "$filename" ]; then
  case "$1" in
    '-a'|'--area') scrot --select --line style=solid,width=3,color=red\
                      -z -q "$quality" "${dir}${filename}.${format}";;
                *) scrot -q "$quality" "${dir}${filename}.${format}";;
  esac

  if [ "$format" = 'png' ] && [ -f "${dir}${filename}.${format}" ]; then  
    pngcrush "${dir}${filename}.${format}" "${dir}${filename}-tmp.${format}"\
    && rm "${dir}${filename}.${format}"\
    && mv "${dir}${filename}-tmp.${format}" "${dir}${filename}.${format}"
  fi

  xclip -selection clipboard -target image/"$format" -i "${dir}${filename}.${format}"
fi
exit 0
