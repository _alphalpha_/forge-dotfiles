#!/bin/sh
. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func || exit 1

awk_menu_tmsg="$(text_box "Select an option..."))"
DEV="$(awk_menu "$(lsblk -nlpo TYPE,PATH,MOUNTPOINT  | awk '{ if ($1 =="crypt") if ($3 != "/")  print $2}' | sort -r)")"
if [ -z "$DEV" ] || [ ! -b "$DEV" ]; then
  exit 0
elif [ "$(lsblk -nlo MOUNTPOINT "$DEV")" ]; then
  DIR="$(lsblk -nlo MOUNTPOINT "$DEV")"
  sync
  sudo umount "$DEV"
  if [ "$(echo "$DIR" | grep "^$HOME/Media")" ] && [ "$(find "$DIR" -maxdepth 0 -type d -empty)" ]; then
    rmdir "$DIR"
  fi
else
  exit 1
fi
sudo cryptsetup close "$DEV"
exit 0
