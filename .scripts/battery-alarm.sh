#!/bin/sh
if ! find /sys/class/power_supply/*B* >>/dev/null; then
  exit 0
fi

bat_status="$(acpi -b | grep -E -o "[0-9]?{2}[0-9]%" | tr -d '%')"
if [ "$1" = "--status" ] || [ "$1" = "-s" ]; then
  echo "BATTERY at ${bat_status}%" | dzen2 -p 1 -x 1 -y 1 -h 52 -bg "#DC6434" -fg "#201010" -fn Bebas\ Neue-35 -xs 0 &
  exit 0
fi

battery_status_check() {
bat_status="$(acpi -b | grep -E -o "[0-9]?{2}[0-9]%" | tr -d '%')"
if [ "$bat_status" -lt 10 ]; then
  echo "WARNING: BATTERY BELOW 10%" | dzen2 -p 7 -x 1 -y 1 -h 52 -bg "#DC6434" -fg "#201010" -fn Bebas\ Neue-35 -xs 0 &
  aplay /usr/share/sounds/458570__imagery2__red-alert-nuclear-buzzer.wav
  sleep 1m
elif [ "$bat_status" -lt 20 ]; then
  echo "WARNING: BATTERY BELOW 20%" | dzen2 -p 7 -x 1 -y 1 -h 52 -bg "#DC6434" -fg "#201010" -fn Bebas\ Neue-35 -xs 0 &
  sleep 5m
elif [ "$bat_status" -lt 30 ]; then
  echo "WARNING: BATTERY BELOW 30%" | dzen2 -p 7 -x 1 -y 1 -h 52 -bg "#DC6434" -fg "#201010" -fn Bebas\ Neue-35 -xs 0 &
  sleep 5m
else
  sleep 5m
fi
}

while [ true ]; do
  case $(acpi -b | awk -F'[ ,]' '{print $3}') in
    'Discharging') battery_status_check;;
    'Charging')    sleep 10m;;
    'Full'|*)      sleep 30m;;
  esac
done
exit 0
