#!/bin/sh
# this script is intended to be used with key bindings
# it can store up to 12 windows for quick selection
# for example bind mod+F1-F12 to store a window
# and mod+shift+F1-F12 to focus a stored window
#
# Examples:
#   $SCRIPTS/window_focus.sh set 9
#   will store the currently focused window in slot 9
#  
#   $SCRIPTS/window_focus.sh get 7
#   will bring the window from slot 7 in focus
#
# by Michael Thorberger

if [ "$1" = 'set' ]; then
  active_win="$(xprop -root _NET_ACTIVE_WINDOW | awk '{print $NF}')"
  case "$2" in
    1)  xprop -root -format FOCUS1 8s  -set FOCUS1  "$active_win";;
    2)  xprop -root -format FOCUS2 8s  -set FOCUS2  "$active_win";;
    3)  xprop -root -format FOCUS3 8s  -set FOCUS3  "$active_win";;
    4)  xprop -root -format FOCUS4 8s  -set FOCUS4  "$active_win";;
    5)  xprop -root -format FOCUS5 8s  -set FOCUS5  "$active_win";;
    6)  xprop -root -format FOCUS6 8s  -set FOCUS6  "$active_win";;
    7)  xprop -root -format FOCUS7 8s  -set FOCUS7  "$active_win";;
    8)  xprop -root -format FOCUS8 8s  -set FOCUS8  "$active_win";;
    9)  xprop -root -format FOCUS9 8s  -set FOCUS9  "$active_win";;
    10) xprop -root -format FOCUS10 8s -set FOCUS10 "$active_win";;
    11) xprop -root -format FOCUS11 8s -set FOCUS11 "$active_win";;
    12) xprop -root -format FOCUS12 8s -set FOCUS12 "$active_win";;
  esac
elif [ "$1" = "get" ]; then
  case "$2" in
    1)  win="$(xprop -root FOCUS1  | awk -F '"' '{print $2}')";;
    2)  win="$(xprop -root FOCUS2  | awk -F '"' '{print $2}')";;
    3)  win="$(xprop -root FOCUS3  | awk -F '"' '{print $2}')";;
    4)  win="$(xprop -root FOCUS4  | awk -F '"' '{print $2}')";;
    5)  win="$(xprop -root FOCUS5  | awk -F '"' '{print $2}')";;
    6)  win="$(xprop -root FOCUS6  | awk -F '"' '{print $2}')";;
    7)  win="$(xprop -root FOCUS7  | awk -F '"' '{print $2}')";;
    8)  win="$(xprop -root FOCUS8  | awk -F '"' '{print $2}')";;
    9)  win="$(xprop -root FOCUS9  | awk -F '"' '{print $2}')";;
    10) win="$(xprop -root FOCUS10 | awk -F '"' '{print $2}')";;
    11) win="$(xprop -root FOCUS11 | awk -F '"' '{print $2}')";;
    12) win="$(xprop -root FOCUS12 | awk -F '"' '{print $2}')";;
  esac
  wmctrl -ia "$win" || xdotool windowactivate "$win"
fi

exit 0
