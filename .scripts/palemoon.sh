#!/bin/sh
# script to launch / download / update the palemoon browser
# by Michael Thorberger
# Script Version 3.1

pm_dir="$HOME/.local/opt/palemoon"
pm_profile_dir="$HOME/.config/palemoon"
pm_profile_name="default"
pm_profile="$pm_profile_dir/$pm_profile_name"
startpage="file:///var/www/html/startpage.html"

palemoon_clear () {
# Delete previous cache
[ -d "$HOME/.cache/moonchild productions" ] && rm -rdv "$HOME/.cache/moonchild productions"
[ -d "$HOME/.moonchild productions" ] && rm -rdv "$HOME/.moonchild productions"
[ -d "$HOME/.mozilla" ] && [ ! -f "/usr/bin/firefox" ] && rm -rdv "$HOME/.mozilla"
[ -d "$pm_profile/cache2" ] && rm -rdv "$pm_profile/cache2"
[ -d "$pm_profile/startupCache" ] && rm -rdv "$pm_profile/startupCache"
[ -d "$pm_profile/storage" ] && rm -rdv "$pm_profile/storage"
[ -d "$pm_profile/thumbnails" ] && rm -rdv "$pm_profile/thumbnails"
[ -f "$pm_profile/sessionstore.bak" ] && rm -v "$pm_profile/sessionstore.bak"
[ -f "$pm_profile/sessionstore.js" ] && rm -v "$pm_profile/sessionstore.js"
[ -f "$pm_profile/sessionCheckpoints.json" ] && rm -v "$pm_profile/sessionCheckpoints.json"
[ -f "$pm_profile/sessionCheckpoints.json.tmp" ] && rm -v "$pm_profile/sessionCheckpoints.json.tmp"
[ -f "$pm_profile/places.sqlite-shm" ] && rm -v "$pm_profile/places.sqlite-shm"
[ -f "$pm_profile/places.sqlite-wal" ] && rm -v "$pm_profile/places.sqlite-wal"
[ -f "$pm_profile/places.sqlite.corrupt" ] && rm -v "$pm_profile/places.sqlite.corrupt"
[ -f "$pm_profile/cookies.sqlite" ] && rm -v "$pm_profile/cookies.sqlite"
#for x in $(find "$pm_profile" -iname "*.sqlite" | grep -vw places.sqlite | grep -vw content-prefs.sqlite); do
#  [ -f "$x" ] && rm -v "$x"
#done
}

check_firejail(){
# Check for firejail
if [ -f /usr/bin/firejail ]; then
  sandbox="firejail --machine-id --profile=/etc/firejail/palemoon-bin.profile"
  if [ ! -f /etc/firejail/palemoon-bin.profile ]; then
    printf "Could not find /etc/firejail/palemoon-bin.profile\\n"
    printf "You need to create a firejail profile or use this script with the '--no-firejail' option\\n"
    printf "You can run .local/Scripts/palemoon-firejail-conf.sh with sudo to autogenerate a profile\\n"
    exit 1
  fi
fi
}

palemoon_start () {
if [ "$pm_profile" ]; then
  profile_opt="--profile $pm_profile"
fi

if [ "$startpage" ]; then
  startpage_opt="--url $startpage"
fi


# Start command
${sandbox} "$pm_dir/palemoon-bin" ${pm_opt} ${profile_opt} ${startpage_opt}
# Remove some stuff when palemoon is closed
[ -d "$HOME/.moonchild productions" ] && rm -rd "$HOME/.moonchild productions"
[ -d "$HOME/.mozilla" ] && [ ! -f "/usr/bin/firefox" ] && rm -rd "$HOME/.mozilla"

}

palemoon_update () {
"$SCRIPTS"/updater/update_palemoon.sh
}

# Help function
show_help(){
printf "%s\\n" "$0"
printf "a script to launch / download / update the Palemoon browser\\n"
printf "does also delete all cached file beroe Palemoon is launched\\n"
printf "\\nOptions:\\n"
printf " -c --clear     removes all cached files, does not start Palemoon\\n"
printf " -h --help      shows this help\\n"
printf " -p --profile   sets a profile (default=default) (%s/default)\\n" "$pm_profile_dir"
printf " -r --resume    starts Palemoon without clearing the cache first\\n"
printf " -u --update    searches for the latest version of Palemoon\\n"
printf " -v --version   shows the version of this script\\n"
printf " --no-firejail  starts Palemoon without firejail\\n"
printf " --url          sets the startpage (default=%s)\\n" "$startpage"
printf "\\nExamples:\\n"
printf "palemoon\\n"
printf "palemoon https://gnu.org\\n"
printf "palemoon --profile foo\\n\\n"
}

### Script begins here
if echo "$*" | grep -q "\-y"; then
  do_not_ask="1"
fi
while [ -n "$1" ]; do
  case "$1" in
    --yes|-y|--do-not-ask) do_not_ask="1";;
    --update|-u|--install|-i) palemoon_update; exit 0;;
    --clear|-c) palemoon_clear; exit 0;;
    --resume|-r) CLEAN_START="0";;
    --no-firejail) USE_FIREJAIL="0";;
    --profile|-p) shift; pm_profile_name="$1";;
    --profile-manager|-pm) pm_opt="--ProfileManager"; unset pm_profile_name; unset pm_profile;;
    --help|-h) show_help; exit 0;;
    --version|-v) printf "Palemoon Script Version %s\\n" "$VERSION"; exit 0;;
    --url|*) if echo "$1" | grep -E "(([A-Za-z]{3,9})://)?([-;:&=\+\$,\w]+@{1})?(([-A-Za-z0-9]+\.)+[A-Za-z]{2,3})(:\d+)?((/[-\+~%/\.\w]+)?/?([&?][-\+=&;%@\.\w]+)?(#[\w]+)?)?"; then startpage="$1"; fi;;
  esac
  shift
done

[ -d "$pm_profile" ] || mkdir -p "$pm_profile"
[ "$CLEAN_START" != "0" ] && palemoon_clear
[ "$USE_FIREJAIL" != "0" ] && check_firejail
palemoon_start
exit 0
