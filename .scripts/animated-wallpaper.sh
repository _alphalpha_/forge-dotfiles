#!/bin/sh
f="$(yad --file)"
echo "$f" | grep -qiE ".png$|.jpg" || exit 1
nice -n 19 display -window root -crop 1 $f &
pid="$!"
cpulimit -p=$pid -l 3
