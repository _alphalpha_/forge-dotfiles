#!/bin/sh

IMAGE_OK="/usr/share/icons/Numix/24/actions/dialog-ok.svg"
IMAGE_CANCEL="/usr/share/icons/Numix/24/actions/dialog-close.svg"

yad --width="300" --fixed --center \
  --title="Exit i3" \
  --timeout="5" \
  --buttons-layout="center" \
  --text=' You pressed the exit shortcut. \n Do you really want to exit i3? \n This will end your X session. ' \
  --button="OK!$IMAGE_OK:i3-msg exit" --button="Cancel!$IMAGE_CANCEL:0"
#--text='<span font="Roboto 12"> You pressed the exit shortcut. \n Do you really want to exit i3? \n This will end your X session. </span>' \
exit 0
