#!/bin/sh
# very hacky to do list
# version 0.8
# by Michael Thorberger

#shellcheck disable=SC2034,SC2154,SC1091,SC1001,SC3037
file="$HOME/.todo"
. "$SCRIPTS"/functions/awk-menu.func    || exit 1
. "$SCRIPTS"/functions/colors.func      || exit 1
. "$SCRIPTS"/functions/line-swap.func   || exit 1
. "$SCRIPTS"/functions/line-move.func   || exit 1
. "$SCRIPTS"/functions/text-box.func    || exit 1
. "$SCRIPTS"/functions/text-center.func || exit 1
. "$SCRIPTS"/functions/cursor.func      || exit 1
. "$SCRIPTS"/functions/draw-line.func   || exit 1

if [ ! -f "$file" ]; then
  touch "$file"
  cat > "$file"  <<EOF
<category="new">
<item="this script is a bit hacky"><status="done">
<item="example: press a"><status="">
<item="create a new category with c"><status="">
<item="navigate left or right to change the menu"><status="">
<item="press ? for help"><status="">
</category>
EOF
fi


gen_help() {
spacing="$((term_width-$(echo $0 | awk '{print length}')))"
help="${txt_special}$(printf "%${term_width}s" | sed 's/ /━/g')
${txt_special}$0$(printf "%${spacing}s")
${txt_special}$(printf "%${term_width}s" | sed 's/ /━/g')
${txt_normal}a todo list script with a combo menu
the left menu is to select the category
and the right one lists the corresponding entries

all inputs are stored in ${clr3}${txt_italic}$file${clr_end}

use ${clr3}${txt_bold}arrow keys${clr_end} or ${clr3}${txt_bold}hjkl${clr_end} to navigate
other keys:
  '${clr3}${txt_bold}a${clr_end}') add new entry
  '${clr3}${txt_bold}l${clr_end}') mark as done
  '${clr3}${txt_bold}f${clr_end}') mark as canceled
  '${clr3}${txt_bold}d${clr_end}') delete entry
  '${clr3}${txt_bold}c${clr_end}') add new category
  '${clr3}${txt_bold}r${clr_end}') redraw
  '${clr3}${txt_bold}v${clr_end}') view $file
  '${clr3}${txt_bold}e${clr_end}') edit $file
  '${clr3}${txt_bold}?${clr_end}') show this help
  '${clr3}${txt_bold}+${clr_end}') move selected entry up
  '${clr3}${txt_bold}-${clr_end}') move selected entry down
  '${clr3}${txt_bold}q${clr_end}') quit   

${clr3}Note:${clr_end} the left menu is a little funky,
also pages dont work in this script, that means,
if you have more entries that fit into the window,
this script may not behave, so it is strongly recommended
to keep these lists short and use a new category instead 
and do not spam inputs while in the left menu, be patient!
"
}

if [ ! "$PAGER" ]; then
  PAGER="less"
fi

if [ ! "$EDITOR" ]; then
  EDITOR="nano"
fi

get_term_info() {
  #term="$(ps --no-headers -o comm $(xdotool getactivewindow getwindowpid))"
  term_width="$(tput cols || stty size | cut -d ' ' -f2)"
  term_height="$(tput lines || stty size | cut -d ' ' -f1)"
}

get_term_info
gen_help

if [ "$1" = '-h' ] || [ "$1" = '--help' ]; then
  echo "$help"
  exit 0
fi

gen_lists() {
if [ -f "$file" ]; then
  category_table="$(awk -F\" '
  /^<category=".*">/ {printf "%s\t%s\t",$2,NR}
  /^<\/category>/ {printf "%s\n",NR}
  ' "$file")"
  active_lines="$(echo "$category_table" | awk -v FS="\t" -v al="$active_category" 'NR == al {printf "%s_%s\n",$2,$3}')"
  A="$(echo "$active_lines" | cut -d\_ -f1)"
  B="$(echo "$active_lines" | cut -d\_ -f2)"
  if [ "$A" ] && [ "$B" ] && [ "$A" -ge "$B" ]; then
    printf "Formating error in %s\\n" "$file"; exit 1
  fi

# $category_pos: the position of $active_category inside $category_table
category_pos="$(echo "$category_table"| awk -v FS="\t" -v a="$A" -v b="$B" '(( $2 == a ) && ( $3 == b )){print NR}')"

category_list="$(printf "%s" "$category_table" | awk -v FS="\t" '{print $1}')"
c_list_len="$(echo "$category_list" | awk 'END{print NR}')"
c_list_lmax="$(echo "$category_list" | awk 'length > lmax {lmax = length}; END{print lmax}')"

item_list="$(awk -v a="$A" -v b="$B" '{
             if ( (NR > a ) && ( NR < b )) {printf "%s\n",$0 } }' "$file" \
           | awk -v lmax="$((term_width - c_list_lmax - 14))" '
  BEGIN{ txt_strike = "\033\1339m"
         txt_normal = "\033\1330m"
         txt_clr1   = "\033\13331m"
         txt_clr2   = "\033\13332m"
         txt_clr3   = "\033\13333m"
         txt_clr4   = "\033\13334m"
         txt_clr5   = "\033\13335m"
         txt_clr6   = "\033\13336m"
         txt_clr7   = "\033\13337m"
       }

  match($0, /<item=.*">/) {
    regex = substr($0, RSTART, RLENGTH)
    a = index(regex, "=")
    b = index(regex, ">")
    out = substr(regex, a+2, b-a-3) 
    if ( $0 ~ /<status="canceled">/) { printf "%s%s",txt_strike,out }
    else printf "%s",out
    l=length(out)
    while ( l < lmax)
      { l++; printf " " }
    if ( $0 ~ /<status="done">/) { printf "%s[%sDONE%s]%s\n",txt_clr5,txt_clr2,txt_clr5,txt_normal }
    else if ( $0 ~ /<status="canceled">/) { printf "%s%s[%sNOPE%s]%s\n",txt_strike,txt_clr5,txt_clr1,txt_clr5,txt_normal }
    else { printf "%s[    ]%s\n",txt_clr5,txt_normal }
    #{printf ("%s\n",st) }
   }')"

i_list_len="$(echo "$item_list" | awk 'END{print NR}')"
i_list_done="$(awk -v a="$A" -v b="$B" -v c="0" '
  {if ((NR > a ) && ( NR < b ) && match($0, /<status="done">/)) {c++}}
  END{print c}' "$file")"
total_len="$(awk 'END{print NR}' "$file")"
fi
}

redraw() {
  clear
  get_term_info
  gen_lists
  line_pos="$(echo "$category_list" | awk 'length > l { l=length }; END{printf "%s",l}')"
  line_pos="$((line_pos+3))"
  if [ "$line_pos" -lt "8" ]; then
    line_pos="8"
  fi
  l=0
  while [ "$(tput lines || stty size | cut -d ' ' -f1)" -ge "$l" ]; do
    l=$((l+1))
    cursor_move "$((line_pos+2))" "$l"
    printf "${clr1}┃"
  done
  cursor_move 1 $(( (term_height/2) - (c_list_len/2) +1 ))
  printf "%s" "$category_list" \
    | awk -v a="$active_category" '
      BEGIN{ a_reset = "\033\133m"; f_yellow = "\033\13333m" }
      {if (NR==a) { print f_yellow a_reverse $0 a_reset}
      else {print f_yellow $0 a_reset} }'
  cursor_move 1 $(( (term_height/2) - (i_list_len/2) +1 ))
  echo  "$item_list" | awk -v l="$((line_pos+2))" '{printf "\033\13333m\033[%sC%s\n",l,$0 }'
  cursor_move 1 1
}

update_status() {
  item_status="$(awk -v l="$((A + awk_menu_line_number))" '
                 {if ( (NR==l) && match($0, /<status=".*">/) )
                   { regex = substr($0, RSTART, RLENGTH)
                     split(regex, list, "\"")
                     printf "%s",list[2]
                 } }' "$file")"
                     
  if [ -z "$item_status" ]; then
    if [ "$1" = "cancel" ]; then
      awk -v l="$((A + awk_menu_line_number))" '
      { if (NR==l) { if ($0 ~ /<status="">/){ gsub(/<status="">/, "<status=\"canceled\">", $0); print} }
        else {print} }' "$file" > "$file".n \
      && rm "$file" && mv "$file".n "$file"    
    else
      awk -v l="$((A + awk_menu_line_number))" '
      { if (NR==l) { if ($0 ~ /<status="">/){ gsub(/<status="">/, "<status=\"done\">", $0); print} }
        else {print} }' "$file" > "$file".n \
      && rm "$file" && mv "$file".n "$file"
    fi
  elif [ "$item_status" = "done" ]; then
    if [ "$1" = "cancel" ]; then
      awk -v l="$((A + awk_menu_line_number))" '
      { if (NR==l) { if ($0 ~ /<status="done">/){ gsub(/<status="done">/, "<status=\"canceled\">", $0); print} }
        else {print} }' "$file" > "$file".n \
      && rm "$file" && mv "$file".n "$file"    
    else
      awk -v l="$((A + awk_menu_line_number))" '
      { if (NR==l) { if ($0 ~ /<status="done">/){ gsub(/<status="done">/, "<status=\"\">", $0); print} }
        else {print} }' "$file" > "$file".n \
      && rm "$file" && mv "$file".n "$file"
    fi
  elif [ "$item_status" = "canceled" ] && [ "$1" = "cancel" ]; then
    awk -v l="$((A + awk_menu_line_number))" '
    { if (NR==l) { if ($0 ~ /<status="canceled">/){ gsub(/<status="canceled">/, "<status=\"\">", $0); print} }
      else {print} }' "$file" > "$file".n \
    && rm "$file" && mv "$file".n "$file"    
  fi
  redraw  
}

add_category(){ 
  unset input
  get_term_info
  gen_lists
  clear
  cursor_move  "1" "$(((term_height/2-1)))"
  echo -n "${bg_clr0}${clr4}$(text_center "Enter something:")"
  cursor_move "20" "$((term_height/2))"
  printf "${clr_end}${clr3}${txt_bold}"
  read -r input
  if [ "$input" ]; then
    echo "\\n<category=\"$input\">\\n</category>" >> "$file"
  fi
  echo -n "${clr_end}"
  gen_lists
  redraw
}

delete_category(){
if [ "$A" -ne "$B" ]; then
  sed "$A","$B"'d' -i "$file"
fi
}

add_item(){ 
  unset input
  get_term_info
  clear
  cursor_move  "1" "$(((term_height/2-1)))"
  echo  "${bg_clr0}${clr4}$(text_center "Enter something:")"
  cursor_move "20" "$((term_height/2))"
  printf "${clr3}${txt_bold}"
  read -r input
  if [ "$input" ]; then
     awk -v i="$input" -v l="$((main_menu_line_number+A))" '{
       if (NR==l){printf "<item=\"%s\"><status=\"\">\n%s\n",i,$0} 
       else {printf "%s\n",$0} 
       }' "$file" > "$file".n   && rm "$file" && mv "$file".n "$file"
     main_menu_line_number="$((main_menu_line_number+1))"
  fi
  echo -n "${clr_end}"
  gen_lists
  redraw
}

delete_item() {
 if [ "$awk_menu_line_number" ] && [ "$A" ]; then
     awk -v l="$((awk_menu_line_number+A))" '{
       if (NR!=l) {print $0} 
       }' "$file" > "$file".n   && rm "$file" && mv "$file".n "$file"
  fi
  gen_lists
  redraw
}

help_function() {
  get_term_info
  echo "$help" | "$PAGER" -R
}

move_category_up() {
  gen_lists
  C="$(echo "$category_table" | awk -v FS="\t" -v p="$((category_pos-1))" 'NR==p{print $2}')"
  D="$(echo "$category_table" | awk -v FS="\t" -v p="$((category_pos-1))" 'NR==p{print $3}')"

  if [ "$A" -ge 1 ] && [ "$B" -gt "$A" ] && [ "$C" -ge 1 ] && [ "$D" -gt "$C" ]; then
    awk -v c="$C" '{ if (NR < c) {print} }' "$file" >> "$file".tmp
    awk -v a="$A" -v b="$B" '{ if ((NR <= b) && (a <= NR )) {print} }' "$file" >> "$file".tmp
    awk -v c="$C" -v d="$D" '{ if ((NR <= d) && (c <= NR )) {print} }' "$file" >> "$file".tmp
    awk -v b="$B" '{ if (b < NR) {print} }' "$file" >> "$file".tmp
    grep -v "^$" "$file".tmp > "$file"
    sed -n "_</category>_</category>\n_" -i "$file"
    if [ -f "$file".tmp ]; then
      rm "$file".tmp
    fi
  fi
}

move_category_down() {
  gen_lists
  C="$(echo "$category_table" | awk -v FS="\t" -v p="$((category_pos+1))" 'NR==p{print $2}')"
  D="$(echo "$category_table" | awk -v FS="\t" -v p="$((category_pos+1))" 'NR==p{print $3}')"

  if [ "$A" -ge 1 ] && [ "$B" -gt "$A" ] && [ "$C" -ge 1 ] && [ "$D" -gt "$C" ]; then
    awk -v a="$A" '{ if (NR < a) {print} }' "$file" >> "$file".tmp
    awk -v c="$C" -v d="$D" '{ if ((NR <= d) && (c <= NR )) {print} }' "$file" >> "$file".tmp
    awk -v a="$A" -v b="$B" '{ if ((NR <= b) && (a <= NR )) {print} }' "$file" >> "$file".tmp
    awk -v d="$D" '{ if (d < NR) {print} }' "$file" >> "$file".tmp
    grep -v "^$" "$file".tmp > "$file"
    sed -n "_</category>_</category>\n_" -i "$file"
    if [ -f "$file".tmp ]; then
      rm "$file".tmp
    fi
  fi
}

main_menu(){
  if [ "$main_menu_line_number" -gt "$i_list_len" ]; then
    main_menu_line_number="$i_list_len"
  fi
  if [ "$i_list_len" -gt "$term_height" ]; then
    redraw
  fi
  done_count="${clr1}[${clr7}$i_list_done${clr1}/${clr7}$i_list_len${clr1}]"
  dc_length="$(echo " [$i_list_done/$i_list_len]" | awk '{ l=length }; END{printf "%s",l}')"
  dc_bump="$((term_width-line_pos-dc_length-2))"
  line_bump="$((line_pos-6))"
  awk_menu_bmsg="$(printf " ${clr4}${txt_special}?${clr_end} ${clr7}help${clr_end}%${line_bump}s┃%${dc_bump}s $done_count")"
  bump=$((line_pos-9))
  awk_menu_keys="acdmCDNrvq?"
  awk_menu_tmsg="$(printf "${clr1}category%${bump}s┃ ${txt_special}to do%${term_width}s${clr_end}")"
  awk_menu_line_number="$main_menu_line_number"
  awk_menu_tgap="$(( (term_height/2) - (i_list_len/2) -1 ))"
  awk_menu -Z -q --no-page "$(echo "$item_list" | awk -v l="$((line_pos+2))" '{printf "\033\13333m\033[%sC%s\n",l,$0}')"
  awk_menu_tgap=0
}

side_menu() {
  done_count="${clr1}[${clr7}$i_list_done${clr1}/${clr7}$i_list_len${clr1}]"
  dc_length="$(echo " [$i_list_done/$i_list_len]" | awk '{ l=length }; END{printf "%s",l}')"
  dc_bump="$((term_width-line_pos-dc_length-2))"
  line_bump="$((line_pos-8))"
  awk_menu_bmsg="$(printf " ${clr4}${txt_special}?${clr_end} ${clr7}help${clr_end}%${line_bump}s┃%${dc_bump}s $done_count")"
  awk_menu_keys="acmCDHNrvq?"
  awk_menu_line_number="$side_menu_line_number"
  awk_menu_tmsg="$(printf "${clr_end}${txt_special}category%${bump}s${clr_end}┃ to do")"
  awk_menu_tgap="$(( (term_height/2) - (c_list_len/2) -1 ))"
  awk_menu -Z -q "${clr3}$category_list"
  awk_menu_tgap=0
}

# script begins here:
if [ -z "$side_menu_line_number" ]; then
  side_menu_line_number=1
fi
if [ -z "$main_menu_line_number" ]; then
  main_menu_line_number=1
fi
if [ -z "$awk_menu_line_number" ]; then
  awk_menu_line_number=1
fi
if [ -z "$active_category" ]; then
  active_category="$side_menu_line_number"
fi

redraw
menu="main_menu"
while [ "$menu" ]; do
  # ensure that that the line number variables will always be correct
  if [ "$main_menu_line_number" -gt "$i_list_len" ]; then
    main_menu_line_number="$i_list_len"
  elif [ "$main_menu_line_number" -lt "1" ]; then
    main_menu_line_number="1"
  fi
  if [ "$side_menu_line_number" -gt "$c_list_len" ]; then
    side_menu_line_number="$c_list_len"
  elif [ "$side_menu_line_number" -lt "1" ]; then
    side_menu_line_number="1"
  fi
  active_category="$side_menu_line_number"

  case "$menu" in
  # main menu
    main_menu) main_menu
      if [ "$awk_menu_key_press" ]; then
        case "$awk_menu_key_press" in
          'up'|'\[A'|'k') if [ "$main_menu_line_number" -gt 1 ]; then
                            main_menu_line_number="$((main_menu_line_number-1))"
                          fi;;
          'down'|'\[B'|'j') if [ "$main_menu_line_number" -lt "$(echo "$item_list" | wc -l )" ]; then
                              main_menu_line_number="$((main_menu_line_number+1))"
                            fi;;
          'right'|'\[C'|'l'|'\\n') update_status;;
          'left'|'\[D'|'h') menu="side_menu";;
          'a') restore_cursor; add_item;;
          'd') delete_item;;
          'c') restore_cursor; add_category;;
          'f') update_status cancel;;
          'r') redraw;;
          'v') $PAGER "$file";;
          'e') $EDITOR "$file";;
          'q') break;;
          '?') help_function;;
          '+') if [ "$main_menu_line_number" -gt 1 ]; then
                 line_swap "$((main_menu_line_number+A-1))" "$((main_menu_line_number+A))" "$file"
                 main_menu_line_number="$((main_menu_line_number-1))"
               fi
               redraw ;;
          '-') if [ "$main_menu_line_number" -lt "$(echo "$item_list" | wc -l )" ]; then
                 line_swap "$((main_menu_line_number+A))" "$((main_menu_line_number+A+1))" "$file"
                 main_menu_line_number="$((main_menu_line_number+1))"
               fi
               redraw ;;
         esac
       fi
    ;;
  # side menu
    side_menu) redraw; side_menu
      if [ "$menu" = "side_menu" ]; then
        if [ "$awk_menu_key_press" ]; then
          case "$awk_menu_key_press" in
            'up'|'\[A'|'k') if [ "$side_menu_line_number" -gt 1 ]; then
                         side_menu_line_number="$((side_menu_line_number-1))"
                         active_category="$side_menu_line_number"
                       fi;;
            'down'|'\[B'|'j') if [ "$side_menu_line_number" -lt "$c_list_len" ]; then
                         side_menu_line_number="$((side_menu_line_number+1))"
                         active_category="$side_menu_line_number"
                       fi;;
            'right'|'\[C'|'l'|'\\n') menu="main_menu";;
            '^t') pcmanfm;;
            '?') help_function;;
            'a'|'c') restore_cursor; add_category
                     side_menu_line_number="$((side_menu_line_number+1))";;
            'd') delete_category;;
            'r') redraw;;
            'v') $PAGER "$file";;
            'e') $EDITOR "$file";;
            'q') break; exit;;
            '+') if [ "$side_menu_line_number" -gt 1 ]; then
                   move_category_up
                   side_menu_line_number="$((side_menu_line_number-1))"
                 fi
                 redraw;;
            '-') if [ "$side_menu_line_number" -lt "$c_list_len" ]; then
                   move_category_down
                   side_menu_line_number="$((side_menu_line_number+1))"
                 fi
                 redraw;;
          esac
        fi
      fi
    ;;
  esac
done
restore_cursor
exit
