#!/bin/sh
# get rss url from youtube channel
# written by Michael Thorberger

. "$SCRIPTS"/functions/colors.func

if [ "$1" ]; then
  url="$1"
else
  printf "Enter a youtube channel url: "
  read -r url
fi

# if url is a video, get the channel page 
if echo "$url" | grep -q "/watch?v="; then
  baseurl="$(curl -s "$url" | tr ',' '\n' | awk -v FS=\" '/\/channel\//{print $8; exit}')"
  url="https://www.youtube.com$baseurl"
fi

# extract rss url
rssurl="$(curl -s "$url" | tr ',' '\n' | awk -v FS=\" '/rssUrl/ {print $4}')"
if [ "$rssurl" ]; then
  echo "${txt_underlined}Rss Url:${txt_normal}\\n${clr5}$rssurl${clr_end}"
  echo "copy to clipboard? [Y|n]"
  read -r ans
  if [ -z "$ans" ] || [ "$ans" != 'n' ] && [ "$ans" != 'N' ]; then
    printf "%s" "$rssurl" | xclip -i -selection clipboard
  fi
else
  printf "Error.\\n"
fi
exit 0
