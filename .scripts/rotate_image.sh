#!/bin/sh

if [ ! -f "$1" ]; then
  printf "%s\tError: Invalid input file\\n" "$0"
  exit 1
else
  file="$1"
  file_format="$(printf "%s\\n" "${1##*.}")"
fi

if [ -z "$2" ]; then
  printf "Using default angle: 90\\n"
  rotation_angle="90"
else
  rotation_angle="$2"
fi

case "$rotation_angle" in
  -270|-180|-90|90|180|270) break;;
  l|left) rotation_angle="-90";;
  r|right) rotation_angle="90";;
  0|360|-360) play -qn synth 0.15 sawtooth A; play -qn synth 0.15 sawtooth E
     exit 0;;
  *) printf "%s\tError: Invalid angle\\n" "$0"
     printf "Usage: ./rotate_image.sh SOME_FILE.jpg ANGLE\\n"
     printf "ANGLE must be a multiple of 90 between -360 and 360\\n"
     exit 1;;
esac

convert "$file" -rotate "$rotation_angle" "$file"
exit 0
