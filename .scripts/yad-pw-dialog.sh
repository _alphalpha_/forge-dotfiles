#!/bin/sh

yad --class="Yad, yad_pw_dialog" \
    --title="Password" \
    --text="Enter password:" \
    --image="dialog-password" \
    --entry --hide-text \
    2>/dev/null

exit 0
