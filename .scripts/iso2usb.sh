#!/bin/sh
# A script that writes iso/img files to usb devices,
# can also use compressed input files.
# Use this script without any parameters to get the menu.
# otherwise $1 = <input file> and $2 = <device> 
# to just set the device without input file, use -d <device>
# optional use -l or --list to print the iso list and exit
# The script searches files in current directory, ~/Downloads and $iso_dir
iso_dir="/home/snapshot"

. "$SCRIPTS"/functions/awk-menu.func    || exit 1
. "$SCRIPTS"/functions/colors.func      || exit 1
. "$SCRIPTS"/functions/text-box.func    || exit 1
. "$SCRIPTS"/functions/text-center.func || exit 1

text_box_opt="-c clr2 -t $(echo "${clr7}$0${clr_end}" | sed "s:$HOME:~:") -tpos 10"

gen_iso_list() {
if [ -d "$HOME/Downloads" ] && [ "$HOME/Downloads" != "$(pwd)" ]; then
  find "$HOME/Downloads" -maxdepth 2 -type f \
  \( -iname "*.iso" -o -iname "*.iso.bz2" -o -iname "*.iso.gz" -o -iname "*.iso.xz" \
  -o -iname "*.img" -o -iname "*.img.bz2" -o -iname "*.img.gz" -o -iname "*.img.xz" \
  \) -printf "%TY-%Tm-%Td %TH:%TM %p\n"
fi
if [ "$iso_dir" != "$(pwd)" ]; then
  find "$(pwd)" -maxdepth 2 -type f \
  \( -iname "*.iso" -o -iname "*.iso.bz2" -o -iname "*.iso.gz" -o -iname "*.iso.xz" \
  -o -iname "*.img" -o -iname "*.img.bz2" -o -iname "*.img.gz" -o -iname "*.img.xz" \
  \) -printf "%TY-%Tm-%Td %TH:%TM %p\n"
fi
find "$iso_dir" -maxdepth 5 -type f \
  \( -iname "*.iso" -o -iname "*.iso.bz2" -o -iname "*.iso.gz" -o -iname "*.iso.xz" \
  -o -iname "*.img" -o -iname "*.img.bz2" -o -iname "*.img.gz" -o -iname "*.img.xz" \
  \) -printf "%TY-%Tm-%Td %TH:%TM %p\n"
} 


iso_menu() {
awk_menu_tmsg="$(text_box $text_box_opt "Select a iso file:" | awk '{print "\033[1;33m" $0 "\033[0m"}')"
selection="$(awk_menu "$(gen_iso_list | sort -u | sed "s:$HOME:~:g")\n> Reload\\n> Set iso_dir\\n> Exit Script")"

if [ ! "$(echo "$selection" | cut -b 1)" = '>' ]; then
  selection="$(echo "$selection" | cut -b 18- | sed "s:~:$HOME:g")"
fi

case "$selection" in
  '> Reload') iso_menu;;
  '> Set iso_dir') printf "Enter path:"; read -r iso_dir; iso_menu;;
  '> Exit Script') exit 0;;
  *) if [ -f "$selection" ]; then
       source_file="$selection"
     elif [ -z "$selection" ]; then
       exit 0
     else
       echo "Error: unknown selection in iso_menu" && exit 1
     fi
esac
}


device_menu() {
awk_menu_tmsg="$(text_box $text_box_opt "Select a target device for $source_file" | awk '{print "\033[1;33m" $0 "\033[0m"}')"
device_list="$(lsblk -dpo NAME,TRAN,SUBSYSTEMS,SIZE,VENDOR,MODEL | awk '/usb/ {printf "%s (%s %s %s %s)\n",$1,$4,$5,$6,$7}')"
selection="$(awk_menu "$device_list\\n> Reload\\n> Back to iso selection\\n> Exit Script")"
case "$selection" in
  '> Reload') device_menu;;
  '> Back to iso selection') iso_menu;;
  '> Exit Script') exit 0;;
  *) selection="$(echo "$selection" | awk '{print $1}')"
     if [ -b "$selection" ]; then
       target_device="$selection"
     elif [ -z "$selection" ]; then
       exit 0
     elif [ ! -b "$selection" ]; then
       echo "Error: selected device is not a block device"; exit 1
     else
       echo "Error: unknown selection in device_menu"; exit 1
     fi
esac
}


extract_msg(){
  echo "${clr7}the input file is $1 compressed${clr_end}"
  echo "${clr7}that is no problem, but the timer and percentage will be displayed incorrect${clr_end}"  
}


final_warning() {
awk_menu_tmsg="$(text_box $text_box_opt \
"source = ${clr4} $source_file ${clr_end} ($(du -h "$source_file" | awk '{print $1}'))
target = ${clr4} $target_device ${clr_end} ($(lsblk "$target_device" -dnpo SIZE,VENDOR,MODEL,LABEL,FSTYPE))" )"

selection="$(awk_menu "> Back to iso selection\\n> Back to device selection\\n> Exit Script
> Let it rip! ${clr5}(Warning: All Data on $target_device will be lost!)${clr_end}" | strip_color)"

case "$selection" in
  '> Back to iso selection')    final_ok="0"; iso_menu;;
  '> Back to device selection') final_ok="0"; device_menu;;
  '> Exit Script') exit 0;;
  "> Let it rip! (Warning: All Data on $target_device will be lost!)") final_ok="1";;
  *) final_ok="0"
    if [ -z "$selection" ]; then
       exit 0
     else
       echo "Error: unknown selection in final_warning" && exit 1
     fi;;
esac
}


################################################################################
# Script starts here:

if [ "$1" ] ; then
  if [ "$1" = '-l' ] || [ "$1" = '--list' ]; then
    gen_iso_list 2>/dev/null | sort -u | sed "s:$HOME:~:g"
    exit 0
  fi
  if [ "$1" = '-d' ] || [ "$1" = '--dummy' ]; then
    sleep 0
  elif [ ! -f "$(pwd -L)/$1" ] && [ ! -d "$1" ]; then
    echo "Error, No such ISO file" && exit 1
  elif [ -d "$1" ]; then
    iso_dir="$1"
  elif [ -f "$(pwd -L)/$1" ]; then
    source_file="$1"
  fi
fi

if [ "$2" ] ; then
  [ -z "$(ls "$2")" ] && echo "Error, No such USB device" && exit 1
  target_device="$2"
fi

if [ -z "$(lsblk -o NAME,TRAN,SUBSYSTEMS -d | grep usb)" ]; then
  echo "No USB Devices found!" && exit 1
fi

iso_menu

while [ ! -b "$target_device" ]; do
  device_menu
done

while [ "$final_ok" != "1" ]; do
  final_warning || exit 1
done

text_box $text_box_opt "Writing ${clr4}$source_file${clr_end} ($(du -h "$source_file" | awk '{print $1}'))
to ${clr4}$target_device${clr_end} ($(lsblk "$target_device" -dnpo SIZE,VENDOR,MODEL)) ...
${clr5}Do not remove device until the green text has appeared!${clr_end}"
sudo sleep 0


# with pv command
if command -v pv > /dev/null ; then
  case "$(echo "$source_file" | awk -F '.' '{print $NF}')" in
    'iso'|'img') pv -s $(du -sb "$source_file" | awk '{print $1}') "$source_file" | sudo dd of="$target_device" || exit 1;;
    'bz2') extract_msg "bz2"
           bzcat "$source_file" | pv -s $(du -sb "$source_file" | awk '{print $1}') | sudo dd of="$target_device" || exit 1;;
    'gz') extract_msg "gz"
          zcat "$source_file" | pv -s $(du -sb "$source_file" | awk '{print $1}') | sudo dd of="$target_device" || exit 1;;
    'xz') extract_msg "xz"
          xzcat "$source_file" | pv -s $(du -sb "$source_file" | awk '{print $1}') | sudo dd of="$target_device" || exit 1;;
  esac
else
# without pv
  case "$(echo "$source_file" | awk -F '.' '{print $NF}')" in
    'iso'|'img') sudo dd if="$source_file" of="$target_device" status=progress|| exit 1;;
    'bz2') extract_msg "bz2"; bzcat "$source_file" | sudo dd of="$target_device" || exit 1;;
    'gz')  extract_msg "gz"; zcat "$source_file" | sudo dd of="$target_device" || exit 1;;
    'xz')  extract_msg "xz"; xzcat "$source_file" | sudo dd of="$target_device" || exit 1;;
  esac
fi

echo "${clr5}Synchronizing...${clr_end}"
sync || exit 1

echo "${clr2}Script completed without errors.${clr_end}"
exit 0
