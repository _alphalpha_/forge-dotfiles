#!/bin/sh
## send signals to the window in focus
## options:
##          cont / continue     (SIGCONT)
##          stop / pause        (SIGSTOP)
##          quit / end          (SIGTERM)
##          kill / -9           (SIGKILL)
##          toggle (default)    (SIGCONT) or (SIGSTOP)
## by Michael Thorberger

window_id="$(xprop -root _NET_ACTIVE_WINDOW | awk '{print $NF}')"
window_pid="$(xprop -id "$window_id" | awk '/_NET_WM_PID/{print $3}')"
window_class="$(xprop -id "$window_id" | awk -F '"' '/WM_CLASS/ {print $4}')"
#window_name="$(xprop -id "$window_id" | awk -F '"' '/_NET_WM_NAME/ {print $2}')"

case "$window_class" in
  'qutebrowser')   # get the correct pid for the python venv
      window_pid="$(xprop -id "$window_id" | awk -F '"' '/QB_PID/{print $2}')"
      ;;
  'conky'|'Conky') # do not kill conky
      if [ "$1" = 'quit' ] || [ "$1" = 'end' ]; then
        exit 0
      fi
      ;;
esac

if [ "$window_pid" ]; then
  case "$1" in
    'cont'|'continue'|'SIGCONT')
      kill -CONT "$window_pid"
      ;;
    'stop'|'pause'|'SIGSTOP')
      kill -STOP "$window_pid"
      ;;
    'quit'|'end'|'SIGTERM')
#      kill "$window_pid"
      wmctrl -ic "$window_id"
      ;;
    'kill'|'-9'|'SIGKILL')
      kill -9 "$window_pid"
      ;;
    'toggle'|*)
      if [ "$(ps "$window_pid" | awk '{if (($3 ~ "^T") && (NR > 1)) {print $1}}')" = "$window_pid" ]; then
        kill -CONT "$window_pid"
      elif [ "$(ps "$window_pid" | awk '{if (($3 ~ "^S") && (NR > 1)) {print $1}}')" = "$window_pid" ]; then
        kill -STOP "$window_pid"
      fi
      ;;
  esac
elif [ "$window_id" ]; then
  case "$1" in
    'quit'|'end'|'SIGTERM')
      wmctrl -ic "$window_id"
      ;;
    'kill'|'-9'|'SIGKILL')
      xkill -id "$window_pid"
      ;;
    *) exit 1
      ;;
  esac
else
  exit 1
fi 
exit 0
