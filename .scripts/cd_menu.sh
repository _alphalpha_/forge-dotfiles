#!/bin/sh
#shellcheck disable=SC1091,SC2034
. "$SCRIPTS"/functions/awk-menu.func || exit 1

while true; do
  awk_menu "$(tree -d -l -L 1 | tr -d '└├─')"
  if [ -z "$awk_menu_output" ]; then
    exit 0
  elif [ "$awk_menu_output" = "." ]; then
    cd ..
  elif [ -d "$awk_menu_output" ]; then
    cd "$awk_menu_output" || exit 1
  else
    exit 1
  fi
done
exit 0
