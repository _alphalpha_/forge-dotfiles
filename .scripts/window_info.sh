#!/bin/sh

. "$SCRIPTS"/functions/dzen_msg.func || exit 1

window_id="$(xwininfo | awk '/Window id:/{print $4}')"
window_name="$(xprop -id "$window_id" | awk -F '"' '/_NET_WM_NAME/ {print $2}')"
window_class="$(xprop -id "$window_id" | awk -F '"' '/WM_CLASS/ {print $4}')"
window_pid="$(xprop -id "$window_id" | awk '/_NET_WM_PID/ {print $3}')"
window_geo="$(xwininfo -id "$window_id" | awk '/upper-left [XY]:|Width:|Height:|Map State:/')"

printf "id:\t\t%s\\nname:\t\t%s\\nclass:\t\t%s\\npid:\t\t%s\\n" \
       "$window_id" "$window_name" "$window_class" "$window_pid" 

printf "id:   %s\\nname:   %s\\nclass:   %s\\npid:   %s\\n" \
       "$window_id" "$window_name" "$window_class" "$window_pid" \
| dzen_msg
