#! /bin/bash
# -*- mode: sh -*-

KEY=$RANDOM

res1=$(mktemp --tmpdir term-tab1.XXXXXXXX)
res2=$(mktemp --tmpdir term-tab2.XXXXXXXX)
res3=$(mktemp --tmpdir term-tab3.XXXXXXXX)

out=$(mktemp --tmpdir term-out.XXXXXXXX)

# cleanup
trap "rm -f $res1 $res2 $res3 $out" EXIT

export YAD_OPTIONS="--bool-fmt=t --separator='\n' --quoted-output"

rc_file="${1:-$HOME/.Xresources}"


for DEVICE in $(lsblk -dpno NAME); do
  if [ "$(sudo smartctl -a "$DEVICE" | grep -w 'SMART support is: Available - device has SMART capability.')" ] \
  && [ "$(sudo smartctl -a "$DEVICE" | grep -w 'SMART support is: Enabled')" ]; then
    LIST="$LIST $DEVICE"
  fi
done

# echo "$LIST" | wc -c

for DEVICE in $LIST; do
  MDL="$(sudo smartctl -a "$DEVICE" | grep -w '^Device Model:' |  awk -F: '{print $NF; exit}' | sed -e 's/^\s*//' )"
echo "$MDL"
  SN="$(sudo smartctl -a "$DEVICE" | grep -w '^Serial Number:' |  awk -F: '{print $NF; exit}' | sed -e 's/^\s*//' )"
echo "$SN"
  PWR_H="$(sudo smartctl -a "$DEVICE" | grep -w 'Power_On_Hours' |  awk -F\  '{print $NF; exit}' | sed -e 's/^\s*//' )"
echo "$PWR_H"
  PWR_C="$(sudo smartctl -a "$DEVICE" | grep -w 'Power_Cycle_Count' |  awk -F\  '{print $NF; exit}' | sed -e 's/^\s*//' )"
echo "$PWR_C"
  FW="$(sudo smartctl -a "$DEVICE" | grep -w '^Firmware Version:' |  awk -F\  '{print $NF; exit}' | sed -e 's/^\s*//' )"
echo "$FW"
  CAP="$(sudo smartctl -a "$DEVICE" | grep -w '^User Capacity:' |  cut -d '[' -f2 |  cut -d ']' -f1 )"
echo "$CAP"
done
fn=$(yad-tools --pfd -- "$font")

#echo $font
echo $fn

# main page
df -T | grep -vwE 'tmpfs|devtmpfs' | tail -n +2 | awk '{printf "%s\n%s\n%s\n%s\n%s\n%s\n", $1,$7, $2, $3, $4, $6}' |\
yad --plug=$KEY --tabnum=1 --title="Disk space usage" \
    --list --no-selection --column="Device" --column="Mountpoint" --column="Type" \
    --column="Total:sz" --column="Free:sz" --column="Usage:bar" > $res1 &
    
# palette page
yad --plug=$KEY --tabnum=2 --form --columns=2 \
    --field="$LIST" \
    --field=$"Red::clr" ${cl1:-#cc0000} \
    --field=$"Green::clr" ${cl2:-#4e9a06} \
    --field=$"Brown::clr" ${cl3:-#c4a000} \
    --field=$"Blue::clr" ${cl4:-#3465a4} \
    --field=$"Magenta::clr" ${cl5:-#75507b} \
    --field=$"Cyan::clr" ${cl6:-#06989a} \
    --field=$"Light gray::clr" ${cl7:-#d3d7cf} \
    --field=$"Gray::clr" ${cl8:-#555753} \
    --field=$"Light red::clr" ${cl9:-#ef2929} \
    --field=$"Light green::clr" ${cl10:-#8ae234} \
    --field=$"Yellow::clr" ${cl11:-#fce94f} \
    --field=$"Light blue::clr" ${cl12:-#729fcf} \
    --field=$"Light magenta::clr" ${cl13:-#ad7fa8} \
    --field=$"Light cyan::clr" ${cl14:-#34e2e2} \
    --field=$"White::clr" ${cl15:-#eeeeec} > $res2 &

# misc page
echo -e $misc | yad --plug=$KEY --tabnum=3 --text-info --editable > $res3 &

# main dialog
yad --window-icon=utilities-terminal \
    --notebook --key=$KEY --tab=$"Main" --tab=$"Palette d wqjoqw qw dwqo" --tab=$"joiwqje oqq kqwpeo jwqpoijewo iewq" --tab=$"Misc" \
    --title=$"Terminal settings" --image=drive-harddisk \
    --width=800 --height=450 --image-on-top --text=$"Terminal settings (URxvt)"

# recreate rc file
if [[ $? -eq 0 ]]; then
    mkdir -p ${rc_file%/*}

    eval TAB1=($(< $res1))
    eval TAB2=($(< $res2))

echo -e "! urxvt settings\n" > $out

    # add main
    cat <<EOF >> $out
URxvt.title: ${TAB1[0]}
URxvt.geometry: ${TAB1[1]}x${TAB1[2]}
URxvt.font: $(yad-tools --pfd "${TAB1[3]}")
URxvt.termName: ${TAB1[4]}
URxvt.loginShell: ${TAB1[5]}
URxvt.scrollBar: ${TAB1[6]}
URxvt.allow_bold: ${TAB1[7]}

URxvt.foreground: ${TAB1[9]}
URxvt.background: ${TAB1[10]}
URxvt.highlightColor: ${TAB1[11]}
URxvt.highlightTextColor: ${TAB1[12]}
EOF
    # add palette
    echo >> $out
    for i in {0..15}; do
        echo "URxvt.color$i: ${TAB2[$i]}" >> $out
    done
    echo >> $out

    # add misc
    cat $res3 >> $out
    echo >> $out

    if [[ $rc_file == $HOME/.Xresources ]]; then
        [[ -e $rc_file ]] && sed -i "/^URxvt.*/d" $rc_file
        cat $out >> $rc_file
    else
        mv -f $out $rc_file
    fi
fi
