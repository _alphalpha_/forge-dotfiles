#!/bin/sh
case "$1" in
  'poweroff') sudo -A -p 'Enter password to shutdown:' /sbin/poweroff;;
    'reboot') sudo -A -p 'Enter password to reboot:' /sbin/reboot;;
      'lock') i3lock-fancy;;
    'logout') WM=$(xprop -id $(xprop -root -notype _NET_SUPPORTING_WM_CHECK | awk '{print $NF}') -f _NET_WM_NAME 8t | awk -F\" '/WM_NAME/{print $2}')
              case "$WM" in
                 'i3') i3-msg exit;;
                'JWM') jwm -exit;;
              'bspwm') bspc quit;;
            'openbox') openbox --exit;;
              'berry') berryc quit;;
           'spectrwm') killall spectrwm;;
       'fvwm'|'NsCDE') killall fvwm3;;
               'hypr') killall Hypr;;
                    *) exit 1;;
              esac;;
 'hibernate') sudo sleep 0
              sudo i3lock-fancy &
              sleep 3
              sudo pm-suspend;;
           *) exit 1;;
esac
exit 0
