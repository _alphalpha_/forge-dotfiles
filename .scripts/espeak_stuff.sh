#!/bin/sh
case "$1" in
  'time'|'--time') espeak -m "the time is $(date "+%H %M")";;
  'date'|'--date')  espeak -m "it is $(date "+%A %d %B")";;
  'warning'|'--warning') espeak -m "warning";;
esac
exit
