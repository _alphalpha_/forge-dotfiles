#!/bin/sh
. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/text-box.func || exit 1
. "$SCRIPTS"/functions/text-center.func || exit 1

awk_menu_tmsg="Reconfigure package"
input_text="> Exit Script
$(find /var/lib/dpkg/info/ -iname "*.config")"

selection="$(awk_menu --no-bmsg "$input_text")"

case "$selection" in
  "> Exit Script") exit 0;;
  *) if [ -f "$selection" ]; then
       sudo dpkg-reconfigure "$(echo "$selection" | awk -F '/' '{gsub(/.config$/,"")};{print $NF}')"
     elif [ -z "$selection" ]; then
       exit 0
     else
       exit 1
     fi;;
esac
exit 0
