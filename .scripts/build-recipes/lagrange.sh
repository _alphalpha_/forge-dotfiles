#!/bin/sh
## https://git.skyjake.fi/gemini/lagrange
project="lagrange"
build_dir="$HOME/tmp/$project"

git clone --recursive --branch release https://git.skyjake.fi/gemini/lagrange "$build_dir"

sudo apt install cmake zip libsdl2-dev libssl-dev libpcre3-dev zlib1g-dev \
         libunistring-dev libfribidi-dev libmpg123-dev

cur_dir="$(pwd)"
cd "$build_dir" || exit 1
  
  if [ ! -d build ]; then
    mkdir -vp build
  fi
  cd build || exit 1
    cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local
    cmake --build . --target
      
    echo "Install $project to /usr/local ? [Y|n]"
    read -r ans
    if [ -z "$ans" ]; then
      ans="y"
    fi
    case "$ans" in
      y|Y) sudo make install;;
    esac
cd "$cur_dir"

exit 0
