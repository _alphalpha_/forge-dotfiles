#!/bin/sh

## https://github.com/devinacker/bsnes-plus


sudo apt install git pkg-config qtbase5-dev-tools libxv-dev libsdl1.2-dev libao-dev libopenal-dev g++
git clone https://github.com/devinacker/bsnes-plus.git ~/tmp/bsnes-plus

CURRENT_DIR="$(pwd)"
cd ~/tmp/bsnes-plus/bsnes
  make
#  sudo make install
  sudo cp out/bsnes /usr/local/bin/bsnes+
  sudo chmod 775 /usr/local/bin/bsnes+
  sudo cp data/bsnes.png /usr/local/share/pixmaps/bsnes+.png
  sudo chmod 644 /usr/local/share/pixmaps/bsnes+.png
  sudo cp data/bsnes.desktop /usr/local/share/applications/bsnes+.desktop
  sudo chmod 644 /usr/local/share/applications/bsnes+.desktop
  sudo sed -i 's/bsnes/bsnes+/g' /usr/local/share/applications/bsnes+.desktop
cd "$CURRENT_DIR"
exit 0



