#!/bin/sh

## visualboyadvance-m https://github.com/visualboyadvance-m/visualboyadvance-m

git_url="https://github.com/visualboyadvance-m/visualboyadvance-m"
project="$(echo "$git_url" | awk -F '/' '{print $NF}')"
build_dir="$HOME/tmp/$project"

printf "checking $git_url/releases\\n"
URL="$(curl -s "$git_url"/releases/latest | grep -o -E "$git_url/releases/tag/v?[0-9]?{3}\.[0-9]?{3}\.?[0-9]?{3}[a-z]?")"
DL_URL="https://github.com$(curl -s "$URL" | grep -o -E "/visualboyadvance-m/visualboyadvance-m/archive/refs/tags/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}[a-z]?\.tar\.?[x|g]?z?")"
FILE_NAME="$(echo "$DL_URL" | awk -F/  '{print $NF; exit}')"

#./installdeps
lib_glew=$(apt-cache search libglew | grep '^libglew[0-9]' | sed 's/ - .*//')
lib_swresample_dev=$(apt-cache search libswresample-dev | awk '{print $1}')
lib_wxgtk="$(apt-cache search libwxgtk | awk '/^libwxgtk[0-9]/&&!/dev/{print $1}')"
lib_wxgtk_dev="$(apt-cache search libwxgtk | awk '/^libwxgtk[0-9]/&&/dev/{print $1}')"

printf "installing dependencies\\n"
sudo apt install build-essential ninja-build g++ nasm cmake ccache gettext zlib1g-dev \
libavcodec-dev libavformat-dev libswscale-dev libavutil-dev libgettextpo-dev \
libsdl2-2.0-0 libsdl2-dev libglu1-mesa-dev libglu1-mesa libgles2-mesa-dev libgl1-mesa-dev\
libpng-dev libzip-dev libopenal-dev libgtk2.0-dev libgtk-3-dev ccache zip \
libsfml-dev libsfml-graphics2.5 libsfml-network2.5 libsfml-window2.5 \
$lib_glew $lib_swresample_dev $lib_wxgtk $lib_wxgtk_dev

CURRENT_DIR="$(pwd)"
[ -d ~/tmp ] || mkdir ~/tmp
cd ~/tmp
#  wget -c "$DL_URL" -O "vbam-$FILE_NAME"
#  [ -d vbam-latest ] && rm -rd vbam-latest
#  mkdir vbam-latest || exit 1
#  case "$FILE_NAME" in
#    *.tar.gz) tar xvzf "vbam-$FILE_NAME" --strip-components 1 -C vbam-latest;;
#    *.tar.xz) tar xvJf "vbam-$FILE_NAME" --strip-components 1 -C vbam-latest;;
#  esac

git clone "$git_url".git vbam-latest
  cd vbam-latest || exit 1
    mkdir build && cd build
    cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr/local -G Ninja
    ninja ..
    sudo ninja install
cd "$CURRENT_DIR"
exit 0
