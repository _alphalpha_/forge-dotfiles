#!/bin/sh

## https://github.com/drhelius/Gearsystem

project="gearsystem"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/drhelius/Gearsystem.git"

sudo apt install make git build-essential libsdl2-dev libglew-dev libgtk-3-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  cd platforms/linux
  make -j "$(nproc)" || exit 1

  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) 
      sudo make install
      sudo cat > /usr/local/share/applications/gearsystem.desktop <<EOF
[Desktop Entry]
Type=Application
Terminal=false
Exec=/usr/local/bin/gearsystem
Name=GearSystem
Icon=/usr/share/icons/Numix-Zafiro-Remix/48/apps/gamepad.svg
Categories=Game;Emulator;
EOF
      ;;
  esac

exit 0
