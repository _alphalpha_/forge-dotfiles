#!/bin/sh

## https://github.com/alx210/emwm

project="emwm"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/alx210/emwm.git"

sudo apt install make git libx11-dev libxt-dev libxext-dev libxrandr-dev libxinerama-dev libxm4 libmotif-dev
git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  make -j "$(nproc)" || exit 1

  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install;;
  esac

exit 0
