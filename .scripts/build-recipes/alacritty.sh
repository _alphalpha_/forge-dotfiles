#!/bin/sh

## alacritty https://github.com/alacritty/alacritty

printf "run this with script with xterm and do not open alacritty while this script is running\\n"
read -r ok

printf "checking https://github.com/alacritty/alacritty/releases for latest version of alacritty\\n"
DL_URL="https://github.com$(curl -s https://github.com/alacritty/alacritty/releases | grep -E -m1 "v?[0-9]?{3}\.[0-9]?{3}\.?[0-9]?{3}[a-z]?\.tar\.?[x|g]?z?" | cut -d\" -f 2)"
LATEST_RELEASE="$(echo "$DL_URL" | sed 's!https://github.com/alacritty/alacritty/archive/refs/tags/v!!;s!.tar.gz!!')"
FILE_NAME="${DL_URL##*/}"
LOCAL_VERSION="$(alacritty -V | awk '{print $2}')"

if [ ! "$(echo "$FILE_NAME" | grep -i alacritty)" ]; then
FILE_NAME="alacritty-$FILE_NAME"
fi

if [ "$LOCAL_VERSION" = "$LATEST_RELEASE" ]; then
  printf "Looks like the latest version is already installed...\\n" # && exit 0
else
  printf "Local version is %s\\n" "$LOCAL_VERSION"
  printf "Latest release is %s\\n" "$LATEST_RELEASE"
fi
printf "Download and compile the latest version? [y|n]\\n"
read -r ans
case "$ans" in
  N|n) exit 0;;
  Y|y) break;;
  *) printf "%s is not a valid answer, exiting script...\\n" "$ans"; exit 1;;
esac

CARGO="$HOME/.local/share/cargo"

if [ "$(whereis rustup | wc -w)" -lt 2 ] && [ ! $(ls "$CARGO/bin/rustup")] ; then
printf 'rustup is required to build alacritty\n'
printf 'install rustup from https://rustup.rs/ with the following command?\n'
printf 'curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh\n'
printf '[y|n]\n'
read -r ans
case "$ans" in
  N|n) exit 0;;
  Y|y) curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh;;
  *) printf "%s is not a valid answer, exiting script...\\n" "$ans"; exit 1;;
esac
fi

"$CARGO/bin/rustup" override set stable
"$CARGO/bin/rustup" update stable


sudo apt install cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev libxkbcommon-dev python3

CURRENT_DIR="$(pwd)"
[ -d ~/tmp ] || mkdir ~/tmp
cd ~/tmp
  DIR_NAME="$(echo "$FILE_NAME" | sed 's/.tar.gz//;s/.tar.xz//')"
  curl -o "$FILE_NAME" -JL "$DL_URL"
  [ -d "$DIR_NAME" ] && rm -rdf "$DIR_NAME"
  mkdir "$DIR_NAME" || exit 1
  case "$FILE_NAME" in
    *.tar.gz) tar xvzf "$FILE_NAME" --strip-components 1 -C "$DIR_NAME";;
    *.tar.xz) tar xvJf "$FILE_NAME" --strip-components 1 -C "$DIR_NAME";;
  esac
  cd "$DIR_NAME" || exit 1
    #cargo build --release
    "$CARGO/bin/cargo" build --release --no-default-features --features=x11
    sudo cp -v target/release/alacritty /usr/local/bin
    [ ! -f /usr/local/share/applications/Alacritty.desktop ] && \
    sudo cp -v extra/linux/Alacritty.desktop /usr/local/share/applications/Alacritty.desktop
    sudo cp -v extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg
    [ -d "/usr/local/share/man/man1" ] || sudo mkdir -pv /usr/local/share/man/man1
    if [ -f extra/alacritty.man ] ; then
      gzip -c extra/alacritty.man | sudo tee /usr/local/share/man/man1/alacritty.1.gz >> /dev/null
    fi
    if [ -f extra/alacritty-msg.man ]; then
      gzip -c extra/alacritty-msg.man | sudo tee /usr/local/share/man/man1/alacritty-msg.1.gz >> /dev/null
    fi
    [ -d "${ZDOTDIR:-~}/.zsh_functions" ] || mkdir -pv ${ZDOTDIR:-~}/.zsh_functions
    echo 'fpath+=${ZDOTDIR:-~}/.zsh_functions' >> ${ZDOTDIR:-~}/.zshrc
    cp -v extra/completions/_alacritty ${ZDOTDIR:-~}/.zsh_functions/_alacritty
cd "$CURRENT_DIR"
exit 0
