#!/bin/sh

## https://github.com/minetest/minetest

project="minetest"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/minetest/minetest.git"

sudo apt install git g++ make libc6-dev cmake libpng-dev libjpeg-dev \
  libxi-dev libgl1-mesa-dev libsqlite3-dev libogg-dev libvorbis-dev \
  libopenal-dev libfreetype6-dev zlib1g-dev libgmp-dev \
  libjsoncpp-dev libzstd-dev libluajit-5.1-dev gettext
# libcurl4-gnutls-dev


git clone --depth 1 "$git_url" "$build_dir"

cd "$build_dir" || exit 1

  git clone --depth 1 https://github.com/minetest/minetest_game.git games/minetest_game
  git clone --depth 1 https://github.com/minetest/irrlicht.git lib/irrlichtmt

  cmake . -DRUN_IN_PLACE=TRUE

  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install;;
  esac

exit 0
