#!/bin/sh

# https://tools.suckless.org/dmenu/
# https://git.suckless.org/dmenu

project="dmenu"
build_dir="$HOME/tmp/$project"
git_url="git://git.suckless.org/dmenu"

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  git checkout 5.0
  curl "https://tools.suckless.org/dmenu/patches/xyw/dmenu-xyw-5.0.diff" > xyz.diff
  curl "https://tools.suckless.org/dmenu/patches/border/dmenu-border-5.2.diff" > border.diff
  git apply xyz.diff
  git apply border.diff
  make -j "$(nproc)"
  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install;;
  esac

exit 0


