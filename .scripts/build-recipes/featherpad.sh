#!/bin/sh

# featherpad https://github.com/tsujan/FeatherPad
project="featherpad"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/tsujan/FeatherPad.git"

git clone "$git_url" "$build_dir"
sudo apt install make cmake g++ libx11-dev libxext-dev \
                 libqt5x11extras5-dev libqt5svg5-dev libhunspell-dev \
                 qt6-base-dev qt6-svg-dev
#                 qtbase5-dev qttools5-dev-tools 


if [ ! -d "$build_dir"/build ]; then
  mkdir -vp "$build_dir"/build
fi

cd "$build_dir"/build || exit 1
  cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr/local ..
  make -j $(nproc) || exit 1
  sudo make install
exit 0
