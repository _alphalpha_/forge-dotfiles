#!/bin/sh

## https://github.com/baskerville/xqp

project="xqp"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/baskerville/xqp.git"

sudo apt install make git gcc

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install;;
  esac

exit 0
