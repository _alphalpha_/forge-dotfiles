#!/bin/sh

## qps https://github.com/lxqt/qps

project="qps"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/lxqt/qps"


printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/[vV]?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }')"
dl_url="$git_url/archive/refs/tags/$version.tar.gz"
file_name="$project-$version.tar.gz"
printf "found version %s\\n" "$version"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then
  printf "installing dependencies with sudo apt install:"
  sudo apt install make cmake lxqt-build-tools liblxqt0-dev libqt5core5a libqt5gui5
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    printf "downloading %s to %s/%s\\n" "$dl_url" "$(pwd)" "$file_name"
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd "$project"-"$version" || exit 1
      mkdir build
      cd build
        cmake .. || exit 1
        make || exit 1
        echo "Install to /usr/local ? [Y|n]" 
        read -r ans
        case "$ans" in
          y|Y) sudo make install;;
        esac
  cd "$cur_dir"
fi
