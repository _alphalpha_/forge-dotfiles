#!/bin/sh

## patchage https://gitlab.com/drobilla/patchage

sudo apt install git libglibmm-2.4-dev libgtkmm-2.4-dev libganv-dev libfmt-dev

CURRENT_DIR="$(pwd)"

#git clone https://gitlab.com/drobilla/ganv.git ~/tmp/ganv
#cd ~/tmp/ganv
#  git submodule update --init --recursive
#  ./waf configure
#  ./waf
#  sudo ./waf install
#cd ..

git clone https://gitlab.com/drobilla/patchage.git ~/tmp/patchage
cd ~/tmp/patchage
  git submodule update --init --recursive
#  ./waf configure
#  ./waf
#  sudo ./waf install
  mkdir build
  cd build
    meson ..
    ninja
    sudo ninja install
  cd ..
cd ..

cd "$CURRENT_DIR"
exit 0
