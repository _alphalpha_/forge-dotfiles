#!/bin/sh

# rtl_433 https://github.com/merbanan/rtl_433
project="rtl_433"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/merbanan/rtl_433.git"

sudo apt install git make cmake autoconf pkg-config build-essential libtool libusb-1.0.0-dev librtlsdr-dev rtl-sdr
git clone "$git_url" "$build_dir"

if [ ! -d "$build_dir"/build ]; then
  mkdir -vp "$build_dir"/build
fi

cd "$build_dir"/build || exit 1
  cmake ..
  make -j $(nproc) || exit 1
  sudo make install
exit 0
