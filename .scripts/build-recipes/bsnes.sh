#!/bin/sh

## https://github.com/bsnes-emu/bsnes

sudo apt install git pkg-config libopenal-dev libao-dev
git clone https://github.com/bsnes-emu/bsnes.git ~/tmp/bsnes

cur_dir="$(pwd)"
cd ~/tmp/bsnes
#  make -C bsnes hiro=qt5
  make -C bsnes hiro=gtk3
  make -C bsnes hiro=gtk3 install
cd "$cur_dir"
exit 0



