#!/bin/sh

git_url="https://github.com/jurplel/qView"
project="$(echo "$git_url" | awk -F '/' '{print $NF}')"
build_dir="$HOME/tmp/$project"

printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
dl_url="$git_url/archive/refs/tags/$version.tar.gz"
file_name="$project-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

sudo apt-get install git make wget tar qt5-qmake libqt5core5a libqt5concurrent5

CURRENT_DIR="$(pwd)"

cd "$build_dir"
  wget -c "$dl_url" -O "$file_name"
  tar xzfv "$file_name"
    cd "$(echo "$file_name" | sed 's/.tar.gz$//')" || exit 1
    [ -d build ] || mkdir build
    cd build
      qmake ..
      make
      sudo cp bin/qview /usr/local/bin/qview
cd "$CURRENT_DIR"
exit 0
