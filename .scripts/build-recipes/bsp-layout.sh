#!/bin/sh

## bsp-layout https://github.com/phenax/bsp-layout

sudo apt install git make
git clone https://github.com/phenax/bsp-layout.git ~/tmp/bsp-layout

CURRENT_DIR="$(pwd)"
cd ~/tmp/bsp-layout
  sudo ./install.sh
cd "$CURRENT_DIR"
exit 0
