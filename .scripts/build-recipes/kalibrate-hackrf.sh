#!/bin/sh

## kalibrate-hackrf https://github.com/scateu/kalibrate-hackrf

sudo apt install git make automake libuhd-dev libhackrf-dev
git clone https://github.com/scateu/kalibrate-hackrf.git ~/tmp/kalibrate-hackrf

CURRENT_DIR="$(pwd)"
cd ~/tmp/kalibrate-hackrf
  ./bootstrap 
  ./configure
  make
  sudo make install
  sudo cp -v /usr/local/bin/kal /usr/local/bin/kal-hackrf
cd "$CURRENT_DIR"
exit 0
