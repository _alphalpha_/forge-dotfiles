#!/bin/sh

## https://github.com/schandinat/green

project="green"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/schandinat/green.git"

sudo apt-get install libpoppler-glib-dev libpoppler-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
   
  case "$ans" in
    y|Y) sudo cp green /usr/local/bin/green;;
  esac

exit 0
