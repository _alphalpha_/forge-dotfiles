#!/bin/sh

## https://github.com/ptrkrysik/gr-gsm
## https://osmocom.org/projects/gr-gsm/wiki/index
## https://osmocom.org/projects/gr-gsm/wiki/Installation

sudo apt install git make cmake  g++ gcc autoconf libtool pkg-config swig doxygen \
python3-scipy gr-osmosdr libosmocore gnuradio libc6 libc6-dev libosmocore-dev libosmocore-utils \
libcppunit-dev liblog4cpp5-dev

CURRENT_DIR="$(pwd)"

git clone https://git.osmocom.org/gr-gsm ~/tmp/gr-gsm
cd ~/tmp/gr-gsm
  mkdir build
  cd build
    cmake ..
    [ -d $HOME/.grc_gnuradio/ ] || mkdir $HOME/.grc_gnuradio
    [ -d $HOME/.gnuradio/ ] || mkdir $HOME/.gnuradio
    make || exit 1
    sudo make install
    sudo ldconfig
cd "$CURRENT_DIR"
exit 0
