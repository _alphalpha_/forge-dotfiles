#!/bin/sh

## xwinwrap https://github.com/ujjwal96/xwinwrap
# Example xwinwrap -g 400x400 -ni -s -nf -b -un -argb -sh circle -- gifview -w WID mygif.gif -a

sudo apt-get install git make xorg-dev build-essential libx11-dev x11proto-xext-dev libxrender-dev libxext-dev gifsicle
git clone https://github.com/ujjwal96/xwinwrap.git ~/tmp/xwinwrap

CURRENT_DIR="$(pwd)"
cd ~/tmp/xwinwrap
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
