#!/bin/sh

## vim https://github.com/vim/vim

sudo apt install make git
CURRENT_DIR="$(pwd)"
git clone https://github.com/vim/vim.git ~/tmp/vim
cd ~/tmp/vim
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
