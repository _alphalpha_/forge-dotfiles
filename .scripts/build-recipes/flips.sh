#!/bin/sh

# https://github.com/Alcaro/Flips
project="Flips"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/Alcaro/Flips.git"

sudo apt install git make g++ build-essential libgtk-3-dev pkg-config
git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  ./make.sh 
  sudo make install
exit 0
