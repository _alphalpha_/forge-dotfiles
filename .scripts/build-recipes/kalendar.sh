#!/bin/sh

## kalendar https://github.com/echo-devim/kalendar
##          https://github.com/Greggk/kalendar

#sudo apt install git make qt5-qmake libsqlite3-dev #qt5-default
git clone https://github.com/Greggk/kalendar.git ~/tmp/kalendar

sudo cp -v ~/tmp/kalendar/bin/Kalendar-stable-2.8.2-x86_64 /usr/local/bin/kalendar

#CURRENT_DIR="$(pwd)"
#cd ~/tmp/kalendar
#  sed -i 's:#define FOLDER_NAME "kalendar":#define FOLDER_NAME ".config/kalendar":' src/persistence/pmanager.h
#  sed -i 's:#define TOOLS_FOLDER "tools":#define TOOLS_FOLDER ".config/kalendar/tools":' src/util/pluginmanager.h
# NOTE: i hacked the source code to move shit out of my $HOME
#  cd src
#    qmake
#    make
#    sudo cp -v Kalendar /usr/local/bin/kalendar
#cd "$CURRENT_DIR"


cat > ~/tmp/kalendarkalendar.desktop <<EOF
[Desktop Entry]
Type=Application
Terminal=false
Exec=/usr/local/bin/kalendar
Name=Kalendar
EOF

sudo cp -v ~/tmp/kalendarkalendar.desktop /usr/local/share/applications/kalendar.desktop

# ~/.config/kalendar/tools needs to be created by hand or kalendar wont start
mkdir -p ~/.config/kalendar/tools

exit 0
