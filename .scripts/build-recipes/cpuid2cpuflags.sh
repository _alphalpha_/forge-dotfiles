#!/bin/sh

## cpuid2cpuflags https://github.com/mgorny/cpuid2cpuflags

sudo apt install git make automake autoconf
git clone https://github.com/mgorny/cpuid2cpuflags.git ~/tmp/cpuid2cpuflags

CURRENT_DIR="$(pwd)"
cd ~/tmp/cpuid2cpuflags
  autoconf
  autoreconf -i
  ./configure
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
