#!/bin/sh

## https://github.com/clbr/dgen

git clone https://github.com/clbr/dgen.git ~/tmp/dgen

CURRENT_DIR="$(pwd)"
cd ~/tmp/dgen
  ./autogen
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
