#!/bin/sh

## gearboy https://github.com/drhelius/Gearboy

sudo apt install git make libsdl2-dev libglew-dev
git clone https://github.com/drhelius/Gearboy.git ~/tmp/gearboy

CURRENT_DIR="$(pwd)"
cd ~/tmp/gearboy/platforms/linux || exit 1
  make || exit 1
  sudo cp -v gearboy /usr/local/bin/gearboy
cd "$CURRENT_DIR"
exit 0
