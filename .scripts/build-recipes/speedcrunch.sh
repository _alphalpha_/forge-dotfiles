#!/bin/sh

## https://bitbucket.org/heldercorreia/speedcrunch

url="https://bitbucket.org/heldercorreia/speedcrunch.git"

git clone https://bitbucket.org/heldercorreia/speedcrunch.git ~/tmp/speedcrunch
git checkout "$(git ls-remote --tags "$url" | awk '/release/ && !/\^/ {print $1}' | tail -n1)"

CURRENT_DIR="$(pwd)"
cd ~/tmp/speedcrunch  mkdir build
  cd build
    cmake ../src
    sudo make install
cd "$CURRENT_DIR"
exit 0
