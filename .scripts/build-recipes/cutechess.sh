#!/bin/sh

## cutechess https://github.com/cutechess/cutechess

sudo apt install git make qt5-qmake-bin qt5-qmake libqt5svg5-dev libkf5coreaddons-dev
git clone https://github.com/cutechess/cutechess.git ~/tmp/cutechess

CURRENT_DIR="$(pwd)"
cd ~/tmp/cutechess
#  /usr/lib/qt5/bin/qmake
#  make check

  mkdir build
  cd build
    cmake ..
    make -j $(nproc)
    sudo make install
    sudo cp -v ~/tmp/cutechess/projects/gui/cutechess /usr/local/bin/cutechess
cd "$CURRENT_DIR"
exit 0
