#!/bin/sh

## kalibrate-rtl https://github.com/steve-m/kalibrate-rtl

sudo apt install git make automake libuhd-dev librtlsdr-dev
git clone https://github.com/steve-m/kalibrate-rtl.git ~/tmp/kalibrate-rtl

CURRENT_DIR="$(pwd)"
cd ~/tmp/kalibrate-rtl
  ./bootstrap 
  ./configure
  make
  sudo make install
  sudo cp -v /usr/local/bin/kal /usr/local/bin/kal-sdr
cd "$CURRENT_DIR"
exit 0
