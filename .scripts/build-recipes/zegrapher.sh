#!/bin/sh

## https://github.com/AdelKS/ZeGrapher

project="zegrapher"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/AdelKS/ZeGrapher.git"

sudo apt install make git meson libboost-all-dev
git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  meson setup build
  cd build
    meson compile

  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo ninja install;;
  esac

exit 0
