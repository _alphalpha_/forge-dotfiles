#!/bin/sh

## dump1090 https://github.com/antirez/dump1090

sudo apt install git make librtlsdr-dev pkg-config
git clone https://github.com/antirez/dump1090.git ~/tmp/dump1090

CURRENT_DIR="$(pwd)"
cd ~/tmp/dump1090
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
