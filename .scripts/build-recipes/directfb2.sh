#!/bin/sh

# flux https://github.com/deniskropp/flux (needed to compile DirectFB2)
project="flux"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/deniskropp/flux.git"

#git clone --recurse-submodules "$git_url" "$build_dir"
#cd "$build_dir" || exit 1
#  autoreconf -fi
#  ./configure
#  make -j $(nproc) || exit 1
#  sudo make install
  
# DirectFB2 https://github.com/directfb2/DirectFB2
project="DirectFB2"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/directfb2/DirectFB2.git"

git clone --recurse-submodules "$git_url" "$build_dir"
cd "$build_dir" || exit 1
  meson setup -Dmulti=true build/ 
  meson compile -C build/ 
  sudo meson install -C build/ 

# DirectFB-examples https://github.com/directfb2/DirectFB-examples
project="DirectFB-examples"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/directfb2/DirectFB-examples.git"

git clone --recurse-submodules "$git_url" "$build_dir"
cd "$build_dir" || exit 1
  meson setup build/ 
  meson compile -C build/ 
  sudo meson install -C build/ 

# DirectFB2-media https://github.com/directfb2/DirectFB2-media
project="DirectFB2-media"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/directfb2/DirectFB2-media.git"

git clone --recurse-submodules "$git_url" "$build_dir"
cd "$build_dir" || exit 1
  meson setup build/ 
  meson compile -C build/ 
  sudo meson install -C build/ 

exit 0
