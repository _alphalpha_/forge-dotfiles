#!/bin/sh

## https://github.com/Jack12816/colorpicker

sudo apt install git make libxfixes-dev libxcomposite-dev libx11-dev libgtk2.0-dev
git clone https://github.com/Jack12816/colorpicker.git ~/tmp/colorpicker

CURRENT_DIR="$(pwd)"
cd ~/tmp/colorpicker
  make
  sudo cp -v colorpicker /usr/local/bin/colorpicker
cd "$CURRENT_DIR"
exit 0
