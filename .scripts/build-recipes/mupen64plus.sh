#!/bin/sh

project="mupen64plus"
build_dir="$HOME/tmp/$project"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

cd "$build_dir"
git clone https://github.com/mupen64plus/mupen64plus-core.git
sudo apt install make cmake nasm git libsdl2-2.0-0 libsdl2-dev libpng16-16 libpng-dev libfreetype6 libfreetype-dev zlib1g zlib1g-dev
cd mupen64plus-core/projects/unix
make all
sudo make install

cd "$build_dir"
git clone https://github.com/mupen64plus/mupen64plus-input-sdl.git
cd mupen64plus-input-sdl/projects/unix
make all
sudo make install

cd "$build_dir"
git clone https://github.com/mupen64plus/mupen64plus-video-rice.git
cd mupen64plus-video-rice/projects/unix
make all
sudo make install

cd "$build_dir"
git clone https://github.com/mupen64plus/mupen64plus-rsp-hle.git
cd mupen64plus-rsp-hle/projects/unix
make all
sudo make install

cd "$build_dir"
git clone https://github.com/mupen64plus/mupen64plus-audio-sdl.git
cd mupen64plus-audio-sdl/projects/unix
make all
sudo make install

cd "$build_dir"
git clone https://github.com/mupen64plus/mupen64plus-ui-console.git
cd mupen64plus-ui-console/projects/unix
make all
sudo make install
