#!/bin/sh

## https://github.com/rncbc/qmidictl

project="qmidictl"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/rncbc/qmidictl.git"

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  if [ ! -d build ]; then
    mkdir build
  fi

  cmake -DCMAKE_INSTALL_PREFIX=/usr/local -B build
  cmake --build build --parallel $(nproc)

    echo "Install $project to /usr/local ? [Y|n]"
    read -r ans
    if [ -z "$ans" ]; then
      ans="y"
    fi
    case "$ans" in
      y|Y) sudo cmake --install build;;
    esac
exit 0
