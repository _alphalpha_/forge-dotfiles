#!/bin/sh
# https://github.com/AppImageCommunity/pkg2appimage/blob/master/legacy/googleearth/Recipe
# The Google Earth license does not allow to redistribute it, but you can generate
# your own Google Earth AppImage by running this script on a x86_64 Linux system:

# NOTE: This does not work anymore
#wget https://raw.githubusercontent.com/AppImageCommunity/pkg2appimage/master/legacy/googleearth/Recipe
#bash -ex Recipe


# just download deb package from google and install
wget -c https://dl.google.com/dl/earth/client/current/google-earth-stable_current_amd64.deb
sudo dpkg -i google-earth-stable_current_amd64.deb
