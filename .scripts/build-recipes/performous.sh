#!/bin/sh

## http://performous.org/
## https://github.com/performous/performous


project="performous"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/performous/performous"
printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/[vV]?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }')"
dl_url="$git_url/archive/refs/tags/$version.tar.gz"
file_name="$project-$version.tar.gz"


if [ -n "$file_name" ] && [ "$version" ]; then

  if [ ! -d "$build_dir/$project-$version" ]; then
    mkdir -vp "$build_dir/$project-$version"
  fi

  sudo apt install make cmake build-essential gettext help2man libopenblas-dev \
                   libfftw3-dev libicu-dev libepoxy-dev libsdl2-dev libfreetype6-dev \
                   libpango1.0-dev librsvg2-dev libxml++2.6-dev libavcodec-dev \
                   libavformat-dev libswscale-dev libjpeg-dev portaudio19-dev \
                   libglm-dev libboost-all-dev nlohmann-json3-dev libaubio-dev \
                   libfmt-dev libportmidi-dev libopencv-dev

  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    printf "Downloading %s" "$dl_url"
    curl -L -o "$file_name" -C - "$dl_url"
    
    if echo "$file_name" | grep ".tar.bz2"; then
      tar xvjf "$file_name" --strip-components 1 -C "$project-$version"
    elif echo "$file_name" | grep ".tar.gz"; then
      tar xvzf "$file_name" --strip-components 1 -C "$project-$version"
    elif echo "$file_name" | grep ".tar.xz"; then
      tar xvJf "$file_name" --strip-components 1 -C "$project-$version"
    else
      exit 1
    fi
    
    cd "$project-$version" || exit 1
  
    if [ ! -d build ]; then
      mkdir -vp build
    fi

    cd build || exit 1
      cmake ..
      make -j $(nproc)
      echo "Install $project to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install;;
      esac
  cd "$cur_dir"
fi

printf "you can find some free songs here: http://performous.org/songs\\n"
printf "put them in ~/.local/share/games/performous\\n"

exit 0

