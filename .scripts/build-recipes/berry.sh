#!/bin/sh

## https://github.com/JLErvin/berry/

project="berry"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/JLErvin/berry.git"

sudo apt install make git gcc libx11-dev libxft-dev libxinerama-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install
         if [ ! -d "$HOME/.config/berry" ]; then
           mkdir -v $HOME/.config/berry
           cp -v examples/sxhkdrc $HOME/.config/berry/sxhkdrc
           cp -v examples/autostart $HOME/.config/berry/autostart
         fi;;
  esac

exit 0
