#!/bin/sh

## yad https://github.com/v1cont/yad

build_dir="$HOME/tmp/yad"

printf "checking https://github.com/v1cont/yad/releases for latest release version\n"
version="$(wget --spider  "https://github.com/v1cont/yad/releases/latest" 2>&1 \
  | grep -m1 -o -E "https://github.com/v1cont/yad/releases/tag/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"

dl_url="https://github.com/v1cont/yad/archive/refs/tags/v$version.tar.gz"
file_name="yad-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

echo "$dl_url"

if [ -n "$file_name" ] && [ "$version" ]; then
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    sudo apt install git make cmake automake autoconf intltool libgtk-3-dev libwebkit2gtk-4.0-dev
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd yad-"$version" || exit 1
      autoreconf -ivf && intltoolize
      ./configure
      make -j $(nproc) && echo "Build complete !" || exit 1
      echo "Install to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install; gtk-update-icon-cache ;;
      esac
  cd "$cur_dir"
fi
exit 0
