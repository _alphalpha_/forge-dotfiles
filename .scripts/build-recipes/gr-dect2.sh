#!/bin/sh

## gr-dect2 https://github.com/SignalsEverywhere/gr-dect2

sudo apt install git make cmake
git clone git://github.com/pavelyazev/gr-dect2.git ~/tmp/gr-dect2
git checkout pyqt4

CURRENT_DIR="$(pwd)"
cd ~/tmp/gr-dect2
  mkdir build
  cd build
    cmake ..
    make
    sudo make install
    sudo ldconfig
  cd ..
  cp -v grc/*.grc ~/
cd "$CURRENT_DIR"
exit 0
