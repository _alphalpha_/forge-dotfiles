#!/bin/sh

## rofi https://github.com/davatorium/rofi
build_dir="$HOME/tmp/rofi"
sudo apt install make automake autoconf gcc pkg-config flex bison check \
libglib2.0-dev libpango1.0-dev libpangocairo-1.0-0 libcairo2-dev gir1.2-glib-2.0 \
libgdk-pixbuf2.0-dev libstartup-notification0-dev libxkbcommon-dev libxkbcommon-x11-dev \
libxcb-randr0-dev libxcb-xinerama0-dev libxcb-xkb-dev libxcb-util-dev libxcb-ewmh-dev \
libxcb-icccm4-dev libxcb-cursor-dev


#printf "checking https://github.com/davatorium/rofi/releases for latest version of rofi\n"

#version="$(wget --spider  "https://github.com/davatorium/rofi/releases/latest" 2>&1 \
#  | grep -m1 -o -E "https://github.com/davatorium/rofi/releases/tag/v?[0-9]?{3}\.[0-9]?{3}\.[0-9]?{3}" \
#  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
#echo "version = $version"
#dl_url="https://github.com/davatorium/rofi/archive/refs/tags/$version.tar.gz"
#file_name="rofi-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

#if [ -n "$file_name" ]; then
#  cur_dir="$(pwd)"
#  cd "$build_dir" || exit 1
#    curl -L -o "$file_name" -C - "$dl_url"
#    case "$file_name" in
#      *.tar.gz) tar xvzf "$file_name";;
#      *.tar.xz) tar xvJf "$file_name";;
#    esac
#    cd rofi-"$version" || exit 1
#      if [ ! -d "build" ]; then
#        mkdir -v "build"
#      fi
#      cd build
#        ../configure
#        make -j $(nproc) && echo "Build complete !" || exit 1
#        echo "Install to /usr/local ? [Y|n]"
#        read -r ans
#        case "$ans" in
#          y|Y) sudo make install;;
#        esac
#  cd "$cur_dir"
#fi

cur_dir="$(pwd)"
cd "$build_dir"
  git clone --recursive  "https://github.com/davatorium/rofi" || exit 1
  cd rofi
    autoconf
    automake
    mkdir build && cd build || exit 1
    ../configure
    meson setup .. || exit 1
    ninja || exit 1
    sudo ninja install
cd "$cur_dir"
exit 0


## example config
#/** Basic config file **/
#configuration {
#  show-icons:       true;
#  font:             "Dejavu 20";
#  icon-theme:       "Numix-Zafiro-Remix";
#  terminal:         "rxvt-unicode";
#}
#@theme "~/.config/rofi/gruvbox-light.rasi"
