#!/bin/sh

## haschcat https://github.com/hashcat/hashcat

sudo apt install git make
git clone https://github.com/hashcat/hashcat.git ~/tmp/hashcat

CURRENT_DIR="$(pwd)"
cd ~/tmp/hashcat
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
