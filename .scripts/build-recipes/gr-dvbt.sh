#!/bin/sh

## gr-dvbt https://github.com/BogdanDIA/gr-dvbt

sudo apt install git make cmake
git clone https://github.com/BogdanDIA/gr-dvbt.git ~/tmp/gr-dvbt

CURRENT_DIR="$(pwd)"
cd ~/tmp/gr-dvbt
  mkdir build
  cd build
    cmake ../
    make || exit 1
    sudo make install
    sudo ldconfig
cd "$CURRENT_DIR"
exit 0
