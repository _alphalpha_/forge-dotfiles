#!/bin/sh

## https://github.com/NsCDE/NsCDE

project="NsCDE"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/NsCDE/NsCDE.git"

sudo apt install make git libevent-dev libx11-dev libxrandr-dev libxrender-dev libxt-dev libxft-dev asciidoctor \
  libfontconfig-dev libfreetype6-dev libfribidi-dev libncurses5-dev libpng-dev libreadline-dev librsvg2-dev libsm-dev \
  libxcursor-dev libxext-dev libxi-dev libxpm-dev python3-xdg ksh

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  ./autogen.sh
  ./configure --enable-mandoc
  make -j "$(nproc)" || exit 1

  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y)
sudo make install

# moves nscde applications out of /usr/local/share/applications
# so they dont show in rofi when using other window managers

if [ ! -d /usr/local/opt/applications ]; then
  sudo mkdir -p /usr/local/opt/applications
fi

if ls /usr/local/share/applications/nscde-* 2>/dev/null; then
  for f in $(ls /usr/local/share/applications/nscde-*); do
    sudo mv -v $f /usr/local/opt/applications
  done
fi
;;
  esac

exit 0
