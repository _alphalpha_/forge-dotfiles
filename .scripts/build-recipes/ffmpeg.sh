#!/bin/sh

# https://ffmpeg.org
project="ffmpeg"
build_dir="$HOME/tmp/$project"
git_url="https://git.ffmpeg.org/ffmpeg.git"

sudo apt-get update -qq && sudo apt-get -y install \
  autoconf automake build-essential cmake git-core \
  libass-dev libfreetype6-dev libgnutls28-dev libmp3lame-dev \
  libsdl2-dev libtool libva-dev libvdpau-dev libvorbis-dev \
  libxcb1-dev libxcb-shm0-dev libxcb-xfixes0-dev \
  meson ninja-build pkg-config texinfo wget \
  nasm yasm zlib1g-dev libx264-dev libvpx-dev libopus-dev \
  libdav1d-dev frei0r-plugins-dev librubberband-dev \
  libsoxr-dev libxvidcore-dev libshine-dev libfontconfig-dev \
  libcdio-dev libcodec2-dev libaom-dev libbluray-dev \
  libcaca-dev flite1-dev libfreetype-dev libfribidi-dev \
  libwebp-dev libopenimageio-dev libopenal-dev ladspa-sdk \
  libgme-dev libmysofa-dev libopenjp2-7-dev libopenmpt-dev \
  librabbitmq-dev libtwolame-dev libcdio-paranoia-dev \
  librsvg2-dev

git clone "$git_url" "$build_dir"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

cd "$build_dir" || exit 1

  ./configure \
  --prefix=/usr/local --extra-version=0+local --toolchain=hardened \
  --libdir=/usr/lib/x86_64-linux-gnu --incdir=/usr/include/x86_64-linux-gnu \
  --arch=amd64 --enable-gpl --disable-stripping --disable-filter=resample \
  --enable-ladspa --enable-libcaca --enable-libflite --enable-libfreetype \
  --enable-libaom --enable-librsvg --enable-libshine --enable-librabbitmq \
  --enable-libass --enable-libx265 --enable-libspeex --enable-libopenjpeg \
  --enable-libssh --enable-libx264 --enable-libdav1d --enable-chromaprint \
  --enable-libdrm --enable-libxml2 --enable-libcodec2 --enable-libvidstab \
  --enable-libgme --enable-libwebp --enable-libtheora --enable-libopenmpt \
  --enable-openal --enable-libjack --enable-libsnappy --enable-libwavpack \
  --enable-opencl --enable-libcdio --enable-libdc1394 --enable-libfribidi \
  --enable-opengl --enable-libopus --enable-libmysofa --enable-libmp3lame \
  --enable-frei0r --enable-libsoxr --enable-libvorbis --enable-libtwolame \
  --enable-libgsm --enable-libxvid --enable-libbluray --enable-libiec61883 \
  --enable-libzmq --enable-libzvbi --enable-sdl2 --enable-lv2 --enable-omx \
  --enable-libmfx --enable-libvpx  --enable-pocketsphinx --enable-libfontconfig \
  --enable-shared --enable-librubberband --extra-libs="-lpthread -lm" \
  || exit 1


# --enable-avresample  --enable-libsrt --enable-gnutls


  make -j "$(nproc)" || exit 1
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install;;
  esac
  
exit 0
