#!/bin/sh

## Composer https://github.com/performous/composer

sudo apt install git make cmake libavutil-dev libavcodec-dev libavcodec-extra libavcodec-extra58 \
libavformat-dev libswscale-dev qtmultimedia5-dev
git clone https://github.com/performous/composer.git ~/tmp/composer

CURRENT_DIR="$(pwd)"
cd ~/tmp/composer
  mkdir build
  cd build
    cmake ..
    make || exit 1
    sudo make install
cd "$CURRENT_DIR"
exit 0
