#!/bin/sh

## https://github.com/parazyd/live-sdk
## https://git.devuan.org/devuan-sdk/live-sdk.git
## https://dev1galaxy.org/viewtopic.php?id=551

project="devuan-sdk"
build_dir="$HOME/tmp/$project"
git_url="https://git.devuan.org/devuan-sdk/live-sdk.git"

sudo apt install make git xorriso squashfs-tools \
 live-boot syslinux-common

git clone --recurse-submodules "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  git submodule update --init --recursive --checkout

  make -j "$(nproc)" || exit 1

  echo "Install $project to $HOME/.local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) mv "$build_dir" "$HOME/.local" ;;
  esac

exit 0
