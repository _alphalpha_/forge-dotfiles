#!/bin/sh

## keepassxc https://github.com/keepassxreboot/keepassxc

project="keepassxc"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/keepassxreboot/keepassxc"
printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/[vV]?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }')"
dl_url="$git_url/archive/$version.tar.gz"
file_name="$project-$version.tar.gz"


if [ -n "$file_name" ] && [ "$version" ]; then

  if [ ! -d "$build_dir/$project-$version" ]; then
    mkdir -vp "$build_dir/$project-$version"
  fi
  sudo apt install make cmake g++ asciidoctor qtbase5-dev \
         qtbase5-private-dev libqt5svg5-dev qttools5-dev-tools \
         libbotan-2-dev libargon2-dev zlib1g-dev libminizip-dev libreadline-dev \
         libqt5x11extras5-dev libxi-dev libxtst-dev \
         libqrencode-dev libusb-1.0-0-dev libpcsclite-dev

  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    printf "Downloading %s" "$dl_url"
    curl -L -o "$file_name" -C - "$dl_url"
    
    if echo "$file_name" | grep ".tar.bz2"; then
      tar xvjf "$file_name" --strip-components 1 -C "$project-$version"
    elif echo "$file_name" | grep ".tar.gz"; then
      tar xvzf "$file_name" --strip-components 1 -C "$project-$version"
    elif echo "$file_name" | grep ".tar.xz"; then
      tar xvJf "$file_name" --strip-components 1 -C "$project-$version"
    else
      exit 1
    fi
    
    cd "$project-$version" || exit 1
  
    if [ ! -d build ]; then
      mkdir -vp build
    fi


    cd build || exit 1

#########################################################################################################################
#cmake options

#-DWITH_XC_AUTOTYPE=[ON|OFF] Enable/Disable Auto-Type (default: ON)
#-DWITH_XC_YUBIKEY=[ON|OFF] Enable/Disable YubiKey HMAC-SHA1 authentication support (default: OFF)
#-DWITH_XC_BROWSER=[ON|OFF] Enable/Disable KeePassXC-Browser extension support (default: OFF)
#-DWITH_XC_NETWORKING=[ON|OFF] Enable/Disable Networking support (e.g., favicon downloading) (default: OFF)
#-DWITH_XC_SSHAGENT=[ON|OFF] Enable/Disable SSHAgent support (default: OFF)
#-DWITH_XC_FDOSECRETS=[ON|OFF] (Linux Only) Enable/Disable Freedesktop.org Secrets Service support (default:OFF)
#-DWITH_XC_KEESHARE=[ON|OFF] Enable/Disable KeeShare group synchronization extension (default: OFF)
#-DWITH_XC_ALL=[ON|OFF] Enable/Disable compiling all plugins above (default: OFF)

#-DWITH_XC_UPDATECHECK=[ON|OFF] Enable/Disable automatic updating checking (requires WITH_XC_NETWORKING) (default: ON)

#-DWITH_TESTS=[ON|OFF] Enable/Disable building of unit tests (default: ON)
#-DWITH_GUI_TESTS=[ON|OFF] Enable/Disable building of GUI tests (default: OFF)
#-DWITH_DEV_BUILD=[ON|OFF] Enable/Disable deprecated method warnings (default: OFF)
#-DWITH_ASAN=[ON|OFF] Enable/Disable address sanitizer checks (Linux / macOS only) (default: OFF)
#-DWITH_COVERAGE=[ON|OFF] Enable/Disable coverage tests (GCC only) (default: OFF)
#-DWITH_APP_BUNDLE=[ON|OFF] Enable Application Bundle for macOS (default: ON)

#-DKEEPASSXC_BUILD_TYPE=[Snapshot|PreRelease|Release] Set the build type to show/hide stability warnings (default: "Snapshot")
#-DKEEPASSXC_DIST_TYPE=[Snap|AppImage|Other] Specify the distribution method (default: "Other")
#-DOVERRIDE_VERSION=[X.X.X] Specify a version number when building. Used with snapshot builds (default: "")
#-DGIT_HEAD_OVERRIDE=[XXXXXXX] Specify the 7 digit git commit ref for this build. Used with distribution builds (default: "")
#########################################################################################################################


#      cmake -DWITH_XC_ALL=ON ..
      cmake -DWITH_XC_AUTOTYPE=ON -DWITH_ASAN=OFF -DWITH_APP_BUNDLE=OFF -DWITH_XC_YUBIKEY=OFF -DKEEPASSXC_BUILD_TYPE=Release ..
      make
      echo "Install $project to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install;;
      esac
  cd "$cur_dir"
fi
exit 0

