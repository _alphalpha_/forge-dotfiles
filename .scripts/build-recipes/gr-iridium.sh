#!/bin/sh

## gr-iridium https://github.com/muccc/gr-iridium

sudo apt install git make cmake swig libcppunit-dev gnuradio-dev
git clone https://github.com/muccc/gr-iridium.git ~/tmp/gr-iridium

CURRENT_DIR="$(pwd)"
cd ~/tmp/gr-iridium
  git checkout maint-3.8
  mkdir build
  cd build
    cmake ..
    make
    sudo make install
    sudo ldconfig
cd "$CURRENT_DIR"
exit 0
