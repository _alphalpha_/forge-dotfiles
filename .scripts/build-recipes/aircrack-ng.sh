#!/bin/sh

## aircrack-ng https://github.com/aircrack-ng/aircrack-ng

printf "checking https://github.com/aircrack-ng/aircrack-ng/releases/latest for latest version\\n"
URL="$(curl -s https://github.com/aircrack-ng/aircrack-ng/releases/latest | grep -o -E "https://github.com/aircrack-ng/aircrack-ng/releases/tag/v?[0-9]?{3}\.[0-9]?{3}\.?[0-9]?{3}[a-z]?")"
DL_URL="https://github.com$(curl -s "$URL" | grep -o -E "/aircrack-ng/aircrack-ng/archive/refs/tags/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}[a-z]?\.tar\.?[x|g]?z?")"
FILE_NAME="$(echo "$DL_URL" | awk -F/  '{print $NF; exit}')"

sudo apt-get install git make wget build-essential autoconf automake shtool libtool pkg-config \
pciutils usbutils ethtool wpasupplicant tcpdump screen iw hostapd \
libnl-3-dev libnl-genl-3-dev libssl-dev ethtool rfkill zlib1g-dev \
libpcap-dev libsqlite3-dev libpcre3-dev libhwloc-dev libcmocka-dev || exit 1

# build fails with gcc10
sudo apt install gcc-9

CURRENT_DIR="$(pwd)"
[ -d ~/tmp ] || mkdir ~/tmp
cd ~/tmp

wget -c "$DL_URL" -O "aircrack-ng-$FILE_NAME"
tar xzfv "aircrack-ng-$FILE_NAME"
cd "$(echo "aircrack-ng-$FILE_NAME" | sed 's/.tar.gz$//')" || exit 1
  #env NOCONFIGURE=1 ./autogen.sh
  ./autogen.sh
  env CC=gcc-9 ./configure --with-experimental
  make
  make check
  sudo make install
  sudo ldconfig
cd "$CURRENT_DIR"
exit 0
