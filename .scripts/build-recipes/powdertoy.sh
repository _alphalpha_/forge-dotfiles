#!/bin/sh

## https://powdertoy.co.uk/Download.html
## https://github.com/The-Powder-Toy/The-Powder-Toy

sudo apt install g++ git python3 meson ninja-build ccache pkg-config libluajit-5.1-dev libcurl4-openssl-dev libfftw3-dev zlib1g-dev libsdl2-dev
git clone https://github.com/The-Powder-Toy/The-Powder-Toy.git ~/tmp/powdertoy

CURRENT_DIR="$(pwd)"
cd ~/tmp/powdertoy
  meson -Dbuildtype=release build-release
  cd build-release
    ninja
    [ -f powder ] && sudo cp -v powder /usr/local/bin/powder
cd "$CURRENT_DIR"
exit 0
