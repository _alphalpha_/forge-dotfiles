#!/bin/sh
## xwallpaper https://github.com/stoeckmann/xwallpaper
build_dir="$HOME/tmp/xwallpaper"

sudo apt install make libpixman-1-dev libxcb-image0-dev libxcb-util0-dev libpng-dev libjpeg-dev \
libjpeg62-turbo libpng16-16 libxpm4
#git clone https://github.com/stoeckmann/xwallpaper.git ~/tmp/xwallpaper

version="$(wget --spider  "https://github.com/stoeckmann/xwallpaper/releases/latest" 2>&1 \
  | grep -m1 -o -E "https://github.com/stoeckmann/xwallpaper/releases/tag/v?[0-9]?{3}\.[0-9]?{3}\.[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
echo "version = $version"

dl_url="https://github.com/stoeckmann/xwallpaper/releases/download/v$version/xwallpaper-$version.tar.xz"
file_name="xwallpaper-$version.tar.xz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ]; then
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    curl -L -o "$file_name" -C - "$dl_url"
    file "$file_name" && tar xvJf "$file_name"
    cd xwallpaper-"$version" || exit 1
      ./autogen.sh
      ./configure
      make || exit 1
      sudo make install
  cd "$cur_dir"
fi
exit 0
