#!/bin/sh

## tpb
## http://download.savannah.nongnu.org/releases/tpb/tpb-0.6.4.tar.gz
## Dependencies: adduser, libc6 (>= 2.14), libice6 (>= 1:1.0.0), libsm6, libx11-6, libxext6, libxinerama1, libxosd2 (>= 2.2.14), debconf (>= 0.5) | debconf-2.0

url="http://download.savannah.nongnu.org/releases/tpb/tpb-0.6.4.tar.gz"
tmpdir="$HOME/tmp/tpb"
curdir="$(pwd)"

if [ ! -d "$tmpdir" ]; then
  mkdir -vp "$tmpdir"
fi

curl -L -o "$tmpdir"/tpb-0.6.4.tar.gz --url "$url" || exit 1
tar xvzf "$tmpdir"/tpb-0.6.4.tar.gz -C "$tmpdir" || exit 1

cd "$tmpdir/tpb-0.6.4" || exit 1
  make && sudo make install
cd "$curdir" || exit 1
exit 0
