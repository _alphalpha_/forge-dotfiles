#!/bin/sh

## sameboy https://github.com/LIJI32/SameBoy

sudo apt install git make clang libsdl2-dev
git clone https://github.com/gbdev/rgbds.git ~/tmp/rgbds
git clone https://github.com/LIJI32/SameBoy.git ~/tmp/sameboy


cur_dir="$(pwd)"
cd ~/tmp/rgbds || exit 1
  make || exit 1
  sudo make install
cd ~/tmp/sameboy || exit 1
  CC=clang make
  sudo make install
cd "$cur_dir"
exit 0
