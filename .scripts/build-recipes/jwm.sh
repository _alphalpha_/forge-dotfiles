#!/bin/sh
## JWM
## https://github.com/joewing/jwm
## http://joewing.net/projects/jwm/
build_dir="$HOME/tmp/jwm"
sudo apt install make libjpeg-dev libpng16-16 libpng-dev libxext6 libxext-dev libxrender1 libxrender-dev \
librsvg2-2 librsvg2-common librsvg2-dev libfribidi0 libfribidi-dev libcairo2 libcairo2-dev libxmu6 libxmu-dev \
libxft2 libxft-dev libxinerama1 libxinerama-dev libpango1.0-dev libpangoxft-1.0-0 libxpm4 libxpm-dev

printf "checking https://github.com/joewing/jwm/releases for latest release version of JWM\\n"
version="$(wget --spider  "https://github.com/joewing/jwm/releases/latest" 2>&1 \
  | grep -m1 -o -E "https://github.com/joewing/jwm/releases/tag/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
echo "version = $version"
dl_url="https://github.com/joewing/jwm/archive/refs/tags/v$version.tar.gz"
file_name="jwm-$version.tar.gz"
printf "found version %s\\n" "$version"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd jwm-"$version" || exit 1
      ./autogen.sh
      ./configure
      make -j $(nproc) && echo "Build complete !" || exit 1
      echo "Install to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install;;
      esac
  cd "$cur_dir"
fi
exit 0
