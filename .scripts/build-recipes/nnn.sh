#!/bin/sh

## https://github.com/jarun/nnn/wiki/Usage

project="nnn"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/jarun/nnn.git"

sudo apt-get install pkg-config libncursesw5-dev libreadline-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
#  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
   
  case "$ans" in
    y|Y) sudo make strip install
         if [ ! -d "$HOME"/.config/nnn/plugins ]; then
           mkdir -vp "$HOME"/.config/nnn/plugins
           cp -v ./plugins/* "$HOME"/.config/nnn/plugins
         fi;;
  esac

exit 0
