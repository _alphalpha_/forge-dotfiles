#!/bin/sh

## bspwm https://github.com/baskerville/bspwm

sudo apt install git make libxcb-xinerama0-dev libxcb-icccm4-dev libxcb-util0-dev \
libxcb-ewmh-dev libxcb-keysyms1-dev libxcb-randr0-dev libxcb-shape0-dev
git clone https://github.com/baskerville/bspwm.git ~/tmp/bspwm

CURRENT_DIR="$(pwd)"
cd ~/tmp/bspwm
  make || exit 1
  sudo make install
cd "$CURRENT_DIR"
exit 0
