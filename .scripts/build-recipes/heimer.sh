#!/bin/sh

## Heimer https://github.com/juzzlin/Heimer.git

git_url="https://github.com/juzzlin/Heimer"
project="$(echo "$git_url" | awk -F '/' '{print $NF}')"
build_dir="$HOME/tmp/$project"

printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
dl_url="$git_url/archive/refs/tags/$version.tar.gz"
file_name="$project-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then
  sudo apt install git make cmake qttools5-dev-tools qttools5-dev libqt5svg5-dev #qt5-default
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    printf "Downloading %s" "$dl_url"
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"

    cd "$project"-"$version" || exit 1
    if [ ! -d build ]; then
      mkdir build
    fi
    cd build
      cmake ..  || exit 1
      make || exit 1
      sudo make install
  cd "$cur_dir"

# change launcher icon
  if grep -q 'Icon=heimer' /usr/local/share/applications/heimer.desktop \
  && [ -f /usr/share/icons/Numix-Zafiro-Remix/48/apps/mindmap.svg ]; then
    sudo sed -i 's:Icon=heimer:Icon=/usr/share/icons/Numix-Zafiro-Remix/48/apps/mindmap.svg:' /usr/local/share/applications/heimer.desktop
  fi
fi
exit 0
