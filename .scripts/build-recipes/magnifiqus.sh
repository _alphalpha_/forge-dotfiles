#!/bin/sh

## https://github.com/redtide/magnifiqus

git clone https://github.com/redtide/magnifiqus.git ~/tmp/magnifiqus
CURRENT_DIR="$(pwd)"
cd ~/tmp/magnifiqus
  [ -d build ] || mkdir build
    cd build
    cmake ..
    make
    sudo make install
cd "$CURRENT_DIR"
exit 0
