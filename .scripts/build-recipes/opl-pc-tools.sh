#!/bin/sh

# https://github.com/brainstream/OPL-PC-Tools
project="OPL-PC-Tools"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/brainstream/OPL-PC-Tools.git"

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  mkdir build && cd build
  cmake ..
  make -j $(nproc)
exit 0
