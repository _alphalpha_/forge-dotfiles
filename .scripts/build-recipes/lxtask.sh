#!/bin/sh

## https://github.com/lxde/lxtask

project="lxtask"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/lxde/lxtask"

if [ ! -d "$HOME/tmp" ]; then
  mkdir -vp "$HOME/tmp"
fi

git clone "https://github.com/lxde/lxtask.git" "$build_dir"

cd "$build_dir" || exit 1

    if [ ! -d build ]; then
      mkdir -vp build
    fi


    cd build || exit 1
#      cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=/usr/local
#      cmake --build . --target install
      cmake ..
      make
      echo "Install $project to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install;;
      esac
  cd "$cur_dir"
fi
exit 0

