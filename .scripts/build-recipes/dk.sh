#!/bin/sh

## https://bitbucket.org/natemaia/dk/src/master/

project="dk"
build_dir="$HOME/tmp/$project"
git_url="https://bitbucket.org/natemaia/dk/src/master.git"

sudo apt install make git libxcb-randr0-dev libxcb-util-dev libxcb-icccm4-dev libxcb-cursor-dev libxcb-keysyms1-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install
         if [ ! -d "$HOME"/.config/dk ]; then
           mkdir -v -p ~/.config/dk
           cp -v /usr/local/share/doc/dk/dkrc "$HOME"/.config/dk/
           cp -v /usr/local/share/doc/dk/sxhkdrc "$HOME"/.config/dk/
         fi;;
  esac

exit 0
