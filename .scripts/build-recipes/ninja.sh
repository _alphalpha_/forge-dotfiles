#!/bin/sh

# ninja https://github.com/ninja-build/ninja
project="ninja"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/ninja-build/ninja.git"

sudo apt install make cmake g++ git
git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  git checkout "$(git describe --tags $(git rev-list --tags --max-count=1))"
  cmake -Bbuild-cmake\ 
  cmake --build build-cmake\ 
  cd build-cmake || exit 1
    make -j $(nproc) || exit 1
    sudo make install
exit 0
