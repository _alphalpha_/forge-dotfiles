#!/bin/sh

## iridium-toolkit https://github.com/muccc/iridium-toolkit

git clone https://github.com/muccc/iridium-toolkit.git ~/.local/opt/iridium-toolkit
git clone https://git.osmocom.org/osmo-ir77 ~/.local/opt/iridium-toolkit/ambe
exit 0
