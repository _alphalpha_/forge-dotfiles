#!/bin/sh

## https://github.com/martonmiklos/avrdude-qt-gui

project="avrdude-qt-gui"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/martonmiklos/avrdude-qt-gui.git"

sudo apt-get install git make qt5-qmake-bin 
git clone "$git_url" "$build_dir"

cd "$build_dir"/qavrdude || exit 1
  qmake
  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo cp bin/qavrdude /usr/local/bin/qavrdude

launcher='[Desktop Entry]
Name=qavrdude
Comment=avrdude-qt-gui
Exec=qavrdude
Icon=/usr/local/share/icons/avrdude-qt-gui.svg
Terminal=false
Type=Application
Categories=Development;'
echo "$launcher" | sudo tee /usr/local/share/applications/qavrdude.desktop

icon='<svg height="100" viewBox="0 0 26.458333 26.458333" width="100" xmlns="http://www.w3.org/2000/svg">
<circle cx="13.303209" cy="13.115356" fill="#7bc1c9" r="12.77184" stroke-width=".081097"/>
<g fill="#6d6d6d"><path d="m18.234104 8.594402h3.168394v1.750744h-3.168394z" stroke-width=".077342"/>
<path d="m18.234104 11.225244h3.168394v1.750744h-3.168394z" stroke-width=".077342"/>
<path d="m18.234104 13.856086h3.168394v1.750744h-3.168394z" stroke-width=".077342"/>
<path d="m18.234104 16.486927h3.168394v1.751434h-3.168394z" stroke-width=".077357"/>
<path d="m5.372214 8.594402h3.168394v1.750744h-3.168394z" stroke-width=".077342"/>
<path d="m5.372214 11.225246h3.168394v1.750744h-3.168394z" stroke-width=".077342"/>
<path d="m5.372214 13.85609h3.168394v1.750744h-3.168394z" stroke-width=".077342"/>
<path d="m5.372214 16.486927h3.168394v1.751434h-3.168394z" stroke-width=".077357"/>
</g><path d="m8.383064 6.515164h9.999622v13.288732h-9.999622z" fill="#333" stroke-width=".090826"/>
<ellipse cx="9.994546" cy="8.074276" fill="#808080" rx=".609472" ry=".610082" stroke-width=".077342"/></svg>
'
echo "$icon" | sudo tee /usr/local/share/icons/avrdude-qt-gui.svg
;;
  esac
exit 0
