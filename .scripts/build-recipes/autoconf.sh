#!/bin/sh

CURRENT_DIR="$(pwd)"
[ -d ~/tmp ] || mkdir ~/tmp
cd ~/tmp
  wget -c "https://ftp.gnu.org/gnu/autoconf/autoconf-latest.tar.xz"
  mkdir autoconf-latest
  tar -xvJf autoconf-latest.tar.xz --strip-components 1 -C autoconf-latest
  cd autoconf-latest
    ./configure
    make || exit 1
    sudo make install
cd "$CURRENT_DIR"
exit 0
