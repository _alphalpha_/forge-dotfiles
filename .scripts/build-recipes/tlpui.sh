#!/bin/sh

## TLPUI https://github.com/d4nj1/TLPUI

sudo apt install python3-gi git python3-setuptools python3-stdeb dh-python python3-all
git clone https://github.com/d4nj1/TLPUI.git ~/tmp/tlpui

CURRENT_DIR="$(pwd)"
cd ~/tmp/tlpui
  python3 setup.py --command-packages=stdeb.command bdist_deb
  sudo dpkg -i deb_dist/python3-tlpui_*all.deb
cd "$CURRENT_DIR"
exit 0
