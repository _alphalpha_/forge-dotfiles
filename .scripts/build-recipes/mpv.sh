#!/bin/sh
## MPV https://mpv.io/

build_dir="$HOME/tmp/mpv"
git_url="https://github.com/mpv-player/mpv"
printf "checking $git_url/releases for latest release version of MPV\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
dl_url="$git_url/archive/refs/tags/v$version.tar.gz"
file_name="mpv-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then
  sudo apt install git lua5.2 liblua5.2-dev librubberband-dev \
    libxrandr-dev libxext-dev libharfbuzz-dev libxss-dev \
    libxcb-xinerama0-dev libsixel-dev liblcms2-dev libzim-dev \
    libjpeg-dev libfontconfig-dev libgl-dev libglx-dev libegl-dev \
    libxv-dev libass-dev libfribidi-dev libfreetype-dev \
    libavutil-dev libavcodec-dev libavformat-dev  libavfilter-dev \
    libavdevice-dev libswscale-dev libxpresent-dev libvdpau-dev \
    libplacebo-dev
    #libavresample-dev
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd mpv-"$version" || exit 1
      meson setup build -Dx11=enabled -Dwayland=disabled -Dlibmpv=true
      meson compile -C build
      echo "Install to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo meson install -C build ;;
      esac
  cd "$cur_dir"
fi
exit 0
