#!/bin/sh

## https://symphonium.net/
## https://github.com/ttdm/Symphonium

sudo apt install git make qt5-qmake
git clone https://github.com/ttdm/Symphonium.git ~/tmp/symphonium

cur_dir="$(pwd)"
cd ~/tmp/symphonium
  #INTERFACE_OPT="$(iselect "ALSA<s:__LINUX_ALSA__>" "JACK<s:__UNIX_JACK__")"
  #INTERFACE="$(grep 'unix:!macx: DEFINES +=' Symphonium.pro | awk -F '= ' '{print $2}')"
  #if [ "$INTERFACE" != "$INTERFACE_OPT" ]; then
  #  sed -i "s/$INTERFACE/$INTERFACE_OPT/" Symphonium.pro
  #fi
  if [ "$(grep "PREFIX = /usr$" Symphonium.pro)" ]; then
    sed -i 's:PREFIX = /usr:PREFIX = /usr/local:' Symphonium.pro
  fi
  qmake
  make
  sudo make install
cd "$cur_dir"
exit 0
