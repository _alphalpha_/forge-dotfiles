#!/bin/sh

## https://github.com/brndnmtthws/conky

project="conky"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/brndnmtthws/conky.git"

sudo apt install git make gcc cmake cmake-curses-gui gperf libiw-dev\
  libimlib2-dev libncurses5-dev libx11-dev libxdamage-dev libxft-dev \
  libxinerama-dev libxml2-dev libxext-dev libcurl4-openssl-dev liblua5.3-dev \
  libical-dev libmicrohttpd-dev python3-yaml python3-jinja2

# optional: pandoc libmicrohttpd-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  git checkout $(git describe --tags $(git rev-list --tags --max-count=1))

  if [ ! -d build ]; then
    mkdir build
  fi
  cd build || exit 1
    ccmake ..
    cmake ..
    make -j "$(nproc)" || exit 1  
    echo "Install $project to /usr/local ? [Y|n]"
    read -r ans
    if [ -z "$ans" ]; then
      ans="y"
    fi
    case "$ans" in
      y|Y) sudo make install;;
    esac
exit 0
