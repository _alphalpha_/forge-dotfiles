#!/bin/sh

# duckstation https://github.com/stenzek/duckstation

# install dependencies
sudo apt install git make cmake ninja-build libsdl2-dev libxrandr-dev pkg-config \
qtbase5-dev qtbase5-private-dev qtbase5-dev-tools qttools5-dev libevdev-dev 

CURRENT_DIR="$(pwd)"

[ -d ~/tmp/duckstation ] && rm -vrd --interactive=never ~/tmp/duckstation
git clone https://github.com/stenzek/duckstation.git ~/tmp/duckstation || exit 1
cd ~/tmp/duckstation
  [ -d build ] || mkdir build
  cd build
    cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr/local -DCMAKE_BUILD_TYPE=Release -GNinja ..
    ninja || exit 1
    [ -d /usr/local/opt/duckstation ] && sudo rm -vrd --interactive=never /usr/local/opt/duckstation
    [ -L /usr/local/bin/duckstation-qt ] && sudo rm -vrd --interactive=never /usr/local/bin/duckstation-qt
    sudo mv bin /usr/local/opt/duckstation
    sudo ln -s /usr/local/opt/duckstation/duckstation-qt /usr/local/bin/duckstation-qt 

# create launcher
    [ -f /usr/local/share/applications/duckstation.desktop ] && sudo rm -vrd --interactive=never /usr/local/share/applications/duckstation.desktop
    sudo cat > duckstation.desktop <<EOF
[Desktop Entry]
Type=Application
Terminal=false
Exec=/usr/local/bin/duckstation-qt
Name=DuckStation
Icon=/usr/share/icons/Numix-Zafiro-Remix/48/apps/psx.svg
Categories=Game;Emulator;
Name[en_US]=DuckStation
EOF
    sudo mv duckstation.desktop /usr/local/share/applications/duckstation.desktop
cd "$CURRENT_DIR"
exit 0
