#!/bin/sh

## https://github.com/MasterQ32/kristall

sudo apt install qt5-default qt5-qmake qttools5-dev-tools qtmultimedia5-dev libqt5svg5-dev libssl-dev make g++
git clone https://github.com/MasterQ32/kristall.git ~/tmp/kristall

cur_dir="$(pwd)"
cd ~/tmp/kristall
  mkdir build
  cd build
    qmake ../src/kristall.pro
    make
    sudo make install
cd "$cur_dir"
exit 0
