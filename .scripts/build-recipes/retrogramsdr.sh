#!/bin/sh

## retrogram-rtlsdr https://github.com/r4d10n/retrogram-rtlsdr
## example: ./retrogram-rtlsdr --rate 1.8e6 --freq 100e6 --step 1e5

sudo apt install git make librtlsdr-dev libncurses5-dev libboost-program-options-dev
git clone https://github.com/r4d10n/retrogram-rtlsdr.git ~/tmp/retrogram-rtlsdr

CURRENT_DIR="$(pwd)"
cd ~/tmp/retrogram-rtlsdr
  make
  sudo cp -v retrogram-rtlsdr /usr/local/bin/retrogram-rtlsdr
cd "$CURRENT_DIR"
exit 0
