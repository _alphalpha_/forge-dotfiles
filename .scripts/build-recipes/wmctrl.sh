#!/bin/sh

## https://github.com/saravanabalagi/wmctrl

build_dir="$HOME/tmp/"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

cur_dir="$(pwd)"
cd "$build_dir" || exit 1
  git clone "https://github.com/saravanabalagi/wmctrl"
  cd "wmctrl" || exit 1
    ./configure
    make
    echo "Install wmctrl to /usr/local ? [Y|n]"
    read -r ans
    if [ -z "$ans" ]; then
      ans="y"
    fi
    case "$ans" in
      y|Y) sudo make install;;
    esac
cd "$cur_dir"

exit 0
