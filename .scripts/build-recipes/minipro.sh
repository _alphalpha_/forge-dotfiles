#!/bin/sh

# https://gitlab.com/DavidGriffith/minipro
project="minipro"
build_dir="$HOME/tmp/$project"
git_url="https://gitlab.com/DavidGriffith/minipro.git"

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  mkdir build && cd build
  cmake ..
  make -j $(nproc)
  sudo make install
exit 0
