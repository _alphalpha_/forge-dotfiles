#!/bin/sh

## rtl-power-fftw https://github.com/AD-Vega/rtl-power-fftw

sudo apt-get install git make cmake pkg-config libfftw3-dev libtclap-dev librtlsdr-dev
git clone https://github.com/AD-Vega/rtl-power-fftw.git ~/tmp/rtl-power-fftw

CURRENT_DIR="$(pwd)"
cd ~/tmp/rtl-power-fftw
  mkdir build
  cd build
    cmake ..
    make
    sudo make install
cd "$CURRENT_DIR"
exit 0
