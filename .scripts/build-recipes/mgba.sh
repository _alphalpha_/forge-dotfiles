#!/bin/sh

## https://github.com/mgba-emu/mgba

git clone https://github.com/mgba-emu/mgba.git ~/tmp/mgba


sudo apt install cmake make gcc gdb ffmpeg pkg-config zlib1g-dev zlib1g libepoxy-dev libepoxy0 \
                 libedit-dev libedit2 libelf-dev libelf1 libsdl2-dev libsdl2-2.0-0 libsdl2-image-dev libsdl2-image-2.0-0 \
                 libsdl2-mixer-dev libsdl2-mixer-2.0-0 libsdl2-gfx-dev libsdl2-gfx-1.0-0 libsdl2-ttf-dev libsdl2-ttf-2.0-0 \
                 libqt5gui5 libqt5gui5-gles libqt5multimedia5 libqt5opengl5 libqt5gamepad5-dev libqt5opengl5-dev libqt5multimedia5 \
                 libavcodec-dev libavcodec-extra58 libavutil-dev libzip-dev libzip4 libqt5core5a libqt5widgets5 libqt5network5

CURRENT_DIR="$(pwd)"
cd ~/tmp/mgba
  mkdir build
  cd build
    cmake -DCMAKE_INSTALL_PREFIX:PATH=/usr/local ..
    make
    sudo make install
cd "$CURRENT_DIR"
exit 0
