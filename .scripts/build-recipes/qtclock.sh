#!/bin/sh

## QTClock https://github.com/arman-ashrafian/QTClock

sudo apt install git make qt5-qmake qt5-qmake-bin
git clone https://github.com/arman-ashrafian/QTClock.git ~/tmp/QTClock

CURRENT_DIR="$(pwd)"
cd ~/tmp/QTClock
  qmake || exit 1
  make || exit 1
  sudo cp -v QTClock /usr/local/bin/qtclock
cd "$CURRENT_DIR"
exit 0
