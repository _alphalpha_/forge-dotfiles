#!/bin/sh

## https://gweled.org/
## https://launchpad.net/gweled

project="gweled"
build_dir="$HOME/tmp/$project"
git_url="https://git.launchpad.net/gweled"

sudo apt install libcanberra-gtk-dev
git clone "$git_url" "$build_dir"

cd "$build_dir"|| exit 1
  ./autogen.sh
  ./configure
  make -j $(nproc) || exit 1
  sudo make install

exit 0
