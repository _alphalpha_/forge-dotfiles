#!/bin/sh

## https://github.com/lxqt/lxqt-build-tools
## https://github.com/lxqt/libqtxdg
## https://github.com/lxqt/liblxqt
## https://github.com/lxqt/libfm-qt


git clone https://github.com/lxqt/lxqt-build-tools.git ~/tmp/lxqt-build-tools
cd ~/tmp/lxqt-build-tools
  [ -d build ] || mkdir build
  cd build
    cmake ..
    make
    sudo make install
cd "$CURRENT_DIR"

git clone https://github.com/lxqt/libqtxdg.git ~/tmp/libqtxdg
cd ~/tmp/libqtxdg
  [ -d build ] || mkdir build
  cd build
    cmake ..
    make
    sudo make install
cd "$CURRENT_DIR"

git clone https://github.com/lxqt/liblxqt.git ~/tmp/liblxqt
cd ~/tmp/liblxqt
  [ -d build ] || mkdir build
  cd build
    cmake -DBUILD_BACKLIGHT_LINUX_BACKEND=OFF ..
    make
    sudo make install
cd "$CURRENT_DIR"

sudo apt install libexif-dev libmenu-cache-dev
#git clone https://github.com/lxqt/libfm-qt.git ~/tmp/libfm-qt
#cd ~/tmp/libfm-qt
#  [ -d build ] || mkdir build
#  cd build
#    cmake ..
#    make
#    sudo make install
#cd "$CURRENT_DIR"

exit 0
