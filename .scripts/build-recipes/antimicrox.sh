#!/bin/sh

## https://github.com/AntiMicroX/antimicrox

project="antimicrox"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/AntiMicroX/antimicrox"
printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
dl_url="$git_url/archive/refs/tags/$version.tar.gz"
file_name="$project-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then
  sudo apt install make cmake extra-cmake-modules libecm-dev libxext-dev libxi-dev libxtst-dev libx11-dev \
           qttools5-dev qttools5-dev-tools libqt5x11extras5-dev libqt5scripttools5 libsdl2-dev gettext itstool
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd "$project"-"$version" || exit 1

    if [ ! -d build ]; then
      mkdir -vp build
    fi
    cd build || exit 1
      cmake .. -DCPACK_GENERATOR="DEB"
      cmake --build . --target package

      echo "Install $project to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo dpkg -i $(find . -maxdepth 1 -type f -iname "*.deb" -printf '%f');;
      esac
  cd "$cur_dir"
fi
exit 0
