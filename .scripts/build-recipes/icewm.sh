#!/bin/sh

## https://github.com/bbidulock/icewm

project="icewm"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/bbidulock/icewm.git"

sudo apt install make git gcc libimlib2-dev libxcomposite-dev libxdamage-dev libxfixes-dev \
                 libxft-dev libxinerama-dev libxpm-dev libxrandr-dev libxrender-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  ./autogen.sh
  ./configure
  make -j "$(nproc)" || exit 1
  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make DESTDIR="$pkgdir" install;;
  esac

exit 0
