#!/bin/sh

## sherlok https://github.com/sherlock-project/sherlock

sudo apt install git python3 python3-pip python3-setuptools python3-colorama \
python3-requests-futures python3-bs4 python3-certifi python3-lxml python3-soupsieve python3-stem

[ -d ~/tmp/sherlock ] && rm -vrd --interactive=never ~/tmp/sherlock
git clone https://github.com/sherlock-project/sherlock.git ~/tmp/sherlock || exit 1
python3 -m pip install -r ~/tmp/sherlock/requirements.txt

[ -d ~/.local/opt/sherlock ] && rm -vrd --interactive=never ~/.local/opt/sherlock
mv ~/tmp/sherlock ~/.local/opt/sherlock
chmod +x ~/.local/opt/sherlock/sherlock/sherlock.py
exit 0
