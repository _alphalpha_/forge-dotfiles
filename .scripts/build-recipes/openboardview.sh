#!/bin/sh

# https://github.com/OpenBoardView/OpenBoardView
project="OpenBoardView"
build_dirs="https://github.com/OpenBoardView/OpenBoardView.git"

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  ./build.sh
  sudo cp bin/openboardview /usr/local/bin/openboardview
exit 0
