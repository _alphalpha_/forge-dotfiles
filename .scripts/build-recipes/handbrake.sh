#!/bin/sh

## https://handbrake.fr
## https://github.com/HandBrake/HandBrake


project="HandBrake"
build_dir="$HOME/tmp/$project"

url="https://handbrake.fr/$(curl "https://handbrake.fr/downloads.php" | grep "source.tar" | cut -d\" -f2)"
dl_url="$(curl "$url" | grep "source.tar" -m1 | cut -d\" -f2)"
file_name="$(echo "$dl_url" | awk -F '/' '{ printf $NF }')"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ]; then
  sudo apt-get update
  sudo apt-get install appstream autoconf automake autopoint build-essential cmake \
               m4 make meson nasm ninja-build patch pkg-config python tar libtool libtool-bin \
               intltool libdbus-glib-1-dev libglib2.0-dev libgstreamer1.0-dev libgs \
               libass-dev libbz2-dev libfontconfig1-dev libfreetype6-dev libfribidi-dev libharfbuzz-dev \
               libjansson-dev liblzma-dev libmp3lame-dev libnuma-dev libogg-dev libopus-dev libsamplerate-dev \
               libspeex-dev libtheora-dev  libturbojpeg0-dev libvorbis-dev libx264-dev \
               libxml2-dev libvpx-dev  zlib1g-dev libva-dev libdrm-dev

  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    printf "Downloading %s" "$dl_url"
    curl -OL -C - "$dl_url"

    if echo "$file_name" | grep ".tar.bz2"; then
      tar xvjf "$file_name"
      file_name="$(echo "$file_name" | sed 's/-source.tar.bz2//')"
    elif echo "$file_name" | grep ".tar.gz"; then
      tar xvzf "$file_name"
      file_name="$(echo "$file_name" | sed 's/-source.tar.gz//')"
    elif echo "$file_name" | grep ".tar.xz"; then
      tar xvJf "$file_name"
      file_name="$(echo "$file_name" | sed 's/-source.tar.xz//')"
    else
      exit 1
    fi

    cd "$file_name" || exit 1
      ./configure --launch-jobs=$(nproc) --launch
      cd build || exit 1
        echo "Install $project to /usr/local ? [Y|n]"
        read -r ans
        if [ -z "$ans" ]; then
          ans="y"
        fi
        case "$ans" in
          y|Y) sudo make install;;
        esac
  cd "$cur_dir"
fi
exit 0

