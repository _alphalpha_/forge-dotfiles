#!/bin/sh

## https://github.com/kolbusa/stalonetray

project="stalonetray"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/kolbusa/stalonetray.git"

sudo apt install autoconf automake docbook-xsl libxpm-dev libx11-dev xsltproc

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  aclocal && autoheader && autoconf && automake --add-missing
  ./configure
  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install;;
  esac

exit 0
