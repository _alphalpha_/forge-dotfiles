#!/bin/sh

## https://github.com/hyprwm/Hypr

project="hypr"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/hyprwm/Hypr.git"

sudo apt install \
  xcb cmake gcc libgtk-3-dev ninja-build libgtkmm-3.0-dev libxcb-randr0 \
  libxcb-randr0-dev libxcb-util-dev libxcb-util0-dev libxcb-util1 \
  libxcb-ewmh-dev libxcb-xinerama0 libxcb-xinerama0-dev libxcb-icccm4-dev \
  libxcb-keysyms1-dev libxcb-cursor-dev libxcb-shape0-dev build-essential

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  if [ -d build ]; then
    rm -rf build
  fi
  mkdir build  || exit 1
  sed "s@set(CMAKE_CXX_COMPILER \"/bin/g++\")@set(CMAKE_CXX_COMPILER \"$(which g++)\")@" -i CMakeLists.txt
  cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release -H./ -B./build -G Ninja
  cmake --build ./build --config Release --target all -j $(nproc)
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo cp -v ./build/Hypr /usr/bin
         sudo cp -v ./example/hypr.desktop /usr/share/xsessions
         if [ ! -d "$HOME"/.config/hypr ]; then
           mkdir -vp "$HOME"/.config/hypr
           cp -v example/hypr.conf "$HOME"/.config/hypr/hypr.conf
         fi;;
  esac

exit 0
