#!/bin/sh

## rfkill
## https://git.kernel.org/pub/scm/linux/kernel/git/jberg/rfkill.git/
## Dependencies: libc6 libsmartcols1 

url="https://git.kernel.org/pub/scm/linux/kernel/git/jberg/rfkill.git/snapshot/rfkill-1.0.tar.gz"
tmpdir="$HOME/tmp/rfkill"
curdir="$(pwd)"

if [ ! -d "$tmpdir" ]; then
  mkdir -vp "$tmpdir"
fi

curl -L -o "$tmpdir"/rfkill-1.0.tar.gz --url "$url" || exit 1
tar xvzf "$tmpdir"/rfkill-1.0.tar.gz -C "$tmpdir" || exit 1

cd "$tmpdir/rfkill-1.0" || exit 1
  make && {
            if [ -f "$tmpdir/rfkill-1.0/rfkill" ]; then
              printf "copy rfkill to /usr/local/bin ?\\n [N/y]"
              read -r ans
              if [ "$ans" = "y" ]; then
                sudo cp -v "$tmpdir/rfkill-1.0/rfkill" /usr/local/bin/rfkill
              fi
            fi
          }
cd "$curdir" || exit 1
exit 0
