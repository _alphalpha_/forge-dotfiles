#!/bin/sh

## GQRX https://github.com/gqrx-sdr/gqrx

project="gqrx"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/gqrx-sdr/gqrx.git"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

sudo apt install make cmake pkg-config g++ gnuradio gnuradio-dev \
  libqt6core6 libqt6gui6 libqt6network6 libqt6widgets6 libqt6svg6 qt6-svg-dev \
  portaudio19-dev libasound2-plugins libgnuradio-runtime3.10.5 \
  libgnuradio-analog3.10.5 libgnuradio-audio3.10.5 libgnuradio-blocks3.10.5 \
  libgnuradio-digital3.10.5 libgnuradio-fft3.10.5 libgnuradio-filter3.10.5 \
  libgnuradio-network3.10.5 libgnuradio-pmt3.10.5 \
  gr-iqbal gr-osmosdr libwebrtc-audio-processing1 -y

git clone "$git_url" "$build_dir"

cur_dir="$(pwd)"
cd "$build_dir" || exit 1

  git checkout $(git describe --tags $(git rev-list --tags --max-count=1))

  if [ ! -d build ]; then
    mkdir build
  fi
  cd build || exit 1
#    qmake AUDIO_BACKEND=portaudio ..
    cmake -DLINUX_AUDIO_BACKEND:STRING=Portaudio ..
    make -j $(($(nproc)-1))
    echo "Install $project to /usr/local ? [Y|n]"
    read -r ans
    if [ -z "$ans" ]; then
      ans="y"
    fi
    case "$ans" in
      y|Y) sudo make install ;;
    esac
cd "$cur_dir"

exit 0
