#!/bin/sh

## https://github.com/yt-dlp/yt-dlp

sudo apt install git make python3 python3-setuptools
git clone https://github.com/yt-dlp/yt-dlp.git ~/tmp/yt-dlp

CURRENT_DIR="$(pwd)"
cd ~/tmp/yt-dlp
  make
  sudo cp -v yt-dlp /usr/local/bin/yt-dlp
cd "$CURRENT_DIR"
exit 0
