#!/bin/sh

# https://github.com/junegunn/fzf

project="fzf"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/junegunn/fzf.git"

git clone "$git_url" "$build_dir"

curdir="$(pwd)"
cd "$build_dir" || exit 1
  git checkout $(git describe --tags $(git rev-list --tags --max-count=1))

  make -j $(nproc)
  sudo make install
#BaseUrl="https://github.com/junegunn/fzf"
#LatestVersion="$(wget --spider "$BaseUrl/releases/latest" 2>&1 \
#           | grep -m1 -o -E "$BaseUrl/releases/tag/v?[0-9]?{3}\.[0-9]?{3}\.[0-9]?{3}" | awk -F '/' '{print $NF}')"
#DownloadLink="$BaseUrl/releases/download/$LatestVersion/fzf-$LatestVersion-Linux_amd64.tar.gz"
#TmpDir="$HOME/.local/tmp"
#[ ! -d "$TmpDir" ] && mkdir -vp "$TmpDir"

#curl -L -o "$TmpDir"/fzf-latest.tar.gz "$DownloadLink"
#tar xvzf "$TmpDir"/fzf-latest.tar.gz -C "$TmpDir"
#if [ -f "$TmpDir"/fzf ]; then
#  chmod 755 "$TmpDir"/fzf
#  sudo mv "$TmpDir"/fzf /usr/local/bin/fzf
#fi
cd "$curdir"
exit 0
