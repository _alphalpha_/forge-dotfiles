#!/bin/sh

## https://github.com/phillbush/xmenu

sudo apt install git make g++ build-essential libimlib2-dev libxinerama-dev libxft-dev libx11-dev
git clone https://github.com/phillbush/xmenu.git ~/tmp/xmenu

CURRENT_DIR="$(pwd)"
cd ~/tmp/xmenu
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
