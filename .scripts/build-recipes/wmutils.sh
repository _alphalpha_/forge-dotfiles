#!/bin/sh

## https://github.com/wmutils/core

project="wmutils"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/wmutils/core.git"

sudo apt install make git gcc libxcb1-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  make -j "$(nproc)" || exit 1  
  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install
    if [ ! -d /usr/local/share/licenses/wmutils ]; then
      sudo mkdir -p /usr/local/share/licenses/wmutils
    fi
    sudo cp LICENSE /usr/local/share/licenses/wmutils/LICENSE
    ;;
  esac

exit 0
