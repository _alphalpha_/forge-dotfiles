#!/bin/sh

## https://github.com/fsphil/hacktv/
## https://github.com/steeviebops/hacktv-gui

CURRENT_DIR="$(pwd)"

sudo apt install git make
git clone https://github.com/fsphil/hacktv.git ~/tmp/hacktv
cd ~/tmp/hacktv
  make || exit 1
  sudo make install

sudo apt install git make ant ecj openjdk-11-jre
git clone https://github.com/steeviebops/hacktv-gui.git ~/tmp/hacktv-gui
cd ~/tmp/hacktv-gui
  ant || exit 1
  sudo cp -v dist/hacktv-gui.jar /usr/local/bin/hacktv-gui.jar

cd "$CURRENT_DIR"
exit 0
