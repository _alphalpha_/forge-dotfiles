#!/bin/sh

## XScreenRuler https://github.com/6d7367/xscreenruler

sudo apt install make git
git clone https://github.com/6d7367/xscreenruler.git ~/tmp/xscreenruler

CURRENT_DIR="$(pwd)"
cd ~/tmp/xscreenruler
  make
  sudo cp -v xscreenruler /usr/local/bin/xscreenruler
cd "$CURRENT_DIR"
exit 0
