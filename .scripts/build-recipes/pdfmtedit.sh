#!/bin/sh

## PDFMtEd https://github.com/glutanimate/PDFMtEd
## exiftool https://exiftool.org

sudo apt install git yad python3 qpdf

CURRENT_DIR="$(pwd)"
[ -d ~/tmp ] || mkdir ~/tmp
cd ~/tmp

# build exiftool first
RELEASE_VERSION="$(curl -s https://exiftool.org/history.html | awk -F\" '/most recent production release/ {print $2}')"
curl -O https://exiftool.org/$RELEASE_VERSION
tar xzf "$RELEASE_VERSION"
cd `echo "$RELEASE_VERSION" | sed 's/.tar.gz//'`
  perl Makefile.PL
  make
  sudo make install
cd ..

# build PDFMtEd
git clone https://github.com/glutanimate/PDFMtEd.git ~/tmp/PDFMtEd
cd ~/tmp/PDFMtEd
  ./install.sh
  
cd "$CURRENT_DIR"
exit 0
