#!/bin/sh

## https://i3wm.org/
## https://github.com/i3/i3

build_dir="$HOME/tmp/i3"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    curl -LO -C - "https://github.com/i3/i3/archive/stable.tar.gz"
    tar xvzf "stable.tar.gz"
    cd "i3-stable" || exit 1
    sudo apt install curl meson ninja-build libxcb1-dev libxcb-keysyms1-dev libxcb-util0-dev libxcb-icccm4-dev \
             libyajl-dev libstartup-notification0-dev libxcb-randr0-dev libev-dev libxcb-xinerama0-dev \
             libxcb-shape0-dev libxcb-cursor-dev libxcb-xkb-dev libxcb-xrm-dev libxkbcommon-x11-dev
    if [ ! -d build ]; then
      mkdir build
    fi
    cd build
      meson ..
#      meson setup -Dmans=true ..
      ninja
      sudo ninja install
cd "$cur_dir"
exit 0
