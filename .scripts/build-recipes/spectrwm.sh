#!/bin/sh

## spectrwm https://github.com/conformal/spectrwm

sudo apt-get install git make libxft-dev libxcb-xtest0-dev libxcb-xinput-dev \
libxcb-xinerama0-dev libxcb-randr0-dev libxcb-util0-dev libx11-xcb-dev
git clone https://github.com/conformal/spectrwm.git ~/tmp/spectrwm

CURRENT_DIR="$(pwd)"
cd ~/tmp/spectrwm/linux
  make || exit 1
  sudo make install
cd "$CURRENT_DIR"
exit 0
