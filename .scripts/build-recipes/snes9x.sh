#!/bin/sh

## snes9x-gtk https://github.com/snes9xgit/snes9x

sudo apt install git meson ninja-build libsdl2-dev libx11-dev libepoxy-dev libgtkmm-3.0-dev portaudio19-dev libminizip-dev
git clone https://github.com/snes9xgit/snes9x.git ~/tmp/snes9x

cd ~/tmp/snes9x
  git init && git submodule update --init --recursive
  cd gtk
    meson build --prefix=/usr/local --buildtype=release --strip
    cd build
    ninja
    sudo ninja install
cd "$CURRENT_DIR"
exit 0
