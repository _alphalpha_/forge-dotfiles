#!/bin/sh

## https://github.com/dylanaraps/pfetch

if [ ! -f ~/tmp/pfetch/pfetch ]; then
  if [ -d ~/tmp/pfetch/ ]; then
    rm -rd ~/tmp/pfetch
  fi
  git clone https://github.com/dylanaraps/pfetch.git ~/tmp/pfetch 
fi
sudo cp -v ~/tmp/pfetch/pfetch /usr/local/bin/pfetch
exit 0
