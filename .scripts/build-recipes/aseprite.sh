#!/bin/sh


# NOT WORKING
printf "this script is not working...\\n"
exit 0



## https://github.com/aseprite/aseprite

sudo apt-get install -y g++ cmake ninja-build libx11-dev libxcursor-dev libxi-dev libgl1-mesa-dev libfontconfig1-dev
git clone https://github.com/aseprite/aseprite.git ~/tmp/aseprite

CURRENT_DIR="$(pwd)"
cd ~/tmp/aseprite
  git submodule update --init --recursive
  [ -d build ] || mkdir build
  cd build

    [ -d skia ] || mkdir skia
    cd skia
    wget -c https://github.com/aseprite/skia/releases/download/m96-2f1f21b8a9/Skia-Linux-Release-x64.zip
    unzip Skia-Linux-Release-x64.zip
    cd ..

# skia build instructions
# https://github.com/aseprite/skia#skia-on-linux

env CC=clang++-11 cmake \
  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
  -DLAF_BACKEND=skia \
  -DSKIA_DIR=$HOME/tmp/aseprite/build/skia \
  -DSKIA_LIBRARY_DIR=$HOME/tmp/aseprite/build/skia/out/Release-x64 \
  -DSKIA_LIBRARY=$HOME/tmp/aseprite/build/skia/out/Release-x64/libskia.a \
  -G Ninja \
  ..

#  cmake \
#  -DCMAKE_BUILD_TYPE=RelWithDebInfo \
#  -DLAF_BACKEND=linux \
#  -G Ninja \
#  ..

  ninja aseprite

cd "$CURRENT_DIR"
exit 0
