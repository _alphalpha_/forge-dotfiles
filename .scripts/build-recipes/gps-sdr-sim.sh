#!/bin/sh

## GPS-SDR-SIM https://github.com/osqzss/gps-sdr-sim

sudo apt install git gcc
git clone https://github.com/osqzss/gps-sdr-sim.git ~/tmp/gps-sdr-sim

CURRENT_DIR="$(pwd)"
cd ~/tmp/gps-sdr-sim
  gcc gpssim.c -lm -O3 -o gps-sdr-sim
  sudo cp -v gps-sdr-sim* /usr/local/bin/
cd "$CURRENT_DIR"
exit 0
