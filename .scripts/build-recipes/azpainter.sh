#!/bin/sh

## http://azsky2.html.xdomain.jp/
## https://gitlab.com/azelpg/azpainter

                 

sudo apt install clang ninja-build pkg-config libx11-dev libxext-dev libxcursor-dev libxi-dev libfreetype6-dev libfontconfig1-dev zlib1g-dev libpng-dev libjpeg-dev libtiff-dev libwebp-dev
git clone https://gitlab.com/azelpg/azpainter.git ~/tmp/azpainter

CURRENT_DIR="$(pwd)"
cd ~/tmp/azpainter
  ./configure
#  make
#  sudo make install
  [ -d build ] || mkdir build
  cd build
    CC=clang ninja
    sudo ninja install
cd "$CURRENT_DIR"
exit 0
