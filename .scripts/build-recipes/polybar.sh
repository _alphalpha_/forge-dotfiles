#!/bin/sh

## Polybar https://github.com/polybar/polybar

project="polybar"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/polybar/polybar"
printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/[vV]?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }')"
dl_url="$git_url/archive/refs/tags/$version.tar.gz"
file_name="$project-$version.tar.gz"


if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then

sudo apt install git wget make cmake pkg-config xcb-proto libxcb-image0-dev libxcb-util0 \
libxcb-util0-dev libxcb-composite0 libxcb-composite0-dev libxcb-xkb1 libxcb-xkb-dev libxcb-xinput0 libxcb1-dev libxcb-randr0-dev\
libxcb-xinput-dev libxcb-cursor0 libxcb-cursor-dev libxcb-xrm0 libxcb-xrm-dev libxcb-icccm4 \
libxcb-icccm4-dev libxcb-ewmh2 libxcb-ewmh-dev libjsoncpp1 libjsoncpp-dev libcairo2-dev libmpdclient-dev \
libxcb-composite0-dev libasound2-dev libcurl4-openssl-dev libiw-dev libuv1-dev libnl-genl-3-dev\
python3-packaging python3-xcbgen python3-sphinx 

git clone "https://github.com/polybar/i3ipcpp" ~/tmp/polybar/polybar-3.6.3/lib/i3ipcpp
git clone "https://github.com/polybar/xpp"  ~/tmp/polybar/polybar-3.6.3/lib/xpp

  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd "$project"-"$version" || exit 1
      sudo ./build.sh
  cd "$cur_dir"
fi
exit 0
