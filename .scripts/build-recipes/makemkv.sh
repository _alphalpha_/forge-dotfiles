#!/bin/sh
## makemkv
## https://forum.makemkv.com/forum/viewtopic.php?f=3&t=224

project="makemkv"
build_dir="$HOME/tmp/$project"
cur_dir="$(pwd)"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

sudo apt-get install build-essential pkg-config libc6-dev libssl-dev libexpat1-dev \
                     libavcodec-dev libgl1-mesa-dev qtbase5-dev zlib1g-dev
cd "$build_dir" || exit 1

  curl -OL "https://www.makemkv.com/download/makemkv-oss-1.17.4.tar.gz"
  tar xvzf "makemkv-oss-1.17.4.tar.gz"
  cd makemkv-oss-1.17.4
    ./configure
    make
    sudo make install
  cd ..

  curl -OL "https://www.makemkv.com/download/makemkv-bin-1.17.4.tar.gz"
  tar xvzf "makemkv-bin-1.17.4.tar.gz"
  cd makemkv-bin-1.17.4
    make
    sudo make install
  cd ..

cd "$cur_dir"
exit 0
