#!/bin/sh

## https://github.com/btimofeev/lazy_ips

sudo apt install git python3 python3-setuptools
git clone https://github.com/btimofeev/lazy_ips.git ~/tmp/lazy_ips

CURRENT_DIR="$(pwd)"
cd ~/tmp/lazy_ips
  sudo python3 setup.py install
cd "$CURRENT_DIR"
exit 0
