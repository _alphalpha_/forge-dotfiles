#!/bin/sh
## https://www.qemu.org
## https://gitlab.com/qemu-project/qemu

url="https://www.qemu.org"
project="qemu"
build_dir="$HOME/tmp/$project"

printf "checking $url for latest release version of $project\\n"

dl_url="$(curl -s https://www.qemu.org/download/ | awk '/wget/ {print $NF}')"
file_name="$(echo "$dl_url" | awk -F '/' '{print $NF}')"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ]; then
  printf "installing dependencies..."
  sudo apt install libaio-dev libbluetooth-dev libcapstone-dev libbrlapi-dev libbz2-dev \
                   libcap-ng-dev libgtk-3-dev \
                   libibverbs-dev libjpeg-dev libturbojpeg0-dev libncurses5-dev libnuma-dev \
                   librbd-dev librdmacm-dev \
                   libsasl2-dev libsdl2-dev libseccomp-dev libsnappy-dev libssh-dev \
                   libvde-dev libvdeplug-dev libvte-2.91-dev libxen-dev liblzo2-dev \
                   xfslibs-dev libnfs-dev libiscsi-dev
                 # libcurl4-gnutls-dev valgrind
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    printf "Downloading %s" "$dl_url"
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvJf "$file_name" || tar xvzf "$file_name"
    extracted_dir="$(echo "$file_name" | sed 's/.tar.xz//g;s/tar.gz//g')"
    cd "$extracted_dir" || exit 1
      ./configure || exit 1
       make -j $(nproc) || exit 1
      echo "Install $project to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install;;
      esac
  cd "$cur_dir"
fi
exit 0
