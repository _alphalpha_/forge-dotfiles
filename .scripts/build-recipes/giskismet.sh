#!/bin/sh

## giskismet https://github.com/xtr4nge/giskismet

sudo apt-get install git make perl libxml-libxml-perl libdbi-perl libdbd-sqlite3-perl
git clone https://github.com/xtr4nge/giskismet.git ~/tmp/giskismet

CURRENT_DIR="$(pwd)"
cd ~/tmp/giskismet
  perl Makefile.PL
  make || exit 1
  sudo make install 
cd "$CURRENT_DIR"
exit 0
