#!/bin/sh

## Ceni https://github.com/fullstory/ceni

sudo apt install git make perl libcurses-perl libcurses-ui-perl libexpect-perl libterm-readkey-perl ifupdown wpasupplicant
git clone https://github.com/fullstory-morgue/ceni.git ~/tmp/ceni

CURRENT_DIR="$(pwd)"
cd ~/tmp/ceni
  perl Makefile.PL
  make
  sudo make install
cd "$CURRENT_DIR"
exit 0
