#!/bin/sh

## https://github.com/dolphin-emu/dolphin

sudo apt install git make cmake
git clone https://github.com/dolphin-emu/dolphin.git ~/tmp/dolphin-emu

CURRENT_DIR="$(pwd)"
cd ~/tmp/dolphin-emu
  git submodule update --init --recursive
  mkdir build
  cd build
    cmake ..
    make
    sudo make install
cd "$CURRENT_DIR"
exit 0
