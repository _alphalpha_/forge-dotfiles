#!/bin/sh

## nmap https://github.com/nmap/nmap

sudo apt install git make
git clone https://github.com/nmap/nmap.git ~/tmp/nmap

CURRENT_DIR="$(pwd)"
cd ~/tmp/nmap
  ./configure
  make || exit 1
  sudo make install
cd "$CURRENT_DIR"
exit 0
