#!/bin/sh

## https://bibletime.info/
## https://github.com/bibletime/bibletime

project="bibletime"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/bibletime/bibletime"
printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/v?[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }' | tr -d '[:alpha:]')"
dl_url="$git_url/archive/refs/tags/v$version.tar.gz"
file_name="$project-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then
  sudo apt install curl make cmake qtbase5-dev qtdeclarative5-dev libclucene-dev \
           libsword-dev po4a xsltproc docbook-xsl docbook-xml #fop

  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    printf "Downloading %s" "$dl_url"
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd "$project"-"$version" || exit 1

    if [ ! -d build ]; then
      mkdir -vp build
    fi
    cd build || exit 1
      # do not build pdf documentation (no need to install fop)
      cmake .. -DBUILD_HOWTO_PDF=OFF -DBUILD_HANDBOOK_PDF=OFF
      make
      echo "Install $project to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install;;
      esac
  cd "$cur_dir"
fi
exit 0
