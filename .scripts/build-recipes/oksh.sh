#!/bin/sh

## https://github.com/ibara/oksh

project="oksh"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/ibara/oksh"
printf "checking $git_url/releases for latest release version of $project\\n"
version="$(wget --spider "$git_url/releases/latest"  2>&1 \
  | grep -m1 -o -E "$git_url/releases/tag/oksh-[0-9]?{3}\.?[0-9]?{3}\.?[0-9]?{3}" \
  | awk -F '/' '{ printf $NF }')"
dl_url="$git_url/archive/refs/tags/$version.tar.gz"
file_name="$project-$version.tar.gz"

if [ ! -d "$build_dir" ]; then
  mkdir -vp "$build_dir"
fi

if [ -n "$file_name" ] && [ "$version" ]; then
  cur_dir="$(pwd)"
  cd "$build_dir" || exit 1
    curl -L -o "$file_name" -C - "$dl_url"
    tar xvzf "$file_name"
    cd "$project"-"$version" || exit 1
      ./configure
      make
      echo "Install $project to /usr/local ? [Y|n]"
      read -r ans
      if [ -z "$ans" ]; then
        ans="y"
      fi
      case "$ans" in
        y|Y) sudo make install;;
      esac
  cd "$cur_dir"
fi
exit 0
