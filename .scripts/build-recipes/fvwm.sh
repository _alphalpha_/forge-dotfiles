#!/bin/sh

## https://github.com/fvwmorg/fvwm3

project="fvwm"
build_dir="$HOME/tmp/$project"
git_url="https://github.com/fvwmorg/fvwm3.git"

sudo apt install make git libevent-dev libx11-dev libxrandr-dev libxrender-dev libxt-dev libxft-dev asciidoctor \
  libfontconfig-dev libfreetype6-dev libfribidi-dev libncurses5-dev libpng-dev libreadline-dev librsvg2-dev libsm-dev \
  libxcursor-dev libxext-dev libxi-dev libxpm-dev

git clone "$git_url" "$build_dir"

cd "$build_dir" || exit 1
  ./autogen.sh
  ./configure --enable-mandoc
  make -j "$(nproc)" || exit 1

  echo "Install $project to /usr/local ? [Y|n]"
  read -r ans
  if [ -z "$ans" ]; then
    ans="y"
  fi
  case "$ans" in
    y|Y) sudo make install;;
  esac

exit 0
