#!/bin/sh

## https://github.com/joncampbell123/dosbox-x

git clone https://github.com/joncampbell123/dosbox-x.git ~/tmp/dosbox-x

CURRENT_DIR="$(pwd)"
cd ~/tmp/dosbox-x
  ./build
  sudo make install
cd "$CURRENT_DIR"
exit 0
