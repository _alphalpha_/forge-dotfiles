#!/bin/sh

# Check if this is a live session
if [ ! -d /lib/live/mount/medium ] && [ ! -d /lib/live/mount/findiso ] && \
   [ ! -d /lib/live/mount/fromiso ] && [ ! -d /lib/live/mount/persistence ] && \
   [ ! -d /run/live/medium ]; then
  exit 0
else
  yad --width="500" --fixed --center \
  --title="Welcome to Forge" \
  --buttons-layout="center" \
  --text=' Welcome to the Forge. \n You should have a look at the Readme.txt and make sure that you have selected your correct keyboard-layout.' \
  --button="Open Readme.txt:featherpad Readme.txt" \
  --button="Select Keyboard Layout:$TERMINAL -e sudo dpkg-reconfigure keyboard-configuration" \
  --button="Install Forge:$TERMINAL -e sudo .local/Scripts/refracta-tools.sh --install" \
  --button="Close this window:0"
fi

exit 0
