#!/bin/sh

[ -f "$1" ] && pdf="$1"
while [ ! -n "$pdf" ] && [ ! "$(file -b --mime-type "$pdf")" = "application/pdf" ]; do

  read screen_width screen_height <<EOF
$(xdpyinfo | awk '/dimensions/{split($2,res,"x"); print res[1],res[2]}')
EOF
  yad_width="$((screen_width * 80 / 100))"
  yad_height="$((screen_height * 80 / 100))"

  pdf="$(yad --title=pdfmted-editor --width="$yad_width" --height="$yad_height" --file)"
  [ -z "$pdf" ] && exit 0
done

pdfmted-editor "$pdf"
