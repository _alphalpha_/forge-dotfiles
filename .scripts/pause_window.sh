#!/bin/sh

pid="$(xdotool getwindowfocus getwindowpid)"
if [ "$pid" -eq "$pid" ]; then
  if [ "$(ps "$pid" | awk '$3 == "T" {print $1}')" = "$pid" ]; then
    kill -CONT "$pid"
  else
    kill -STOP "$pid"
  fi
fi
