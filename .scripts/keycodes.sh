#!/usr/bin/zsh
# credits to https://wiki.archlinux.org/title/Keyboard_input
xev | awk -F'[ )]+' '/^KeyPress/ { a[NR+2] } NR in a { printf "%-3s %s\n", $5, $8 }'
