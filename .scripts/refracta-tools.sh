#!/bin/sh
# refracta-tools by Michael Thorberger
# Based on refractainstaller 9.5.3 by fsmithred@gmail.com which is based on 
# refractainstaller-8.0.3 by Dean Linkous and also based 
# on refractasnapshot-10.2.5 by fsmithred@gmail.com which is based on refractasnapshot-8.0.4 by Dean Linkous
# with ideas borrowed from dzsnapshot-gui.sh by David Hare, 
# which was based on an earlier version of refractasnapshot.
# Portions may be copyright fsmithred@gmail.com and/or Dean Linkous and/or David Hare and/or others.
# UEFI code adapted from similar scripts by Colin Watson and Patrick J. Volkerding
# The code inside the create_new_usb_grub function is adapted from https://mbusb.aguslr.com/
# The awk menu uses code from Hui-Jun Chen's fm.awk https://github.com/huijunchen9260/fm.awk
# For Artix snapshots i used code from https://gitlab.archlinux.org/mkinitcpio/mkinitcpio-archiso
# Licence: GPL-3 https://www.gnu.org/licenses/gpl-3.0.en.html
# This is free software with no warrantees. Use at your own risk!!
#

SCRIPT_VERSION="2024.08.04"

# ▀█▀ █▀█ █▀▀ ▀█▀ █▀█ █   █   █▀▀ █▀▄   █▀▀ █▀█ █▀█ █▀▀ ▀█▀ █▀▀
#  █  █ █ ▀▀█  █  █▀█ █   █   █▀▀ █▀▄   █   █ █ █ █ █▀▀  █  █ █
# ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀
LOGFILE_INSTALL="/refractainstaller_$(date +%Y-%m-%d_%H-%M).log"

HOSTNAME="";			ROOT_PART=""; 		ROOT_FS_TYPE="ext4"; 	ROOT_ENCRYPT=""
SEPERATE_BOOT="yes";	BOOT_PART=""; 		BOOT_FS_TYPE="ext2";
SEPERATE_HOME="no" ;	HOME_PART=""; 		HOME_FS_TYPE="ext4";	HOME_ENCRYPT=""
SEPERATE_SWAP="file";	SWAP_PART="";		SWAPFILE="/.swapfile"
BOOT_METHOD="" # Legacy or UEFI
BOOT_LOADER="" # grub or extlinux
USER_NAME=""
DISABLE_AUTODESK="yes"
DISABLE_IPV6="yes"
USE_UUID="yes"
USE_UUID_IN_EXTLINUX="yes"
BLACKLIST_PCSPKR_IN_EXTLINUX="yes"
#use_existing_swap="no"
additional_partitions="no"
#no_format="yes"
home_boot_excludes="/usr/lib/refractainstaller/home_boot_exclude.list"

# SWAP_SIZE can be: 64MB 128MB 256MB 512MB 1G 2G 4G 8G 16G 32G 64G
SWAP_SIZE="256MB"
# swap_size_count will be overwritten when SWAP_SIZE is triggered in the menu
swap_size_count="262144"
#DEBUG="yes"

##### Don't mess with these unless you know what you're doing.
# This comments out the line in /etc/pmount.allow which allows a user to mount all fixed drives. (/dev/sd[a-z][0-9]*)
# Change it to "yes" or leave it blank to retain this. (not recommended)
pmount_fixed="no"

# SSH Settings
# If ssh_pass="yes", then PasswordAuthentication will be set to "yes"
# If ssh_pass="no", then PasswordAuthentication will be set to "no"
# In either of the above cases, if PermitRootLogin was set to "yes",
# it will be changed to "prohibit-password" (meaning with auth keys only)
# If ssh_pass is null or set to anything other than "yes" or "no", then
# /etc/ssh/sshd_config will not be altered.
ssh_pass="yes"

# Location of grub-efi packages. If grub-efi is not installed, you can
# install the packages in the chroot to install the efi bootloader.

# SORT OUT i386 vs. amd64 packages (for future use, maybe)
#grub_efi_pkg="/lib/live/mount/medium/pool/DEBIAN/main/g/grub2/grub-efi-amd64_*.deb"			# grub-efi*.deb should work
#grub_efi_bin_pkg="/lib/live/mount/medium/pool/DEBIAN/main/g/grub2/grub-efi-amd64-bin_*.deb"	# and also gets ia32
#grub_pc_pkg="/lib/live/mount/medium/pool/DEBIAN/main/g/grub2/grub-pc-bin*.deb   # grub-pc*.deb should work
#./pool/DEBIAN/main/g/grub2/grub-pc_2.02~beta2-22+deb8u1_amd64.deb

# Directory that holds grub packages (in case wrong grub is in the live system).
# Leave it commented if the packages are in / (root of the live filesystem.)
grub_package_dir="/pkg"

# UEFI ID
# grub-efi will use some magic to choose a name for the bootloader
# directory found in boot/efi/EFI/, such as debian, devuan or refracta.
# If you want to specify a name, put it here by uncommenting the line
# and replacing newinstall with the new name.
# Don't make a null variable here! (Or let me know what it does.)
# Default is commented out.
#efi_name_opt='--bootloader-id=newinstall'

tempdir="$(mktemp -d /tmp/refracta_temp.XXXX)"
rsync_excludes="$tempdir/installer_exclude_list.txt"


# █▀▀ █▀█ █▀█ █▀█ █▀▀ █ █ █▀█ ▀█▀   █▀▀ █▀█ █▀█ █▀▀ ▀█▀ █▀▀
# ▀▀█ █ █ █▀█ █▀▀ ▀▀█ █▀█ █ █  █    █   █ █ █ █ █▀▀  █  █ █
# ▀▀▀ ▀ ▀ ▀ ▀ ▀   ▀▀▀ ▀ ▀ ▀▀▀  ▀    ▀▀▀ ▀▀▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀

# limit the cpu to prevent overheating
use_cpu_limit='yes'
cpu_limit_percent='85'

snapshot_dir='/home/snapshot'
work_dir='/home/snapshot/work'
efi_work='/home/snapshot/efi-files'
snapshot_excludes="$tempdir/snapshot_exclude.list"
LOGFILE_SNAPSHOT="$snapshot_dir/refractasnapshot_$(date +%Y-%m-%d_%H-%M).log"

make_efi='yes'
force_efi='no'
save_work='yes'
delete_wifi_passwords='yes'
stamp='datetime'
snapshot_basename='snapshot'
volid='liveiso'
make_sha256sum='yes'
make_pkglist='yes'
make_isohybrid='yes'
WAIT_OPT='no'
# rsync delete options (or any others you want to add). Use only one option per variable!
# This is only for copy_filesystem() and only if $save_work is "yes"
rsync_option1='--delete-before'
rsync_option2=' --delete-excluded'
rsync_option3=''

SQUASHFS_COMPRESSION='xz-smaller' # gzip | xz | xz-smaller | zstd

####################################
# Turn stuff on and off section

# Allow users to mount all fixed drives with pmount for live iso.
# Refractainstaller removes this upon installation. (Default is "yes")
pmount_fixed='yes'

update_mlocate='yes'
clear_geany='yes'

# Allow password login to ssh for users (not root).
# If ssh_pass="yes", then PasswordAuthentication will be set to "yes"
# If ssh_pass="no", then PasswordAuthentication will be set to "no"
# In either of the above cases, if PermitRootLogin was set to "yes",
# it will be changed to "prohibit-password" (meaning with auth keys only)
# If ssh_pass is null or set to anything other than "yes" or "no", then
# /etc/ssh/sshd_config will not be altered.
ssh_pass='yes'

########################################################
# Custom boot menu and help files section.

# If you're running refractasnapshot on some linux distribution other
# than Refracta, You might also want to edit or remove some of the help
# files in the isolinux directory. (f1.txt, f2.txt...)
# If you want those changes to persist between runs, you should create
# a custom iso/isolinux directory, and set iso_dir (below) to point
# to that directory.

# If the primary user's name is not "user", then live-boot needs to see
# the user's name in the boot command. In that case, the script will
# automatically add the correct option. If you set a user name here, it
# will override that process. Use this if you want to log into the live
# media as someone other than the primary user (i.e. any user whose
# uid:gid are not 1000:1000.)
# Under most circumstances, leave this blank or commented out.
#username=""

# Change to "yes" if you want to be able to view or edit the boot menu
# or any other config files before the final image is made.
# NOTE: For SolusOS and possibly others, boot entries should contain "union=unionfs" in place of "union=aufs".
# NOTE: For anything later than jessie, union=aufs should be removed or possibly replaced with union=overlay
edit_boot_menu='no'

# Change this if you're using customized boot menu files, AND your
# menu file is other than the default, live.cfg, AND you set
# $edit_boot_menu to "yes".
livecfg='live.cfg'

# Uncomment this to add boot help files specific to the Refracta distribution.
# Otherwise, generic help files, mostly empty, will be used. If you want
# to use your own customized files, see iso_dir settings above.
#refracta_boot_help='yes'

# Prepare the initrd to support encrypted volumes. Uncomment this
# if you plan to use the snapshot on a live usb with an encrypted
# persistent volume. This will edit /etc/cryptsetup-initramfs/conf-hook
# to set CRYPTSETUP=y
#initrd_crypt='yes'

# Uncomment to include your network configuration in the snapshot.
# This will preserve your /etc/network/interfaces and any saved wireless
# configurations. This works for NetworkManager, simple-netaid/netman
# and wicd.
# It will also add "ip=frommedia" to the boot command, so that the saved
# configuration will be used.
# Default is commented; interfaces file in $work_dir/myfs gets replaced
# and only contains the loopback interface.
#
# NOTE!!! If you're using some other network manager, and you don't want
# your configs to be copied, you need to add the appropriate files to
# the excludes list.
#netconfig_opt='ip=frommedia'

# Uncomment to use old or new style interface names.
# Use net.ifnames=0 to force old interface names with udev. (eth0)
# Use net.ifnames=1 to force new interface names with eudev. (enp0s1)
#ifnames_opt='net.ifnames=0'

# DEPRECATED:
# This patch is no longer needed. Instead, the script will create some
# files in /dev to help with booting. If you leave this variable set
# to "yes" the script will check for previous application of the patch
# and give you the chance to edit the file manually. If you don't remove
# the lines that were added by the patch, nothing bad will happen.
#
# Debian Jessie systems without systemd and with util-linux-2.25 will
# create an unbootable iso. The workaround is to add a few lines to
# /usr/share/initramfs-tools/init and then rebuild the initrd.
# If this option is set to "yes" then the script will check for systemd
# and for the version of util-linux. If needed, the script will apply
# the patch and rebuild the initrd.
#
# Warning: If you also need to run the nocrypt.sh script because you're
# creating a snapshot from a system INSTALLED ON AN ENCRYPTED PARTITION,
# you need to run nocrypt.sh after letting this patch run. (Hint: you
# can abort the snapshot run at the Disk Space Report, run nocrypt,
# then make your snapshot.)
#
# Default is "no" or commented out.
#patch_init_nosystemd="yes"


#  █▄█ █ █ █   ▀█▀ ▀█▀ █ █ █▀▀ █▀▄   █▀▀ █▀█ █▀█ █▀▀ ▀█▀ █▀▀
#  █ █ █ █ █    █   █  █ █ ▀▀█ █▀▄   █   █ █ █ █ █▀▀  █  █ █
#  ▀ ▀ ▀▀▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀▀▀ ▀▀    ▀▀▀ ▀▀▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀

# Comment this to disable the logfile
MULTIUSB_LOG="multiusb-log_$(date +%Y%m%d_%H%M).txt"

# mountpoint for the main partition
MULTIUSB_DIR="$tempdir/mnt/multiusb"

# mountpoint for iso files
ISO_DIR="$tempdir/mnt/iso"

# Compression algorythm for initrafms (gzip|xz|zstd)
INIT_COMPRESSION='gzip'

###### End of config section
#==============================================================================#
# Check if user is root
SUDOER=$(printenv | awk -F '=' '/SUDO_USER/ {print $2}')
if [ ! "$(id -u)" -eq "0" ] && [ "$(whoami)" != "root" ]; then
  printf "\\n\\tYou need to be root :)\\n"
  exit 1
fi

if [ -n "$SUDOER" ]; then
  printf "sudo user:        %s\\n" "$SUDOER" >> "$EARLY_LOG"
  USERHOME="/home/$SUDOER"
else
  USERHOME="/root"
fi
#==============================================================================#
c_end="\033[0m"
c0="\033[30m"; bgc0="\033[40m"
c1="\033[31m"; bgc1="\033[41m"
c2="\033[32m"; bgc2="\033[42m"
c3="\033[33m"; bgc3="\033[43m"
c4="\033[34m"; bgc4="\033[44m"
c5="\033[35m"; bgc5="\033[45m"
c6="\033[36m"; bgc6="\033[46m"
c7="\033[37m"; bgc7="\033[47m"

txt_n="$clr_end"
txt_b="\033[1m"
txt_r="\033[2m"
txt_i="\033[3m"
txt_u="\033[4m"
txt_blink="\033[5m"
txt_special="\033[7m"
txt_invisible="\033[8m"
txt_strike="\033[9m"
txt_u_fat="\033[21m"
txt_clear="\033[31m"

strip_color(){ sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,3})*)?[mGK]//g"; }

get_term_info(){
  term_height="$(tput lines || stty size | cut -d ' ' -f1)"
  term_width="$(tput cols || stty size | cut -d ' ' -f2)"
}
#==============================================================================#
cursor_move(){
  get_term_info
  unset col row
  if [ "$1" = "-xy" ]; then
    shift
    col="$1"
    row="$((term_height-$2))"
  else
    col="$1"
    row="$2"
  fi
  if [ "$col" ] && [ "$row" ]; then
    printf "\033[%s;%sH" "$row" "$col"
  else
    printf "error: cursor_move function needs two inputs: col row\\n"
    exit 1
  fi
}
cursor_move_up(){
  unset move_len; move_len="$1"
  [ "$move_len" ] || move_len=1
  printf "\033[%sA" "$move_len"
}
cursor_move_down(){
  unset move_len; move_len="$1"
  [ "$move_len" ] || move_len=1
  printf "\033[%sB" "$move_len"
}
cursor_move_right(){
  unset move_len; move_len="$1"
  [ "$move_len" ] || move_len=1
  printf "\033[%sC" "$move_len"
}
cursor_move_left(){
  unset move_len; move_len="$1"
  [ "$move_len" ] || move_len=1
  printf "\033[%sD" "$move_len"
}
cursor_save(){ printf "\033[s"; }
cursor_restore(){ printf "\033[u"; }

#==============================================================================#
text_center(){
  if [ -n "$1" ]; then
    input_text="$*"
  else
    input_text="$(cat)"
  fi
  get_term_info
  echo "$input_text" | sed  -e :a -e "s/^.\{1,$term_width\}$/ & /;ta"
}

text_box_opt="-c clr3 -t $(printf "%s" "$0" | sed "s:/home/$SUDOER:~:") -tpos 10"
text_box(){
  border1='━'  # horizontal
  border2='┃'  # vertical
  border3='┫'  # left
  border4='┣'  # right
  corner1='┏'  # top left
  corner2='┓'  # top right
  corner3='┗'  # bottom left
  corner4='┛'  # bottom right

  while true; do
    case "$1" in
      '-c') clr="$2"; shift 2;;
      '-t') title="$2"; shift 2;;
      '-tpos') title_position="$2"; shift 2;;
      *) break;;
    esac
  done

  if [ "$1" ]; then
    input_text="$(echo "\\n${*}")"
  else
    input_text="\\n$(cat)"
  fi

  get_term_info
  line_count=$(printf %s "$input_text" | awk 'END{ print NR }')
  line_num=0

  if [ "$title" ] && [ ! "$title_position" ]; then
    title_position="10"
  fi

  case "$clr" in
    'clr0') clr=$(echo -n "\033[30m");;
    'clr1') clr=$(echo -n "\033[31m");;
    'clr2') clr=$(echo -n "\033[32m");;
    'clr3') clr=$(echo -n "\033[33m");;
    'clr4') clr=$(echo -n "\033[34m");;
    'clr5') clr=$(echo -n "\033[35m");;
    'clr6') clr=$(echo -n "\033[36m");;
    'clr7') clr=$(echo -n "\033[37m");;
  esac

  cursor_move_left "$term_width"
  printf "%s%$((term_width-2))s%s\\n" "${clr}$corner1" " " "$corner2" | sed "s/ /$border1/g"

  if [ "$title" ] ; then
    if [ "$title_position" -eq "$title_position" ]; then
      cursor_move "$title_position" 1
    else
      cursor_move 6 1
    fi
    printf "%s %s %s" "${clr}$border3${c_end}" "$title" "${clr}$border4${c_end}"
    cursor_move 1 2
    unset title
    title=""
    export title
  fi

  while [ "$line_num" -lt "$line_count" ]; do
    line_num="$((line_num+1))"
    line="$(echo "$input_text" | awk -v n="$line_num" '(NR==n){ print; exit }')"
    if [ "$(echo "$line" | cut -b1)" != ' ' ]; then
      line=" $line"
    fi
    printf "%s%$((-term_width+1))s" "${clr}$border2${c_end}" "$line"
    cursor_move_right "$term_width"
    printf "%s\\n" "${clr}$border2${c_end}"
  done

  printf "%s%$((term_width-2))s%s\\n" "${clr}$corner3" " " "$corner4${c_end}" | sed "s/ /$border1/g"
}

#==============================================================================#
pushd(){
  cur_dir="$(pwd)"
  printf "cd into %s" "$1" | log
  cd "$1"
}

popd(){
  printf "cd into %s" "$cur_dir" | log
  cd "$cur_dir"
}

space_error(){
  printf "Error: Not enough free space.\\n"
  exit 1
}

check_exit(){
printf "\\n\\tOops, something went wrong\\n\\tSee %s for details.\\n" "$LOG" | log
if [ "$ACTIVE_SWAP" ]; then
  swapoff "$ACTIVE_SWAP"
fi
exit 1
}

check_mkdir(){
if [ "$1" ] && [ ! -d "$1" ]; then
#  printf "creating %s ..." "$1"
  mkdir -vp "$1" || check_exit
fi
}

check_rmdir(){
if [ "$1" ] && [ -d "$1" ]; then
  if [ "$(find "$1" -maxdepth 0 -type d -empty)" ]; then
    rmdir "$1"
  fi
fi
}

check_rm(){
if [ -f "$1" ] || [ -d "$1" ] || [ -L "$1" ] && [ "$1" != '/' ]; then
  rm -rdfv "$1"
fi
}

check_cd(){
cd "$*" || { echo "ERROR: \"$* \" not found" && exit 1 ;}
}

check_unmount(){
if [ "$1" ]; then
  grep -qw "$1" /proc/mounts && umount "$1"
fi
}

check_bool(){
case $(cat) in
  'no'|'false'|'off'|'0') printf "${c1}0${c_end}";;
   'yes'|'true'|'on'|'1') printf "${c2}1${c_end}";;
                       *) printf "${c7}?${c_end}";;
esac
}

#==============================================================================#
crypt_close(){
if [ "$1" ] && [ -e "$2" ]; then
  case "$1" in
                     'LUKS') cryptsetup luksClose "$2";;
    'TrueCrypt'|'VeraCrypt') cryptsetup tcryptClose "$2";;
  esac
fi
}


#==============================================================================#
qemu_test(){
if [ -e "$1" ]; then
  case $(uname -m) in
         'x86_64') if [ -e /dev/kvm ]; then
                     sudo qemu-system-x86_64 -enable-kvm -cpu host -m 2560 -drive format=raw,file="$1"
                   else
                     sudo qemu-system-x86_64 -m 2560 -drive format=raw,file="$1"
                   fi;;
    'i386'|'i686') if [ -e /dev/kvm ]; then
                     sudo qemu-system-i386 -enable-kvm -cpu host -m 2560 -drive format=raw,file="$1"
                   else
                     sudo qemu-system-i386 -m 2560 -drive format=raw,file="$1"
                   fi;;
  esac
fi
}

# █▀▄ █▀▀ █▀▄ █▀█ █▀█ ▀█▀ █▀▀ ▀█▀ █▀▄ █▀█ █▀█
# █ █ █▀▀ █▀▄ █ █ █ █  █  ▀▀█  █  █▀▄ █▀█ █▀▀
# ▀▀  ▀▀▀ ▀▀  ▀▀▀ ▀▀▀  ▀  ▀▀▀  ▀  ▀ ▀ ▀ ▀ ▀  
debian_bootstrap(){
PKG_LIST_REQUIRED='apt,base-files,base-passwd,bash,bsdutils,coreutils,dash,debconf,debianutils,diffutils,dpkg,e2fsprogs,findutils,grep,gzip,hostname,init-system-helpers,libc-bin,libpam-modules,libpam-modules-bin,libpam-runtime,login,mawk,mount,ncurses-base,ncurses-bin,passwd,perl-base,sed,sysvinit-utils,tar,tzdata,util-linux'
PKG_LIST_IMPORTANT='adduser,apt-utils,cpio,cron,cron-daemon-common,debconf-i18n,debian-archive-keyring,debian-keyring,dmidecode,fdisk,gpgv,groff-base,ifupdown,init,iproute2,iputils-ping,isc-dhcp-client,isc-dhcp-common,kmod,less,logrotate,man-db,nano,netbase,nftables,procps,readline-common,rsyslog,sensible-utils,tasksel-data,udev,vim-common,vim-tiny,whiptail'
PKG_LIST_WANTED='linux-image-amd64,linux-base,zsh,sudo,iw,wpasupplicant,net-tools,bc,wavemon,htop,nnn,fzf,lnav,apt-transport-https,debootstrap,fdisk,gdisk,curl,iptables,bzip2,zstd,isolinux,extlinux,syslinux'

  printf "${c2}starting debootstrap${c_end}\\n"
  debootstrap \
    --verbose \
    --variant=minbase \
    --include="$PKG_LIST_REQUIRED","$PKG_LIST_IMPORTANT","$PKG_LIST_WANTED" \
    --exclude=pulseaudio \
    --merged-usr \
    --arch amd64 \
    "stable" "/target" "https://deb.debian.org/debian"
#--exclude=systemd,sysv-rc,dbus,dbus-x11,pulseaudio \

  # pin systemd
  for pkg in 'systemd' 'systemd:i386' 'systemd-*'; do
    cat >> /target/etc/apt/preferences.d/nosystemd <<EOF
# $pkg
Package: $pkg
Pin: version *
Pin-Priority: -1
EOF
  done

  # pin dbus
#  for pkg in 'dbus' 'dbus:i386' 'dbus-*'; do
#    cat >> /target/etc/apt/preferences.d/nodbus <<EOF
# $pkg
#Package: $pkg
#Pin: version *
#Pin-Priority: -1
#EOF
#  done

  # pin pulseaudio
  cat > /target/etc/apt/preferences.d/nopulseaudio <<EOF
# pulseaudio
Package: pulseaudio
Pin: version *
Pin-Priority: -1
EOF

  gen_inittab
#  mount --make-rslave --rbind /proc /target/proc
#  mount --make-rslave --rbind /sys /target/sys
#  mount --make-rslave --rbind /dev /target/dev
#  mount --make-rslave --rbind /run /target/run

} # end of debian_bootstrap

#==============================================================================#
# Prepare logfile
EARLY_LOG="$LOGFILE_INSTALL"
LOG="$EARLY_LOG"
touch "$LOG"

log(){
strip_color | tee -a "$LOG"
}

#==============================================================================#
seperator(){
printf "\\n#==============================================================================#\\n" 2>&1 | log
}

date >> "$EARLY_LOG"
seperator

print_dots(){
  if [ "${#1}" -ge 0 ]; then
    own_len="${#1}"
  else
    own_len="$(echo "$1" | awk '{print length($1)}')"
  fi
  req_len="$2"
  dot_len="$((req_len-own_len+1))"
  printf "%${dot_len}s" "." | tr ' ' '.'
}

display_digits(){
  if [ ! "$font_style" ]; then
    font_style='fine'
  fi

  case "$font_style" in
   'fat')   zero_a=' █▀█'
            zero_b=' █ █'
            zero_c=' ▀▀▀'
            one_a=' ▀█ '
            one_b='  █ '
            one_c=' ▀▀▀'
            two_a=' ▀▀█'
            two_b=' █▀▀'
            two_c=' ▀▀▀'
            three_a=' ▀▀█'
            three_b=' ▀▀█'
            three_c=' ▀▀▀'
            four_a=' █ █'
            four_b=' ▀▀█'
            four_c='   ▀'
            five_a=' █▀▀'
            five_b=' ▀▀█'
            five_c=' ▀▀▀'
            six_a=' █▀▀'
            six_b=' █▀█'
            six_c=' ▀▀▀'
            seven_a=' ▀▀█'
            seven_b=' ▄▀ '
            seven_c=' ▀  '
            eight_a=' █▀█'
            eight_b=' █▀█'
            eight_c=' ▀▀▀'
            nine_a=' █▀█'
            nine_b=' ▀▀█'
            nine_c=' ▀▀▀'
            colon_a='    '
            colon_b='  ▀ '
            colon_c='  ▀ '
            dot_a='    '
            dot_b='    '
            dot_c='  ▀ '
            day_a=' █▀▄ █▀█ █ █           '
            day_b=' █ █ █▀█  █            '
            day_c=' ▀▀  ▀ ▀  ▀            '
            month_a=' █▄█ █▀█ █▀█ ▀█▀ █ █   '
            month_b=' █ █ █ █ █ █  █  █▀█   '
            month_c=' ▀ ▀ ▀▀▀ ▀ ▀  ▀  ▀ ▀   '
            year_a=' █ █ █▀▀ █▀█ █▀▄       '
            year_b='  █  █▀▀ █▀█ █▀▄       '
            year_c='  ▀  ▀▀▀ ▀ ▀ ▀ ▀       '
            ;;
   'fine')  zero_a=' ┏━┓'
            zero_b=' ┃ ┃'
            zero_c=' ┗━┛'
            one_a=' ╺┓ '
            one_b='  ┃ '
            one_c=' ╺┻╸'
            two_a=' ┏━┓'
            two_b=' ┏━┛'
            two_c=' ┗━╸'
            three_a=' ┏━┓'
            three_b=' ╺━┫'
            three_c=' ┗━┛'
            four_a=' ╻ ╻'
            four_b=' ┗━┫'
            four_c='   ╹'
            five_a=' ┏━╸'
            five_b=' ┗━┓'
            five_c=' ┗━┛'
            six_a=' ┏━┓'
            six_b=' ┣━┓'
            six_c=' ┗━┛'
            seven_a=' ┏━┓'
            seven_b='   ┃'
            seven_c='   ╹'
            eight_a=' ┏━┓'
            eight_b=' ┣━┫'
            eight_c=' ┗━┛'
            nine_a=' ┏━┓'
            nine_b=' ┗━┫'
            nine_c=' ┗━┛'
            colon_a='    '
            colon_b='  ╹ '
            colon_c='  ╹ '
            dot_a='    '
            dot_b='    '
            dot_c='  ╹ '
            day_a=' ╺┳┓┏━┓╻ ╻             '
            day_b='  ┃┃┣━┫┗┳┛             '
            day_c=' ╺┻┛╹ ╹ ╹              '
            month_a=' ┏┳┓┏━┓┏┓╻╺┳╸╻ ╻       '
            month_b=' ┃┃┃┃ ┃┃┗┫ ┃ ┣━┫       '
            month_c=' ╹ ╹┗━┛╹ ╹ ╹ ╹ ╹       '
            year_a=' ╻ ╻┏━╸┏━┓┏━┓          '
            year_b=' ┗┳┛┣╸ ┣━┫┣┳┛          '
            year_c='  ╹ ┗━╸╹ ╹╹┗╸          '
            ;;
    'ugly') zero_a='  _ '
            zero_b=' | |'
            zero_c=' |_|'
            one_a='  '
            one_b=' |'
            one_c=' |'
            two_a='  _ '
            two_b='  _|'
            two_c=' |_ '
            three_a=' _ '
            three_b=' _|'
            three_c=' _|'
            four_a='    '
            four_b=' |_|'
            four_c='   |'
            five_a='  _ '
            five_b=' |_ '
            five_c='  _|'
            six_a='  _ '
            six_b=' |_ '
            six_c=' |_|'
            seven_a=' __ '
            seven_b='   |'
            seven_c='   |'
            eight_a='  _'
            eight_b=' |_|'
            eight_c=' |_|'
            nine_a='  _ '
            nine_b=' |_|'
            nine_c='   |'
            colon_a='   '
            colon_b=' o '
            colon_c=' o '
            dot_a='   '
            dot_b='   '
            dot_c=' o '
            day_a='         '
            day_b=' Day:    '
            day_c='         '
            month_a='            '
            month_b=' Month:     '
            month_c='            '
            year_a='           '
            year_b=' Year:     '
            year_c='           '
            ;;
  esac

  while [ "$#" -gt '0' ]; do
    if [ "$1" = 'offset' ]; then
      printf "     "
    fi
    shift
    if [ "$digit_line" = 'a' ]; then
      case "$1" in
       '1') printf "%s" "$one_a"  ;;
       '2') printf "%s" "$two_a"  ;;
       '3') printf "%s" "$three_a";;
       '4') printf "%s" "$four_a" ;;
       '5') printf "%s" "$five_a" ;;
       '6') printf "%s" "$six_a"  ;;
       '7') printf "%s" "$seven_a";;
       '8') printf "%s" "$eight_a";;
       '9') printf "%s" "$nine_a" ;;
       '0') printf "%s" "$zero_a" ;;
       ':') printf "%s" "$colon_a";;
       '.') printf "%s" "$dot_a"  ;;
       'day') printf "%s" "$day_a";;
       'month') printf "%s" "$month_a";;
       'year') printf "%s" "$year_a";;
      esac
    elif [ "$digit_line" = 'b' ]; then
      case "$1" in
       '1') printf "%s" "$one_b"  ;;
       '2') printf "%s" "$two_b"  ;;
       '3') printf "%s" "$three_b";;
       '4') printf "%s" "$four_b" ;;
       '5') printf "%s" "$five_b" ;;
       '6') printf "%s" "$six_b"  ;;
       '7') printf "%s" "$seven_b";;
       '8') printf "%s" "$eight_b";;
       '9') printf "%s" "$nine_b" ;;
       '0') printf "%s" "$zero_b" ;;
       ':') printf "%s" "$colon_b";;
       '.') printf "%s" "$dot_b"  ;;
       'day') printf "%s" "$day_b";;
       'month') printf "%s" "$month_b";;
       'year') printf "%s" "$year_b";;
      esac
    elif [ "$digit_line" = 'c' ]; then
      case "$1" in
       '1') printf "%s" "$one_c"  ;;
       '2') printf "%s" "$two_c"  ;;
       '3') printf "%s" "$three_c";;
       '4') printf "%s" "$four_c" ;;
       '5') printf "%s" "$five_c" ;;
       '6') printf "%s" "$six_c"  ;;
       '7') printf "%s" "$seven_c";;
       '8') printf "%s" "$eight_c";;
       '9') printf "%s" "$nine_c" ;;
       '0') printf "%s" "$zero_c" ;;
       ':') printf "%s" "$colon_c";;
       '.') printf "%s" "$dot_c"  ;;
       'day') printf "%s" "$day_c";;
       'month') printf "%s" "$month_c";;
       'year') printf "%s" "$year_c";;
      esac
    fi
  done
  printf "\\n"
}

#==============================================================================#
# Check what distro this is
check_base_distro(){
# $1 = root path of distro (default = /)
  printf "Detecting Distro: " 2>&1 | log
  if [ -n "$1" ]; then
    if [ -d "$1" ]; then
      ROOT_DIR="${1%%/}"
    else
      printf "Error: %s is not a directory" "$1"; exit 1
    fi
  fi

  if [ -f "${ROOT_DIR}/etc/os-release" ]; then
    BASE_DISTRO="$(awk -F '=' '/^ID=/ {print $2}' "${ROOT_DIR}/etc/os-release")"
    BASE_DISTRO_LIKE="$(awk -F '=' '/^ID_LIKE=/ {print $2}' "${ROOT_DIR}/etc/os-release")"
  fi

  if [ -z "$BASE_DISTRO_LIKE" ]; then
    if [ -e "${ROOT_DIR}/bin/dpkg" ] || [ -e "${ROOT_DIR}/usr/bin/dpkg" ]; then
      BASE_DISTRO_LIKE='debian'
    elif [ -e "${ROOT_DIR}/bin/apt" ] || [ -e "${ROOT_DIR}/usr/bin/apt" ]; then
      BASE_DISTRO_LIKE='debian'
    elif [ -e "${ROOT_DIR}/bin/pacman" ] || [ -e "${ROOT_DIR}/usr/bin/pacman" ]; then
      BASE_DISTRO_LIKE='arch'
    fi
  fi

HYPERBOLA_TESTING="1" # will use hyperbola init hooks, does not work
  if [ "$BASE_DISTRO" = 'hyperbola' ]; then
    if [ "$HYPERBOLA_TESTING" = '1' ]; then
      BASE_DISTRO_LIKE='hyperbola'
    else
      BASE_DISTRO_LIKE='arch'
    fi
  fi

  if [ -z "$BASE_DISTRO_LIKE" ]; then
    if [ "$BASE_DISTRO" = 'arch' ] || [ "$BASE_DISTRO" = 'artix' ];then
      BASE_DISTRO_LIKE='arch'
    elif [ "$BASE_DISTRO" = 'hyperbola' ] && [ "$HYPERBOLA_TESTING" = '1' ]; then
      BASE_DISTRO_LIKE='hyperbola'
    elif [ "$BASE_DISTRO" = 'hyperbola' ] && [ "$HYPERBOLA_TESTING" != '1' ]; then
      BASE_DISTRO_LIKE='arch'
    fi
  fi

  if [ -z "$BASE_DISTRO" ] && [ -n "$BASE_DISTRO_LIKE" ]; then
    BASE_DISTRO="$BASE_DISTRO_LIKE"
    printf "${c5}%s${c_end}" "$BASE_DISTRO" 2>&1 | log
  elif [ -n "$BASE_DISTRO" ] && [ -z "$BASE_DISTRO_LIKE" ]; then
    BASE_DISTRO_LIKE="$BASE_DISTRO"
  elif [ -z "$BASE_DISTRO" ] && [ -z "$BASE_DISTRO_LIKE" ]; then
    printf "Unknown" 2>&1 | log
    exit 1
  else
    printf "${c5}%s${c_end}" "$BASE_DISTRO" 2>&1 | log
  fi

  if [  "$BASE_DISTRO" != "$BASE_DISTRO_LIKE" ]; then
    printf "\n........based on: ${c5}%s${c_end}\\n" "$BASE_DISTRO_LIKE" 2>&1 | log
  else
    printf "\\n" 2>&1 | log
  fi

  if [ "$BASE_DISTRO_LIKE" != 'debian' ] && \
     [ "$BASE_DISTRO_LIKE" != 'arch' ] && \
     [ "$BASE_DISTRO_LIKE" != 'hyperbola' ]; then
    printf "Error: could not detect base distro\\n" 2>&1 | log
    exit 1
  fi
}

#==============================================================================#
# Store current directory
CURRENT_DIR="$(pwd)"

popcd(){
if [ "$CURRENT_DIR" != "$(pwd)" ]; then
  check_cd "$CURRENT_DIR"
fi
}

#==============================================================================#
# Check if internet connection works
check_internet(){
if [ "$(ping 'gnu.org' -nqc 3 | awk -F ', ' '/received/ {gsub(" ? received", ""); print $2 }' )" = "3" ]; then
  INTERNET_STATUS='UP'
else
  INTERNET_STATUS='DOWN'
fi
}

# █▀▀ █ █ █▀▀ █▀▀ █ █   █▀▄ █▀▀ █▀█ █▀▀ █▀█ █▀▄ █▀▀
# █   █▀█ █▀▀ █   █▀▄   █ █ █▀▀ █▀▀ █▀▀ █ █ █ █ ▀▀█
# ▀▀▀ ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀▀  ▀▀▀ ▀   ▀▀▀ ▀ ▀ ▀▀  ▀▀▀
#exec 2>&1

bload_list(){
if [ -z "$bl_list" ]; then
  bl_list='> none'
fi
if [ "$1" ]; then
  if [ "$1" = '-r' ] && [ "$2" ]; then
    bl_list="$(echo "$bl_list" | awk -v var="$2" '$1 != var{print}')"
  else
    bl_list="${bl_list}\\n> $1"
  fi
else
  printf "\\n%s" "$bl_list"
fi
}

check_isolinuxbin(){
  if [ -f /usr/lib/ISOLINUX/isolinux.bin ]; then
    isolinuxbin="/usr/lib/ISOLINUX/isolinux.bin"
  elif [ -f /usr/lib/syslinux/isolinux.bin ]; then
    isolinuxbin="/usr/lib/syslinux/isolinux.bin"
  elif [ -f /usr/lib/syslinux/bios/isolinux.bin ]; then
    isolinuxbin="/usr/lib/syslinux/bios/isolinux.bin"
  fi
}

check_dependencies(){
seperator
check_base_distro
printf "Checking dependencies:\\n" >> "$EARLY_LOG"

REQUIRED="lvm cat cut find df du grep sed awk tr fdisk sgdisk mksquashfs
          lsblk mkinitcpio cpio bzip2 gzip zstd xz lzop extlinux mkdosfs
          syslinux xorriso rsync chmod chown mknod mkdir debootstrap "
# optional required ??

# https://www.truecrypt71a.com/
# https://www.veracrypt.fr/en/Downloads.html

# find longest string length
REQUIRED_LEN='0'
for x in $(echo "$REQUIRED"); do
  num=$(echo "$x" | awk 'length > l {l=length}; END{printf "%s",l}')
  if [ "$num" -gt "$REQUIRED_LEN" ]; then
    REQUIRED_LEN="$num"
  fi
done

for x in $(echo "$REQUIRED"); do
  printf "Searching ${txt_b}%s${txt_n}..." "$x" 2>> "$EARLY_LOG"
  print_dots "$x" "$REQUIRED_LEN" 2>> "$EARLY_LOG"
  if command -v "$x" > /dev/null \
  || command -v /bin/"$x" > /dev/null \
  || command -v /usr/bin/"$x" > /dev/null \
  || command -v /sbin/"$x" > /dev/null \
  || command -v /usr/sbin/"$x" > /dev/null \
  || command -v /usr/local/bin/"$x" > /dev/null \
  || command -v /usr/local/sbin/"$x" > /dev/null ; then
    printf "${c2}OK${c_end}\\n" 2>> "$EARLY_LOG"
  else
    printf "${c1}Error${c_end}\\n" 2>> "$EARLY_LOG"
    case "$x" in
      'mksquashfs') MISSING="$MISSING squashfs-tools";;
         'mkdosfs') MISSING="$MISSING dosfstools";;
            'cpio') MISSING="$MISSING cpio";;
         'xorriso') case "$BASE_DISTRO_LIKE" in
                      'arch') MISSING="$MISSING libisoburn";;
                           *) MISSING="$MISSING xorriso";;
                    esac;;
            'find') MISSING="$MISSING findutils";;
             'lvm') MISSING="$MISSING lvm2";;
           'rsync') MISSING="$MISSING rsync";;
        'extlinux') case "$BASE_DISTRO_LIKE" in
                          'arch') MISSING="$MISSING syslinux";;
                      'debian'|*) MISSING="$MISSING extlinux";;
                    esac;;
            'zstd') MISSING="$MISSING zstd";;
           'fdisk') MISSING="$MISSING fdisk";;
          'sgdisk') if [ "$BASE_DISTRO_LIKE" = 'arch' ]; then
                      MISSING="$MISSING gptfdisk"
                    fi;;
      'mkinitcpio') if [ "$BASE_DISTRO_LIKE" = 'arch' ]; then
                      MISSING="$MISSING mkinitcpio"
                    fi;;
     'debootstrap') case "$START_MENU" in
                      '> bootstrap'*) MISSING="$MISSING debootstrap"
                    esac
    esac
  fi
done
unset x

check_isolinuxbin
if [ ! -f "$isolinuxbin" ]; then
  case "$BASE_DISTRO_LIKE" in
    'debian') MISSING="$MISSING isolinux";;
      'arch') MISSING="$MISSING syslinux";;
  esac
fi

# check grub
  printf "Searching ${txt_b}grub${txt_n}..." 2>> "$EARLY_LOG"
  print_dots 4 10 2>> "$EARLY_LOG"

case "$BASE_DISTRO_LIKE" in
  'debian') if [ "$(dpkg -l | awk '/grub/ {if ($1 ~ "ii|ih") {print $2}}')" ]; then
              bload_list grub
              SCRIPT_STATUS_GRUB='OK'
              printf "${c2}OK${c_end}\\n" 2>> "$EARLY_LOG"
            else
              SCRIPT_STATUS_GRUB='Not Found'
              MISSING="$MISSING grub-pc-bin grub-common"
              printf "${c1}ERROR${c_end}\\n" 2>> "$EARLY_LOG"
            fi;;
    'arch') if [ "$(pacman -Ql grub 2>> /dev/null)" ] ; then
              bload_list grub
              SCRIPT_STATUS_GRUB='OK'
              printf "${c2}OK${c_end}\\n" 2>> "$EARLY_LOG"
            else
              SCRIPT_STATUS_GRUB='Not Found'
              MISSING="$MISSING grub"
              printf "${c1}ERROR${c_end}\\n" 2>> "$EARLY_LOG"
            fi;;
esac
echo "SCRIPT_STATUS_GRUB=$SCRIPT_STATUS_GRUB" >> "$EARLY_LOG"

  if [ ! -d '/usr/lib/syslinux' ] && [ ! -d '/lib/syslinux' ]; then
  case "$BASE_DISTRO_LIKE" in
    'debian') MISSING="$MISSING syslinux-common";;
  esac
  fi

# check extlinux
if [ -f '/usr/bin/extlinux' ]; then
  SCRIPT_STATUS_EXTLINUX='OK'
  bload_list extlinux
else
  SCRIPT_STATUS_EXTLINUX='Not found'
  case "$BASE_DISTRO_LIKE" in
    'debian') MISSING="$MISSING extlinux";;
      'arch') MISSING="$MISSING syslinux";;
  esac
fi
# check syslinux
if [ -f '/usr/bin/syslinux' ]; then
  SCRIPT_STATUS_SYSLINUX='OK'
else
  SCRIPT_STATUS_SYSLINUX='Not found'
fi
echo "SCRIPT_STATUS_SYSLINUX=$SCRIPT_STATUS_SYSLINUX" >> "$EARLY_LOG"

# check syslinux-efi
if [ -f '/usr/bin/syslinux-efi' ]; then
  SCRIPT_STATUS_SYSLINUX_EFI='OK'
else
  SCRIPT_STATUS_SYSLINUX_EFI='Not found'
fi
echo "SCRIPT_STATUS_SYSLINUX_EFI=$SCRIPT_STATUS_SYSLINUX_EFI" >> "$EARLY_LOG"

# check live-boot
case "$BASE_DISTRO_LIKE" in
  'debian') # live-boot
            if [ ! -f '/bin/live-boot' ]; then
              MISSING="$MISSING live-boot"
            fi

            # live-boot-initramfs-tools
            if [ ! -f '/usr/share/initramfs-tools/hooks/live' ]; then
              MISSING="$MISSING live-boot-initramfs-tools"
            fi

            # live-config
            if [ ! -f '/bin/live-config' ]; then
              MISSING="$MISSING live-config"
            fi

            # live-config-sysvinit
            if ! find /lib/live/config/*sysvinit -maxdepth 0 2>> /dev/null \
            && command -v /sbin/openrc 2>> /dev/null; then
              MISSING="$MISSING live-config-sysvinit"
            fi

            # live-tools
            if [ ! -f '/etc/init.d/live-tools' ]; then
              MISSING="$MISSING live-tools"
            fi
            ;;
esac

# artix initcpio hooks
if [ "$BASE_DISTRO" = 'artix' ]; then
  printf "Searching /etc/initcpio/hooks/artix..." 2>> "$EARLY_LOG"

  if [ ! -f /etc/initcpio/hooks/artix ]; then
    MISSING="$MISSING iso-initcpio"
    printf "Error\\n" 2>> "$EARLY_LOG"
  fi
fi

# hyperbola initcpio hooks
if [ "$BASE_DISTRO" = 'hyperbola' ]; then
  printf "Searching /lib/initcpio/hooks/hyperiso..." 2>> "$EARLY_LOG"
  if [ ! -f /lib/initcpio/hooks/hyperiso ]; then
    MISSING="$MISSING hyperiso"
    printf "Error\\n" 2>> "$EARLY_LOG"
  fi
fi

# remove dubles and print on one line
MISSING="$(echo "$MISSING" | awk 'NF && !x[$0]++{printf "%s ",$0}')"

# download missing packages
if [ "$(echo "$MISSING" | awk '{print NF}')" -ge '1' ]; then
  printf "\\n${c1}Error:${c_end} Could not find the following package(s):\\n ${c4}%s${c_end}\\n" "$MISSING"
  printf "\\ninstall now? [Y|n]"
  unset ans
  read -r 'ans'
  if [ ! "$ans" ]; then
    ans='y'
  fi
  if expr "$ans" : "^[Yy]" >>/dev/null; then
    check_internet
    if [ "$INTERNET_STATUS" = 'UP' ]; then
      case "$BASE_DISTRO_LIKE" in
        'debian') apt update
                  awk -v l="$MISSING" 'BEGIN { printf "\033\13332m"
                              cmd = "apt install --no-install-recommends -y "l
                              while ( ( cmd | getline ans ) > 0 ) {
                                printf "%s\n",ans
                              }
                              close(cmd);
                              exit
                            }
                            END{printf "\033\1330m"}';;
          'arch') pacman -Syyu
                  awk -v l="$MISSING" 'BEGIN { printf "\033\13332m"
                              cmd = "pacman -S "l
                              while ( ( cmd | getline ans ) > 0 ) {
                                printf "%s\n",ans
                              }
                              close(cmd);
                              exit
                            }
                            END{printf "\033\1330m"}';;
      esac
    else
      printf "${c1}Error:${c_end} no internet connection?\\n"
      exit 1
    fi
  fi
fi
unset MISSING REQUIRED

if command -v lsinitramfs >> /dev/null ; then
  LSINITFS='lsinitramfs'
elif command -v lsinitcpio >> /dev/null ; then
  LSINITFS='lsinitcpio'
else
  printf "Error: could not find lsinitcpio or lsinitramfs"
  exit 1
fi

#==============================================================================#
# search crypto tools
unset CRYPTO_TOOLS
# check TrueCrypt
printf "Searching truecrypt..." >> "$EARLY_LOG"
if command -v truecrypt >> /dev/null || command -v /sbin/truecrypt >> /dev/null ; then
  CRYPTO_TOOLS="$CRYPTO_TOOLS \\n > TrueCrypt"
  printf "OK\\n" >> "$EARLY_LOG"
else
  CRYPTO_TOOLS="$CRYPTO_TOOLS\\nTrueCrypt (Not available)"
  printf "Error\\n" >> "$EARLY_LOG"
fi

# check VeraCryt
printf "Searching veracrypt..." >> "$EARLY_LOG"
if command -v veracrypt >> /dev/null || command -v /sbin/veracrypt >> /dev/null ; then
  CRYPTO_TOOLS="$CRYPTO_TOOLS \\n > VeraCrypt"
  printf "OK\\n" >> "$EARLY_LOG"
else
  CRYPTO_TOOLS="$CRYPTO_TOOLS\\nVeraCrypt (Not available)"
  printf "Error\\n" >> "$EARLY_LOG"
fi

# check cryptsetup
printf "Searching cryptsetup..." >> "$EARLY_LOG"
if command -v cryptsetup >> /dev/null || command -v /sbin/cryptsetup >> /dev/null ; then
  CRYPTO_TOOLS="$CRYPTO_TOOLS\\n > LUKS"
  printf "OK\\n" >> "$EARLY_LOG"
else
  CRYPTO_TOOLS="(cryptsetup not found)"
  printf "Error\\n" >> "$EARLY_LOG"
fi

CRYPTO_TOOLS="$(echo "$CRYPTO_TOOLS" | awk '!/^[[:space:]]*$/')"
#==============================================================================#
# search partitioning tools
unset PARTITION_TOOLS
PARTITION_TOOL_SUGGESTIONS="fdisk cfdisk gdisk parted gparted shred"
for x in $(echo "$PARTITION_TOOL_SUGGESTIONS"); do
  printf "Searching %s..." "$x" >> "$EARLY_LOG"
  if command -v "$x" >> /dev/null || command -v /sbin/"$x" >> /dev/null ; then
    PARTITION_TOOLS="$PARTITION_TOOLS\\n > $x"
    printf "OK\\n" >> "$EARLY_LOG"
  else
    printf "Error\\n" >> "$EARLY_LOG"
  fi
done
unset PARTITION_TOOL_SUGGESTIONS
PARTITION_TOOLS="$(echo "$PARTITION_TOOLS" | awk '!/^[[:space:]]*$/')"

#==============================================================================#
# check isohdpfx
if [ -f /usr/lib/syslinux/mbr/isohdpfx.bin ]; then
  isohdpfxbin="/usr/lib/syslinux/mbr/isohdpfx.bin"
elif [ -f /usr/lib/syslinux/isohdpfx.bin ]; then
  isohdpfxbin="/usr/lib/syslinux/isohdpfx.bin"
elif [ -f /usr/lib/ISOLINUX/isohdpfx.bin ]; then
  isohdpfxbin="/usr/lib/ISOLINUX/isohdpfx.bin"
elif [ -f /usr/lib/syslinux/bios/isohdpfx.bin ]; then
  isohdpfxbin="/usr/lib/syslinux/bios/isohdpfx.bin"
else
  printf "Cannot create isohybrid.\\n"
  printf "File: isohdpfx.bin not found.\\n"
  printf "The resulting image will be a standard iso file.\\n"
fi

#check_dependencies
#==============================================================================#
# Set text editor
if [ -x /bin/nano ] || [ -x /usr/bin/nano ] || [ -x /usr/local/bin/nano ]; then
  TEXT_EDITOR='nano'
elif [ -x /bin/nano-tiny ] || [ -x /usr/bin/nano-tiny ] || [ -x /usr/local/bin/nano-tiny ]; then
  TEXT_EDITOR='nano-tiny'
elif [ -x /bin/nvim ] || [ -x /usr/bin/nvim ] || [ -x /usr/local/bin/nvim ]; then
  TEXT_EDITOR='nvim'
elif [ -x /bin/vim ] || [ -x /usr/bin/vim ] || [ -x /usr/local/bin/vim ]; then
  TEXT_EDITOR='vim'
elif [ -x /bin/vim-tiny ] || [ -x /usr/bin/vim-tiny ] || [ -x /usr/local/bin/vim-tiny ]; then
  TEXT_EDITOR='vim-tiny'
elif [ -x /bin/vi ] || [ -x /usr/bin/vi ] || [ -x /usr/local/bin/vi ]; then
  TEXT_EDITOR='vi'
elif [ -x /bin/pico ] || [ -x /usr/bin/pico ] || [ -x /usr/local/bin/pico ]; then
  TEXT_EDITOR='pico'
elif [ -x /bin/joe ] || [ -x /usr/bin/joe ] || [ -x /usr/local/bin/joe ]; then
  TEXT_EDITOR='joe'
elif [ -x /bin/ed ] || [ -x /usr/bin/ed ] || [ -x /usr/local/bin/ed ]; then
  TEXT_EDITOR='ed'
elif [ -x /bin/emacs ] || [ -x /usr/bin/emacs ] || [ -x /usr/local/bin/emacs ]; then
  TEXT_EDITOR='emacs'
else
  printf "\\nno texteditor found :(\\n"
  exit 1
fi

if [ -n "$MISSING" ]; then
  printf "Error: the following dependencies were not found:\\n"
  printf "%s\\n" "$MISSING"
  exit 1
fi
}

get_path(){
  if echo "$@" | grep -q '^/'; then
    echo "$@" | awk -F '/' '{OFS="/"; $NF=""}1'
  else
    printf "Error in function get_path: input did not contain a valid path\\n"
    exit 1
  fi
}

#==============================================================================#
awk_menu(){
INPUT="$*"

if [ -z "$LINE" ] || [ "$LINE" -lt 1 ]; then
  LINE="1"
elif [ "$LINE" -gt "$(echo "$INPUT" | awk 'END{print NR}')" ]; then
  LINE="$(echo "$INPUT" | awk 'END{print NR}')"
fi

awk -v mode="${MENU_MODE}" -v menumsg="${INPUT}" -v line="${LINE}" -v tmsg="$TOPTEXT" '
BEGIN {
  SUBSEP = ","
  RS = "\a"
  a_bold = "\033\1331m"
  a_reverse = "\033\1337m"
  a_clean = "\033\1332K"
  a_reset = "\033\133m"
  b_red = "\033\13341m"
  f_red = "\033\13331m"
  f_green = "\033\13332m"
  f_yellow = "\033\13333m"
  f_blue = "\033\13334m"
  f_magenta = "\033\13335m"
  f_cyan = "\033\13336m"
  f_white = "\033\13337m"

  if (start_page) { curpage = start_page }
  else { curpage = 1 }
  if (line) { cursor = line }
  else { cursor = 1 }

  system("stty -isig -icanon -echo")
  printf "\033\133?25l" >> "/dev/stderr" # hide cursor
  LANG = ENVIRON["LANG"]
  ENVIRON["LANG"] = C
  main();
}

function main() {
  do {
    list = menumsg
    delim = "\n"
    num = 1
    if ( tmsg ) {
      tmsg_len = split(tmsg,T,"\n")
      if (tmsg_len == 0) { tmsg_len=1 }
    }
    if ( bmsg ) {
      bmsg_len = split(bmsg,B,"\n")
      if (bmsg_len == 0) { bmsg_len=1 }
    }
    bmsg = "  " a_reverse "k|↑" a_reset " up  " "\t" a_reverse "j|↓" a_reset " down" "\t" a_reverse "Enter" a_reset " select" "\t" a_reverse "q" a_reset " back "
    menu_TUI( delim, tmsg, bmsg)
  } while (1)
}

function CUP(lines, cols) {
  printf("\033\133%s;%sH", lines, cols) >> "/dev/stderr"
}

function dim_setup() {
  cmd = "stty size"
  cmd | getline d
  close(cmd)
  split(d, dim, " ")
  top = 1
  bottom = dim[1];
  fin = bottom - ( bottom - top) % num
  end = fin - 1;
  dispnum = (bottom - bmsg_len - tmsg_len - tgap )
}

function menu_TUI_page(list, delim) {
  answer = ""
  page = 0
  split("", pagearr, ":") # delete saved array
  dim_setup()
  Narr = split(list, disp, delim)
  # generate display content for each page (pagearr)
  for (entry = 1; entry in disp; entry++) {
    if ((+entry) % (+dispnum) == 1 || Narr == 1) { # if first item in each page
       pagearr[++page] = disp[entry]
    }
    else {
       pagearr[page] = pagearr[page] "\n" disp[entry]
    }
    loc = disp[entry]
    gsub(/\033\[[0-9][0-9]m|\033\[[0-9]m|\033\[m/, "", loc)
    if (parent != "" && loc == parent) {
       cursor = entry - dispnum*(page - 1)
       curpage = page
    }
  }
}

function key_collect(list, pagerind) {
  key = ""; rep = 0
  do {
    cmd = " dd ibs=1 count=1 2>/dev/null"
    cmd | getline ans;
    close(cmd)
    gsub(/[\\^\[\]]/, "\\\\&", ans) # escape special char
    if (ans ~ /.*WINCH/ && pagerind == 0) { # trap SIGWINCH
       cursor = 1; curpage = 1;
       if (pagerind == 0) {
          menu_TUI_page(list, delim)
          redraw(tmsg, bmsg)
       }
       else if (pagerind == 1) {
          printf "\033\1332J\033\133H" >> "/dev/stderr"
          dim_setup()
          Npager = (Nmsgarr >= dim[1] ? dim[1] : Nmsgarr)
          for (i = 1; i <= Npager; i++) {
            CUP(i, 1)
            printf "%s", msgarr[i] >> "/dev/stderr"
           }
       }
      gsub(/WINCH/, "", ans);
    }
    if (ans ~ /\033/ && rep == 1) { ans = ""; continue; } # first char of escape seq
    else { key = key ans; }
    if (key ~ /[^\x00-\x7f]/) { break } # print non-ascii char
    if (key ~ /^\\\[5$|^\\\[6$$/) { ans = ""; continue; } # PageUp / PageDown
  } while (ans !~ /[\x00-\x5a]|[\x5f-\x7f]/)
  return key
}

function redraw(tmsg, bmsg) {
  if ( menu_mode != "Z" && menu_mode != "K" ) {
    # clear screen and move cursor to 0, 0
    printf "\033\1332J\033\133H" >> "/dev/stderr"
  }
  # draw tmsg
  CUP(top, 1)
  print tmsg >> "/dev/stderr"

  # draw page
  CUP(top + tmsg_len + tgap , 1)
  print pagearr[curpage] >> "/dev/stderr"

  # draw cursor
  CUP(top + tmsg_len + tgap + cursor*num - num, 1)
  printf "%s%s%s", a_reverse, disp[Ncursor], a_reset >> "/dev/stderr"

  if ( bmsg != "off" ) {
    CUP(dim[1] , 1)
    # draw page counter
    if ( show_page != "off" ) {
      printf " [%sPage %s%d%s / %s%s%d%s] ",f_white, a_bold, curpage, a_reset, f_white, a_bold, page, a_reset >> "/dev/stderr"
      CUP(dim[1] , 17)
    }
    # draw bmsg
    printf "%s",bmsg >> "/dev/stderr"
  }
}

function pager(msg) { # pager to print out stuff and navigate
  printf "\033\1332J\033\133H" >> "/dev/stderr"
  Nmsgarr = split(msg, msgarr, "\n")
  Npager = (Nmsgarr >= dim[1] ? dim[1] : Nmsgarr)
  for (i = 1; i <= Npager; i++) {
    CUP(i, 1)
    printf "%s", msgarr[i] >> "/dev/stderr"
  }
  pagerind = 1;
  while (key = key_collect(list, pagerind)) {
    if (key == "\003" || key == "\033" || key == "q" || key == "h" )
      break
    if ((key == "j" || key ~ /\[B/) && i < Nmsgarr)
      { printf "\033\133%d;H\n", Npager >> "/dev/stderr"; printf msgarr[i++] >> "/dev/stderr" }
    if ((key == "k" || key ~ /\[A/) && i > dim[1] + 1)
      { printf "\033\133H\033\133L" >> "/dev/stderr"; i--; printf msgarr[i-dim[1]] >> "/dev/stderr" }
  }
  pagerind = 0;
}

function menu_TUI( delim, tmsg, bmsg) {
  if (mode == "NUM_INPUT" ) {
    while (input !~ /[0-9]|\[[A-D]|[hjklq]|\003/ ) {
      input = key_collect(list, pagerind)
      if ( input ~ /[0-9]/ ) { printf "%s",input; exit }
      if ( input ~ /\[A|k/ ) { printf "up"; exit }
      if ( input ~ /\[B|j/ ) { printf "down"; exit }
      if ( input ~ /\[C|l/ ) { printf "right"; exit }
      if ( input ~ /\[D|h/ ) { printf "left"; exit }
      if ( input == "\n" ) { printf "done"; exit }
      if ( input ~ /q|\003/ ) {printf "exit"; exit }
    }
    exit
  }

  menu_TUI_page(list, delim)
  while (answer !~ /^[[:digit:]]/) {
    oldCursor = 1;
      while (1) {
    ## calculate cursor and Ncursor
    cursor = ( cursor+dispnum*(curpage-1) > Narr ? Narr - dispnum*(curpage-1) : cursor )
    Ncursor = cursor+dispnum*(curpage-1)
    redraw(tmsg, bmsg)
        answer = key_collect(list, pagerind)
#  Key: entry choosing and searching
#      if (answer ~ /[?]/ || answer == "h" || answer == "\061") { pager(help); answer = ""; break; exit}
        ########################
        #  Key: Total Redraw   #
        ########################
#        if ( answer == "\n" || answer == "l" || answer ~ /\[C/ ) { answer = Ncursor; break }
        if ( answer == "\n" ) { answer = Ncursor; break }
        if ( answer ~ /q|\003/ ) exit
        if ( (answer == "K" || answer ~ /\[H/) && ( curpage != 1 || cursor != 1 ) ) { curpage = 1; cursor = 1; break }
        if ( (answer == "K" || answer ~ /\[H/) && curpage = 1 && cursor == 1 ) continue
        if ( (answer == "J" || answer ~ /\[F/) && ( curpage != page || cursor != Narr - dispnum*(curpage-1) ) ) { curpage = page; cursor = Narr - dispnum*(curpage-1); break }
        if ( (answer == "J" || answer ~ /\[F/) && curpage == page && cursor = Narr - dispnum*(curpage-1) ) continue
        if ( (answer == "n" || answer ~ /\[6~/) && +curpage < +page ) { curpage++; break }
        if ( (answer == "n" || answer ~ /\[6~/) && +curpage == +page && cursor != Narr - dispnum*(curpage-1) ) { cursor = ( +curpage == +page ? Narr - dispnum*(curpage-1) : dispnum ); break }
        if ( (answer == "n" || answer ~ /\[6~/) && +curpage == +page && cursor == Narr - dispnum*(curpage-1) ) continue
        if ( (answer == "p" || answer ~ /\[5~/) && +curpage == 1 && cursor != 1 ) { cursor = 1; break }
        if ( (answer == "p" || answer ~ /\[5~/) && +curpage == 1 && cursor == 1) continue
        if ( (answer == "G" || answer ~ /\[F/) && ( curpage != page || cursor != Narr - dispnum*(curpage-1) ) ) { curpage = page; cursor = Narr - dispnum*(curpage-1); break }
        if ( (answer == "G" || answer ~ /\[F/) && curpage == page && cursor = Narr - dispnum*(curpage-1) ) continue

        #########################
        #  Key: Partial Redraw  #
        #########################
        if ( (answer == "j" || answer ~ /\[B/) && +cursor <= +dispnum ) { oldCursor = cursor; cursor++; }
        if ( (answer == "j" || answer ~ /\[B/) && +cursor <= +dispnum && (!disp[cursor])) { oldCursor = cursor; cursor++; break  }
        if ( (answer == "j" || answer ~ /\[B/) && +cursor > +dispnum && +curpage < +page && +page > 1 ) { cursor = 1; curpage++; break }
        if ( (answer == "k" || answer ~ /\[A/) && +cursor == 1 && +curpage > 1 && +page > 1 ) { cursor = dispnum; curpage--; break }
        if ( (answer == "k" || answer ~ /\[A/) && +cursor > 1 ) { oldCursor = cursor; cursor--; }
        if ( (answer == "k" || answer ~ /\[A/) && +cursor > 1 && (!disp[cursor])) { oldCursor = cursor; cursor--; break }
      }
  }
  printf "%s\n",disp[Ncursor]
  exit
}
END {
  printf "\033\1332J\033\133H" >> "/dev/stderr" # clear screen
  printf "\033\133?7h" >> "/dev/stderr" # line wrap
  printf "\033\1338" >> "/dev/stderr" # restore cursor
  printf "\033\133?25h" >> "/dev/stderr" # show cursor
  printf "\033\133?1049l" >> "/dev/stderr" # back from alternate buffer
  system("stty isig icanon echo")
  ENVIRON["LANG"] = LANG; # restore LANG
}
' | awk '{ gsub(/^[ \t]+|[ \t]+$/, ""); print }'
}

#==============================================================================#
awk_menu_ask_exit(){
ASK_EXIT="$(awk_menu "   > Exit Script
   > Continue Script")"
if [ "$ASK_EXIT" = '> Exit Script' ]; then
  exit 0
fi
}

#==============================================================================#
check_mounts(){
MOUNTS="$(awk '$1 ~ "^/dev/" {print $1}' /proc/mounts)"
FSTAB="$(awk '$1 !~ "#" {print $1}' /etc/fstab)"
unset CHECK
for device in $MOUNTS; do
  if ! echo "$FSTAB" | grep -q "$device" \
  && ! echo "$FSTAB" | grep -q "$(lsblk "$device" -no UUID)"; then
    CHECK="$CHECK
$device"
  fi
done

if [ "$CHECK" ]; then
  while [ -n "$CHECK" ] && [ "$MESSAGE" != '> unmount' ] && [ "$MESSAGE" != '> continue' ]; do
    TOPTEXT="$(text_box $text_box_opt "  Looks like something is mounted and not listed in /etc/fstab
$CHECK${c_end}\\n ")"
    MESSAGE="$(awk_menu "    > unmount
    > continue
    < exit" | awk '{print $2}')"
    case "$MESSAGE" in
       'unmount') for x in $CHECK ; do
                    umount "$x" && CHECK=$(echo "$CHECK"|  grep -vw "$x" | grep "."); done;;
      'continue') break;;
        'exit'|*) exit 0;;
    esac
  done
fi
unset TOPTEXT
}


#==============================================================================#
string_test(){
  unset STRING_STATUS
  NEW_STRING="$(printf "%s" "$NEW_STRING" | awk '{ gsub(/^[ \t]+|[ \t]+$/, ""); print }')"
  STRING_LENGTH="$(expr length "$NEW_STRING")"
  if [ -z "$NEW_STRING" ] \
  || [ "$NEW_STRING" = "check_length_64" ] \
  || [ "$NEW_STRING" = "check_length_255" ] \
  || [ "$NEW_STRING" = "check_chars" ] \
  || [ "$NEW_STRING" = "check_forbidden" ] \
  || [ "$NEW_STRING" = "no_spaces" ] \
  || [ "$NEW_STRING" = "space_to_underscore" ] \
  || [ "$NEW_STRING" = "all_lower" ] \
  || [ "$NEW_STRING" = "all_upper" ] \
  || [ "$STRING_LENGTH" -lt 1 ]; then
    printf "Error: string_test function is missing an argument." 2>&1 | log
    exit 1
  else
    STRING_STATUS='GOOD'
  fi

while [ "$#" -gt '0' ]; do
  case "$1" in
    'no_spaces') # this removes all spaces
        if echo "$NEW_STRING" | grep -q ' '; then
          NEW_STRING=$(echo "$NEW_STRING" | tr -d '[:blank:]')
        fi;;
    'space_to_underscore') # convert spaces to underscores
        if echo "$NEW_STRING" | grep -q ' '; then
          NEW_STRING=$(echo "$NEW_STRING" | tr ' ' '_')
        fi;;
    'check_length_64') # check if string is longer than 64 characters
        if [ "$STRING_LENGTH" -gt '64' ]; then
          STRING_STATUS='BAD'
        fi;;
    'check_length_80') # check if string is longer than 80 characters
        if [ "$STRING_LENGTH" -gt '80' ]; then
          STRING_STATUS='BAD'
        fi;;
    'check_length_255') # check if string is longer than 255 characters
        if [ "$STRING_LENGTH" -gt '255' ]; then
          STRING_STATUS='BAD'
        fi;;
    'check_chars') # check if string uses special characters
                   # '-' and '_' are allowed except at beginning and end
        if [ "$(printf "%s" "$NEW_STRING" | cut -b 1,"$STRING_LENGTH" | tr -d '[:alnum:]')" ] \
        || [ "$(printf "%s" "$NEW_STRING" | cut -b 2-$((STRING_LENGTH-1)) | tr -d '[:alnum:]\-_')" ] ; then
          STRING_STATUS='BAD'
        fi;;
    'check_chars_allow_dots') # same as above but also allow dots
        if [ "$(printf "%s" "$NEW_STRING" | cut -b 1,"$STRING_LENGTH" | tr -d '[:alnum:]')" ] \
        || [ "$(printf "%s" "$NEW_STRING" | cut -b 2-$((STRING_LENGTH-1)) | tr -d '[:alnum:]\-_.')" ] ; then
          STRING_STATUS='BAD'
        fi;;
    'check_forbidden') # make sure username is not a group name
        for x in $(grep -v "$SUDOER" /etc/group | awk -F ':' '{print $1}'); do
          if [ "$NEW_STRING" = "$x" ]; then
            STRING_STATUS='BAD'
          fi
        done;;
    'all_lower') # convert string to lower case
      if echo "$NEW_STRING" | grep -q '[[:upper:]]'; then
        NEW_STRING="$(printf "%s" "$NEW_STRING" | tr '[:upper:]' '[:lower:]')"
      fi;;
    'all_upper') # convert string to upper case
      if echo "$NEW_STRING" | grep -q '[[:lower:]]'; then
        NEW_STRING="$(printf "%s" "$NEW_STRING" | tr '[:lower:]' '[:upper:]')"
      fi;;
  esac
  shift
done

if [ "$STRING_STATUS" = 'BAD' ]; then
  printf "\\nError: Invalid string\\n"
  sleep 2.5
fi
}

#==============================================================================#
check_path(){
unset NEW_PATH_STATUS

# check if path starts with '/' and remove it (beginning slash will be added automatically)
if [ "$(printf "%s" "$NEW_PATH" | awk '{print substr($0,1,1)}')" = "/" ]; then
  NEW_PATH=$(printf "%s" "$NEW_PATH" | awk '{print substr($0,2,length($0))}')
fi

# check length
NEW_PATH_LENGTH="$(expr length "$NEW_PATH")"
if [ -z "$NEW_PATH_LENGTH" ]; then
  NEW_PATH_ERR_MSG="Lenght error. Try again."
  NEW_PATH_STATUS="ERROR"
fi
if [ ! "$NEW_PATH_LENGTH" -ge 1 -a "$NEW_PATH_LENGTH" -le 63 ]; then
  NEW_PATH_ERR_MSG="Lenght error. Try again."
  NEW_PATH_STATUS="ERROR"
fi

# check if there are spaces
if expr index ' ' "$NEW_PATH"; then
  NEW_PATH_ERR_MSG="Syntax error. No spaces allowed."
  NEW_PATH_STATUS="ERROR"
fi

# check if path only contains alphanumerical characters and '.' or '/'
if [ "$(echo "$NEW_PATH" | tr -d /. | tr -d '[:alnum:]')" ]; then
  NEW_PATH_ERR_MSG="Syntax error: Invalid character detected."
  NEW_PATH_STATUS="ERROR"
fi

# make sure there is no '//'
if echo "$NEW_PATH" | grep -qF '//'; then
  NEW_PATH_ERR_MSG="Syntax error: '//' is not allowed."
  NEW_PATH_STATUS="ERROR"
fi

# make sure there is no '..'
if echo "$NEW_PATH" | grep -qF '..'; then
  NEW_PATH_ERR_MSG="Syntax error: '..' is not allowed."
  NEW_PATH_STATUS="ERROR"
fi

# make sure last byte is not '.'
if [ "$(echo "$NEW_PATH" | awk '{print substr($0,length($0),length($0))}')" = '.' ]; then
  NEW_PATH_ERR_MSG="Syntax error: '.' is not allowed as the last character."
  NEW_PATH_STATUS="ERROR"
fi

# make sure last byte is not '/'
if [ "$(echo "$NEW_PATH" | awk '{print substr($0,length($0),length($0))}')" = '/' ]; then
  NEW_PATH_ERR_MSG="Syntax error: '/' is not allowed as the last character."
  NEW_PATH_STATUS="ERROR"
fi

# give ok if there are no errors
if [ "$NEW_PATH_STATUS" = "ERROR" ]; then
  NEW_PATH=""
else
  NEW_PATH_STATUS="OK"
fi
}

#==============================================================================#
select_usb(){
if ! lsblk -do TRAN,SUBSYSTEMS | grep -q 'usb'; then
  echo "No USB Devices found!"
  exit 1
fi
TARGET_DEVICE=""
if [ -z "$TARGET_DEVICE" ]; then
  device_list="$(lsblk -dnpo NAME,TRAN,SUBSYSTEMS,SIZE,VENDOR,MODEL \
                 | awk '/usb/ {printf "%s (%s %s %s %s)\n",$1,$4,$5,$6,$7}')"
fi
TOPTEXT="$(text_box $text_box_opt "\\nselect USB device\\n\\n${WARNING}\\n")"
LINE="1"
while [ ! -b "$TARGET_DEVICE" ]; do
  TARGET_DEVICE="$(awk_menu "$device_list" | awk '{print $1}')"
  if [ -z "$TARGET_DEVICE" ]; then
    exit 0
  fi
done
MOUNTLIST="$(lsblk -no MOUNTPOINT $TARGET_DEVICE | awk 'NF')"
if expr "$MOUNTLIST" : "/"; then
  LINE="3"
  OK="0"
  while [ "$OK" != "1" ]; do
    TOPTEXT="$(text_box $text_box_opt "\\n  $TARGET_DEVICE is currently mounted\\n    ")"
    TARGET_MOUNTED="$(awk_menu "  > unmount now
  < exit" | awk '{print $2}')"
    case "$TARGET_MOUNTED" in
	  'unmount') OK="1"
                 for device in $(echo "$MOUNTLIST" | grep '/'); do
                   umount "$device"
                 done; unset MOUNTLIST;;
         'exit') OK="1"; exit 0;;
    esac
  done
  unset OK TOPTEXT
fi
clear
}

#==============================================================================#
show_time(){
  unset marker
  case "$1" in
    '-m1') marker='^^^';;
    '-m2') marker='▀▀▀';;
  esac
  TIME_STRING="offset $TIME_H_A $TIME_H_B : $TIME_M_A $TIME_M_B : $TIME_S_A $TIME_S_B"
  clear
  printf "\\n\\n    Enter the time:\\n\\n"
  for letter in a b c; do
    digit_line="$letter"
    display_digits $TIME_STRING
  done
  case "$POS" in
    '1')   printf "      %s\\n" "$marker";;
    '2')   printf "          %s\\n" "$marker";;
    '3')   printf "                  %s\\n" "$marker";;
    '4')   printf "                      %s\\n" "$marker";;
    '5')   printf "                              %s\\n" "$marker";;
    '6')   printf "                                  %s\\n" "$marker";;
  esac
}

#==============================================================================#
set_time(){
  MENU_MODE='NUM_INPUT'
  read -r TIME_H_A TIME_H_B TIME_M_A TIME_M_B TIME_S_A TIME_S_B <<EOF
$(date +"%H%M%S" | sed 's/.\{1\}/& /g')
EOF
  unset done input_num
  POS='1'
  while [ "$POS" -lt '7' ] || [ "$input_num" != 'exit' ]; do
    cursor_hide
    show_time -m2
    input_num="$(awk_menu)"
    case "$input_num" in
       [0-9])    case "$POS" in
                   '1') TIME_H_A="$input_num";;
                   '2') TIME_H_B="$input_num";;
                   '3') TIME_M_A="$input_num";;
                   '4') TIME_M_B="$input_num";;
                   '5') TIME_S_A="$input_num";;
                   '6') TIME_S_B="$input_num";;
                 esac
                 POS="$((POS+1))"
                 ;;
       'left')   POS="$((POS-1))"
                 if [ "$POS" -lt '1' ]; then
                   POS='1'
                 fi
                 ;;
       'right')  POS="$((POS+1))"
                 if [ "$POS" -gt '6' ]; then
                   POS='6'
                 fi
                 ;;
       'down')   case "$POS" in
                   '1') TIME_H_A="$((TIME_H_A-1))"
                        if [ "$TIME_H_A" -lt '0' ]; then
                          TIME_H_A='2'
                        fi
                        ;;
                   '2') TIME_H_B="$((TIME_H_B-1))"
                        if [ "$TIME_H_B" -lt '0' ]; then
                          TIME_H_B='9'
                        fi
                        ;;
                   '3') TIME_M_A="$((TIME_M_A-1))"
                        if [ "$TIME_M_A" -lt '0' ]; then
                          TIME_M_A='5'
                        fi
                        ;;
                   '4') TIME_M_B="$((TIME_M_B-1))"
                        if [ "$TIME_M_B" -lt '0' ]; then
                          TIME_M_B='9'
                        fi
                        ;;
                   '5') TIME_S_A="$((TIME_S_A-1))"
                        if [ "$TIME_S_A" -lt '0' ]; then
                          TIME_S_A='5'
                        fi
                        ;;
                   '6') TIME_S_B="$((TIME_S_B-1))"
                        if [ "$TIME_S_B" -lt '0' ]; then
                          TIME_S_B='9'
                        fi
                        ;;
                 esac
                 ;;
       'up')     case "$POS" in
                   '1') TIME_H_A="$((TIME_H_A+1))"
                        if [ "$TIME_H_A" -gt '2' ]; then
                          TIME_H_A='0'
                        fi
                        ;;
                   '2') TIME_H_B="$((TIME_H_B+1))"
                        if [ "$TIME_H_B" -gt '9' ]; then
                          TIME_H_B='0'
                        fi
                        ;;
                   '3') TIME_M_A="$((TIME_M_A+1))"
                        if [ "$TIME_M_A" -gt '5' ]; then
                          TIME_M_A='0'
                        fi
                        ;;
                   '4') TIME_M_B="$((TIME_M_B+1))"
                        if [ "$TIME_M_B" -gt '9' ]; then
                          TIME_M_B='0'
                        fi
                        ;;
                   '5') TIME_S_A="$((TIME_S_A+1))"
                        if [ "$TIME_S_A" -gt '5' ]; then
                          TIME_S_A='0'
                        fi
                        ;;
                   '6') TIME_S_B="$((TIME_S_B+1))"
                        if [ "$TIME_S_B" -gt '9' ]; then
                          TIME_S_B='0'
                        fi
                        ;;
                 esac
                 ;;
       'done')   POS="$((POS+1))"
                 ;;
       'exit'|*) break;;
     esac
     TIME_H="$TIME_H_A$TIME_H_B"
     if [ "$TIME_H" -gt '23' ]; then
       TIME_H_A='2'
       TIME_H_B='3'
     fi
     TIME_M="$TIME_M_A$TIME_M_B"
     if [ "$TIME_M" -gt '59' ]; then
       TIME_M_A='5'
       TIME_M_B='9'
     fi
     TIME_S="$TIME_S_A$TIME_S_B"
     if [ "$TIME_S" -gt '59' ]; then
       TIME_S_A='5'
       TIME_S_B='9'
     fi
     if [ "$POS" -gt '6' ]; then
       done='1'
       break
     fi
  done
  if [ "$done" = '1' ]; then
    if [ "$(expr "$TIME_H$TIME_M$TIME_S" : "[0-9][0-9][0-9][0-9][0-9][0-9]")" = '6' ]; then
      printf "\\n\\n\\t the entered time is %s:%s:%s\\n" "$TIME_H" "$TIME_M" "$TIME_S"
      printf "\\t Do you want to apply this to the hardware time? (y/N) "
      read -r time_apply
      if expr "$time_apply" : "^[Yy]" >>/dev/null; then
        hwclock --set --date "$(date +%Y-%m-%d) $TIME_H:$TIME_M:$TIME_S"
        hwclock --hctosys
        printf "\\t System Time updated!\\n"
        unset time_apply
      fi
    else
       printf "invalid time\\n"
    fi
  fi
  cursor_unhide
  unset done MENU_MODE POS
}

#==============================================================================#
show_date(){
  unset marker
  case "$1" in
    '-m1') marker='^^^';;
    '-m2') marker='▀▀▀';;
  esac
  clear
  case "$POS" in
    '1'|'2'|'3'|'4') printf "\\n\\n    Enter the year:\\n\\n";;
    '5'|'6') printf "\\n\\n    Enter the month:\\n\\n";;
    '7'|'8') printf "\\n\\n    Enter the day:\\n\\n";;
  esac
  for letter in a b c; do
    digit_line="$letter"
    case "$POS" in
      '1'|'2'|'3'|'4') display_digits offset year $DATE_Y_A $DATE_Y_B $DATE_Y_C $DATE_Y_D;;
      '5'|'6') display_digits offset month $DATE_M_A $DATE_M_B;;
      '7'|'8') display_digits offset day $DATE_D_A $DATE_D_B;;
    esac
  done
  case "$POS" in
    '1') printf "                             %s\\n" "$marker";;
    '2') printf "                                 %s\\n" "$marker";;
    '3') printf "                                     %s\\n" "$marker";;
    '4') printf "                                         %s\\n" "$marker";;
    '5') printf "                             %s\\n" "$marker";;
    '6') printf "                                 %s\\n" "$marker";;
    '7') printf "                             %s\\n" "$marker";;
    '8') printf "                                 %s\\n" "$marker";;
  esac
}

#==============================================================================#
set_date(){
  MENU_MODE='NUM_INPUT'
  read -r DATE_D_A DATE_D_B DATE_M_A DATE_M_B DATE_Y_A DATE_Y_B DATE_Y_C DATE_Y_D <<EOF
$(date +"%d%m%Y" | sed 's/.\{1\}/& /g')
EOF
  unset done input_num
  POS='1'
  while [ "$POS" -lt '9' ] || [ "$input_num" != 'exit' ]; do
    cursor_hide
    show_date -m2
    input_num="$(awk_menu)"
    case "$input_num" in
      [0-9])     case "$POS" in
                   '1') DATE_Y_A="$input_num";;
                   '2') DATE_Y_B="$input_num";;
                   '3') DATE_Y_C="$input_num";;
                   '4') DATE_Y_D="$input_num";;
                   '5') DATE_M_A="$input_num";;
                   '6') DATE_M_B="$input_num";;
                   '7') DATE_D_A="$input_num";;
                   '8') DATE_D_B="$input_num";;
                  esac
                  POS="$((POS+1))"
                  ;;
      'left')     POS="$((POS-1))"
                  if [ "$POS" -lt '1' ]; then
                    POS='1'
                  fi
                  ;;
      'right')    POS="$((POS+1))"
                  if [ "$POS" -gt '8' ]; then
                    POS='8'
                  fi
                  ;;
      'down')    case "$POS" in
                   '1') DATE_Y_A="$((DATE_Y_A-1))"
                        if [ "$DATE_Y_A" -lt '0' ]; then
                          DATE_Y_A='9'
                        fi
                        ;;
                   '2') DATE_Y_B="$((DATE_Y_B-1))"
                        if [ "$DATE_Y_A" -lt '0' ]; then
                          DATE_Y_B='9'
                        fi
                        ;;
                   '3') DATE_Y_C="$((DATE_Y_C-1))"
                        if [ "$DATE_Y_C" -lt '0' ]; then
                          DATE_Y_C='9'
                        fi
                        ;;
                   '4') DATE_Y_D="$((DATE_Y_D-1))"
                        if [ "$DATE_Y_D" -lt '0' ]; then
                          DATE_Y_D='9'
                        fi
                        ;;
                   '5') DATE_M_A="$((DATE_M_A-1))"
                        if [ "$DATE_M_A" -lt '0' ]; then
                          DATE_M_A='1'
                        fi
                        ;;
                   '6') DATE_M_B="$((DATE_M_B-1))"
                        if [ "$DATE_M_B" -lt '0' ]; then
                          DATE_M_B='9'
                        fi
                        ;;
                   '7') DATE_D_A="$((DATE_D_A-1))"
                        if [ "$DATE_D_A" -lt '0' ]; then
                          DATE_D_A='3'
                        fi
                        ;;
                   '8') DATE_D_B="$((DATE_D_B-1))"
                        if [ "$DATE_D_B" -lt '0' ]; then
                          DATE_D_B='9'
                        fi
                        ;;
                 esac
                 ;;
      'up')      case "$POS" in
                   '1') DATE_Y_A="$((DATE_Y_A+1))"
                        if [ "$DATE_Y_A" -gt '9' ]; then
                          DATE_Y_A='0'
                        fi
                        ;;
                   '2') DATE_Y_B="$((DATE_Y_B+1))"
                        if [ "$DATE_Y_B" -gt '9' ]; then
                          DATE_Y_B='0'
                        fi
                        ;;
                   '3') DATE_Y_C="$((DATE_Y_C+1))"
                        if [ "$DATE_Y_C" -gt '9' ]; then
                          DATE_Y_C='0'
                        fi
                        ;;
                   '4') DATE_Y_D="$((DATE_Y_D+1))"
                        if [ "$DATE_Y_D" -gt '9' ]; then
                          DATE_Y_D='0'
                        fi
                        ;;
                   '5') DATE_M_A="$((DATE_M_A+1))"
                        if [ "$DATE_M_A" -gt '1' ]; then
                          DATE_M_A='0'
                        fi
                        ;;
                   '6') DATE_M_B="$((DATE_M_B+1))"
                        if [ "$DATE_M_B" -gt '9' ]; then
                          DATE_M_B='0'
                        fi
                        ;;
                   '7') DATE_D_A="$((DATE_D_A+1))"
                        if [ "$DATE_D_A" -gt '3' ]; then
                          DATE_D_A='0'
                        fi
                        ;;
                   '8') DATE_D_B="$((DATE_D_B+1))"
                        if [ "$DATE_D_B" -gt '9' ]; then
                          DATE_D_B='0'
                        fi
                        ;;
                 esac
                 ;;
      'done')    POS="$((POS+1))"
                 ;;
      'exit'|*)  break;;
    esac
    DATE_M="$DATE_M_A$DATE_M_B"
    if [ "$DATE_M" -gt '12' ]; then
      DATE_M_A='1'
      DATE_M_B='2'
    fi
    DATE_D="$DATE_D_A$DATE_D_B"
    case "$DATE_M" in
      '01'|'03'|'05'|'07'|'08'|'10'|'12') if [ "$DATE_D" -gt '31' ]; then
                                            DATE_D_A='3'
                                            DATE_D_B='1'
                                          fi;;
                '02'|'04'|'06'|'09'|'11') if [ "$DATE_D" -gt '30' ]; then
                                            DATE_D_A='3'
                                            DATE_D_B='0'
                                          fi;;
    esac
    if [ "$POS" -gt '8' ]; then
      done='1'
      break
    fi
  done
  DATE_Y="$DATE_Y_A$DATE_Y_B$DATE_Y_C$DATE_Y_D"
  if [ "$done" = '1' ]; then
    if [ "$(expr "$DATE_Y$DATE_M$DATE_D" : "[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]")" = '8' ]; then
      printf "\\n\\n\\t Year %s\\n" "$DATE_Y"
      printf "\\t Month %s\\n" "$DATE_M"
      printf "\\t Day %s\\n" "$DATE_D"
      printf "\\t Do you want to apply this to the hardware time? (y/N) "
      read -r time_apply
      if expr "$time_apply" : "^[Yy]" >>/dev/null; then
        hwclock --set --date "$DATE_Y-$DATE_M-$DATE_D $(date +%H:%M:%S)"
        hwclock --hctosys
        printf "\\t System Date updated!\\n"
        unset time_apply
      fi
    else
      printf "invalid time\\n"
    fi
  fi
  cursor_unhide
  unset done MENU_MODE POS
}

#==============================================================================#
extract_init(){
# extract initial ram disks aka initrd
# $1 = init file
# $2 = output dir

  if [ -z "$1" ]; then
    printf "Error: extract_init function is missing input parameters\\n"
    printf "Example: extract_init INIT_FILE OUTPUT_DIR"
    exit 1
  fi

  if [ ! -f "$1" ]; then
    printf "Error: init file %s not found." "$1" 2>&1 | log
    exit 1
  else
    printf "found init file: %s\\n" "$INIT_FILE" 2>&1 | log
  fi

  INIT_FILE="$1"
  INIT_NAME="$(echo "$INIT_FILE" | awk -F '/' '{print $(NF)}')"

  if echo "$INIT_NAME" | grep -iq "initrd" ; then
    INIT_NAME_BASE="initrd.img"
  elif echo "$INIT_NAME" | grep -iq "initramfs"; then
    INIT_NAME_BASE="initramfs.img"
  fi

  if [ -z "$2" ]; then
    TMP_INIT="$tempdir/init"
  else
    TMP_INIT="$2"
  fi

  # prepare temp dir
  TMP_INIT_EX="$TMP_INIT/extracted"
  TMP_INIT_FILE="$TMP_INIT/$INIT_NAME"
  check_mkdir "$TMP_INIT"
  check_mkdir "$TMP_INIT_EX"
  printf "extracting %s ...\\n" "$INIT_FILE" 2>&1 | tee -a "$LOG"
  printf "detecting compression type..."
  # check compression
  case $(file "$INIT_FILE" | awk '{print $2}') in
           'XZ') printf "XZ\\n"
                 if echo "$INIT_NAME" | grep -iq ".xz$" ; then
                   xz -dk "$INIT_FILE"
                   INIT_NAME="$(echo "$INIT_NAME" | sed 's/.xz$//')"
                 else
                   cp "$INIT_FILE" "$TMP_INIT_FILE.xz"
                   xz -d "$TMP_INIT_FILE.xz"
                 fi;;
    'Zstandard') printf "Zstandard\\n"
                 if echo "$INIT_NAME" | grep -iq ".zst$" ; then
                   zstd -dk "$INIT_FILE"
                   INIT_NAME="$(echo "$INIT_NAME" | sed 's/.zst$//')"
                 else
                   cp "$INIT_FILE" "$TMP_INIT_FILE.zst"
                   zstd -d "$TMP_INIT_FILE.zst"
                 fi;;
         'gzip') printf "gzip\\n"
                 if echo "$INIT_NAME" | grep -iq ".gz$" ; then
                   gzip -dk "$INIT_FILE"
                   INIT_NAME="$(echo "$INIT_NAME" | sed 's/.gz$//')"
                 else
                   cp "$INIT_FILE" "$TMP_INIT_FILE.gz"
                   gzip -d "$TMP_INIT_FILE.gz"
                 fi;;
         'LZMA') printf "lzma\\n"
                 if echo "$INIT_NAME" | grep -iq ".lzma$" ; then
                   lzma -dk "$INIT_FILE"
                   INIT_NAME="$(echo "$INIT_NAME" | sed 's/.lzma$//')"
                 else
                   cp "$INIT_FILE" "$TMP_INIT_FILE.lzma"
                   lzma -d "$TMP_INIT_FILE.lzma"
                 fi;;
         'lzop') printf "lzop\\n"
                 if echo "$INIT_NAME" | grep -i ".lzo$" ; then
                   lzop -dk "$INIT_FILE"
                   INIT_NAME="$(echo "$INIT_NAME" | sed 's/.lzo$//')"
                 else
                   cp "$INIT_FILE" "$TMP_INIT_FILE.lzo"
                   lzop -d "$TMP_INIT_FILE.lzo"
                 fi;;
        'bzip2') printf "bzip2\\n"
                 if echo "$INIT_NAME" | grep -iq ".bz2$" ; then
                   bzip2 -dk "$INIT_FILE"
                   INIT_NAME="$(echo "$INIT_NAME" | sed 's/.bz2$//')"
                 else
                   cp "$INIT_FILE" "$TMP_INIT_FILE"
                   bzip2 -d "$TMP_INIT_FILE.bz2"
                 fi;;
          'LZ4') printf "lz4\\n"
                 if echo "$INIT_NAME" | grep -iq ".lz4$" ; then
                   lz4 -dk "$INIT_FILE"
                   INIT_NAME="$(echo "$INIT_NAME" | sed 's/.lz4$//')"
                 else
                   cp "$INIT_FILE" "$TMP_INIT_FILE.lz4"
                   lz4 -d "$TMP_INIT_FILE.lz4"
                 fi;;
              *) printf "maybe this is not compressed...\\n"
                 cp "$INIT_FILE" "$TMP_INIT_FILE";;
  esac

  if file "$INIT_FILE" | grep -iq "cpio" ; then
    if echo "$INIT_NAME" | grep -iq ".img$" ; then
      INIT_NAME="$(echo "$INIT_NAME" | sed 's/.img//')"
      mv "$TMP_INIT/$INIT_NAME.img" "$TMP_INIT_FILE"
    fi
  fi

  cpio -idvD "$TMP_INIT_EX" < "$TMP_INIT_FILE" ||\
 { printf "Error while extracting init file.\\n" 2>&1 | tee -a "$LOG"; exit 1; }
}

rebuild_init(){
# $1 = input dir
# $2 = output file
# $3 = compression (gzip or xz)

  if [ -z "$1" ]; then
    TMP_INIT_EX="$TMP_INIT/extracted"
  else
    TMP_INIT_EX="$1"
  fi

  if [ ! -d "$TMP_INIT_EX" ]; then
    printf "Error: invalid input dir\\n"
    exit 1
  fi

  if [ -z "$2" ]; then
    check_mkdir "$TMP_INIT"
    TMP_INIT_NEW="$TMP_INIT/new"
    check_mkdir "$TMP_INIT_NEW"
    NEW_INIT_FILE="$TMP_INIT_NEW"/"$INIT_NAME"
  else
    NEW_INIT_FILE="$2"
  fi

  case "$3" in
        'xz') INIT_COMPRESSION="xz";;
      'zsdt') INIT_COMPRESSION="zstd";;
      'none') INIT_COMPRESSION="none";;
    'gzip'|*) INIT_COMPRESSION="gzip";;
  esac
  if [ "$INIT_COMPRESSION" = "gzip" ]; then
    find "$TMP_INIT_EX" -print0 |\
      sed "s:$TMP_INIT_EX:.:g" |\
        cpio -0 --dot -D "$TMP_INIT_EX" -H newc -o |\
          gzip -c > "$NEW_INIT_FILE"
  elif [ "$INIT_COMPRESSION" = "xz" ]; then
    find "$TMP_INIT_EX" -print0 |\
      sed "s:$TMP_INIT_EX:.:g" |\
        cpio -0 --dot -D "$TMP_INIT_EX" -H newc -o |\
          xz --check=crc32 --x86 --lzma2=dict=512KiB > "$NEW_INIT_FILE"
  elif [ "$INIT_COMPRESSION" = "zstd" ]; then
    find "$TMP_INIT_EX" -print0 |\
      sed "s:$TMP_INIT_EX:.:g" |\
        cpio -0 --dot -D "$TMP_INIT_EX" -H newc -o |\
          zstd -19 -T0 > "$NEW_INIT_FILE"
  else
    find "$TMP_INIT_EX" -print0 |\
      sed "s:$TMP_INIT_EX:.:g" |\
        cpio -0 --dot -D "$TMP_INIT_EX" -H newc -o > "$NEW_INIT_FILE"
  fi
}

#==============================================================================#
generate_grub_template(){
[ "$DISABLE_IPV6" = "yes" ] && ipv6_opt='ipv6.disable=1' || ipv6_opt=""
KEYBOARD_LAYOUT="$(awk -F '"' '/XKBLAYOUT/ {print $2}' /etc/default/keyboard)"
if [ -n "$KEYBOARD_LAYOUT" ]; then
  KBD_OPT="keyboard-layout=$KEYBOARD_LAYOUT"
fi

LOCALE_LANG="$(awk -F '=' '$1~/^LANG$/ {print $2}' /etc/default/locale)"
if [ -n "$LOCALE_LANG" ]; then
  LOCALE_OPT="locales=$LOCALE_LANG"
fi
grub_template="$tempdir/grub.cfg.template"
cat > "$grub_template" <<EOF
if loadfont $prefix/font.pf2 ; then
  set gfxmode=640x480
  insmod efi_gop
  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod gfxterm
  insmod jpeg
  insmod png
  terminal_output gfxterm
fi

insmod iso9660
insmod gzio
insmod part_gpt
insmod part_msdos
insmod ntfs
insmod ext2
insmod loopback

background_image /boot/grub/splash.png
set menu_color_normal=white/black
set menu_color_highlight=dark-gray/white
set timeout=15

menuentry "${DISTRO} (defaults) DE German" {
    set gfxpayload=keep
    linux   /live/vmlinuz boot=live keyboard-layout=de locales=en_US.UTF-8 ${ifnames_opt} ${netconfig_opt} ${username_opt} ${ipv6_opt}
    initrd  /live/initrd.img
}

menuentry "${DISTRO} (defaults) US English" {
    set gfxpayload=keep
    linux   /live/vmlinuz boot=live keyboard-layout=us locales=en_US.UTF-8 ${ifnames_opt} ${netconfig_opt} ${username_opt} ${ipv6_opt}
    initrd  /live/initrd.img
}


menuentry "Other language (Press e to edit)" {
    set gfxpayload=keep
    linux   /live/vmlinuz boot=live ${ifnames_opt} ${netconfig_opt} ${username_opt} ${LOCALE_OPT} ${KBD_OPT} ${ipv6_opt}
    initrd  /live/initrd.img
}

submenu "Advanced options ..." {

    menuentry "${DISTRO} (to RAM)" {
	set gfxpayload=keep
	linux   /live/vmlinuz boot=live toram ${ifnames_opt} ${netconfig_opt} ${username_opt} ${ipv6_opt}
	initrd  /live/initrd.img
    }

    menuentry "${DISTRO} (failsafe)" {
	set gfxpayload=keep
	linux   /live/vmlinuz boot=live nocomponents=xinit noapm noapic nolapic nodma nosmp forcepae nomodeset vga=normal ${ifnames_opt} ${netconfig_opt} ${username_opt}
	initrd  /live/initrd.img
    }

    menuentry "Memory test" {
	kernel   /live/memtest
    }
}
EOF
}

#==============================================================================#
generate_net_interfaces(){
# generate a new /etc/network/interfaces file, needs two variables:
# net_interfaces = full path of new /etc/network/interfaces
# net_wpa = full path of new /etc/wpa_supplicant.conf
if [ ! "$net_interfaces" ]; then
  printf "Error in generate_net_interfaces function: net_interfaces not set\\n"
  exit 1
elif [ ! "$net_wpa" ]; then
  printf "Error in generate_net_interfaces function: net_wpa not set\\n"
  exit 1
fi

check_rm "$net_interfaces"
printf "generating new %s\\n" "$net_interfaces" 2>&1 | log

net_path="$(get_path "$net_interfaces")"
if [ ! -d "$net_path" ]; then
  mkdir -vp "$net_path"
fi

if [ ! -f "$net_interfaces" ]; then
  touch "$net_interfaces" || exit 1
fi
chown root:root "$net_interfaces"
chmod 640 "$net_interfaces"

if ip a | grep -q "^[0-9]: lo"; then
  printf "found loopback device\\n" 2>&1 | log
  printf "auto lo\\niface lo inet loopback\\n" >> "$net_interfaces"
fi

for nic in $(ip a | awk '/^[0-9]: e/ {print $2}' | tr -d '[:punct:]'); do
  printf "found %s\\n" "$nic" 2>&1 | log
  printf "\\nauto %s\\nallow-hotplug %s\\niface %s inet dhcp\\n" "$nic" "$nic" "$nic" >> "$net_interfaces"
done

if ip a | grep -q "^[0-9]: w"; then
  for nic in $(ip a | awk '/^[0-9]: w/ {print $2}' | tr -d '[:punct:]'); do
    printf "found %s\\n" "$nic" 2>&1 | log
    printf "\\nauto %s\\nallow-hotplug %s\\niface %s inet manual\\n\\twpa-roam /etc/wpa_supplicant.conf\\n" "$nic" "$nic" "$nic" >> "$net_interfaces"
  done
  if [ ! -f "$net_wpa" ]; then
    touch "$net_wpa"
  fi
  chown root:sudo "$net_wpa"
  chmod 660 "$net_wpa"
fi

if [ -f /target/etc/iptables.conf ]; then
  printf "found /target/etc/iptables.conf\\n" 2>&1 | log
  printf "\\nup iptables-restore < /etc/iptables.conf\\n" >> "$net_interfaces"
  if [ "$DISABLE_IPV6" = 'no' ]; then
    printf "up ip6tables-restore < /etc/iptables.conf\\n" >> "$net_interfaces"
  fi
fi
}

#==============================================================================#
# █ █ █▀▀ █▀▀ ▀█▀   ▀█▀ █▀▀ █▀▀ ▀█▀
# █ █ █▀▀ █▀▀  █     █  █▀▀ ▀▀█  █
# ▀▀▀ ▀▀▀ ▀   ▀▀▀    ▀  ▀▀▀ ▀▀▀  ▀
early_efi_test(){
# Test for efi boot
if [ -d /sys/firmware/efi ]; then
	UEFI_POSSIBLE='yes'
fi
bios_grub_dev=$(env LC_ALL=C fdisk -l | awk '/BIOS boot/ { print $1 }')
# Test for grub version
grubversion="$(dpkg -l | awk '$1 ~ /ii|hi/ && $2 ~ /grub-[eglp]/ && $2 !~ /bin|doc/ && !/dummy/ {print $2}')"
}

efi_test(){
# Check for UEFI boot and EFI partition
if [ -d /sys/firmware/efi ]; then
	uefi_boot='yes'
	esp_count=$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print}' | awk 'END{print NR}')

#	if [ -z "$gpt_list" ]; then
#		gpt_message="There is no disk with a gpt partition table.
#	You should exit this script and run gdisk to create one for uefi boot."
#	fi
#	if [ "$esp_count" -eq 1 ]; then
#		esp_dev=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $1 }')
#		esp_dev_message="EFI partition found at ${esp_dev}
#	If this is not on the first hard disk, something may be wrong,
#	and you should investigate the situation."
#		if ! blkid -c /dev/null -s TYPE "$esp_dev" | grep -q 'vfat' ; then
#			must_choose_esp="yes"
#			esp_dev_message="EFI partition found at ${esp_dev}
#	will need to be formatted FAT32"
#		fi
#	else
#		must_choose_esp="yes"
#		if [ "$esp_count" -eq 0 ]; then
#			esp_dev_message="There is no EFI partition. You will need to create one."
#		elif [ "$esp_count" -gt 1 ]; then
#			esp_dev_message="More than one EFI partition was detected.
#	You will need to select one. Normally, it's on the first hard disk."
#		fi
#	fi
	if ! echo "$grubversion" | grep -q 'grub-efi'; then   # grub-efi-${grub_arch}*.deb to include grub-efi-ia32
		grub_package="grub-efi*.deb"  # make sep vars for grub-x and grub-x-bin. Maybe sep. messages. Or sep. dirs?
		grub_debs="$(ls "$grub_package_dir"/${grub_package})"    # don't quote $grub_package here.
		if [ -n "$grub_debs" ]; then
			grub_package_message="grub package(s) found in $grub_package_dir"
		fi
		grub_efi_warning="			### WARNING ###
	grub-efi is not installed.

	If you have the deb packages, you will be given a chance to install
	them into the new system.
${grub_package_message}
${grub_debs}"
	fi

else
	# not uefi, do bios install.
	esp_list=$(env LC_ALL=C fdisk -l | awk '/EFI System/ { print $0 }')
	if [ -n "$esp_list" ]; then
		esp_dev_message="EFI partition(s) found. Do not format any EFI
 partitions if you plan to use them for uefi booting.
 ${esp_list}"
	fi
	if [ -n "$gpt_list" ] && [ -z "$bios_grub_dev" ]; then
		gpt_message="To boot a gpt disk in legacy bios you must create a
 small (>1M) unformatted partition with bios_grub flag in parted/gparted
 or EF02 in gdisk. Or boot from a disk that has dos partition table.
 More info: http://www.rodsbooks.com/gdisk/bios.html"
	fi

###### grub-pc and grub-pc-bin get installed out of order
###### Need to make $grub_package and $grub_bin_package
###### and install them in correct order.
	if echo "$grubversion" | grep -q 'grub-efi' || [ -z "$grubversion" ]; then
		grub_package="grub-pc*.deb"
		grub_debs="$(ls "$grub_package_dir"/${grub_package})"  # don't quote $grub_package here.
		[ -n "$grub_debs" ] && grub_package_message="grub package(s) found in $grub_package_dir"
		grub_efi_warning="		### WARNING ###
	grub-pc is not installed but you booted in bios mode.
	If you have the grub-pc deb packages, you will be given a chance to
	install them into the new system.
${grub_package_message}
${grub_debs}"
	elif echo "$grubversion" | grep -q 'grub-pc'; then
		grub_efi_warning="Boot method: bios
	GRUB version: grub-pc (for bios boot)
If this is not what you want, exit and examine the situation."

		while true; do
		echo "
	${grub_efi_warning}
	${esp_dev_message}
	${gpt_message}

	1) Help
	2) Continue
	3) Abort the installation
"
		read -r "ans"
		case "$ans" in
#			1) show_installer_help; break;;
			'1') break;;
			'2') break;;
			'3') exit 0;;
		esac
	done
	fi
fi
}

ask_format_efi(){
unset ASK_EFI
LINE='3'
TOPTEXT="$(text_box $text_box_opt "
 ${c1}WARNING:${c_end}  The selected partition does not contain a FAT32 filesystem.
 If you just created a new efi partition (ef00), you need to format it.
 DO NOT FORMAT A PRE-EXISTING EFI PARTITION!!!
 ")"
while [ -z "$ASK_EFI" ]; do
  ASK_EFI="$(awk_menu " > Yes, create a fat32 filesystem on $esp_dev
 > No, proceed without a bootloader.
 < Abort the install to investigate the situation." | awk '{print $2}')"
  case "$ASK_EFI" in
     'Yes,') printf "Formating %s to FAT32" "$esp_dev"
             mkfs.vfat -F 32 "$esp_dev" && break;;
      'No,') printf "%s will not be formated" "$esp_dev" && break;;
    'Abort') exit 0;;
          *) unset ASK_EFI;;
  esac
done
}

choose_esp(){
esp_info=$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $0}')
esp_dev_list=$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $1}')
esp_count=$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $0}' | awk 'END{print NR}')

if [ "$esp_count" -eq 0 ]; then
	esp_dev_message="There is no EFI partition.\nYou will need to create one or proceed without a bootloader."
	echo "$esp_dev_message"
##		ask_partition
else
	echo "
******************************************************
 Enter the device name for the EFI partition to use.
 (example: /dev/sda1)

$esp_info

enter device:"
	read -r "esp_dev"
	if ! echo "$esp_dev_list" | grep -q "$esp_dev"; then
		printf "Not a valid EFI partition.\\n"
		printf "Press ctrl-c to exit, or press ENTER to proceed without a bootloader.\\n"
		printf "\\nDO NOT SELECT AN EFI PARITION FOR ANOTHER PURPOSE.\\n"
		esp_dev=""
	fi

	if [ -n "$esp_dev" ]; then
		if ! blkid -c /dev/null -s TYPE "$esp_dev" | grep -q 'vfat'; then
			ask_format_efi
		fi
	fi
fi
}

recheck_efi(){
# Re-check EFI partition count after partitioning.
[ "$esp_count" -eq 1 ] && esp_count=$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $0}' | awk 'END{print NR}')
[ "$esp_count" -gt 1 ] && must_choose_esp='yes'
[ "$must_choose_esp" = 'yes' ] && choose_esp
}

gpt_check(){
# Test for esp partition, test for gpt partition table
if [ "$BOOT_METHOD" = "UEFI" ] && [ -b "$EFI_PART" ]; then
  gpt_list="$(env LC_ALL=C fdisk -l "$(echo "$EFI_PART" | tr -d '[:digit:]')" | awk '/Disklabel type/ {print $3}' | grep 'gpt')"
  if [ -n "$gpt_list" ]; then
    GPT_TEST='OK'
  else
    GPT_TEST='Not OK'
  fi
elif [ "$BOOT_METHOD" = 'Legacy' ]; then
  if [ "$SEPERATE_BOOT" = 'yes' ]; then
    gpt_list="$(env LC_ALL=C fdisk -l "$(echo "$BOOT_PART" | tr -d '[:digit:]')" | awk '/Disklabel type/ {print $3}' | grep 'gpt')"
    bios_grub_dev=$(env LC_ALL=C fdisk -l "$(echo "$BOOT_PART" | tr -d '[:digit:]')" | awk '/BIOS boot/ {print $1}')
  elif [ "$SEPERATE_BOOT" = 'no' ]; then
    gpt_list="$(env LC_ALL=C fdisk -l "$(echo "$ROOT_PART" | tr -d '[:digit:]')" | awk '/Disklabel type/ {print $3}' | grep 'gpt')"
  fi
else
  GPT_TEST='Not OK'
fi
}

bootflag_check(){
if [ "$SEPERATE_BOOT" = "yes" ]; then
  FLAG_DEV_PART="$BOOT_PART"
  FLAG_DEV="$(echo "$BOOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$BOOT_PART" | tr -d '[:alpha:] [=/=]')"
else
  FLAG_DEV_PART="$ROOT_PART"
  FLAG_DEV="$(echo "$ROOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$ROOT_PART" | tr -d '[:alpha:] [=/=]')"
fi

if [ "$(fdisk -l "$FLAG_DEV" | awk '/Disklabel type:/{print $NF}')" = "gpt" ]; then
  if [ "$(sgdisk "$FLAG_DEV" --attributes="$FLAG_PART":show | grep 'legacy BIOS bootable')" ]; then
    BOOTFLAG_MSG=""
  else
    BOOTFLAG_MSG="Looks like you did not set a boot flag on $FLAG_DEV_PART"
  fi
else
  if [ "$(fdisk -l | grep -e ^"$FLAG_DEV_PART" | awk '{print $2}')" = '*' ]; then
    BOOTFLAG_MSG=""
  else
    BOOTFLAG_MSG="Looks like you did not set a boot flag on $FLAG_DEV_PART"
  fi
fi
}

cursor_hide(){
  printf "\033\133?25l" >> "/dev/stderr"
}

cursor_unhide(){
  printf "\033\133?25h" >> "/dev/stderr"
  printf "\033\133?1049l" >> "/dev/stderr"
}

#beginning of installer script===============================================#
#     ____           __        ____             _____           _       __
#    /  _/___  _____/ /_____ _/ / /__  _____   / ___/__________(_)___  / /_
#    / // __ \/ ___/ __/ __ `/ / / _ \/ ___/   \__ \/ ___/ ___/ / __ \/ __/
#  _/ // / / (__  ) /_/ /_/ / / /  __/ /      ___/ / /__/ /  / / /_/ / /_
# /___/_/ /_/____/\__/\__,_/_/_/\___/_/      /____/\___/_/  /_/ .___/\__/
#                                                            /_/
#
make_install(){
check_dependencies
LOG="$LOGFILE_INSTALL"
touch "$LOG"
#exec 2>> "$ERROR_LOG"
printf "\\nStarting install script\\n" 2>&1 | log

early_efi_test;
if [ "$BOOT_METHOD" = 'UEFI' ]; then
  efi_test
fi

case "$1" in
  *'bootstrap') install_source='bootstrap';;
             *) install_source='from_iso';;
esac

# Check if this is a live session
if [ ! -d /lib/live/mount/medium ] && [ ! -d /lib/live/mount/findiso ] && \
   [ ! -d /lib/live/mount/fromiso ] && [ ! -d /lib/live/mount/persistence ] && \
   [ ! -d /run/live/medium ]; then
  clear
  TOPTEXT="$(text_box -c clr1 -t $(printf "%s" "$0" | sed "s:/home/$SUDOER:~:") -tpos 10 \
"   ${c1}WARNING:${c_end} Not running from live-CD or live-USB
            or unsupported configuration.
            Be sure you know what you are doing.
            This may not work.")"
  warn_menu="$(awk_menu "    > Continue \\n    < Exit" | awk '{print $2}')"
  if [ "$warn_menu" != 'Continue' ]; then
    exit 0
  fi
  unset warn_menu TOPTEXT
fi

install_rdy_check(){
  allow_install='1'
  # root
  if [ ! -b "$ROOT_PART" ]; then
    ROOT_PART="${c1}${txt_u}*NEED TO SET A ROOT PARTITION !${c_end}"
    if [ ! "$ROOT_FS_TYPE" ]; then
      ROOT_FS_TYPE='ext4'
    fi
    allow_install='0'
  fi
  # boot
  if [ ! -b "$BOOT_PART" ]; then
    BOOT_PART="${c1}${txt_u}*NEED TO SET A BOOT PARTITION !${c_end}"
      if [ ! "$BOOT_FS_TYPE" ]; then
      BOOT_FS_TYPE='ext2'
    fi
    if [ "$SEPERATE_BOOT" = 'yes' ]; then
      allow_install='0'
    fi
  fi
  # home
  if [ ! -b "$HOME_PART" ]; then
    HOME_PART="${c1}${txt_u}*NEED TO SET A HOME PARTITION !${c_end}"
    if [ ! "$HOME_FS_TYPE" ]; then
      HOME_FS_TYPE='ext4'
    fi
    if [ "$SEPERATE_HOME" = 'yes' ]; then
      allow_install='0'
    fi
  fi
  # swap
  case "$SEPERATE_SWAP" in
     'yes') if [ ! -b "$SWAP_PART" ]; then
              SWAP_PART="${c1}${txt_u}*NEED TO SET A SWAP PARTITION !${c_end}"
              allow_install='0'
            fi;;
    'file') if [ ! -f "$SWAPFILE" ]; then
              SWAPFILE="/.swapfile"
            fi;;
  esac
  # efi
  if [ "$BOOT_METHOD" = 'UEFI' ] && [ ! -b "$EFI_PART" ]; then
    clear
    printf "\\n\\n\\tError, EFI partition not found!"
    sleep 2
    allow_install='0'
    unset SELECTION
  elif [ "$BOOT_METHOD" = 'UEFI' ] && [ ! "$GPT_TEST" = 'OK' ]; then
    clear
    printf "\\n\\n\\tError, no GPT partition table detected!"
    sleep 2
    allow_install='0'
    unset SELECTION
  fi
  # dual-boot
  if echo "$BOOT_FS_TYPE" | grep -q '(Try Dual-Boot)'; then
    if ! echo "$dualboot_test" | grep -q 'ok'; then
      allow_install='0'
    fi
  fi
}

update_system_settings_menu(){
  TIME="$(date +%H:%M:%S)"
  DATE="$(date +%d.%m.%Y)"
  TIMEZONE_DATA="$(sed 's:Etc/::g' /etc/timezone)"
  KB_LAYOUT_DATA="$(grep XKB /etc/default/keyboard | cut -d '=' -f 2 | tr -d '"' | paste -s -d '-')"
  LOCALE_DATA="$(awk -F '=' '$1~/^LANG$/ {print $2}' /etc/default/locale)"
  CONSOLE_SETUP_DATA="$(awk '/^CHARMAP|^FONTSIZE/{printf "%s ",$1}' /etc/default/console-setup)"

DATETIME_HANDLER="> Change Time            ${c_end}${c3}=${c_end} $TIME
 > Change Date            ${c_end}${c3}=${c_end} $DATE"
}

install_init(){
# Set default variables (if not configured above)
OLD_USER_NAME=$(awk -F: '/1000:1000/ {print $1}' /etc/passwd)
[ "$LOG" ] || LOG="/var/log/refractainstaller.log"
[ "$SEPERATE_BOOT" ] || SEPERATE_BOOT='yes'
[ "$SEPERATE_HOME" ] || SEPERATE_HOME='no'
[ "$SEPERATE_SWAP" ] || SEPERATE_SWAP='file'
[ "$ROOT_ENCRYPT" ] || ROOT_ENCRYPT='no'
[ "$BOOT_ENCRYPT" ] || BOOT_ENCRYPT='no'
[ "$HOME_ENCRYPT" ] || HOME_ENCRYPT='no'
install_rdy_check

[ "$BOOT_METHOD" ] || BOOT_METHOD='Legacy'
[ "$EFI_PART" ] || EFI_PART="${c1}${txt_u}*NEED TO SET AN EFI PARTITION !${c_end}"
[ -z "$NEW_BOOTLOADER" ] && NEW_BOOTLOADER='extlinux'
[ "$HOSTNAME" ] && NEW_HOSTNAME="$HOSTNAME" || NEW_HOSTNAME=$(hostname)
[ -z "$USER_NAME" ] && NEW_USER_NAME="$OLD_USER_NAME" || NEW_USER_NAME="$USER_NAME"
[ -z "$NEW_USER_HOME_DIR" ] && NEW_USER_HOME_DIR=$(grep ':1000:' /etc/passwd | cut -d ':' -f 6)
[ -z "$NEW_USER_LOGIN_SHELL" ] && NEW_USER_LOGIN_SHELL=$(grep ':1000:' /etc/passwd | cut -d ':' -f 7)
if [ -z "$SUDO_SELECTOR" ]; then
  SUDO_SELECTOR='PERMIT_SUDO'
  SUDO_MENU_HANDLE='Permit sudo for new user (and keep root account.)'
fi
[ "$DISABLE_AUTODESK" ] || DISABLE_AUTODESK='no'
[ "$USE_UUID" ] || USE_UUID='no'

allow_install='0'
SCRIPT_STATUS='OK'
SCRIPT_ERROR=''
exit_code='0'
esp_count='0'

update_system_settings_menu

if [ "$BASE_DISTRO_LIKE" = 'debian' ]; then
  TIMEZONE_HANDLER="> Change Timezone        ${c_end}${c3}=${c_end} $TIMEZONE_DATA"
  KB_LAYOUT_HANDLER="> Change Keyboard Layout ${c_end}${c3}=${c_end} $KB_LAYOUT_DATA"
  LOCALE_HANDER="> Change Locale Language ${c_end}${c3}=${c_end} $LOCALE_DATA"
  CONSOLE_SETUP_HANDER="> Console-Setup (tty)      $CONSOLE_SETUP_DATA"
else
  TIMEZONE_HANDLER="> Change Timezone (Not Available)"
  KB_LAYOUT_HANDLER="> Change Keyboard Layout (Not Available)"
  LOCALE_HANDER="> Change Locale Language (Not Available)"
  CONSOLE_SETUP_HANDER="> Change Console-Setup (Not Available)"
fi
}
install_init

#==============================================================================#
update_user_metadata(){
[ -z "$GROUPS_IN" ] && GROUPS_IN="$(grep -w "$OLD_USER_NAME" /etc/group | awk -F ':' '{print $1}')"
[ -z "$GROUPS_OUT" ] && GROUPS_OUT="$(grep -v -w "$OLD_USER_NAME" /etc/group | awk -F ':' '{print $1}')"

USER_SETTINGS_MENU=" > Username     $NEW_USER_NAME
 > Groups       $(echo "$GROUPS_IN" | awk '{printf "%s ",$0}' | awk '{printf "%s,%s,%s,%s,%s,%s,%s...\n" ,$1,$2,$3,$4,$5,$6,$7}')
 > Full Name        $USER_REAL_NAME
 > Room Number      $USER_ROOM_NR
 > Phone (work)     $USER_PHONE_WORK
 > Phone (home)     $USER_PHONE_HOME
 > Home Directory   $NEW_USER_HOME_DIR
 > Login Shell      $NEW_USER_LOGIN_SHELL
 < Back"

USER_INFO="$(awk -F ':' '/:1000:/ {print $5}' /etc/passwd)"
if [ "$NEW_USER_REAL_NAME_EVENT" ]; then
  USER_REAL_NAME="$NEW_USER_REAL_NAME"
else
  USER_REAL_NAME="$(echo "$USER_INFO" | awk -F ',' '{print $1}')"
fi

if [ "$NEW_USER_ROOM_NR_EVENT" ]; then
  USER_ROOM_NR="$NEW_USER_ROOM_NR"
else
  USER_ROOM_NR="$(echo "$USER_INFO" | awk -F ',' '{print $2}')"
fi

if [ "$NEW_USER_PHONE_WORK_EVENT" ]; then
  USER_PHONE_WORK="$NEW_USER_PHONE_WORK"
else
  USER_PHONE_WORK="$(echo "$USER_INFO" | awk -F ',' '{print $3}')"
fi

if [ "$NEW_USER_PHONE_HOME_EVENT" ]; then
  USER_PHONE_HOME="$NEW_USER_PHONE_HOME"
else
  USER_PHONE_HOME="$(echo "$USER_INFO" | awk -F ',' '{print $4}')"
fi

if [ -z "$NEW_USER_HOME_DIR" ]; then
  NEW_USER_HOME_DIR=$(awk -F ':' '/:1000:/ {print $6}' /etc/passwd)
fi

if [ -z "$NEW_USER_LOGIN_SHELL" ]; then
  NEW_USER_LOGIN_SHELL=$(awk -F ':' '/:1000:/ {print $7}' /etc/passwd)
fi
}

#==============================================================================#
select_drive(){
TOPTEXT="$(text_box $text_box_opt "Select a device")"
LINE='1'
unset SELECTED_DRIVE
SELECTED_DRIVE="$(awk_menu "$(lsblk -dno NAME,SIZE,TYPE,MODEL \
                | awk '!/loop/ ;END{printf " < Back\n"}')" | awk '{print $1}')"
unset TOPTEXT
}

#==============================================================================#
select_part(){
unset PART_SELECTOR
PART_HANDLE="$(lsblk -pin -lo NAME,SIZE,TYPE | awk '/part/{print $1,$2}')"

if echo "$PART_HANDLE" | grep -qw 'lvm'; then
  PART_HANDLE=$(echo "$PART_HANDLE" | grep -vw 'lvm')
  PART_HANDLE_LVM=$(lvscan | grep -wi 'ACTIVE' | tr -d \' | tr -d '[]' \
                  | awk '{print $2,$3,$4}' | sed 's/$/ Logical Volume/g;s/ MiB/M/g')
  PART_HANDLE=$(printf "%s\\n%s" "$PART_HANDLE" "$PART_HANDLE_LVM")
fi

for p in $ROOT_PART $BOOT_PART $HOME_PART $SWAP_PART $esp_dev; do
  if [ "$p" ] && echo "$PART_HANDLE" | grep -qw "$p"; then
    PART_HANDLE=$(echo "$PART_HANDLE" | grep -vw "$p")
  fi
done

dev_view="$(lsblk -dpno NAME,SIZE,VENDOR,MODEL | awk '{
printf "\033\13335m[\033\13333m%s\033\13335m]\033\1330m",$1
printf " %s\\t%s - \033\13334m%s %s %s %s %s %s\033\1330m\\n",$2,$3,$4,$5,$6,$7,$8,$9
}')"

if [ "$1" ]; then
  PART_OLD="$1"
fi
if [ "$2" ]; then
  PART_TYPE="${c2}${txt_b}$2${c_end}"
  TOPTEXT="$(echo "$dev_view \\n Select a partition for: $PART_TYPE\\n \\n \\n" | text_box $text_box_opt)"
else
  TOPTEXT="$(echo "$dev_view \\n Select a partition:\\n \\n \\n" | text_box $text_box_opt)"
fi

PART_SELECTOR="$(awk_menu "$PART_HANDLE \\nnone \\n < Back" | awk '{print $1}')"
unset TOPTEXT
case "$PART_SELECTOR" in
  'none') echo 'none';;
     '<') echo "$PART_OLD";;
       *) echo "$PART_SELECTOR";;
esac
}

#==============================================================================#
select_filesystem(){
PARTNAME="$1"
TOPTEXT="$(text_box $text_box_opt "How do you want to format the ${txt_special}${c4}${PARTNAME}${c_end} partition?")"
case "$PARTNAME" in
  'boot') b_opt="\\n  > Do not format ${c2}(Try Dual-Boot)${c_end}"
          LCHECK="$(echo "$BOOT_FS_TYPE" | strip_color)";;
  'root') LCHECK="$(echo "$ROOT_FS_TYPE" | strip_color)";;
  'home') LCHECK="$(echo "$HOME_FS_TYPE" | strip_color)";;
esac
case "$LCHECK" in
                           'ext2') LINE='1';;
                           'ext3') LINE='2';;
                           'ext4') LINE='3';;
                          'btrfs') LINE='4';;
                            'xfs') LINE='5';;
                           'vfat') LINE='6';;
                           'ntfs') LINE='7';;
                  'Do not format') LINE='8';;
  'Do not format (Try Dual-Boot)') LINE='9';;
esac
SELECT_FS="$(awk_menu "  > ext2
  > ext3
  > ext4
  > btrfs
  > xfs
  > vfat
  > ntfs
  > Do not format${b_opt}" | awk -F '> ' '{print $2}')"
unset TOPTEXT PARTNAME LCHECK b_opt
}

#==============================================================================#
create_filesystem(){
if [ ! -b "$1" ] || [ -z "$2" ]; then
  printf "Error: create_filesystem function needs more parameters\\n"
  exit 1
fi
FSPART="$1"
FSTYPE="$2"
case "$FSTYPE" in
  'ext2') mkfs.ext2 -F "$FSPART" && tune2fs -r10000 "$FSPART" || check_exit;;
  'ext3') mkfs.ext3 -F "$FSPART" && tune2fs -r10000 "$FSPART" || check_exit;;
  'ext4') mkfs.ext4 -F "$FSPART" && tune2fs -r10000 "$FSPART" || check_exit;;
  'vfat') mkfs.vfat -F 32 "$FSPART" || check_exit;;
  'ntfs') mkfs.ntfs -F "$FSPART" || check_exit;;
 'btrfs') mkfs.btrfs "$FSPART" || check_exit;;
   'xfs') mkfs.xfs "$FSPART" || check_exit;;
esac
unset FSTYPE FSPART
}

#==============================================================================#
get_swap_file_size()  {
if [ -z "$line_swap_size" ]; then
  line_swap_size='4'
fi
TOPTEXT="$(text_box $text_box_opt "How large shall the swap file be?")"
LINE="$line_swap_size"
NEW_SWAP_SIZE="$(awk_menu "  > 64MB
  > 128MB
  > 256MB
  > 512MB
  > 1GB
  > 2GB
  > 4GB
  > 8GB
  > 16GB
  > 32GB
  > 64GB
  < Back" | awk -F '[<>] ' '{print $2}')"
unset TOPTEXT
if [ "$NEW_SWAP_SIZE" ] && [ "$NEW_SWAP_SIZE" != "Back" ] \
&& [ ! "$SWAP_SIZE" = "$NEW_SWAP_SIZE" ]; then
  SWAP_SIZE="$NEW_SWAP_SIZE"
fi
case "$SWAP_SIZE" in
   '64MB') swap_size_count='65536'   ; line_swap_size='1';;
  '128MB') swap_size_count='131072'  ; line_swap_size='2';;
  '256MB') swap_size_count='262144'  ; line_swap_size='3';;
  '512MB') swap_size_count='524288'  ; line_swap_size='4';;
    '1GB') swap_size_count='1048576' ; line_swap_size='5';;
    '2GB') swap_size_count='2097152' ; line_swap_size='6';;
    '4GB') swap_size_count='4194304' ; line_swap_size='7';;
    '8GB') swap_size_count='8388608' ; line_swap_size='8';;
   '16GB') swap_size_count='16777216'; line_swap_size='9';;
   '32GB') swap_size_count='33554432'; line_swap_size='10';;
   '64GB') swap_size_count='67108864'; line_swap_size='11';;
esac
}

#==============================================================================#
ask_encryption(){
ASK_ENCRYPT_VAL=""
exec 2>&1
TOPTEXT="$(text_box $text_box_opt "Select encryption tool")"
NEW_ASK_ENCRYPT_VAL="$(awk_menu "$CRYPTO_TOOLS \\n > No encryption")"
unset TOPTEXT
if [ "$NEW_ASK_ENCRYPT_VAL" ] && [ ! "$ASK_ENCRYPT_VAL" = "$NEW_ASK_ENCRYPT_VAL" ]; then
  ASK_ENCRYPT_VAL="$NEW_ASK_ENCRYPT_VAL"
fi
}

gen_inittab(){
cat > "/target/etc/inittab" <<EOF
# /etc/inittab: init(8) configuration.
# $Id: inittab,v 1.91 2002/01/25 13:35:21 miquels Exp $

# The default runlevel.
id:2:initdefault:

# Boot-time system configuration/initialization script.
# This is run first except when booting in emergency (-b) mode.
si::sysinit:/etc/init.d/rcS

# What to do in single-user mode.
~~:S:wait:/sbin/sulogin

# /etc/init.d executes the S and K scripts upon change
# of runlevel.
#
# Runlevel 0 is halt.
# Runlevel 1 is single-user.
# Runlevels 2-5 are multi-user.
# Runlevel 6 is reboot.

l0:0:wait:/etc/init.d/rc 0
l1:1:wait:/etc/init.d/rc 1
l2:2:wait:/etc/init.d/rc 2
l3:3:wait:/etc/init.d/rc 3
l4:4:wait:/etc/init.d/rc 4
l5:5:wait:/etc/init.d/rc 5
l6:6:wait:/etc/init.d/rc 6
# Normally not reached, but fallthrough in case of emergency.
z6:6:respawn:/sbin/sulogin

# What to do when CTRL-ALT-DEL is pressed.
ca:12345:ctrlaltdel:/sbin/shutdown -t1 -a -r now

# Action on special keypress (ALT-UpArrow).
#kb::kbrequest:/bin/echo "Keyboard Request--edit /etc/inittab to let this work."

# What to do when the power fails/returns.
pf::powerwait:/etc/init.d/powerfail start
pn::powerfailnow:/etc/init.d/powerfail now
po::powerokwait:/etc/init.d/powerfail stop

# /sbin/getty invocations for the runlevels.
#
# The "id" field MUST be the same as the last
# characters of the device (after "tty").
#
# Format:
#  <id>:<runlevels>:<action>:<process>
#
# Note that on most Debian systems tty7 is used by the X Window System,
# so if you want to add more getty's go ahead but skip tty7 if you run X.
#
1:2345:respawn:/sbin/getty 38400 tty1
2:23:respawn:/sbin/getty 38400 tty2
3:23:respawn:/sbin/getty 38400 tty3
4:23:respawn:/sbin/getty 38400 tty4
5:23:respawn:/sbin/getty 38400 tty5
6:23:respawn:/sbin/getty 38400 tty6

# Example how to put a getty on a serial line (for a terminal)
#
#T0:23:respawn:/sbin/getty -L ttyS0 9600 vt100
#T1:23:respawn:/sbin/getty -L ttyS1 9600 vt100

# Example how to put a getty on a modem line.
#
#T3:23:respawn:/sbin/mgetty -x0 -s 57600 ttyS3
EOF
}

#==============================================================================#
auto_set_bootflag(){
if [ "$SEPERATE_BOOT" = "yes" ]; then
  FLAG_DEV="$(echo "$BOOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$BOOT_PART" | tr -d '[:alpha:] [=/=]')"
else
  FLAG_DEV="$(echo "$ROOT_PART" | tr -d '[:digit:]')"
  FLAG_PART="$(echo "$ROOT_PART" | tr -d '[:alpha:] [=/=]')"
fi
# MBR or GPT
if [ "$(fdisk -l "$FLAG_DEV" | awk '/Disklabel type:/{print $NF}')" = "gpt" ]; then
  sgdisk "$FLAG_DEV" --attributes="$FLAG_PART":set:2
else
  echo "set $FLAG_PART boot on" | parted "$FLAG_DEV"
fi
}

#==============================================================================#
dualboot_message(){
if [ "$db_msg_seen" != '1' ]; then
echo "$(text_box -c clr1 -t $(printf "%s" "$0" | sed "s:/home/$SUDOER:~:") -tpos 10 \
"The dual-boot option is new and needs testing
it will probably work if both installations
were made with this installer

Use at your own risk
please make a backup of your boot partition now

")
Press Enter when you are ready
(you will be able to turn this off before any changes are made)
"
read -r confirm
unset confirm
db_msg_seen='1'
fi
}

#==============================================================================#
test_dualboot(){
dualboot_test="${c1}failed${c_end}"
db_dev="$BOOT_PART"
# search grub config or syslinuxcfg
# test if enough space for vmlinux and initrd
db_free="$(df --output=avail "$db_dev" | awk 'NR==2')"
db_need="$()"
# set $dualboot_test to ok or something
if [ "$db_free" -gt "$db_need" ] && [ "THIS" = "READY" ]; then
  dualboot_test="${c4}ok${c_end}"
fi
}

#==============================================================================#
# Main Menu
menu_open(){
exec 2>&1

# Adjust Menu entries
case "$SEPERATE_BOOT" in
  'yes') if echo "$BOOT_FS_TYPE" | grep -q '(Try Dual-Boot)' && [ -b "$BOOT_PART" ]; then
           DUAL_BOOT_MENU="\\n   --> ${c1}test Dual-Boot${c_end}      = $dualboot_test"
         else
           unset DUAL_BOOT_MENU
         fi
         BOOT_HANDLER="> Seperate Boot Partition = $SEPERATE_BOOT
   --> Set Boot Partition  = $BOOT_PART
   --> Set Boot Filesystem = $BOOT_FS_TYPE ${DUAL_BOOT_MENU}";;
   'no') BOOT_HANDLER="> Seperate Boot Partition = $SEPERATE_BOOT";;
esac

case "$SEPERATE_HOME" in
  'yes') HOME_HANDLER="> Seperate Home Partition = $SEPERATE_HOME
   --> Set Home Partition  = $HOME_PART
   --> Set Home Filesystem = $HOME_FS_TYPE
   --> Encrypt Home ?      = $HOME_ENCRYPT";;
   'no') HOME_HANDLER="> Seperate Home Partition = $SEPERATE_HOME";;
esac

case "$SEPERATE_SWAP" in
   'yes') SWAP_HANDLER="> Seperate Swap Partition = $SEPERATE_SWAP
   --> Swap Partition      = $SWAP_PART";;
    'no') SWAP_HANDLER="> Seperate Swap Partition = $SEPERATE_SWAP";;
  'file') SWAP_HANDLER="> Seperate Swap Partition = $SEPERATE_SWAP
   --> Swapfile location   = $SWAPFILE
   --> Swapfile size       = $SWAP_SIZE";;
esac

case "$BOOT_METHOD" in
  'UEFI') UEFI_HANDLER="> Boot Method             = $BOOT_METHOD
   --> EFI Partition       = $EFI_PART
   --> GPT Test            = $GPT_TEST";;
       *) UEFI_HANDLER="> Boot Method             = $BOOT_METHOD";;
esac

# calculate line numbers
if [ "$line_main" -gt '10' ] && [ "$SEPERATE_BOOT" = 'yes' ]; then
  line_main="$((line_main + 2))"
fi

if [ "$line_main" -gt '12' ] && echo "$BOOT_FS_TYPE" | grep -q '(Try Dual-Boot)'; then
  line_main="$((line_main + 1))"
fi

if [ "$line_main" -gt '14' ] && [ "$SEPERATE_HOME" = 'yes' ]; then
  line_main="$((line_main + 2))"
fi


if [ -z "$line_main" ]; then
  LINE='1'
else
  LINE="$line_main"
fi

MAIN_MENU=" > Prepare Partitions
 > Boot Options
 > System Settings
 > User Account Settings

 > Set Root Partition      = $ROOT_PART
   --> Set Root Filesystem = $ROOT_FS_TYPE
   --> Encrypt Root ?      = $ROOT_ENCRYPT

 $BOOT_HANDLER

 $HOME_HANDLER

 $SWAP_HANDLER

> Start Installation

< Exit"

if [ -z "$line_main" ]; then
  line_main='1'
fi
if [ -z "$SELECTION" ]; then
  LINE="$line_main"
  TOPTEXT="$(text_box $text_box_opt "Installer")"
  SELECTION="$(awk_menu "$MAIN_MENU" | strip_color)"
fi

case "$SELECTION" in
#==============================================================================#
# Main Menu: Exit
  '< Exit')	exit 0;;
#==============================================================================#
# Main Menu: Partition setup menu
  '> Prepare Partitions') line_main='1'
    TOPTEXT="Prepare partitions:\\n$(lsblk -o NAME,SIZE,TYPE,VENDOR,MODEL,RM,RO)\\n"
    LINE='1'
    PREPARE_PARTITION="$(awk_menu "$PARTITION_TOOLS\\n < Back" )"
    unset TOPTEXT
    case "$PREPARE_PARTITION" in
         '< Back') unset SELECTION;;
        '> fdisk') select_drive
                   if [ -b "/dev/$SELECTED_DRIVE" ]; then
                     fdisk "/dev/$SELECTED_DRIVE"
                   fi;;
       '> cfdisk') select_drive
                   if [ -b "/dev/$SELECTED_DRIVE" ]; then
                     cfdisk "/dev/$SELECTED_DRIVE"
                   fi;;
        '> gdisk') select_drive
                   if [ -b "/dev/$SELECTED_DRIVE" ]; then
                     gdisk "/dev/$SELECTED_DRIVE"
                   fi;;
      '> gparted') select_drive
                   if [ -b "/dev/$SELECTED_DRIVE" ]; then
                     gparted "/dev/$SELECTED_DRIVE"
                   fi;;
       '> parted') select_drive
                   if [ -b "/dev/$SELECTED_DRIVE" ]; then
                     parted "/dev/$SELECTED_DRIVE"
                   fi;;
        '> shred') select_drive
                   if [ -b "/dev/$SELECTED_DRIVE" ]; then
                     TOPTEXT="$(text_box $text_box_opt "
  Do you really want to ${c1}shred${c_end} ${c4}/dev/$SELECTED_DRIVE${c_end}?\\n ")"
                     SHRED_WARNING="$(awk_menu " > Yes, give me zeroes\\n > Yes, shred with random data\\n < Back")"
                     unset TOPTEXT
                     case "$SHRED_WARNING" in
                               '> Yes, give me zeroes') echo "this can take a while..."
                                                        shred -vzn0 "/dev/$SELECTED_DRIVE";;
                       '> Yes, shred with random data') echo "this can take a while..."
                                                        shred -vz "/dev/$SELECTED_DRIVE";;
                     esac
                     unset SELECTION
                   fi;;
    esac;;
#==============================================================================#
# Main Menu: Boot Options
  '> Boot Options') line_main='2'
    while [ "$BOOT_OPT_MENU" != '< Back' ] && [ "$SELECTION" ]; do
    TOPTEXT="$(text_box $text_box_opt "Boot Options")"
      if [ -z "$line_boot_opt" ]; then
        LINE='1'
      elif [ "$line_boot_opt" -gt '2' ] && [ "$BOOT_METHOD" = 'UEFI' ]; then
        LINE="$((line_boot_opt + 2))"
      else
        LINE="$line_boot_opt"
      fi
      BOOT_OPT_MENU="$(awk_menu " > Select Bootloader       = $NEW_BOOTLOADER
 $UEFI_HANDLER
 > Try Dual-Boot
 < Back")"
    if [ -z "$BOOT_OPT_MENU" ]; then
      BOOT_OPT_MENU='< Back'
    fi
    case "$BOOT_OPT_MENU" in
    # Boot Options: Set bootloader
      '> Select Bootloader       = '*) line_boot_opt='1'
        if [ "$BOOT_METHOD" = 'UEFI' ]; then
          bload_list -r extlinux
          if [ "$NEW_BOOTLOADER" = 'extlinux' ]; then
            if bload_list | grep -iq "grub" ; then
              NEW_BOOTLOADER='grub'
            else
              NEW_BOOTLOADER='none'
            fi
          fi
        fi
        case "$NEW_BOOTLOADER" in
              'none') LINE='1';;
              'grub') LINE='2';;
          'extlinux') LINE='3';;
        esac
        TOPTEXT="$(text_box $text_box_opt 'Select a bootloader:')"
        NEW_BOOTLOADER="$(awk_menu $(bload_list) | awk '{print $2}')"
        unset TOPTEXT;;
    # Boot Options: Set boot method
      '> Boot Method             = '*) line_boot_opt='2'
        if [ "$UEFI_POSSIBLE" = 'yes' ]; then
          case "$BOOT_METHOD" in
            'Legacy') efi_test
                      BOOT_METHOD='UEFI'
                      NEW_BOOTLOADER='grub'
                      gpt_check;;
            'UEFI')   BOOT_METHOD='Legacy';;
          esac
        else
          BOOT_METHOD="Legacy"
        fi;;
    # Boot Options: Set EFI partition
      '--> EFI Partition       = '*) line_boot_opt='1'
        ESP_COUNT=$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print}' | awk 'END{print NR}')
        if [ "$ESP_COUNT" -eq 0 ]; then
          LINE="1"
          TOPTEXT="$(text_box $text_box_opt "   no EFI partition found
   You need to create a small >1MB partition with esp-flag and FAT32 filesystem.")"
          EFI_ANSWER="$(awk_menu 'OK')"
        elif [ "$ESP_COUNT" -eq 1 ]; then
          EFI_PART="$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $1}')"
        elif [ "$ESP_COUNT" -gt 1 ]; then
          ESP_LIST="$(env LC_ALL=C fdisk -l | awk '/EFI System/ {print $1}')"
          TOPTEXT="$(text_box $text_box_opt "Found multiple EFI partitions,
   select EFI partition for this installation")"
          NEW_EFI_PART="$(awk_menu "$ESP_LIST")"
          if [ -b "$NEW_EFI_PART" ]; then
            EFI_PART="$NEW_EFI_PART"
          fi
        fi

        if [ -b "$EFI_PART" ]; then
          gpt_check
          if ! blkid -c /dev/null -s TYPE "$EFI_PART" | grep -q 'vfat' ; then
      	    ask_format_efi
          fi
        else
          EFI_PART="${c1}${txt_u}*NEED TO SET AN EFI PARTITION !${c_end}"
        fi

        if [ -b "$EFI_PART" ]; then
          esp_dev="$EFI_PART"
        fi;;
      # Boot Options: Try Dual-Boot
        '> Try Dual-Boot') line_boot_opt='3'
          BOOT_FS_TYPE="  > Do not format ${c2}(Try Dual-Boot)${c_end}"
          dualboot_message;;
      # Boot Options: Back
        '< Back') unset BOOT_OPT_MENU SELECTION;;
      esac
    done;;
#==============================================================================#
# Main Menu: System Settings Menu
  '> System Settings') line_main='3'
    update_system_settings_menu
    unset SYSTEM_SETTINGS_MENU_EVENT
    SYSTEM_SETTINGS_MENU=\
" > Change Hostname        ${c_end}${c3}=${c_end} $NEW_HOSTNAME
 > sudo Setup             ${c_end}${c3}=${c_end} $SUDO_MENU_HANDLE
 > Disable auto desktop   ${c_end}${c3}=${c_end} $DISABLE_AUTODESK
 > Disable IPv6           ${c_end}${c3}=${c_end} $DISABLE_IPV6
 > Use UUID in fstab?     ${c_end}${c3}=${c_end} $USE_UUID
 $DATETIME_HANDLER
 $TIMEZONE_HANDLER
 $KB_LAYOUT_HANDLER
 $LOCALE_HANDER
 $CONSOLE_SETUP_HANDER
 > Edit Mirrorlist
 > Edit iptables.conf
 > Edit rc.local
 > Edit sysctl.conf
 > Reconfigure Packages
 < Back"

    if [ -z "$line_sys_settings" ]; then
      line_sys_settings='1'
    fi
    while [ -z "$SYSTEM_SETTINGS_MENU_EVENT" ]; do
      LINE="$line_sys_settings"
      TOPTEXT="$(text_box $text_box_opt "System Settings")"
      update_system_settings_menu
      SYSTEM_SETTINGS_MENU_EVENT="$(awk_menu "$SYSTEM_SETTINGS_MENU" | strip_color | awk -F '=' '{print $1}' | awk '{ gsub(/^[ \t]+|[ \t]+$/, ""); print }')"
      case "$SYSTEM_SETTINGS_MENU_EVENT" in
        '> Change Hostname') line_sys_settings="1"
          OLD_HOSTNAME="$NEW_HOSTNAME"
          NEW_HOSTNAME=""
          while [ -z "$NEW_HOSTNAME" ]; do
            clear
            printf "%s\\n" "$NEW_NAME_STATUS"
            printf "\\nThe current hostname is %s.\\n" "$OLD_HOSTNAME"
            printf "To change that, enter the new hostname here.\\n"
            printf "To leave it unchanged, just press ENTER.\\n\\n"
            printf "You can use alphanumeric characters anywhere in the hostname,\\n"
            printf "and you can use the minus sign (-) as long as it's not at the beginning or end.\\n\\n"
            printf "\\nNew hostname: "
            read -r NEW_STRING
            string_test check_chars check_length_64 no_spaces all_lower
            if [ "$STRING_STATUS" = 'GOOD' ]; then
              NEW_HOSTNAME="$NEW_STRING"
              unset NEW_STRING STRING_STATUS
            fi
          done;;

        '> sudo Setup') line_sys_settings='2'
          unset SUDO_SELECTOR
          if [ -z "$SUDO_MENU_LINE" ]; then
            SUDO_MENU_LINE='7'
          fi
          while [ -z "$SUDO_SELECTOR" ]; do
            LINE="$SUDO_MENU_LINE"
            TOPTEXT="$(text_box $text_box_opt " Most live images use 'sudo' for access.
 No password is required.
 It is recommended to disable sudo in an installation and use 'su'
 with a root password. Optionally you may permit sudo for the new user
 or you may use sudo as default for the new user, with no root account.")"
            while [ ! "$(echo "$SUDO_SELECTOR" | grep '[>|<]')" ]; do
              SUDO_SELECTOR="$(awk_menu "  > Disable sudo
  > Permit sudo for new user (and keep root account.)
  > Use sudo as default for new user (and disable root account.)
  > Use sudo only for shutdown (and keep root account.)
  < Back")"
              if [ -z "$SUDO_SELECTOR" ]; then break; fi
            done
            case "$SUDO_SELECTOR" in
              '> Disable sudo'*) SUDO_MENU_LINE='1'
                SUDO_MENU_HANDLE='Disable sudo (recommended)'
                sudoconfig='FALSE'
                sudo_is_default='FALSE'
                sudo_shutdown='FALSE';;

              '> Permit sudo for new user'*) SUDO_MENU_LINE='2'
                SUDO_MENU_HANDLE='Permit sudo for new user (and keep root account.)'
                sudoconfig='TRUE'
                sudo_is_default='FALSE'
                sudo_shutdown='FALSE';;

              '> Use sudo as default for new user'*) SUDO_MENU_LINE='3'
                SUDO_MENU_HANDLE='Use sudo as default for new user (and disable root account.)'
                sudoconfig='FALSE'
                sudo_is_default='TRUE'
                sudo_shutdown='FALSE';;

              '> Use sudo only for shutdown'*) SUDO_MENU_LINE='4'
                SUDO_MENU_HANDLE='Use sudo only for shutdown (and keep root account.)'
                sudoconfig='FALSE'
                sudo_is_default='FALSE'
                sudo_shutdown="TRUE";;
            esac
          done;;

        '> Disable auto desktop') line_sys_settings='3'
          case "$DISABLE_AUTODESK" in
            'yes') DISABLE_AUTODESK='no';;
            'no') DISABLE_AUTODESK='yes';;
          esac;;

        '> Disable IPv6') line_sys_settings='4'
          case "$DISABLE_IPV6" in
            yes) DISABLE_IPV6='no';;
            no) DISABLE_IPV6='yes';;
          esac;;

        '> Use UUID in fstab?') line_sys_settings='5'
           case "$USE_UUID" in
             'yes') UUID_LINE='1';;
                 *) UUID_LINE='2';;
           esac
           TOPTEXT="$(text_box $text_box_opt " Would you like fstab to use UUID to identify filesystems?
 This is useful if your drive order changes between reboots.")"
           LINE="$UUID_LINE"
      	   UUID_OPTION="$(awk_menu "   > yes \\n   > no")"
      	   unset TOPTEXT
           case "$UUID_OPTION" in
	         '> yes') USE_UUID='yes'
                      if [ "$ROOT_ENCRYPT" != 'no' ] || [ "$HOME_ENCRYPT" != 'no' ]; then
			            uuid_message="-->	UUIDs will be used in crypttab
	/dev/mapper/<name> will be used in fstab."
		              fi;;
              '> no') USE_UUID='no';;
                   *) ;;
           esac;;

        '> Change Time') line_sys_settings='6'
          set_time;;

        '> Change Date') line_sys_settings='7'
          set_date;;

        '> Change Timezone') line_sys_settings='8'
          dpkg-reconfigure tzdata;;

        '> Change Keyboard Layout') line_sys_settings='9'
          dpkg-reconfigure keyboard-configuration;;

        '> Change Locale Language') line_sys_settings='10'
          dpkg-reconfigure locales;;

        '> Console-Setup (tty)'*) line_sys_settings='11'
          dpkg-reconfigure console-setup;;

        '> Edit Mirrorlist') line_sys_settings='12'
          $TEXT_EDITOR /etc/apt/sources.list;;

        '> Edit iptables.conf') line_sys_settings='13'
          $TEXT_EDITOR /etc/iptables.conf;;

        '> Edit rc.local') line_sys_settings='14'
          $TEXT_EDITOR /etc/rc.local;;

        '> Edit sysctl.conf') line_sys_settings='15'
          $TEXT_EDITOR /etc/sysctl.conf;;

        '> Reconfigure Packages') line_sys_settings='16'
          SELECT_PKG="$(awk_menu "$(echo "$(ls -1 /var/lib/dpkg/info/*.config \
          | awk -F '/' '{print $NF}' | awk -F '.' '{print $1}' \
          | awk '{gsub(/^/, " > ", $0)} {print}')\\n < Back")" \
          | awk '{print substr($0,3,length($0))}')"
          if [ "$SELECT_PKG" != 'Back' ]; then
            dpkg-reconfigure "$SELECT_PKG"
          fi;;

        *) SYSTEM_SETTINGS_MENU_EVENT='1'
           line_sys_settings='1'
           unset SELECTION;;
      esac
    done;;
#==============================================================================#
# Main Menu: User Account Settings Menu
  '> User Account Settings') line_main='4'
    update_user_metadata
    if [ -z "$line_user_menu" ]; then
      line_user_menu='1'
    fi
    LINE='1'
    TOPTEXT="$(text_box $text_box_opt "User Account Settings")"
    USER_MENU_EVENT="$(awk_menu "$USER_SETTINGS_MENU" | awk -F '[<>] ' '/[a-z]/{print $2}')"
    case "$USER_MENU_EVENT" in
  # User Settings Menu: Change user name
      'Username'*) line_user_menu='1'
        TMP_USER_NAME="$NEW_USER_NAME"; NEW_USER_NAME=""
        while [ -z "$NEW_USER_NAME" ]; do
          clear
          printf "%s\\n\\n" "$NEW_NAME_STATUS"
          printf "The current username is %s.\\n" "$TMP_USER_NAME"
          printf "To change that, enter the new username here.\\n\\n"
          printf "You can use alphanumeric characters anywhere in the username,\\n"
          printf "and you can use the minus sign (-) as long as it's not at the beginning or end.\\n"
          printf "\\nNew username: "
          read -r NEW_STRING
          if [ "$NEW_STRING" ]; then
            string_test check_chars check_length_64 check_forbidden no_spaces all_lower
            if [ "$STRING_STATUS" = 'GOOD' ]; then
              NEW_USER_NAME="$NEW_STRING"
              unset NEW_STRING STRING_STATUS
            fi
          fi
        done
        GROUPS_IN="$(echo "$GROUPS_IN" | sed "s/$TMP_USER_NAME/$NEW_USER_NAME/")"
        NEW_USER_HOME_DIR="/home/$NEW_USER_NAME";;
  # User Settings Menu: Change user groups
      'Groups'*) line_user_menu='2'
        update_user_metadata
        clear
        LINE='1'
        TOPTEXT="$(text_box $text_box_opt "GROUPS")"
        x="$(awk_menu $(seq 1 100))"
        USER_GROUP_EVENT="$(awk_menu " < Back
 ## Remove $NEW_USER_NAME from following groups:
$(echo "$GROUPS_IN" | awk '{$1=$1};1')
 ## Add $NEW_USER_NAME to other groups:
$(echo "$GROUPS_OUT" | awk '{$1=$1};1')")"
        if [ "$USER_GROUP_EVENT" != 'BACK' ] \
        && [ "$USER_GROUP_EVENT" != "$OLD_USER_NAME" ] \
        && [ "$USER_GROUP_EVENT" != "$NEW_USER_NAME" ] \
        && [ $(echo "$USER_GROUP_EVENT" | awk '{print substr($0,1)}') != '#' ]; then
          if [ "$(echo "$GROUPS_IN" | grep -w "$USER_GROUP_EVENT")" ]; then
            GROUPS_OUT="$(echo "$GROUPS_OUT" | awk -v var="$USER_GROUP_EVENT" '{print} BEGIN{print var}')"
            GROUPS_IN="$(echo "$GROUPS_IN" | sed "s/$USER_GROUP_EVENT//" | awk /./)"
          else
            GROUPS_OUT="$(echo "$GROUPS_OUT" | sed "s/$USER_GROUP_EVENT//" | awk /./)"
            GROUPS_IN="$(echo "$GROUPS_IN" | awk -v var="$USER_GROUP_EVENT" '{print} BEGIN{print var}')"
          fi
          update_user_metadata
        fi;;
  # User Settings Menu: Change user real name
      'Full Name'*) line_user_menu='3'
        NEW_USER_REAL_NAME_EVENT='1'
        unset NEW_USER_REAL_NAME NEW_INPUT_STATUS
        printf "\\nEnter %s's real name: " "$NEW_USER_NAME"
        read -r NEW_STRING
        string_test check_chars check_length_64 no_spaces
        if [ "$STRING_STATUS" = 'GOOD' ]; then
          NEW_USER_REAL_NAME="$NEW_STRING"
          unset NEW_STRING STRING_STATUS
        fi
        update_user_metadata;;
  # User Settings Menu: Change user room
      'Room Number'*) line_user_menu='4'
        NEW_USER_ROOM_NR_EVENT='1'
        printf "%s\\nEnter %s's room number: " "$NEW_INPUT_STATUS" "$NEW_USER_NAME"
        read -r NEW_STRING
        string_test check_chars check_length_64 no_spaces
        if [ "$STRING_STATUS" = 'GOOD' ]; then
          NEW_USER_ROOM_NR="$NEW_STRING"
          unset NEW_STRING STRING_STATUS
        fi
        update_user_metadata;;
  # User Settings Menu: Change user phone (work)
      'Phone (work)'*) line_user_menu='5'
        NEW_USER_PHONE_WORK_EVENT='1'
        printf "%s\\nEnter %s's phone number (work): " "$NEW_INPUT_STATUS" "$NEW_USER_NAME"
        read -r NEW_STRING
        string_test check_chars check_length_64 no_spaces
        if [ "$STRING_STATUS" = 'GOOD' ]; then
          NEW_USER_PHONE_WORK="$NEW_STRING"
          unset NEW_STRING STRING_STATUS
        fi
        update_user_metadata;;
  # User Settings Menu: Change user phone (home)
      'Phone (home)'*) line_user_menu='6'
        NEW_USER_PHONE_HOME_EVENT='1'
        printf "%s\\nEnter %s's phone number (home): " "$NEW_INPUT_STATUS" "$NEW_USER_NAME"
        read -r NEW_STRING
        string_test check_chars check_length_64 no_spaces
        if [ "$STRING_STATUS" = 'GOOD' ]; then
          NEW_USER_PHONE_HOME="$NEW_STRING"
          unset NEW_STRING STRING_STATUS
        fi
        update_user_metadata;;
  # User Settings Menu: Change user home directory
      'Home Directory'*) line_user_menu='7'
        NEW_USER_HOME_DIR_EVENT='1'
        unset NEW_PATH_STATUS
        while [ "$NEW_PATH_STATUS" != 'OK' ]; do
          clear
          printf "%s\\n\\n\\tEnter a path for %s's home directory: " "$NEW_PATH_STATUS" "$NEW_USER_NAME"
          read -r "NEW_PATH"
          check_path
          NEW_USER_HOME_DIR="$NEW_PATH"
        done
        clear
        update_user_metadata;;
  # User Settings Menu: Change user shell
      'Login Shell'*) line_user_menu='8'
        TOPTEXT="$(text_box $text_box_opt "Select login shell:")"
        NEW_USER_LOGIN_SHELL="$(awk_menu "$(grep -v '#\|tmux\|screen' /etc/shells)")"
        update_user_metadata;;
  # User Settings Menu: Back
      *) unset SELECTION;;
    esac ;;
#==============================================================================#
# Main Menu: Select / partition
  '> Set Root Partition      = '*) line_main='6'
    ROOT_PART="$(select_part "$ROOT_PART" '/')"
    install_rdy_check
    unset SELECTION;;
# Main Menu: Set / filesystem
  '--> Set Root Filesystem = '*) line_main='7'
    select_filesystem 'root'
    if [ "$SELECT_FS" ] && [ ! "$ROOT_FS_TYPE" = "$SELECT_FS" ]; then
      ROOT_FS_TYPE="$SELECT_FS"
    fi
  	unset SELECTION;;
# Main Menu: Set / encryption
  '--> Encrypt Root ?      = '*) line_main='8'
    ask_encryption
   	case "$ASK_ENCRYPT_VAL" in
  	             '> LUKS') ROOT_ENCRYPT='LUKS';;
            '> TrueCrypt') ROOT_ENCRYPT='TrueCrypt';;
   	        '> VeraCrypt') ROOT_ENCRYPT='VeraCrypt';;
   	  '> No encryption'|*) ROOT_ENCRYPT='no';;
   	esac
   	if [ "$SEPERATE_BOOT" = 'no' ]; then
   	  SEPERATE_BOOT='yes'
   	fi
    unset SELECTION;;
# Main Menu: Seperate /boot
  '> Seperate Boot Partition = '*) line_main='10'
    case "$SEPERATE_BOOT" in
      'yes') SEPERATE_BOOT='no'; unset BOOT_PART BOOT_FS_TYPE;;
      'no')  SEPERATE_BOOT='yes';;
    esac
    install_rdy_check
    unset SELECTION;;
# Main Menu: Select /boot partition
  '--> Set Boot Partition  = '*) line_main='10'
    BOOT_PART="$(select_part "$BOOT_PART" '/boot')"
    install_rdy_check
    unset SELECTION;;
# Main Menu: Set /boot filesystem
  '--> Set Boot Filesystem = '*) line_main='10'
    select_filesystem 'boot'
    if [ "$SELECT_FS" ] && [ ! "$BOOT_FS_TYPE" = "$SELECT_FS" ]; then
      BOOT_FS_TYPE="$SELECT_FS"
    fi
    unset SELECTION;;
# Main Menu: test Dual-Boot
  '--> test Dual-Boot      ='*)
    test_dualboot
    unset SELECTION;;
# Main Menu: Seperate /home
  '> Seperate Home Partition = '*) line_main='14'
    case "$SEPERATE_HOME" in
      'yes') SEPERATE_HOME='no'; unset HOME_PART HOME_FS_TYPE;;
       'no') SEPERATE_HOME='yes';;
    esac
    install_rdy_check
    unset SELECTION;;
# Main Menu: Select /home partition
  '--> Set Home Partition  = '*) line_main='13'
    HOME_PART="$(select_part "$HOME_PART" '/home')"
    install_rdy_check
    unset SELECTION;;
# Main Menu: Set /home filesystem
  '--> Set Home Filesystem = '*) line_main='14'
    select_filesystem 'home'
    HOME_FS_TYPE="$SELECT_FS"
    unset SELECTION;;
# Main Menu: Set /home encryption
  'Encrypt Home ?      = '*) line_main='15'
    ask_encryption
    case "$ASK_ENCRYPT_VAL" in
                 '> LUKS') HOME_ENCRYPT='LUKS';;
   	        '> TrueCrypt') HOME_ENCRYPT='TrueCrypt';;
   	        '> VeraCrypt') HOME_ENCRYPT='VeraCrypt';;
   	  '> No encryption'|*) HOME_ENCRYPT='no';;
   	esac
    unset SELECTION;;
# Main Menu: Use swap?
  '> Seperate Swap Partition = '*) line_main='11'
    case "$SEPERATE_SWAP" in
       'yes') SEPERATE_SWAP='no';;
        'no') SEPERATE_SWAP='file';;
      'file') SEPERATE_SWAP='yes';;
    esac
    unset SELECTION;;
# Main Menu: Set swap partition
  '--> Swap Partition      = '*) line_main='11'
    SWAP_PART="$(select_part "$SWAP_PART" 'swap')"
    install_rdy_check
    unset SELECTION;;
# Main Menu: Set swap file
  '--> Swapfile location   = '*) line_main='14'
    unset NEW_PATH_STATUS
    while [ "$NEW_PATH_STATUS" != 'OK' ]; do
      clear
      if [ -n "$NEW_PATH_ERR_MSG" ]; then
        printf "%s\\n" "$NEW_PATH_ERR_MSG"
      fi
      printf "Where shall the swap file be located: /"
      read -r "NEW_PATH"
      printf "\\n"
      check_path;
    done;
    if [ -n "$NEW_PATH" ]; then
      SWAPFILE="/$NEW_PATH"
    fi
    unset SELECTION;;
# Main Menu: swap file size
  '--> Swapfile size       = '*) line_main='15'
     get_swap_file_size
     unset SELECTION;;
#==============================================================================#
# Main Menu: Start installation
  '> Start Installation')
    install_rdy_check
    if [ "$allow_install" = '1' ]; then
      gpt_check
      case "$BOOT_METHOD" in
        'Legacy') bootflag_check
                  if [ "$BOOTFLAG_MSG" ]; then
                    while [ -z "$ASK_BOOTFLAG" ]; do
                      LINE='1'
                      TOPTEXT="$(text_box $text_box_opt "$BOOTFLAG_MSG")"
                      ASK_BOOTFLAG="$(awk_menu "  < Let me go back and check that
  > Let this script set the bootflag now automatically
  > I dont care, continue with installation")"
                      case "$ASK_BOOTFLAG" in
                             '< Let me go back'*) line_main='5'
                                                  unset SELECTION
                                                  menu_open;;
                        *'set the bootflag now'*) auto_set_bootflag;;
                                '> I dont care'*) unset BOOTFLAG_MSG;;
                                               *) unset ASK_BOOTFLAG;;
                      esac
                    done
                  fi
                  early_efi_test;;
          'UEFI') if [ -z "$gpt_list" ]; then
                    LINE='5'
                    TOPTEXT="$(text_box $text_box_opt " Could not find gpt partition table!
 UEFI boot requires that your device has a gpt partition table
 Use parted/gparted or gdisk to create it")"
                    unset ASK_GPT
                    while [ -z "$ASK_GPT" ]; do
                      ASK_GPT="$(awk_menu "   < OK" | awk '{print $NF}')"
                    done
                    if [ "$ASK_GPT" = 'OK' ]; then
                    line_main='5'; unset SELECTION
                    fi
                  fi
                  if [ -n "$esp_dev" ]; then
                    if ! blkid -c /dev/null -s TYPE "$esp_dev" | grep -q 'vfat'; then
                      ask_format_efi
                    fi
                  fi;;
      esac
    else
      unset SELECTION
    fi;;
  esac
}

# Fix root's path (for Buster/Beowulf and later)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin

# █▀█ █▀█ █▀▀ █▀█   █▄█ █▀▀ █▀█ █ █
# █ █ █▀▀ █▀▀ █ █   █ █ █▀▀ █ █ █ █
# ▀▀▀ ▀   ▀▀▀ ▀ ▀   ▀ ▀ ▀▀▀ ▀ ▀ ▀▀▀
while [ "$allow_install" != '1' ]; do
  menu_open
done

install_dev="$ROOT_PART"

#pre-install

# comment all locales in locale.gen
#sed -i '/^[a-z][a-z]_/s/^/# /' /etc/locale.gen

##### find the current active locale and uncomment it
#CURRENT_LOCALE=$(grep -v ^\# /etc/default/locale | cut -d= -f2)
#if [ -n "$CURRENT_LOCALE" ]; then
#	sed -i "s/# $CURRENT_LOCALE/$CURRENT_LOCALE/" /etc/locale.gen
#fi

if [ "$NEW_BOOTLOADER" = 'grub' ]; then
# Select location for bootloader.
# If location is entered but does not exist, then exit with error.
  select_grub_dev (){
  clear
  printf "\\nWhere would you like the GRUB bootloader to be installed?\\n"
  printf "\\n\\t(probably a drive, like /dev/sda)\\n\\n"
  printf "If you don't want to install the bootloader, leave this blank.\\n"
  read -r "grub_dev"
  if [ -n "$grub_dev" ]; then
	if ! [ -b "$grub_dev" ]; then
	  printf "%s is not a block device." "$grub_dev"; exit 1
	fi
  fi

# If you enter a partition instead of a drive for grub_dev...  ##### NOT FOR NVME DISKS (or >9 partitions)
  if [ ${grub_dev: -1} = [1-9] ]; then							#### (This way should work for nvme)
	grub_partition="$grub_dev"
  else
	partition_table=$(env LC_ALL=C fdisk -l "$grub_dev" | awk '/Disklabel type/ { print $3 }')
  fi

  if [ "$partition_table" = "gpt" ] && [ -z "$bios_grub_dev" ]; then
	bios_boot_warning="bootloader will fail without BIOS boot partition."
	unset grub_dev
	printf "\\nWARNING: There is no BIOS boot partition."
	printf "\\nPress ENTER to proceed without bootloader or ctrl-c to quit."
	read -r "confirm"
  fi
  }

  case "$SEPERATE_BOOT" in
    'yes') [ -b "$BOOT_PART" ] && grub_dev="$(echo "$BOOT_PART" | tr -d '[:digit:]')";;
     'no') [ -b "$ROOT_PART" ] && grub_dev="$(echo "$ROOT_PART" | tr -d '[:digit:]')";;
  esac

  if [ "$uefi_boot" = "yes" ]; then
	grub_dev="efi"
	if [ -z "$esp_dev" ]; then
	  unset grub_dev
    fi
  elif [ -z "$grub_package" ]; then  # grub_package is null if correct grub is installed.
	if [ -z "$grub_dev" ]; then
	  select_grub_dev
	fi
  fi
fi

# Enter device for /boot partition or skip. If one is entered, test it.
boot_dev="$BOOT_PART"

# Enter device for /home partition or skip. If one is entered, test it.
home_dev="$HOME_PART"

###############################################################################################################
# Choose filesystem type for /boot if it exists.
if [ "$SEPERATE_BOOT" = 'yes' ] && [ -n "$boot_dev" ]; then
  if [ "$no_format" = 'yes' ]; then
    fs_type_boot=$(blkid -s TYPE "$boot_dev" -o value)
  fi
fi

# Choose filesystem type for /
if [ "$no_format" = 'yes' ]; then
  ROOT_FS_TYPE=$(blkid -s TYPE "$install_dev" -o value)
fi

# Show available swap partitions and choose one.
swap_info=$(/sbin/blkid | awk '/TYPE="swap"/ {print "\n" $0 }')
swap_device_list=$(/sbin/blkid -s TYPE | awk -F: '/swap/ {print "\n" $1 }')

if [ -n "$swap_device_list" ]; then
  use_existing_swap='yes'
  case "$SEPERATE_SWAP" in
    'file') swap_dev="$SWAPFILE";;
      'no') unset swap_dev;;
     'yes') [ -b "$swap_dev" ] || use_existing_swap="no"; swap_dev="$SWAP_PART";;
	esac
fi

# █▀▀ █ █ █▄█ █▄█ █▀█ █▀▄ █ █
# ▀▀█ █ █ █ █ █ █ █▀█ █▀▄  █
# ▀▀▀ ▀▀▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀ ▀  ▀
# Show a summary of what will be done
case "$NEW_BOOTLOADER" in
      'grub') if [ "$grub_dev" = 'efi' ]; then
                boot_dev_msg="--> EFI bootloader can be installed."
              elif [ -z "$grub_dev" ]; then
                boot_dev_msg="--> ${c4}grub${c_end} bootloader will not be installed."
              else
                boot_dev_msg="--> ${c4}grub${c_end} bootloader will be installed in ${c4}$grub_dev${c_end}"
              fi;;
  'extlinux') boot_dev_msg="--> ${c4}extlinux${c_end} bootloader will be installed on${c4}${}${c_end}";;
      'none') boot_dev_msg="--> no bootloader will be installed.";;
esac

if [ "$ROOT_ENCRYPT" != 'no' ]; then
  os_enc_message=", and will be encrypted with $ROOT_ENCRYPT."
fi

case "$SEPERATE_HOME" in
   'no') home_part_msg="--> ${c4}/home${c_end} will not be on a separate partition.";;
  'yes') home_part_msg="--> ${c4}/home${c_end} will be installed on ${c4}$home_dev${c_end} and formatted as $HOME_FS_TYPE";;
esac

if [ "$SEPERATE_HOME" = 'no' ] && [ "$HOME_ENCRYPT" != 'no' ]; then
  home_enc_msg=", and will be encrypted with $HOME_ENCRYPT."
fi

if [ "$SEPERATE_BOOT" = 'yes' ] && [ -n "$boot_dev" ] && [ "$BOOT_PART" ]; then
  if [ "$BOOT_FS_TYPE" = 'Do not format' ]; then
    boot_part_msg="--> ${c4}/boot${c_end} will be installed on ${c4}$boot_dev${c_end} and not formatted"
  else
    boot_part_msg="--> ${c4}/boot${c_end} will be installed on ${c4}$boot_dev${c_end}\\n\\t and formatted as ${c4}$BOOT_FS_TYPE${c_end}."
  fi
fi

case "$SEPERATE_SWAP" in
  'file') swap_msg="--> ${c4}swap${c_end} will be ${c4}$SWAPFILE${c_end} with $SWAP_SIZE";;
   'yes') swap_msg="--> ${c4}swap${c_end} partition will be ${c4}$SWAP_PART${c_end}";;
    'no') swap_msg="--> no swap partition or file";;
esac

while true; do
  clear; echo "\\n\\n
${c3}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━
${c4}    ╻┏┓╻┏━┓╺┳╸┏━┓╻  ╻  ┏━┓╺┳╸╻┏━┓┏┓╻     ┏━┓╻ ╻┏┳┓┏┳┓┏━┓┏━┓╻ ╻
    ┃┃┗┫┗━┓ ┃ ┣━┫┃  ┃  ┣━┫ ┃ ┃┃ ┃┃┗┫     ┗━┓┃ ┃┃┃┃┃┃┃┣━┫┣┳┛┗┳┛
    ╹╹ ╹┗━┛ ╹ ╹ ╹┗━╸┗━╸╹ ╹ ╹ ╹┗━┛╹ ╹     ┗━┛┗━┛╹ ╹╹ ╹╹ ╹╹┗╸ ╹
${c3}━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━━${c_end}
 ${c1}WARNING:${c_end} This is your last chance to exit before any changes are made.

 $boot_dev_msg
 --> ${c4}/root${c_end} system will be installed on ${c4}$install_dev${c_end}
     and formatted as ${c4}$ROOT_FS_TYPE${c_end} $os_enc_message
 $boot_part_msg
 $home_part_msg $home_enc_msg
 ${uuid_message}
 ${swap_msg}

 --> Hostname: $NEW_HOSTNAME
 --> Username: $NEW_USER_NAME

 Proceed with the installation?
   1) Yes
   2) No, abort the installation.

"
    read -r "ans"
    case $ans in
      [1Yy]*) break;;
      [2Nn]*) exit 0;;
    esac
done

# █▀▄ █▀▀ █▀▀ ▀█▀ █▀█   ▀█▀ █▀█ █▀▀ ▀█▀ █▀█ █   █   █▀█ ▀█▀ ▀█▀ █▀█ █▀█
# █▀▄ █▀▀ █ █  █  █ █    █  █ █ ▀▀█  █  █▀█ █   █   █▀█  █   █  █ █ █ █
# ▀▀  ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀

# █ █ █▀█ █▄█ █▀█ █ █ █▀█ ▀█▀
# █ █ █ █ █ █ █ █ █ █ █ █  █
# ▀▀▀ ▀ ▀ ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀
# Unmount or close anything that might need unmounting or closing
cleanup(){
printf "\\nCleaning up...\\n" 2>&1 | log
if [ "$ACTIVE_SWAP" ]; then
  swapoff "$ACTIVE_SWAP"
  unset ACTIVE_SWAP
fi
check_unmount '/target/proc'
check_unmount '/target/dev'
check_unmount '/target/sys'
check_unmount '/target/boot/efi'
check_unmount '/target/boot'
check_unmount '/target_home'
check_unmount '/target'
check_unmount "$install_dev"
check_unmount '/dev/mapper/root_fs'
if [ -h /dev/mapper/root_fs ]; then
  crypt_close "$ROOT_ENCRYPT" /dev/mapper/root_fs
fi
if [ "$SEPERATE_HOME" = 'yes' ] && [ -b "$home_dev" ]; then
  check_unmount "$home_dev"
  check_unmount'/dev/mapper/home_fs'
  if [ -h /dev/mapper/home_fs ]; then
    crypt_close "$HOME_ENCRYPT" /dev/mapper/home_fs
  fi
fi
if [ "$SEPERATE_BOOT" = 'yes' ] && [ -b "$boot_dev" ]; then
  check_unmount "$boot_dev"
fi
check_rmdir '/target_home'
check_rmdir '/target/boot'
check_rmdir '/target'
}
cleanup

# make mount point, format, adjust reserve and mount
# install_dev must maintain the device name for cryptsetup
# install_part will be either device name or /dev/mapper name as needed.
printf  "Preparing $s...\\n" "$install_dev"
check_mkdir "/target"
if [ "$ROOT_ENCRYPT" != 'no' ]; then
  case "$ROOT_ENCRYPT" in
	     'LUKS') printf "You will need to create a passphrase.\\n"
                 cryptsetup luksFormat "$install_dev" || check_exit
                 printf "Encrypted partition created. Opening it...\\n"
                 cryptsetup luksOpen "$install_dev" root_fs || check_exit
                 install_part="/dev/mapper/root_fs"
                 crypttype_root='luks';;
	'TrueCrypt') truecrypt -t -c "$ROOT_PART"
	             cryptsetup open "$ROOT_PART" --type tcrypt root_fs
	             install_part="/dev/mapper/root_fs"
	             crypttype_root='tcrypt';;
	'VeraCrypt') veracrypt -t -c "$ROOT_PART"
	             cryptsetup open "$ROOT_PART" --type tcrypt root_fs
	             install_part="/dev/mapper/root_fs"
	             crypttype_root='tcrypt';;
  esac
else
    install_part="$install_dev"
fi

# format /
if [ "$no_format" != "yes" ] && [ "$ROOT_FS_TYPE" != 'Do not format' ] && [ -n "$ROOT_FS_TYPE" ]; then
  create_filesystem "$install_part" "$ROOT_FS_TYPE"
fi

if [ "$(lsblk -no MOUNTPOINT "$install_part")" != "/target" ]; then
  mount "$install_part" /target || check_exit
fi

# /home
# make mount point for separate home if needed
# and add /home/* to the excludes list if it's not already there
if [ "$SEPERATE_HOME" = 'yes' ]; then
	printf "\\n\\tPreparing %s..." "$home_dev"
	check_mkdir "/target_home"
	if [ "$HOME_ENCRYPT" != 'no' ]; then
      home_dev_real="$home_dev"
      home_dev="/dev/mapper/home_fs"
	  case "$HOME_ENCRYPT" in
          'LUKS') printf "You will need to create a passphrase.\\n"
		          cryptsetup luksFormat "$home_dev" || check_exit
		          printf "Encrypted partition created. Opening it...\\n"
		          cryptsetup luksOpen "$home_dev" home_fs || check_exit
                  crypttype_home='luks';;
     'TrueCrypt') truecrypt -t -c "$HOME_PART"
                  cryptsetup open "$HOME_PART" --type tcrypt home_fs
                  crypttype_home='tcrypt';;
     'VeraCrypt') veracrypt -t -c "$HOME_PART"
                  cryptsetup open "$HOME_PART" --type tcrypt home_fs
                  crypttype_home='tcrypt';;
	  esac
    else
	  HOME_PART="$home_dev"
	fi

# format /home
    if [ "$no_format" != 'yes' ] && [ "$HOME_FS_TYPE" != 'Do not format' ] && [ -n "$HOME_FS_TYPE" ]; then
      create_filesystem "$home_dev" "$HOME_FS_TYPE"
    fi
	if [ "$HOME_ENCRYPT" != 'no' ]; then
	  if [ "$(lsblk -no MOUNTPOINT "$home_dev")" != "/target_home" ]; then
		mount "$home_dev" /target_home || check_exit
	  fi
	else
	  if [ "$(lsblk -no MOUNTPOINT "$HOME_PART")" != "/target_home" ]; then
		mount "$HOME_PART" /target_home || check_exit
	  fi
	fi
	sep_home_opt="--exclude=/home/*"
fi

# /boot
# make mount point for separate /boot if needed and add /boot/* to the excludes list
# if it's not already there allow default for reserved blocks (don't need tune2fs here)
if [ "$SEPERATE_BOOT" = "yes" ] && [ -n "$boot_dev" ]; then
	check_mkdir "/target/boot"
# format /boot
    if [ "$no_format" != "yes" ] && [ "$BOOT_FS_TYPE" != 'Do not format' ] && [ -n "$BOOT_FS_TYPE" ]; then
      create_filesystem "$boot_dev" "$BOOT_FS_TYPE"
    fi
	#mount "$boot_dev" /target_boot
	mount "$boot_dev" /target/boot
	sep_boot_opt="--exclude=/boot/*"
fi

cat > "$rsync_excludes" <<EOF
- /dev/*
- /cdrom/*
- /media/*
- /target
- /swapfile
- /.swapfile
- /.swap
- $SWAPFILE
- /mnt/*
- /sys/*
- /proc/*
- /tmp/*
- /live
- /lost+found
- /boot/lost+found
- /boot/grub/grub.cfg
- /boot/grub/menu.lst
- /boot/grub/device.map
- /etc/udev/rules.d/70-persistent-cd.rules
- /etc/udev/rules.d/70-persistent-net.rules
- /etc/fstab
- /etc/mtab
- /home/snapshot
- /home/*/.gvfs
- /var/cache/apt/*
- /var/lib/dbus/machine-id
- /etc/popularity-contest.conf
# Added for newer version of live-config/live-boot in sid (to become Jessie)
- /lib/live/overlay
- /lib/live/image
- /lib/live/rootfs
- /lib/live/mount
- /run/*
# Added for symlink /lib
- /usr/lib/live/overlay
- /usr/lib/live/image
- /usr/lib/live/rootfs
- /usr/lib/live/mount
# Artix buildiso
- /var/lib/artools/buildiso/
- /home/*/artools-workspace/
EOF

# █▄█ █▀█ ▀█▀ █▀█    █▀▀ █▀█ █▀█ █ █
# █ █ █▀█  █  █ █    █   █ █ █▀▀  █
# ▀ ▀ ▀ ▀ ▀▀▀ ▀ ▀    ▀▀▀ ▀▀▀ ▀    ▀
case "$install_source" in
   'from_iso') # copy everything over except the things listed in the exclude list
               printf "\\nStarting main copy...\\n" 2>&1 | log
               rsync -av / /target/ \
                     --filter='P lost+found' --filter='H lost+found' \
                     --exclude-from="$rsync_excludes" ${sep_home_opt} ${sep_boot_opt} \
                     --delete-before --delete-excluded
               # copy separate /home if needed
               if [ "$SEPERATE_HOME" = 'yes' ]; then
                 printf "Copying home folders to new partition...\\n" 2>&1 | log
                 rsync -av /home/ /target_home/  \
                       --filter='P lost+found' --filter='H lost+found' \
                       --exclude-from="$home_boot_excludes"
               fi
               # copy separate /boot if needed
               if [ "$SEPERATE_BOOT" = 'yes' ] && [ -n "$boot_dev" ]; then
                 printf "Copying files to boot partition...\\n" 2>&1 | log
                 rsync -av /boot/ /target/boot/  \
                       --filter='P lost+found' --filter='H lost+found' \
                       --exclude-from="$home_boot_excludes"
               fi;;
  'bootstrap') if command -v debootstrap >> /dev/null \
               || command -v /sbin/debootstrap >> /dev/null \
               || command -v /usr/sbin/debootstrap >> /dev/null \
               || command -v /usr/local/sbin/debootstrap >> /dev/null ; then
                 debian_bootstrap
               else
                 printf "Error: debootstrap not found\\n"
                 exit 1
               fi;;
esac

# swap
if [ "$SEPERATE_SWAP" = 'file' ] && [ "$SWAPFILE" ]; then
  printf "\\nCreating swap file: %s with a size of %s" "$SWAPFILE" "$SWAP_SIZE" 2>&1 | log
  dd if=/dev/zero of="/target$SWAPFILE" bs=1K count="$swap_size_count" || check_exit
  chmod 600 "/target$SWAPFILE"
  mkswap --label swap "/target$SWAPFILE" || check_exit
  swapon "/target$SWAPFILE" && ACTIVE_SWAP="/target$SWAPFILE"
elif [ "$SEPERATE_SWAP" = 'yes' ] && [ -b "$SWAP_PART" ]; then
  mkswap --label swap "$SWAP_PART" || check_exit
  swapon "$SWAP_PART" && ACTIVE_SWAP="$SWAP_PART"
fi

# Disallow mounting of all fixed drives with pmount
if [ -f /target/etc/pmount.allow ] && [ $pmount_fixed = "no" ]; then
  sed -i 's:/dev/sd\[a-z\]:#/dev/sd\[a-z\]:' /target/etc/pmount.allow
fi

# Re-enable updatedb if it was disabled by an older version of refractasnapshot
if [ -e /target/usr/bin/updatedb.mlocate ] && [ ! -x /target/usr/bin/updatedb.mlocate ]; then
  chmod +x /target/usr/bin/updatedb.mlocate
fi

# █▀▀ █▀▀ ▀█▀ █▀█ █▀▄
# █▀▀ ▀▀█  █  █▀█ █▀▄
# ▀   ▀▀▀  ▀  ▀ ▀ ▀▀
# /
if [ "$ROOT_ENCRYPT" = 'no' ] && [ "$USE_UUID" = 'yes' ]; then
  install_part="UUID=$(blkid -s UUID "$install_dev" -o value)"
fi
printf "\\nGenerating /etc/fstab...\\n" 2>&1 | log
printf "%s\\t/\\t%s\\tdefaults,noatime\\t0\\t1\\n" "$install_part" "$ROOT_FS_TYPE" >> /target/etc/fstab
# /home
if [ "$SEPERATE_HOME" = 'yes' ] && [ -n "$HOME_PART" ]; then
  if [ "$HOME_FS_TYPE" = 'Do not format' ]; then
    HOME_FS_TYPE="$(lsblk -no FSTYPE "$home_dev" | grep -v -e '^$')"
  fi
  if [ "$HOME_ENCRYPT" = 'no' ]; then
	printf "Adding /home entry to fstab...\\n" 2>&1 | log
	if [ "$USE_UUID" = 'yes' ]; then
	  home_part="UUID=$(blkid -s UUID "$home_dev" -o value)"
	  printf "%s\\t/home\\t%s\\tdefaults,noatime\\t0\\t2\\n" "$home_part" "$HOME_FS_TYPE" >> /target/etc/fstab
    fi
  fi
  printf "%s\\t/home\\t%s\\tdefaults,noatime\\t0\\t2\\n" "$home_dev" "$HOME_FS_TYPE" >> /target/etc/fstab || check_exit
fi
# /boot
if [ "$SEPERATE_BOOT" = 'yes' ] && [ -n "$boot_dev" ]; then
  printf "Adding /boot entry to fstab...\\n" 2>&1 | log
  if [ "$BOOT_FS_TYPE" = 'Do not format' ]; then
    BOOT_FS_TYPE="$(lsblk -no FSTYPE "$boot_dev" | grep -v -e '^$')"
  fi
  case "$USE_UUID" in
    'yes') printf "%s\\t/boot\\t%s\\tdefaults,noatime,\\t0\\t1\\n" \
           "UUID=$(blkid -s UUID "$boot_dev" -o value)" \
           "$BOOT_FS_TYPE" >> /target/etc/fstab;;
        *) printf "%s\\t/boot\\t%s\\tdefaults,noatime,\\t0\\t1\\n" \
           "$boot_dev" "$BOOT_FS_TYPE" >> /target/etc/fstab || check_exit;;
  esac
fi
# swap
if [ "$SEPERATE_SWAP" != 'no' ]; then
  printf "Adding swap entry to fstab...\\n" 2>&1 | log
  if [ "$use_existing_swap" = 'yes' ]; then
    if [ "$USE_UUID" = 'yes' ]; then
      swap_part="UUID=$(blkid -s UUID "$swap_dev" -o value)"
    else
      swap_part="$swap_dev"
    fi
    printf "%s\\tswap\\tswap\\tdefaults\\t0\\t0\\n" "$swap_part" >> /target/etc/fstab
  elif [ "$SEPERATE_SWAP" = 'yes' ] && [ -b "$SWAP_PART" ]; then
    printf "%s\\tswap\\tswap\\tdefaults\\t0\\t0\\n" "$SWAP_PART" >> /target/etc/fstab
  elif [ "$SEPERATE_SWAP" = 'file' ] && [ -e "/target$SWAPFILE" ]; then
    printf "%s\\tswap\\tswap\\tdefaults\\t0\\t0\\n" "$SWAPFILE" >> /target/etc/fstab
  fi
fi

# █▀▀ █▀▄ █ █ █▀█ ▀█▀ ▀█▀ █▀█ █▀▄
# █   █▀▄  █  █▀▀  █   █  █▀█ █▀▄
# ▀▀▀ ▀ ▀  ▀  ▀    ▀   ▀  ▀ ▀ ▀▀
# /
if [ "$ROOT_ENCRYPT" != 'no' ]; then
  printf "Adding %s entry to crypttab...\\n" "$install_dev" 2>&1 | log
  if [ "$USE_UUID" = 'yes' ]; then
    if blkid "$install_dev" --output export | grep -q '^UUID='; then
      install_crypt="UUID=$(blkid "$install_dev" -s UUID -o value)"
      printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_crypt" "$crypttype_root" >> /target/etc/crypttab
    elif blkid "$install_dev" --output export | grep -q '^PARTUUID='; then
      install_crypt="PARTUUID=$(blkid "$install_dev" -s PARTUUID -o value)"
      printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_crypt" "$crypttype_root" >> /target/etc/crypttab
    else
      printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_dev" "$crypttype_root" >> /target/etc/crypttab
    fi
  else
    printf "root_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$install_dev" "$crypttype_root" >> /target/etc/crypttab
  fi
fi
# /home
if [ "$HOME_ENCRYPT" != 'no' ]; then
  printf "Adding %s entry to crypttab...\\n" "$home_dev_real" 2>&1 | log
  if [ "$USE_UUID" = "yes" ]; then
    if blkid "$home_dev_real" --output export | grep -q '^UUID='; then
      home_crypt="UUID=$(blkid "$home_dev_real" -s UUID -o value)"
      printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_crypt" "$crypttype_home" >> /target/etc/crypttab
    elif blkid "$home_dev_real" --output export | grep -q '^PARTUUID='; then
      home_crypt="PARTUUID=$(blkid "$home_dev_real" -s PARTUUID -o value)"
      printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_crypt" "$crypttype_home" >> /target/etc/crypttab
    else
      printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_dev_real" "$crypttype_home" >> /target/etc/crypttab
    fi
  else
    printf "home_fs\\t\\t%s\\t\\tnone\\t\\t%s\\n" "$home_dev_real" "$crypttype_home" >> /target/etc/crypttab
  fi
fi

#####  May need to check for /etc/default/grub and warn if absent ##########
# Tell grub to use encrypted /boot directory.
if [ "$BOOT_ENCRYPT" = "yes" ]; then
  if ! [ "$(grep ^GRUB_ENABLE_CRYPTODISK /target/etc/default/grub)" ]; then
	printf "\\nGRUB_ENABLE_CRYPTODISK=y\\n" >> /target/etc/default/grub
  fi
fi

# █▀▀ █▀▀ █ █
# ▀▀█ ▀▀█ █▀█
# ▀▀▀ ▀▀▀ ▀ ▀
# Allow users to login to ssh with passwords if desired.
# Allow root login only with auth keys.
# or do nothing.
if [ -f /target/etc/ssh/sshd_config ]; then
  case "$ssh_pass" in
    'yes') sed -i~ 's/PasswordAuthentication no/PasswordAuthentication yes/' /target/etc/ssh/sshd_config
	       sed -i 's/PermitRootLogin yes/PermitRootLogin prohibit-password/' /target/etc/ssh/sshd_config;;
     'no') sed -i~ 's/.*PasswordAuthentication yes/PasswordAuthentication no/' /target/etc/ssh/sshd_config
           sed -i 's/PermitRootLogin yes/PermitRootLogin prohibit-password/' /target/etc/ssh/sshd_config;;
        *) echo "WARNING: ssh_pass value not recognized. No changes were made to /etc/ssh/sshd_config";;
  esac
fi

# █▀▄ █▀█ █▀█ ▀█▀ █   █▀█ █▀█ █▀▄ █▀▀ █▀▄
# █▀▄ █ █ █ █  █  █   █ █ █▀█ █ █ █▀▀ █▀▄
# ▀▀  ▀▀▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀ ▀▀  ▀▀▀ ▀ ▀
# mount /dev /proc and /sys so grub will behave and chroot will work
for x in dev proc sys; do
  check_mkdir "/target/$x"
  printf "Mounting /%s...\\n" "$x"
  mount --bind "/$x/" "/target/$x/" || check_exit
done

# If /boot is separate partition, need to mount it in chroot for grub and for efi
if [ "$SEPERATE_BOOT" = 'yes' ] && [ -n "$boot_dev" ]; then
  if ! lsblk  -no MOUNTPOINT "$boot_dev" | grep -q '/target/boot'; then
    mount "$boot_dev" /target/boot
  fi
fi

# This test is not complete and should probably be done earlier. grub_dev="efi" above
if [ -n "$esp_dev" ]; then
  uefi_ready='yes'
fi

# add entry for esp_dev to fstab if needed
if [ "$uefi_ready" = "yes" ] && [ $uefi_boot = "yes" ]; then
	[ "$USE_UUID" = "yes" ] && esp_part="UUID=$(blkid "$esp_dev" -s UUID -o value)" || esp_part="$esp_dev"
    printf "Adding esp entry to fstab...\\n"
    printf "%s\\t/boot/efi\\tvfat\\tdefaults\\t0\\t1\\n" "$esp_part" >> /target/etc/fstab
	check_mkdir /target/boot/efi
	mount "$esp_dev" /target/boot/efi/
fi

# vmlinuz
if [ -f "/boot/vmlinuz-$(uname -r)" ]; then
  KERN_FILE="/boot/vmlinuz-$(uname -r)"
elif [ -f "/boot/vmlinuz" ]; then
  KERN_FILE="/boot/vmlinuz"
elif [ -f "/vmlinuz-$(uname -r)" ]; then
  KERN_FILE="/vmlinuz-$(uname -r)"
elif [ -f /vmlinuz ]; then
  KERN_FILE="/vmlinuz"
elif [ "$(find /boot -maxdepth 1 -iname "*vmlinuz*")" ]; then
  KERN_FILE="$(find /boot -maxdepth 1 -iname "*vmlinuz*" | sort | awk 'NR==1 {print}')"
elif [ "$(find / -maxdepth 1 -iname "*vmlinuz*")" ]; then
  KERN_FILE="$(find / -maxdepth 1 -iname "*vmlinuz*" | sort | awk 'NR==1 {print}')"
else
  printf "Error: could not find vmlinuz\\n"
  exit 1
fi

if [ -L "$KERN_FILE" ]; then
  KERN_FILE="$(readlink -e "$KERN_FILE")"
fi

if [ ! -f "$KERN_FILE" ]; then
  printf "Error: could not find vmlinuz\\n"
  exit 1
fi

KERN_NAME="$(echo "$KERN_FILE" | awk -F '/' '{print $(NF)}')"
cp -v "$KERN_FILE" "/target/boot/$KERN_NAME" 2>&1 | log || check_exit
ln -vsr "/target/boot/$KERN_NAME" /target/vmlinuz  2>&1 | log || check_exit
ln -vsrf "/target/boot/$KERN_NAME" /target/boot/vmlinuz  2>&1 | log || check_exit

# initrd
if [ -f "/boot/initrd.img-$(uname -r)" ]; then
  INIT_FILE="/boot/initrd.img-$(uname -r)"
elif [ -f "/boot/initrd.img" ]; then
  INIT_FILE="/boot/initrd.img"
elif [ -f "/initrd.img-$(uname -r)" ]; then
  INIT_FILE="/initrd.img-$(uname -r)"
elif [ -f "/initrd.img" ]; then
  INIT_FILE="/initrd.img"
elif [ -f "/boot/initramfs.img-$(uname -r)" ]; then
  INIT_FILE="/boot/initramfs.img-$(uname -r)"
elif [ -f "/boot/initramfs.img" ]; then
  INIT_FILE="/boot/initramfs.img"
elif [ -f "/initramfs.img-$(uname -r)" ]; then
  INIT_FILE="/initramfs.img-$(uname -r)"
elif [ -f "/initramfs.img" ]; then
  INIT_FILE="/initramfs.img"
elif [ "$(find /boot -maxdepth 1 -iname "*initrd*" -not -iname "*fallback*")" ]; then
  INIT_FILE="$(find /boot -maxdepth 1 -iname "*initrd*" -not -iname "*fallback*" | sort | awk 'NR==1 {print}')"
elif [ "$(find /boot -maxdepth 1 -iname "*initrd*")" ]; then
  INIT_FILE="$(find /boot -maxdepth 1 -iname "*initrd*" | sort | awk 'NR==1 {print}')"
elif [ "$(find /boot -maxdepth 1 -iname "*initramfs*" -not -iname "*fallback*")" ]; then
  INIT_FILE="$(find /boot -maxdepth 1 -iname "*initramfs*" -not -iname "*fallback*" | sort | awk 'NR==1 {print}')"
elif [ "$(find /boot -maxdepth 1 -iname "*initramfs*")" ]; then
  INIT_FILE="$(find /boot -maxdepth 1 -iname "*initramfs*" | sort | awk 'NR==1 {print}')"
elif [ "$(find / -maxdepth 1 -iname "*initrd*")" ]; then
  INIT_FILE="$(find / -maxdepth 1 -iname "*initrd*" | sort | awk 'NR==1 {print}')"
elif [ "$(find / -maxdepth 1 -iname "*initramfs*")" ]; then
  INIT_FILE="$(find / -maxdepth 1 -iname "*initramfs*" | sort | awk 'NR==1 {print}')"
fi

if [ -L "$INIT_FILE" ]; then
  INIT_FILE="$(readlink -e "$INIT_FILE")"
fi

if [ ! -f "$INIT_FILE" ]; then
  printf "Error: could not find init file\\n"; exit 1
fi

INIT_NAME="$(echo "$INIT_FILE" | awk -F '/' '{print $(NF)}')"
if echo "$INIT_NAME" | grep -iq "initrd" ; then
  INIT_NAME_BASE="initrd.img"
elif echo "$INIT_NAME" | grep -iq "initramfs" ; then
  INIT_NAME_BASE="initramfs.img"
fi

cp -v "$INIT_FILE" "/target/boot/$INIT_NAME" 2>&1 | log || check_exit

if [ ! -L "/target/$INIT_NAME_BASE" ]; then
  ln -vsr "/target/boot/$INIT_NAME" "/target/$INIT_NAME_BASE" 2>&1 | log || check_exit
fi
if [ ! -L "/target/boot/$INIT_NAME_BASE" ]; then
  ln -vsrf "/target/boot/$INIT_NAME" "/target/boot/$INIT_NAME_BASE" 2>&1 | log || check_exit
fi

# █▀▀ █▀▄ █ █ █▀▄
# █ █ █▀▄ █ █ █▀▄
# ▀▀▀ ▀ ▀ ▀▀▀ ▀▀
install_grub(){
printf "\\n Setting up grub bootloader... Please wait...\\n" 2>&1 | log

if [ "$BASE_DISTRO_LIKE" = 'debian' ]; then
  grubversion="$(dpkg -l | awk '$1 ~ /ii|hi/ && $2 ~ /grub-[eglp]/ && $2 !~ /bin|doc/ && !/dummy/ {print $2}')"
  case "$BOOT_METHOD" in
      'UEFI') if ! echo "$grubversion" | grep -q 'grub-efi'; then
                grub_deb="$(find "/${grub_package_dir##/}" -maxdepth 1 -iname "grub-efi*.deb" -not -iname "*bin*")"
                grub_bin_deb="$(find "/${grub_package_dir##/}" -maxdepth 1 -iname "grub-efi*bin*.deb")"
                  if chroot /target dpkg -l | awk -v pak="$grubversion" '$1 ~ /ii|hi/ && $2 == pak {print}' > /dev/null; then
                    chroot /target dpkg --force-remove-essential,remove-protected,remove-essential,depends --purge "$grubversion"
                  fi
                  if chroot /target dpkg -l | awk -v pak="$grubversion" '$1 ~ /ii|hi/ && $2 == pak {print}' > /dev/null; then
                    chroot /target dpkg --force-remove-essential,remove-protected,remove-essential,depends --purge "$grubversion"
                  fi
                chroot /target dpkg --install "${grub_package_dir%%/}/${grub_deb##*/}"
                chroot /target dpkg --install "${grub_package_dir%%/}/${grub_bin_deb##*/}"
                grubversion="${grub_deb%%_*}"
              fi;;
    'Legacy') if ! echo "$grubversion" | grep -q 'grub-pc'; then
                grub_deb="$(find "/${grub_package_dir##/}" -maxdepth 1 -iname "grub-pc*.deb" -not -iname "*bin*")"
                grub_bin_deb="$(find "/${grub_package_dir##/}" -maxdepth 1 -iname "grub-pc*bin*.deb")"
                if echo "$grubversion" | grep -q 'grub-efi'; then
                  if chroot /target dpkg -l | awk -v pak="grub-efi" '$1 ~ /ii|hi/ && $2 == pak && /dummy/{print}' > /dev/null; then
                    chroot /target dpkg --force-remove-essential,remove-protected,remove-essential,depends --purge 'grub-efi'
                  fi
                  if chroot /target dpkg -l | awk -v pak="$grubversion" '$1 ~ /ii|hi/ && $2 == pak {print}' > /dev/null; then
                    chroot /target dpkg --force-remove-essential,remove-protected,remove-essential,depends --purge "$grubversion"
                  fi
                  if chroot /target dpkg -l | awk -v pak="$grubversion-bin" '$1 ~ /ii|hi/ && $2 == pak {print}' > /dev/null; then
                    chroot /target dpkg --force-remove-essential,remove-protected,remove-essential,depends --purge "${grubversion}-bin"
                  fi
                fi
                chroot /target dpkg --install "${grub_package_dir%%/}/${grub_bin_deb##*/}"
                chroot /target dpkg --install "${grub_package_dir%%/}/${grub_deb##*/}"
                grubversion='grub-pc'
              fi;;
  esac
fi

if [ ! -n "$grub_partition" ] && [ -b "$boot_dev" ]; then
  grub_partition="$boot_dev"
fi
grub_dev="$(echo "$grub_partition" | tr -d '[:digit:]')"

# If grub is installed to a partition, we need to know if it's grub-pc or grub-legacy/grub-gfx to handle it properly.
if [ "$grub_dev" != "efi" ]; then
  if [ "$grubversion" != 'grub-pc' ]; then
	# isolate the device (sdx) letter then use tr like this to translate to the right number for grub
	GRUBDEVICENUM=$(echo "$grub_partition" | tr -d '/dev/sd[:digit:]' | tr 'a-j' '0-9')
	# isolate the partition number
	INSTALLPARTNUM=$(echo "$grub_partition" | tr -d '[:alpha:][/]')
	# and reduce it by 1 for grub
	GRUBPARTNUM="$(( INSTALLPARTNUM - 1))"
	# finally get the finished grub root syntax
	GRUBROOT="(hd$GRUBDEVICENUM,$GRUBPARTNUM)"
	chroot /target grub-install "$grub_partition"
	grub --batch <<EOF
	root $GRUBROOT
	setup $GRUBROOT
	quit
EOF
  else
	chroot /target grub-install --recheck --no-floppy --force "$grub_dev" 2>&1 | log || check_exit
  fi

#elif [ "$grub_dev" = "efi" ]; then
else
	chroot /target grub-install "${efi_name_opt}" 2>&1 | log || check_exit
#elif [ -n "$grub_dev" ]; then
#    printf "\\nInstalling the grub boot loader...\\n"
#    chroot /target grub-install "$grub_dev" 2>&1 | log || check_exit
fi

chroot /target update-grub || check_exit
}

################################################################################################
if [ "$NEW_BOOTLOADER" = 'extlinux' ]; then
  case "$SEPERATE_BOOT" in
    'yes') BOOT_DEVICE=$(echo "$boot_dev" | tr -d '[:digit:]');;
     'no') BOOT_DEVICE=$(echo "$ROOT_PART" | tr -d '[:digit:]');;
        *) printf "Error while installing extlinux: SEPERATE_BOOT not set\\n" 2>&1 | log
           exit 1;;
  esac
  check_mkdir "/target/boot/syslinux"
  if [ -f /usr/lib/syslinux/modules/bios/vesamenu.c32 ]; then
    cp -vr /usr/lib/syslinux/modules/bios/*.c32 /target/boot/syslinux/
  elif [ -f /usr/lib/syslinux/bios/vesamenu.c32 ]; then
    cp -vr /usr/lib/syslinux/bios/*.c32 /target/boot/syslinux/
  elif [ -f /lib/syslinux/bios/vesamenu.c32 ]; then
    cp -vr /lib/syslinux/bios/*.c32 /target/boot/syslinux/
  else
    printf "Error: Could not find syslinux c32 files...\\n" 2>&1 | log
    exit 1
  fi
  if [ -f /boot/syslinux/splash.png ]; then
    cp /boot/syslinux/splash.png /target/boot/syslinux/splash.png
  fi
  extlinux -i /target/boot/syslinux
# MBR or GPT
  if [ "$(fdisk -l "$BOOT_DEVICE" | awk '/Disklabel type:/{print $NF}')" = "gpt" ]; then
    if [ -f /usr/lib/syslinux/mbr/gptmbr.bin ]; then
      dd bs=440 count=1 if=/usr/lib/syslinux/mbr/gptmbr.bin of="$BOOT_DEVICE"
    elif [ -f /usr/lib/syslinux/bios/gptmbr.bin ]; then
      dd bs=440 count=1 if=/usr/lib/syslinux/mbr/gptmbr.bin of="$BOOT_DEVICE"
    elif [ -f /lib/syslinux/bios/gptmbr.bin ]; then
      dd bs=440 count=1 if=/lib/syslinux/mbr/gptmbr.bin of="$BOOT_DEVICE"
    else
      printf "Error: Could not find syslinux gptmbr.bin\\n" 2>&1 | log
      exit 1
    fi
  else
    if [ -f /usr/lib/syslinux/mbr/mbr.bin ]; then
      dd bs=440 count=1 if=/usr/lib/syslinux/mbr/mbr.bin of="$BOOT_DEVICE"
    elif [ -f /usr/lib/syslinux/bios/mbr.bin ]; then
      dd bs=440 count=1 if=/usr/lib/syslinux/bios/mbr.bin of="$BOOT_DEVICE"
    elif [ -f /lib/syslinux/bios/mbr.bin ]; then
      dd bs=440 count=1 if=/lib/syslinux/bios/mbr.bin of="$BOOT_DEVICE"
    else
      printf "Error: Could not find syslinux mbr.bin\\n" 2>&1 | log
      exit 1
    fi
  fi
  # Generate syslinux.cfg
  if [ "$ROOT_ENCRYPT" != 'no' ]; then
    if [ "$USE_UUID_IN_EXTLINUX" = 'yes' ]; then
      if blkid "$install_dev" --output export | grep -q '^UUID='; then
        UUID="$(blkid "$install_dev" --output export | grep '^UUID=')"
        crypt_dev="$(echo "$UUID" | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
        root_dev="$install_part"
      elif blkid "$install_dev" --output export | grep -q '^PARTUUID='; then
        UUID="$(blkid "$install_dev" --output export | grep '^PARTUUID=')"
        crypt_dev="$(echo "$UUID" | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
        root_dev="$install_part"
      else
        crypt_dev="$(echo "$install_dev" | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
        root_dev="$install_part"
      fi
    else
	  crypt_dev="$(echo "$install_dev" | sed 's/^/cryptdevice=/;s/$/:root_fs/;s/\"//g')"
	  root_dev="$install_part"
    fi
  else
    if [ "$USE_UUID_IN_EXTLINUX" = 'yes' ]; then
      unset crypt_dev
      if blkid "$install_dev" --output export | grep -q '^UUID='; then
        UUID="$(blkid "$install_dev" --output export | grep '^UUID=' | tr -d \")"
        root_dev="$UUID"
      elif blkid "$install_dev" --output export | grep -q '^PARTUUID='; then
        UUID="$(blkid "$install_dev" --output export | grep '^PARTUUID=' | tr -d \")"
        root_dev="$UUID"
      else
        root_dev="$install_dev"
      fi
    else
      root_dev="$install_dev"
    fi
  fi

  if [ "$BLACKLIST_PCSPKR_IN_EXTLINUX" = 'yes' ]; then
    blacklist_pcspkr="modprobe.blacklist=pcspkr"
  else
    unset blacklist_pcspkr
  fi

#  case "$install_source" in
#     'bootstrap') openrc_init='init=/sbin/openrc-init';;
#    'from_iso'|*) unset openrc_init;;
#  esac

  cat > /target/boot/syslinux/syslinux.cfg << EOF
UI vesamenu.c32
PROMPT	1
MENU TITLE Boot Menu
MENU BACKGROUND splash.png
MENU HSHIFT 18
MENU VSHIFT 3
MENU ROWS 7
MENU WIDTH 40
MENU TABMSGROW 29
TIMEOUT	5
DEFAULT	$BASE_DISTRO
LABEL $BASE_DISTRO
	MENU LABEL $BASE_DISTRO
	LINUX	../vmlinuz
	APPEND	lang=de $crypt_dev root=$root_dev $blacklist_pcspkr $openrc_init
	INITRD	../$INIT_NAME_BASE
LABEL memtest
	MENU LABEL Memory test
	KERNEL ../memtest
LABEL reboot
	MENU LABEL Reboot
	COM32 reboot.c32
LABEL poweroff
	MENU LABEL Power Off
	COM32 poweroff.c32
EOF

elif [ "$NEW_BOOTLOADER" = "grub" ]; then
######  INSERT PAUSE TO ALLOW MANUAL WORK BEFORE GRUB (e.g. uefi)
  ASK_GRUB=""
  while [ -z "$ASK_GRUB" ]; do
    LINE="1"
    TOPTEXT="$(text_box $text_box_opt " The system is now ready to install grub
 If you want to, you now have some time to chroot into the system" )"
    ASK_GRUB="$(awk_menu " > Install Grub and finish the installation
 > Continue wihtout a bootloader
 < Abort the installation and exit")"
# $grub_package is null if installed grub matches boot type (uefi or bios)
    case "$ASK_GRUB" in
      '> Install Grub and finish the installation')
        if [ -n "$grub_package" ]; then
        # copy grub packages
          if [ "$BASE_DISTRO_LIKE" = 'debian' ]; then
            find "$grub_package_dir" -maxdepth 1 -name "$grub_package" -exec cp {} /target \;
#	chroot /target find . -name $grub_package -maxdepth 1 -exec dpkg -i {} \;
# this works, but grub-pc/grub-pc-bin installed out of order.
            if [ -n "$grub_package" ]; then
              chroot /target /bin/bash -c "dpkg -i $grub_package"
            fi
            echo "$grub_package" | grep -qe 'grub-pc' && grubversion='grub-pc'
            echo "$grub_package" | grep -qe 'grub-efi' && grubversion='grub-efi' && grub_dev='efi'
          fi
        fi
        if [ -z "$bios_boot_warning" ]; then
          install_grub
        fi;;
      '> Continue wihtout a bootloader')
        printf "Grub bootloader will not be installed\\n";;
      '< Abort the installation and exit')
       cleanup; exit 0;;
       *) unset ASK_GRUB;;
    esac
  done
fi

# Run update-initramfs to include dm-mod if using encryption
if [ "$ROOT_ENCRYPT" != 'no' ] || [ "$HOME_ENCRYPT" != 'no' ]; then
  if [ -f /usr/sbin/update-initramfs.orig.initramfs-tools ]; then
	chroot /target /usr/sbin/update-initramfs.orig.initramfs-tools -u -k all 2>&1 | log
  elif [ -f /usr/sbin/update-initramfs ]; then
	chroot /target /usr/sbin/update-initramfs -u -k all 2>&1 | log
  fi
fi

##### This should not run if grub_dev=efi and choose 3 above (no bootloader)
#if [[ -n $grub_dev ]] || [[ -n $grub_partition ]] ; then
#    chroot /target update-grub || check_exit
#fi

if [ -f /target/boot/grub/setup_left_core_image_in_filesystem ]; then
	rm -f /target/boot/grub/setup_left_core_image_in_filesystem
fi

# INSTALLATION FINISHED - BEGIN CONFIGURE USERNAME, HOSTNAME, PASSWORDS, SUDO
############################################################################################
# Need to mount the target home partition under the target root partition
# so the commands can find it (for changing user configs gksu)
if [ "$HOME_ENCRYPT" != "no" ]; then
  [ "$SEPERATE_HOME" = 'yes' ] && [ -b "$home_dev" ] && mount "$home_dev" /target/home
else
  [ "$SEPERATE_HOME" = 'yes' ] && [ -b "$HOME_PART" ] && mount "$HOME_PART" /target/home
fi

# it might not be on in some live builds
chroot /target /bin/sh -c "shadowconfig on"

# █ █ █▀▀ █▀▀ █▀▄   █▀█ █▀▀ █▀▀ █▀█ █ █ █▀█ ▀█▀
# █ █ ▀▀█ █▀▀ █▀▄   █▀█ █   █   █ █ █ █ █ █  █
# ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀   ▀ ▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀
case "$install_source" in
   'from_iso') if [ "$OLD_USER_NAME" != "$NEW_USER_NAME" ]; then
                 chroot /target usermod -l "$NEW_USER_NAME" "$OLD_USER_NAME" || check_exit
                 chroot /target usermod -d "/home/$NEW_USER_NAME" -m "$NEW_USER_NAME" || check_exit
                 chroot /target groupmod -n "$NEW_USER_NAME" "$OLD_USER_NAME" || check_exit
                 for i in $(grep "/home/$OLD_USER_NAME" -r "/target/home/$NEW_USER_NAME/.config" 2>/dev/null | awk -F ":" '{ print $1 }'); do
 	               sed -i "s:/home/$OLD_USER_NAME:/home/$NEW_USER_NAME:g" "$i"
                 done
                 if [ -L "/target/home/$NEW_USER_NAME/Desktop/Home" ]; then
                   rm "/target/home/$NEW_USER_NAME/Desktop/Home"
                   ln -s "/home/$NEW_USER_NAME" "/target/home/$NEW_USER_NAME/Desktop/Home"
                 fi
               fi;;
  'bootstrap') [ -z "$NEW_USER_NAME" ] && NEW_USER_NAME="$OLD_USER_NAME"
               chroot /target useradd -m "$NEW_USER_NAME";;
esac
[ -z "$NEW_USER_NAME" ] && NEW_USER_NAME="$OLD_USER_NAME"
PASSWD_OK='0'
while [ "$PASSWD_OK" != '1' ]; do
  printf "\\nCreate a password for %s \\n" "$NEW_USER_NAME" 2>&1 | log
  # Redirect stderr from the error log to the screen, so we can see the prompts from passwd
#  exec 2>&1
  chroot /target passwd "$NEW_USER_NAME" && PASSWD_OK="1" || printf "Error!\\n"
#  exec 2>> "$ERROR_LOG"
done
unset PASSWD_OK

if [ "$NEW_USER_GROUPS" ]; then
  chroot /target usermod -G "$(echo "$GROUPS_IN" | awk '{printf "%s ",$0}' \
  | awk '{gsub(/ /, ",", $0)} {print}' | sed 's/,$//')" "$NEW_USER_NAME"
fi

if [ "$NEW_USER_REAL_NAME_EVENT" ]; then
  chroot /target chfn -f "$NEW_USER_REAL_NAME" "$NEW_USER_NAME"
fi
if [ "$NEW_USER_ROOM_NR_EVENT" ]; then
  chroot /target chfn -r "$NEW_USER_ROOM_NR" "$NEW_USER_NAME"
fi
if [ "$NEW_USER_PHONE_WORK_EVENT" ]; then
  chroot /target chfn -w "$NEW_USER_PHONE_WORK" "$NEW_USER_NAME"
fi
if [ "$NEW_USER_PHONE_HOME_EVENT" ]; then
  chroot /target chfn -h "$NEW_USER_PHONE_HOME" "$NEW_USER_NAME"
fi
check_mkdir "$NEW_USER_HOME_DIR"
if [ "$NEW_USER_HOME_DIR" != "$HOME" ]; then
  chroot /target usermod -md "$NEW_USER_HOME_DIR" "$NEW_USER_NAME"
fi
if [ "$NEW_USER_LOGIN_SHELL" != "$(chroot /target awk -F ':' '/:1000:/ {print $7}' /etc/passwd)" ]; then
  chroot /target usermod -s "$NEW_USER_LOGIN_SHELL" "$NEW_USER_NAME"
fi

# █▀▀ █ █ █▀▄ █▀█
# ▀▀█ █ █ █ █ █ █
# ▀▀▀ ▀▀▀ ▀▀  ▀▀▀
# =>wheezy live-config now uses /etc/sudoers.d
check_rm "/target/etc/sudoers.d/live"

# squeeze (or other distro) might have used /etc/sudoers
if [ "$OLD_USER_NAME" != "$NEW_USER_NAME" ]; then
  if grep -qs "$OLD_USER_NAME" /target/etc/sudoers ; then
    sed -i "/$OLD_USER_NAME/d" /target/etc/sudoers
  fi
fi

if [ "$sudoconfig" = 'TRUE' ] || [ "$sudo_is_default" = 'TRUE' ]; then
  # $NEW_USER_NAME is permitted to use sudo so add him to sudo group
  chroot /target usermod -a -G sudo "$NEW_USER_NAME"
  # it shoud be already there in =>wheezy.. in case it's not:
  if ! grep -qs "^%sudo" /target/etc/sudoers ; then
    echo "%sudo ALL=(ALL:ALL) ALL" >> /etc/sudoers
  fi
fi

if [ "$sudo_is_default" = 'TRUE' ]; then
  # disable root account
  printf "disabling root account...\\n"
  rootpass_hash=$(awk -F ":" '/^root/ {print $3 ":" $4 ":" $5 ":" $6}' /target/etc/shadow)
  sed -i "s|^root:.*|root:\*:${rootpass_hash}:::|" /target/etc/shadow
else
  # files that may have been written by live-config to force live sudo mode # should they just be deleted?
  # rm -f /target/home/*/.gconf/apps/gksu/%gconf.xml
  # rm -f /target/home/*/.*/share/config/*desurc

  # fix gksu in user's home ($NEW_USER_NAME will not use sudo by default)
  if [ -f /target/home/"$NEW_USER_NAME"/.gconf/apps/gksu/%gconf.xml ]; then
    sed -i '/sudo-mode/s/true/false/' /target/home/"$NEW_USER_NAME"/.gconf/apps/gksu/%gconf.xml
  fi

  if [ -f "/target/home/"$NEW_USER_NAME"/.su-to-rootrc" ]; then
    sed -i 's/SU_TO_ROOT_SU=sudo/SU_TO_ROOT_SU=su/' /target/home/"$NEW_USER_NAME"/.su-to-rootrc
  fi

  # detects .kde/ .kde4/ .trinity/ (kdesurc or tdesurc)
  if [ "$(find /target/home/"$NEW_USER_NAME"/.*/share/config -iname "*desurc")" ]; then
    for file in /target/home/"$NEW_USER_NAME"/.*/share/config/*desurc ; do
      sed -i 's/super-user-command=sudo/super-user-command=su/' "$file"
    done
  fi

  if [ "$sudo_shutdown" = "TRUE" ]; then
    ### Maybe move this up so it's available to option "a" (disable sudo) ########
    sudo_include_file="/target/etc/sudoers.d/user_shutdown"
    [ -f "$sudo_include_file" ] && mv "$sudo_include_file" "${sudo_include_file}.old"
    echo "$NEW_USER_NAME ALL= NOPASSWD: /usr/sbin/pm-suspend, /usr/sbin/pm-hibernate, /sbin/halt, /sbin/reboot" > "$sudo_include_file"
  fi

  # set root password
  PASSWD_OK="0"
  while [ "$PASSWD_OK" != '1' ]; do
    printf "\\nCreate a password for the root user\\n"
    exec 2>&1
    chroot /target passwd && PASSWD_OK='1' || printf "Error!\\n"
#    exec 2>> "$ERROR_LOG"
  done
  unset PASSWD_OK
fi

# █▀█ █ █ ▀█▀ █▀█ █   █▀█ █▀▀ ▀█▀ █▀█
# █▀█ █ █  █  █ █ █   █ █ █ █  █  █ █
# ▀ ▀ ▀▀▀  ▀  ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀
if [ "$DISABLE_AUTODESK" = 'yes' ]; then
# Disable autologin
  [ -f /target/etc/gdm/gdm.conf ] && sed -i 's/^AutomaticLogin/#AutomaticLogin/' /target/etc/gdm/gdm.conf
  [ -f /target/etc/gdm3/daemon.conf ] && sed -i 's/^AutomaticLogin/#AutomaticLogin/' /target/etc/gdm3/daemon.conf
  [ -f /target/etc/lightdm/lightdm.conf ] && sed -i 's/^autologin/#autologin/g' /target/etc/lightdm/lightdm.conf
  check_rm /target/etc/default/kdm.d/live-autologin
  [ -f /target/etc/kde3/kdm/kdmrc ] && sed -i 's/^AutoLogin/#AutoLogin/g' /target/etc/kde3/kdm/kdmrc && sed -i 's/^AutoReLogin/#AutoReLogin/g' /target/etc/kde3/kdm/kdmrc
  [ -f /target/etc/kde4/kdm/kdmrc ] && sed -i 's/^AutoLogin/#AutoLogin/g' /target/etc/kde4/kdm/kdmrc && sed -i 's/^AutoReLogin/#AutoReLogin/g' /target/etc/kde4/kdm/kdmrc
  check_rm /target/etc/default/kdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/kdm/kdmrc ] && sed -i 's/^AutoLogin/#AutoLogin/g' /target/etc/trinity/kdm/kdmrc && sed -i 's/^AutoReLogin/#AutoReLogin/g' /target/etc/trinity/kdm/kdmrc
  check_rm /target/etc/default/tdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/tdm/tdmrc ] && sed -i 's/^AutoLogin/#AutoLogin/g' /target/etc/trinity/tdm/tdmrc && sed -i 's/^AutoReLogin/#AutoReLogin/g' /target/etc/trinity/tdm/tdmrc
  [ -f /target/etc/slim.conf ] && sed -i 's/^[ ]*default_user/#default_user/' && sed -i 's/^[ ]*auto_login.*$/#auto_login no/' /target/etc/slim.conf
  [ -f /target/etc/lxdm/lxdm.conf ] && sed -i 's/^autologin=/#autologin=/' /target/etc/lxdm/lxdm.conf
# No display manager
  check_rm /target/etc/profile.d/zz-live-config_xinit.sh
  disable_auto_console='yes'
else
# Keep autologin and update username in the display manager config.
  [ -f /target/etc/gdm/gdm.conf ] && sed -i "/AutomaticLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/gdm/gdm.conf
  [ -f /target/etc/gdm3/daemon.conf ] && sed -i "/AutomaticLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/gdm3/daemon.conf
  [ -f /target/etc/lightdm/lightdm.conf ] && sed -i "/autologin/s/=$OLD_USER_NAME/=$NEW_USER_NAME/" /target/etc/lightdm/lightdm.conf
  [ -f /target/etc/default/kdm.d/live-autologin ] && sed -i "s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/default/kdm.d/live-autologin
  [ -f /target/etc/kde3/kdm/kdmrc ] && sed -i "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde3/kdm/kdmrc && sed -i "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde3/kdm/kdmrc
  [ -f /target/etc/kde4/kdm/kdmrc ] && sed -i "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde4/kdm/kdmrc && sed -i "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/kde4/kdm/kdmrc
  [ -f /target/etc/default/kdm-trinity.d/live-autologin ] && sed -i "s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/default/kdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/kdm/kdmrc ] && sed -i "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/kdm/kdmrc && sed -i "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/kdm/kdmrc
  [ -f /target/etc/default/tdm-trinity.d/live-autologin ] && sed -i "s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/default/tdm-trinity.d/live-autologin
  [ -f /target/etc/trinity/tdm/tdmrc ] && sed -i "/AutoLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/tdm/tdmrc && sed -i "/AutoReLogin/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/trinity/tdm/tdmrc
  [ -f /target/etc/slim.conf ] && sed -i "/default_user/s/\s\+$OLD_USER_NAME/ $NEW_USER_NAME/" /target/etc/slim.conf
  [ -f /target/etc/lxdm/lxdm.conf ] && sed -i "/^autologin=/s/$OLD_USER_NAME/$NEW_USER_NAME/" /target/etc/lxdm/lxdm.conf
fi

# Disable console autologin
if [ "$disable_auto_console" = 'yes' ]; then
  if grep -q "respawn:/bin/login -f" /target/etc/inittab ; then
	mv /target/etc/inittab /target/etc/inittab."$(date +%Y%m%d_%H%M)"
	gen_inittab
  fi
else
  sed -i "/respawn:/s/$OLD_USER_NAME/$NEW_USER_NAME/g" /target/etc/inittab
fi

if [ "$additional_partitions" = 'yes' ]; then
  if ! [ -h /usr/lib/refractainstaller/post-install/move-dir-mount-gui.sh ]; then
	ln -s /usr/lib/refractainstaller/move-dir-mount-gui.sh /usr/lib/refractainstaller/post-install/move-dir-mount-gui.sh
  fi
else
  [ -h /usr/lib/refractainstaller/post-install/move-dir-mount.sh ] \
  && rm /usr/lib/refractainstaller/post-install/move-dir-mount.sh
fi

# █▀█ █▀▀ ▀█▀ █ █ █▀█ █▀▄ █ █
# █ █ █▀▀  █  █▄█ █ █ █▀▄ █▀▄
# ▀ ▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀ ▀ ▀ ▀ ▀
# generate a new /etc/network/interfaces file
net_interfaces="/target/etc/network/interfaces"
net_wpa="/target/etc/wpa_supplicant.conf"
generate_net_interfaces

# █ █ █▀█ █▀▀ ▀█▀ █▀█ █▀█ █▄█ █▀▀
# █▀█ █ █ ▀▀█  █  █ █ █▀█ █ █ █▀▀
# ▀ ▀ ▀▀▀ ▀▀▀  ▀  ▀ ▀ ▀ ▀ ▀ ▀ ▀▀▀
case "$install_source" in
   'bootstrap') if [ ! "$NEW_HOSTNAME" ]; then
                  NEW_HOSTNAME='hostname'
                fi
                chroot /target hostname "$NEW_HOSTNAME";;
  'from_iso'|*) if [ "$NEW_HOSTNAME" != "$(hostname)" ]; then
                  sed -i "s/$(hostname)/$NEW_HOSTNAME/" /target/etc/hostname
                  sed -i "s/ $(hostname) / $NEW_HOSTNAME /g" /target/etc/hosts
                fi;;
esac
# if [ -f "/target/etc/init.d/hostname.sh" ]; then /target/etc/init.d/hostname.sh start; fi

# █▀█ █▀▄ ▀▀█ █ █ █▀▀ ▀█▀   █▀▀ █▀▀ █▀▀ █▀▀
# █▀█ █ █   █ █ █ ▀▀█  █    █   █▀▀ █ █ ▀▀█
# ▀ ▀ ▀▀  ▀▀  ▀▀▀ ▀▀▀  ▀    ▀▀▀ ▀   ▀▀▀ ▀▀▀

# NEW_USER_NAME
if [ ! "$OLD_USER_NAME" = "$NEW_USER_NAME" ]; then

# symlinks in /root
  for LNK in $(find "/target/root" -type l); do
	if [ "$(readlink "$LNK" | grep "^/home/$OLD_USER_NAME/")" ]; then
	  NEWLNK="$(readlink "$LNK" | sed "s:/home/$OLD_USER_NAME/::;s:^:/home/$NEW_USER_NAME/:")"
	  ln -sfn "$NEWLNK" "$LNK"
	fi
  done

# symlinks in ~
  for LNK in $(find "/target/home/$NEW_USER_NAME" -type l); do
	if [ "$(readlink "$LNK" | grep "^/home/$OLD_USER_NAME/")" ]; then
	  NEWLNK="$(readlink "$LNK" | sed "s:/home/$OLD_USER_NAME/::;s:^:/home/$NEW_USER_NAME/:")"
	  ln -sfn "$NEWLNK" "$LNK"
	fi
  done

# username references in ~
#  for FL in $(grep -rl "/home/$OLD_USER_NAME/" "/target/home/$NEW_USER_NAME"); do
#	sed -i "s:/home/$OLD_USER_NAME/:/home/$NEW_USER_NAME/:g" "$FL"
#  done
fi

find /usr/share/applications | sort | sed 's:/usr/share/applications/::g;s:^:0 :g' > "/target/home/$NEW_USER_NAME/.cache/rofi3.druncache"
chroot /target chown "$NEW_USER_NAME":"$NEW_USER_NAME" "/target/home/$NEW_USER_NAME/.cache/rofi3.druncache"
chroot /target chmod 755 "/target/home/$NEW_USER_NAME/.cache/rofi3.druncache"

mkdir /target/root/.cache
chown root:root /target/root/.cache
touch /target/root/.cache/zshistory
chown root:root /target/root/.cache/zshistory

case "$install_source" in
  'bootstrap') cp -v "$0" "/target/home/$NEW_USER_NAME/";;
esac

# █▀▀ █   █▀▀ █▀█ █▀█ █ █ █▀█
# █   █   █▀▀ █▀█ █ █ █ █ █▀▀
# ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀ ▀ ▀ ▀▀▀ ▀
# copy error log to installation before calling cleanup function
cp "$LOG" /target/home/"$NEW_USER_NAME"/
chown 1000:1000 /target/home/"$NEW_USER_NAME"/"${LOG##*/}"
check_rm "$rsync_excludes"
# cleanup call moved to the end of installation_complete_event

# ▀█▀ █▀█ █▀▀ ▀█▀ █▀█ █   █   █▀█ ▀█▀ ▀█▀ █▀█ █▀█
#  █  █ █ ▀▀█  █  █▀█ █   █   █▀█  █   █  █ █ █ █
# ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀ ▀▀▀ ▀ ▀  ▀  ▀▀▀ ▀▀▀ ▀ ▀
# █▀▀ █▀█ █▄█ █▀█ █   █▀▀ ▀█▀ █▀▀
# █   █ █ █ █ █▀▀ █   █▀▀  █  █▀▀
# ▀▀▀ ▀▀▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀  ▀  ▀▀▀
installation_complete_event(){
if find /firmware* -maxdepth 0 >> /dev/null; then
  FIRMWARE_DIR="$(find /firmware* -maxdepth 0 -type d | tail -n1)"
  FIRMWARE_PLACEHOLDER="Install wifi drivers"
elif find /home/*/firmware* -maxdepth 0 >> /dev/null; then
  FIRMWARE_DIR="$(find /home/*/firmware* -maxdepth 0 -type d | tail -n1)"
  FIRMWARE_PLACEHOLDER="Install wifi drivers"
else
  unset FIRMWARE_PLACEHOLDER
fi

if [ "$FIRMWARE_PLACEHOLDER" ]; then
  FINAL_MENU="   > $FIRMWARE_PLACEHOLDER
   > Reboot
   > Exit"
else
  FINAL_MENU="   > Reboot
   > Exit"
fi

unset SELECTION
while [ -z "$SELECTION" ]; do
  LINE='15'
  FMSG=" Installation Complete!\\n
    username is ${c3}$NEW_USER_NAME${c_end}
    hostname is ${c3}$NEW_HOSTNAME${c_end}"
  if [ "$install_source" = 'bootstrap' ]; then
  FMSG="$FMSG\\n
  to install openrc and related packages
  reboot and run  ${c5}--debootstrap-second-stage${c_end}"
  fi
  TOPTEXT="$(text_box $text_box_opt "$FMSG")"
  SELECTION="$(awk_menu "$FINAL_MENU")"

  case "$SELECTION" in
    *'drivers') WIFI_DRIVERS="$(find "$FIRMWARE_DIR" -type f -iname "*.deb")"
                TOPTEXT="$(text_box $text_box_opt " Select a package:\\n")"
                DRIVER_SELECT="$(awk_menu "$WIFI_DRIVERS" "\\n < Back")"
                if [ ! "$DRIVER_SELECT" = "< Back" ] && [ -f "$DRIVER_SELECT" ]; then
                  LINE='4'
                  TOPTEXT="$(text_box $text_box_opt \
                          " You have selected $DRIVER_SELECT \\n Do you want to install it now?\\n")"
                  DRIVER_CONFIRM="$(awk_menu "   > Yes\\n   < No")"
                  case "$DRIVER_CONFIRM" in
                    '> Yes') if [ "$BASE_DISTRO_LIKE" = 'debian' ]; then
                               chroot /target/ dpkg -i "$DRIVER_SELECT"
                             fi;;
                     '< No') break;;
                  esac
                fi
                unset SELECTION;;
    '> Reboot') cleanup; /sbin/reboot;;
      '> Exit') cleanup; exit 0;;
             *) unset SELECTION;;
  esac
done
cleanup
}
installation_complete_event
} # end of installer

#beginning of snapshot script====================================================#
#    _____                        __          __     _____           _       __  
#   / ___/____  ____ _____  _____/ /_  ____  / /_   / ___/__________(_)___  / /_ 
#   \__ \/ __ \/ __ `/ __ \/ ___/ __ \/ __ \/ __/   \__ \/ ___/ ___/ / __ \/ __/ 
#  ___/ / / / / /_/ / /_/ (__  ) / / / /_/ / /_    ___/ / /__/ /  / / /_/ / /_   
# /____/_/ /_/\__,_/ .___/____/_/ /_/\____/\__/   /____/\___/_/  /_/ .___/\__/   
#                 /_/                                             /_/            
#
check_snapshot_dirs(){
# Create snapshot_dir and work_dir if necessary.
# Don't use /media/* for $snapshot_dir or $work_dir unless it is a mounted filesystem
snapdir_is_remote=$(echo ${snapshot_dir} | awk -F / '{ print "/" $2 "/" $3 }' | grep /media/)
workdir_is_remote=$(echo ${work_dir} | awk -F / '{ print "/" $2 "/" $3 }' | grep /media/)

if [ -n "$snapdir_is_remote" ] && grep -q "${snapdir_is_remote}" /proc/mounts; then
   printf "%s is mounted" "$snapshot_dir"
elif [ -n "$snapdir_is_remote" ]; then
   printf "Error...\\nThe selected snapshot directory cannot be accessed.\\nDo you need to mount it?\\n"; exit 1
fi

if [ -n "$workdir_is_remote" ] && grep -q "${workdir_is_remote}" /proc/mounts; then
   printf "%s is mounted\\n" "$work_dir"
elif [ -n "$workdir_is_remote" ]; then
   printf "Error...\\nThe selected work directory cannot be accessed.\\nDo you need to mount it?\\n"; exit 1
fi

# Check that snapshot_dir exists
check_mkdir "$snapshot_dir"
chmod -v 777 "$snapshot_dir"

# Clear work_dir if needed
if [ "$save_work" = 'no' ] && [ -d "$work_dir" ]; then
  printf "cleaning %s ..." "$work_dir"
  rm -vrf "$work_dir"
fi

# Create work_dir
check_mkdir "$work_dir"
check_mkdir "$work_dir/iso"
check_mkdir "$work_dir/iso/isolinux"
check_mkdir "$work_dir/iso/live"
check_mkdir "$work_dir/myfs"
}

#==============================================================================#
# Prepare for Snapshot
prepare_snapshot(){
# Fix root's path (for Buster/Beowulf and later)
PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
# Record errors in a logfile.
LOG="$LOGFILE_SNAPSHOT"
[ -e "$LOG" ] || touch "$LOG"
#exec 2>>"$ERROR_LOG"

kernel_image="$(awk '{ for(i=1; i<=NF; i++) { a=match($i,/BOOT_IMAGE=/); d=""
                         if(a) { gsub(/BOOT_IMAGE=/,d,$i); print $i } }
                     }' /proc/cmdline)"
kernel_image="${kernel_image##\.\.}"

if [ ! -f "$kernel_image" ]; then
  kernel_image="$(find / -maxdepth 2 -not \( -path /lost+found -prune \) -not \( -path /root -prune \) -iname "vmlinuz*" ! -name "*.old" | awk 'NR == 1 {print}')"
fi

initrd_image="$(awk '{ for(i=1; i<=NF; i++) { a=match($i,/initrd=/); d=""
                         if(a) { gsub(/initrd=/,d,$i); print $i } }
                     }' /proc/cmdline)"
initrd_image="${initrd_image##\.\.}"

if [ ! -f "$initrd_image" ]; then
  initrd_image="$(find / -maxdepth 2 -not \( -path /lost+found -prune \) -not \( -path /root -prune \) -iname "init*.img*" ! -name "*.old" | awk 'NR == 1 {print}')"
fi

case "$SQUASHFS_COMPRESSION" in
        'gzip') squash_comp='-comp gzip';;
         'lzo') squash_comp='-comp lzo';;
          'xz') squash_comp='-comp xz';;
  'xz-smaller') squash_comp='-comp xz -Xbcj x86';;
        'zstd') squash_comp='-comp zstd -Xcompression-level 20';;
             *) unset squash_comp;;
esac

cat > "$snapshot_excludes" <<EOF
- */.uuid
- /.swapfile
- /.swap
- /lost+found
- /boot/lost+found
- /root/.aptitude
- /root/.bash_history
- /root/.cache
- /root/.cdw
- /root/.disk-manager.conf
- /root/.fstab.log
- /root/.lesshst
- /root/.links2
- /root/.lnav
- /root/*/.log
- /root/.local/share/*
- /root/.nano_history
- /root/.synaptic
- /root/.thumbnails
- /root/.VirtualBox
- /root/.ICEauthority
- /root/.Xauthority
- /root/.ssh
- /target/boot
- /storage
- /home/*/Music
- /home/*/Media
- /home/*/Rec
- /home/*/Screenshots
- /home/*/.Trash*
- /home/*/.mozilla/*/Cache/*
- /home/*/.mozilla/*/urlclassifier3.sqlite
- /home/*/.mozilla/*/places.sqlite
- /home/*/.mozilla/*/cookies.sqlite
- /home/*/.mozilla/*/signons.sqlite
- /home/*/.mozilla/*/formhistory.sqlite
- /home/*/.mozilla/*/downloads.sqlite
- /home/*/.adobe
- /home/*/.aptitude
- /home/*/.bash_history
- /home/*/.bibletime/cache
- /home/*/.bibletime/indices
- /home/*/.bibletime/sessions
- /home/*/.cache
- /home/*/.cdw
- /home/*/.dbus
- /home/*/.din
- /home/*/.gksu*
- /home/*/.gvfs
- /home/*/.hardinfo
- /home/*/.ibam
- /home/*/.lesshst
- /home/*/.links2
- /home/*/.lnav
- /home/*/.log
- /home/*/.macromedia
- /home/*/.moc
- /home/*/.nano_history
- /home/*/.pulse*
- /home/*/.qpsrc
- /home/*/.recently-used
- /home/*/.recently-used.xbel
- /home/*/.local/XDG_RUNTIME_DIR/*
- /home/*/.local/share/Trash
- /home/*/.local/share/recently-used.xbel
- /home/*/.local/share/GottCode
- /home/*/.local/share/fonts/.uuid
- /home/*/.local/share/bibletime
- /home/*/.local/share/mc
- /home/*/.local/share/nano
- /home/*/.local/share/newsbeuter/*
- /home/*/.local/share/SpeedCrunch
- /home/*/.local/share/qalculate
- /home/*/.local/share/run/*
- /home/*/.local/tmp/*
- /home/*/.thumbnails
- /home/*/.vbox*
- /home/*/.VirtualBox
- /home/*/VirtualBox\ VMs
- /home/*/.w3m
- /home/*/.wine
- /home/*/.wget-hsts
- /home/*/.xsession-errors*
- /home/*/.ICEauthority
- /home/*/.Xauthority
- /home/*/.zcompdump*
- /home/*/.config/moc/cache
- /home/*/.config/moc/last_directory
- /home/*/.config/moc/pid
- /home/*/.config/moc/socket2
- /home/*/.config/pulse
- /home/*/.config/obs-studio/logs/
- /home/*/.config/obs-studio/profiler_data/
- /home/*/.config/smplayer/file_settings
- /dev/*
- /cdrom/*
- /media/*
- /swapfile
- /.swapfile
- /mnt/*
- /sys/*
- /proc/*
- /tmp/*
- /live
- /persistence.conf
- /boot/grub/grub.cfg
- /boot/grub/menu.lst
- /boot/grub/device.map
- /boot/*.bak
- /boot/*.old-dkms
- /etc/udev/rules.d/70-persistent-cd.rules
- /etc/udev/rules.d/70-persistent-net.rules
- /etc/fstab
- /etc/fstab.d/*
- /etc/mtab
- /etc/blkid.tab*
- /etc/apt/sources.list~
- /etc/crypttab
- /etc/initramfs-tools/conf.d/resume     # see remove-cryptroot and nocrypt.sh
- /etc/initramfs-tools/conf.d/cryptroot  # see remove-cryptroot and nocrypt.sh
- /etc/popularity-contest.conf
- /home/snapshot

# Added for newer version of live-config/live-boot in wheezy
# These are only relevant here if you create a snapshot while you're running a live-CD or live-usb.
- /lib/live/overlay
- /lib/live/image
- /lib/live/rootfs
- /lib/live/mount
- /run/*

# Added for symlink /lib
- /usr/lib/live/overlay
- /usr/lib/live/image
- /usr/lib/live/rootfs
- /usr/lib/live/mount

## Entries below are optional. They are included either for privacy or to reduce the size of the snapshot.
## If you have any large files or directories, you should exclude them from being copied by adding them to this list.

# Uncomment this to exclude everything in /var/log/
- /var/log/*
# As of version 9.2.0, current log files are truncated, and archived log files are excluded.
#- /var/log/*gz

# The next three lines exclude everything in /var/log except /var/log/clamav/ (or anything else beginning with "c")
# and /var/log/gdm (or anything beginning with "g"). If clamav log files are excluded, freshclam will give errors at boot.
#- /var/log/[a-b,A-Z]*
#- /var/log/[d-f]*
#- /var/log/[h-z]*
- /var/cache/apt/archives/partial
- /var/cache/apt/archives/*.deb
- /var/cache/apt/pkgcache.bin
- /var/cache/apt/srcpkgcache.bin
- /var/cache/apt/apt-file/*
- /var/cache/debconf/*~old
- /var/cache/fontconfig
- /var/cache/samba/*
- /var/lib/mlocate/mlocate.db
- /var/lib/apt/lists/*
- /var/lib/apt/*~
- /var/lib/apt/cdroms.list
- /var/lib/aptitude/*.old
- /var/lib/dhcp/*
- /var/lib/dpkg/*~old
- /var/spool/mail/*
- /var/mail/*
- /var/backups/*.gz
- /var/backups/*.bak
- /var/lib/dbus/machine-id
- /var/lib/live/config/*

- /usr/share/icons/*/icon-theme.cache

# You might want to comment these out if you're making a snapshot for your own personal use, not to be shared with others.
- /home/*/.gnupg
- /home/*/.ssh
- /home/*/.xchat2
- /home/*/.config/hexchat

# Exclude ssh_host_keys. New ones will be generated upon live boot.
# If you really want to clone your existing ssh host keys in your snapshot, comment out these two lines.
- /etc/ssh/ssh_host_*_key*
- /etc/ssh/ssh_host_key*

# To exclude all hidden files and directories in your home, uncomment the next line.
# You will lose custom desktop configs if you do.
#- /home/*/.[a-z,A-Z,0-9]*
EOF
}

#==============================================================================#
cpu_limit_func(){
if [ "$use_cpu_limit" = 'yes' ] && [ "$(nproc)" -gt '1' ]; then
  if [ "$cpu_limit_percent" -ge '50' ] && [ "$cpu_limit_percent" -le '100' ]; then
    cpu_limit_percent=$(( $cpu_limit_percent * $(nproc) ))
  fi
fi
}

#==============================================================================#
check_grub (){
case "$BASE_DISTRO_LIKE" in
  'debian') if [ "$(dpkg -l | awk '/^ii  grub-efi-amd64 /{printf "1"}')" = 1 ]; then
              grub_efi_check_result='good'
            else
              grub_efi_check_result='failed'
            fi
            if [ "$(dpkg -l | awk '/^ii  grub-pc-bin /{printf "1"}')" = 1 ]; then
              grub_check_result='good'
            else
              grub_check_result='failed'
            fi;;
    'arch') if [ "$(pacman -Ql grub)" ]; then
              grub_check_result='good'
            else
              grub_check_result='failed'
            fi;;
esac

printf "grub-check: %s\\n" "$grub_check_result" 2>&1 | log
printf "grub-efi-check: %s\\n" "$grub_efi_check_result" 2>&1 | log

if [ "$grub_efi_check_result" = 'failed' ] && [ "$make_efi" = 'yes' ] && [ "$force_efi" != 'yes' ]; then
  grub_message='Warning: grub-efi-amd64 is not installed. The snapshot may not be compatible with UEFI.
To disable this warning, set make_efi=no in the config section or set force_efi=yes for special use.'
fi
if [ "$force_efi" = 'yes' ]; then
  make_efi='yes'
else
  make_efi='no'
fi
printf "force_efi is %s\\n make_efi is %s" "$force_efi" "$make_efi" 2>&1 | log
}

#==============================================================================#
make_snapshot(){

check_dependencies
check_grub
mv -v "$EARLY_LOG" "$LOGFILE_SNAPSHOT"
check_snapshot_dirs
prepare_snapshot
generate_grub_template
LINE='1'
# Check for grub-efi

# Create a message to say whether the filesystem copy will be saved or not
if [ "$save_work" = 'yes' ]; then
	save_msg="The temporary copy of the filesystem will be saved at $work_dir/myfs.\\n"
else
	save_msg="The temporary copy of the filesystem will be created at $work_dir/myfs and removed when this program finishes.\\n"
fi

#==============================================================================#
# Check disk space on mounted /, /home, /media, /mnt, /tmp
check_disk_space(){
printf "checking disk space ...\\n"
disk_space=$(df -h -x tmpfs -x devtmpfs -x iso9660 \
           | awk '{ print "  " $2 "\t" $3 "\t" $4 "\t" $5 "  \t" $6 "\t\t\t" $1 }')
}

#==============================================================================#
# Check initrd for cryptroot, resume, cryptsetup
check_initrd(){
printf "checking initrd...\\n"
if $LSINITFS "$initrd_image" | grep -q conf.d/cryptroot; then
  remove_cryptroot='yes'
  cryptroot_msg="The snapshot initrd will be modified to allow booting the unencrypted snapshot."
elif $LSINITFS "$initrd_image" | grep -q cryptroot | grep -Ev 'scripts|crypttab|bin'; then
  remove_cryptroot='yes'
  cryptroot_msg="The snapshot initrd will be modified to allow booting the unencrypted snapshot."
elif [ "$($LSINITFS "$initrd_image" | grep cryptroot/crypttab)" ] && [ ! "$initrd_crypt" = "yes" ]; then
  remove_cryptroot='yes'
  cryptroot_msg="The snapshot initrd will be modified to allow booting the unencrypted snapshot."
fi
if $LSINITFS "$initrd_image" | grep -Eq 'conf.d/resume|conf.d/zz-resume-auto'; then
  remove_resume='yes'
  swap_msg="The snapshot initrd will be modified to allow booting without the host's swap partition."
fi
if [ "$initrd_crypt" = 'yes' ]; then
  if $LSINITFS "$initrd_image" | grep -q cryptsetup ; then
	crypt_msg="The host initrd already allows live-usb encrypted persistence. No change is needed."
	initrd_crypt='no'
  else
	crypt_msg="The host initrd will be modified to allow live-usb encrypted persistence.
A backup copy will be made at ${initrd_image}_pre-snapshot. (Does not apply to any re-run tasks.)"
  fi
fi
}

#==============================================================================#
clean_initrd(){
  if [ -L "$initrd_image" ]; then
    real_initrd_name="/$(readlink "$initrd_image")"
  else
    real_initrd_name="$initrd_image"
  fi
  extract_init "$real_initrd_name" "$tempdir/init_extract"
  check_rm "$tempdir/init_extract/conf/conf.d/cryptroot"
  check_rm "$tempdir/init_extract/cryptroot"
  check_rm "$tempdir/init_extract/conf/conf.d/resume"
  check_rm "$tempdir/init_extract/conf/conf.d/zz-resume-auto"
  check_rm "$tempdir/init_extract/cryptroot"
  rebuild_init "$tempdir/init_extract" \
               "${work_dir}/iso/live/${initrd_image##*/}" \
               "$(file "$real_initrd_name" | awk '{print $2}')"

  check_rm "$tempdir/init_extract"
  unset real_initrd_name
}

#==============================================================================#
report_space(){
# Show current settings and disk space
[ -f "/usr/bin/less" ] && pager="/usr/bin/less" || pager="/bin/more"
echo $"
 You will need plenty of free space. It is recommended that free space
 (Avail) in the partition that holds the work directory (probably \"/\")
 should be two times the total installed system size (Used). You can
 deduct the space taken up by previous snapshots and any saved copies of
 the system from the Used amount.
 ${grub_message}
 ${dosfstools_message}
 $save_msg
 * The snapshot directory is currently set to $snapshot_dir
 $tmp_warning
 Turn off NUM LOCK for some laptops.

 ${crypt_msg}
 ${cryptroot_msg}
 ${swap_msg}

 Current disk usage:
 $disk_space

 To proceed, press q.
 To exit, press q and then press ctrl-c
" | "$pager"
}

#==============================================================================#
housekeeping(){
# Use the login name set in the config file. If not set, use the primary user's name.
# If the name is not "user" then add boot option. Also use the same username for cleaning geany history.
if [ -n "$username" ]; then
  username_opt="username=$username"
else
  username=$(awk -F":" '/home/ {print $1}' /etc/passwd | awk 'NR == 1 {print}')
  [ "$username" != user ] && username_opt="username=$username"
fi

if [ ! -e "$kernel_image" ]; then
  printf "\\nWarning: Kernel is missing.\\n"
  exit 1
fi

if [ "$BASE_DISTRO_LIKE" = 'debian' ] && [ ! -e "$initrd_image" ]; then
  printf "\\nWarning: Init file is missing.\\n"
  exit 1
fi

# update the mlocate database
if [ "$update_mlocate" = 'yes' ]; then
  if command -v updatedb ; then
    printf "\\nRunning updatedb...\\n"
    updatedb
  fi
fi
}

#==============================================================================#
copy_filesystem(){
seperator
printf "# rsync" 2>&1 | log

if [ -f /usr/bin/cpulimit ] && [ "$use_cpu_limit" = 'yes' ]; then
  cpulimit -e rsync -l "$cpu_limit_percent" &
  pid="$!"
fi

# do NOT quote these curly brackets here:
rsync -avX / "$work_dir/myfs/" ${rsync_option1} ${rsync_option2} ${rsync_option3} \
  --exclude="$work_dir" --exclude="$snapshot_dir" --exclude="$efi_work" \
  --exclude-from="$tempdir/snapshot_exclude.list"
[ -n "$pid" ] && kill "$pid"
}

#==============================================================================#
edit_system(){
# Truncate logs, remove archived logs.
for oldlog in $(find /var/log -name "*gz"); do
  check_rm "$oldlog"
done
find myfs/var/log/ -type f -exec truncate -s 0 {} \;

#==============================================================================#
# Allow all fixed drives to be mounted with pmount
if [ "$pmount_fixed" = "yes" ] && [ -f "$work_dir"/myfs/etc/pmount.allow ]; then
  sed -i 's:#/dev/sd\[a-z\]:/dev/sd\[a-z\]:' "$work_dir"/myfs/etc/pmount.allow
fi

#==============================================================================#
# Clear list of recently used files in geany for primary user.
if  [ -f "$work_dir"/myfs/home/"$username"/.config/geany/geany.conf ]; then
  if [ "$clear_geany" = "yes" ]; then
    sed -i 's/recent_files=.*;/recent_files=/' "$work_dir"/myfs/home/"$username"/.config/geany/geany.conf
  fi
fi

#==============================================================================#
# Enable or disable password login through ssh for users (not root)
# Remove obsolete live-config file
check_rm "$work_dir"/myfs/lib/live/config/1161-openssh-server

sshd_conf="$work_dir/myfs/etc/ssh/sshd_config"
if [ -f "$sshd_conf" ]; then
  sed -i 's/PermitRootLogin yes/PermitRootLogin prohibit-password/' "$sshd_conf"
  if [ "$ssh_pass" = 'yes' ]; then
	sed -i 's|.*PasswordAuthentication.*no|PasswordAuthentication yes|' "$sshd_conf"
  elif [ "$ssh_pass" = 'no' ]; then
	sed -i 's|.*PasswordAuthentication.*yes|PasswordAuthentication no|' "$sshd_conf"
  fi
fi

# /etc/fstab should exist, even if it's empty,to prevent error messages at boot
touch "$work_dir"/myfs/etc/fstab

# Blank out systemd machine id. If it does not exist, systemd-journald
# will fail, but if it exists and is empty, systemd will automatically
# set up a new unique ID.
if [ -e "$work_dir"/myfs/etc/machine-id ]; then
  rm -f "$work_dir"/myfs/etc/machine-id
  : > "$work_dir"/myfs/etc/machine-id
fi

# add some basic files to /dev
mknod -m 622 "$work_dir"/myfs/dev/console c 5 1
mknod -m 666 "$work_dir"/myfs/dev/null c 1 3
mknod -m 666 "$work_dir"/myfs/dev/zero c 1 5
mknod -m 666 "$work_dir"/myfs/dev/ptmx c 5 2
mknod -m 666 "$work_dir"/myfs/dev/tty c 5 0
mknod -m 444 "$work_dir"/myfs/dev/random c 1 8
mknod -m 444 "$work_dir"/myfs/dev/urandom c 1 9
#chown -v root:tty "$work_dir"/myfs/dev/{console,ptmx,tty}
chown -v root:tty "$work_dir"/myfs/dev/console
chown -v root:tty "$work_dir"/myfs/dev/ptmx
chown -v root:tty "$work_dir"/myfs/dev/tty

ln -sv /proc/self/fd "$work_dir"/myfs/dev/fd
ln -sv /proc/self/fd/0 "$work_dir"/myfs/dev/stdin
ln -sv /proc/self/fd/1 "$work_dir"/myfs/dev/stdout
ln -sv /proc/self/fd/2 "$work_dir"/myfs/dev/stderr
ln -sv /proc/kcore "$work_dir"/myfs/dev/core
mkdir -v "$work_dir"/myfs/dev/shm
mkdir -v "$work_dir"/myfs/dev/pts
chmod 1777 "$work_dir"/myfs/dev/shm

# Clear configs from /etc/network/interfaces, wicd and NetworkManager
# and netman, so they aren't stealthily included in the snapshot.
net_interfaces="$work_dir/myfs/etc/network/interfaces"
net_wpa="$work_dir/myfs/etc/wpa_supplicant.conf"

if [ "$delete_wifi_passwords" = 'yes' ]; then
  rm -vf "$net_wpa"
  rm -vf "$work_dir"/myfs/var/lib/wicd/configurations/*
  rm -vf "$work_dir"/myfs/etc/wicd/wireless-settings.conf
  rm -vf "$work_dir"/myfs/etc/NetworkManager/system-connections/*
  rm -vf "$work_dir"/myfs/etc/network/wifi/*
fi

# generate a new /etc/network/interfaces file
generate_net_interfaces
}

#==============================================================================#
# Prepare initrd to use encryption
# This is only going to work if the latest kernel version is running.
# (i.e. the one linked from /initrd.img)
# Add '-k all' or specify the initrd to use???
prepare_initrd_crypt(){
cp "$initrd_image" "${initrd_image}_pre-snapshot"
sed -i 's/.*CRYPTSETUP=.*/CRYPTSETUP=y/' /etc/cryptsetup-initramfs/conf-hook
if [ -f /usr/sbin/update-initramfs.orig.initramfs-tools ]; then
  /usr/sbin/update-initramfs.orig.initramfs-tools -u
else
  /usr/sbin/update-initramfs -u
fi
}

#==============================================================================#
copy_isolinux(){
check_isolinuxbin
printf "installing isolinux\\n" 2>&1 | log
check_mkdir "$work_dir/iso/isolinux"
rsync -av "$isolinuxbin" "$work_dir"/iso/isolinux

if [ -f /usr/lib/syslinux/modules/bios/vesamenu.c32 ]; then
  rsync -av /usr/lib/syslinux/modules/bios/vesamenu.c32 "$work_dir"/iso/isolinux
elif [ -f /usr/lib/syslinux/bios/vesamenu.c32 ]; then
  rsync -av /usr/lib/syslinux/bios/vesamenu.c32 "$work_dir"/iso/isolinux
fi

if [ -f /usr/lib/syslinux/modules/bios/chain.c32 ]; then
  rsync -av /usr/lib/syslinux/modules/bios/chain.c32 "$work_dir"/iso/isolinux
elif [ -f /usr/lib/syslinux/bios/chain.c32 ]; then
  rsync -av /usr/lib/syslinux/bios/chain.c32 "$work_dir"/iso/isolinux
fi

if [ -f /usr/lib/syslinux/modules/bios/ldlinux.c32 ]; then
  rsync -av /usr/lib/syslinux/modules/bios/ldlinux.c32 "$work_dir"/iso/isolinux
elif [ -f /usr/lib/syslinux/bios/ldlinux.c32 ]; then
  rsync -av /usr/lib/syslinux/bios/ldlinux.c32 "$work_dir"/iso/isolinux
fi

if [ -f /usr/lib/syslinux/modules/bios/libcom32.c32 ]; then
  rsync -av /usr/lib/syslinux/modules/bios/libcom32.c32 "$work_dir"/iso/isolinux
elif [ -f /usr/lib/syslinux/bios/libcom32.c32 ]; then
  rsync -av /usr/lib/syslinux/bios/libcom32.c32 "$work_dir"/iso/isolinux
fi

if [ -f /usr/lib/syslinux/modules/bios/libutil.c32 ]; then
  rsync -av /usr/lib/syslinux/modules/bios/libutil.c32 "$work_dir"/iso/isolinux
elif [ -f /usr/lib/syslinux/bios/libutil.c32 ]; then
  rsync -av /usr/lib/syslinux/bios/libutil.c32 "$work_dir"/iso/isolinux
fi

if [ -f /usr/lib/refractasnapshot/iso/isolinux/boot.cat ]; then
  rsync -av /usr/lib/refractasnapshot/iso/isolinux/boot.cat "$work_dir"/iso/isolinux
fi

# Add Refracta-specific boot help files
#if [ "$refracta_boot_help" = 'yes' ] && [ -d /usr/lib/refractasnapshot/boot_help ]; then
#  cp -a /usr/lib/refractasnapshot/boot_help/* "$work_dir"/iso/isolinux
#fi

[ "$DISABLE_IPV6" = "yes" ] && ipv6_opt="ipv6.disable=1" || ipv6_opt=""
KEYBOARD_LAYOUT="$(awk -F '"' '/XKBLAYOUT/ {print $2}' /etc/default/keyboard)"
if [ -n "$KEYBOARD_LAYOUT" ]; then
  KBD_OPT="keyboard-layout=$KEYBOARD_LAYOUT"
fi
LOCALE_LANG="$(awk -F '=' '$1~/^LANG$/ {print $2}' /etc/default/locale)"
if [ -n "$LOCALE_LANG" ]; then
  LOCALE_OPT="locales=$LOCALE_LANG"
fi
}

extra_steps(){
check_base_distro
#==============================================================================#
# extra steps for debian based systems
if [ "$BASE_DISTRO_LIKE" = 'debian' ]; then
  toram_opt='toram'

#==============================================================================#
# some extra steps for arch / artix
elif [ "$BASE_DISTRO_LIKE" = 'arch' ]; then
  dist_opt='archisobasedir=live archisolabel=liveiso arch=x86_64'
  toram_opt='copytoram'
  gen_arch_initcpio_hooks

  case "$BASE_DISTRO" in
        'artix') dist_opt="archisobasedir=live archisolabel=liveiso arch=x86_64 label=liveiso"
                 HOOKS="base udev artix_shutdown archiso archiso_loop_mnt artix_kms modconf block filesystems keyboard keymap";;
    'hyperbola') HOOKS="base udev shutdown archiso archiso_loop_mnt hyperiso_kms modconf block filesystems keyboard keymap";;
       'arch'|*) HOOKS="base udev shutdown archiso archiso_loop_mnt arch_kms modconf block filesystems keyboard keymap";;
  esac

  printf "generating %s/mkinitcpio.conf\\n" "$work_dir" 2>&1 | log
  cat > "$work_dir"/mkinitcpio.conf <<EOF
MODULES="loop dm-snapshot"
HOOKS="$HOOKS"
COMPRESSION="zstd"
COMPRESSION_OPTIONS="--ultra -22"
EOF
  cat "$work_dir"/mkinitcpio.conf | sed s'/^/# /g' 2>&1 | log
  seperator
  printf "# mkinitcpio\\n" 2>&1 | log
  initrd_image="$work_dir"/initramfs.img
  mkinitcpio -k "$kernel_image" -c "$work_dir"/mkinitcpio.conf \
             --skiphooks autodetect -g "$initrd_image" 2>&1 | log

#==============================================================================#
# some extra steps for hyperbola
elif [ "$BASE_DISTRO_LIKE" = 'hyperbola' ]; then
  dist_opt="hyperisobasedir=live hyperisolabel=liveiso arch=x86_64"
  toram_opt="copytoram"
  cat > "$work_dir"/iso/live/aitab <<EOF
# <img>         <mnt>                 <arch>   <sfs_comp>  <fs_type>  <fs_size>
filesystem      /                     x86_64   xz          ext4       100%
EOF
  cat > "$work_dir"/mkinitcpio.conf <<EOF
MODULES="loop"
HOOKS="base udev memdisk hyperiso hyperiso_loop_mnt hyperiso_kms block pcmcia filesystems keyboard"
COMPRESSION="zstd"
COMPRESSION_OPTIONS=(--ultra -22)
EOF

  seperator
  printf "# mkinitcpio\\n" 2>&1 | log
  cat "$work_dir"/mkinitcpio.conf | sed s'/^/# /g' 2>&1 | log
  printf "\\n" 2>&1 | log
  initrd_image="$work_dir"/initramfs.img

  mkinitcpio -v -r "$work_dir"/myfs -k "$kernel_image" \
    -c "$work_dir"/mkinitcpio.conf -g "$initrd_image" 2>&1 | log

fi

#==============================================================================#
# find the correct filenames for kernel and initrafms
KERNEL_NAME=$(echo "$kernel_image" | awk -F / '{print $(NF)}')
INITRAMFS_NAME=$(echo "$initrd_image" | awk -F / '{print $(NF)}')

if [ "$username_opt" = 'username=' ]; then
  unset username_opt
fi

seperator
# generate /isolinux/live.cfg
printf "generating %s/iso/isolinux/live.cfg\\n" "$work_dir" 2>&1 | log
cat > "$work_dir"/iso/isolinux/live.cfg <<EOF

label live-de
	menu label ${DISTRO} (German)
	kernel /live/$KERNEL_NAME
	append initrd=/live/$INITRAMFS_NAME boot=live keyboard-layout=de locales=en_US.UTF-8 ${ifnames_opt} ${netconfig_opt} ${username_opt} ${dist_opt} ${ipv6_opt} modprobe.blacklist=pcspkr

label live-en
	menu label ${DISTRO} (English)
	kernel /live/$KERNEL_NAME
	append initrd=/live/$INITRAMFS_NAME boot=live keyboard-layout=us locales=en_US.UTF-8 ${ifnames_opt} ${netconfig_opt} ${username_opt} ${dist_opt} ${ipv6_opt} modprobe.blacklist=pcspkr

label lang
	menu label Other language (TAB to edit)
	kernel /live/$KERNEL_NAME
	append initrd=/live/$INITRAMFS_NAME boot=live keyboard-layout=us locales=en_US.UTF-8 ${ifnames_opt} ${netconfig_opt} ${username_opt} ${dist_opt} ${LOCALE_OPT} ${KBD_OPT} ${ipv6_opt} modprobe.blacklist=pcspkr

label toram
	menu label ${DISTRO} (to RAM)
	kernel /live/$KERNEL_NAME
	append initrd=/live/$INITRAMFS_NAME boot=live ${toram_opt} ${ifnames_opt} ${netconfig_opt} ${username_opt} ${dist_opt} ${ipv6_opt} modprobe.blacklist=pcspkr

label failsafe
	menu label ${DISTRO} (failsafe)
	kernel /live/$KERNEL_NAME noapic noapm nodma nomce nolapic nosmp forcepae nomodeset vga=normal ${dist_opt} ${ifnames_opt} ${netconfig_opt} ${username_opt}
	append initrd=/live/$INITRAMFS_NAME boot=live

label memtest
	menu label Memory test
	kernel /live/memtest

label chain.c32 hd0,0
	menu label Boot hard disk
	chain.c32 hd0,0

label harddisk
	menu label Boot hard disk (old way)
	localboot 0x80
EOF

cat "$work_dir"/iso/isolinux/live.cfg | sed 's/^/# /g' 2>&1 | log

# generate /isolinux/menu.cfg
seperator
printf "generating %s/iso/isolinux/menu.cfg\\n" "$work_dir" 2>&1 | log
cat > "$work_dir"/iso/isolinux/menu.cfg <<EOF
menu hshift 6
menu width 64

menu title Live Media
include stdmenu.cfg
include live.cfg
label help
	menu label Help
	config prompt.cfg
EOF

# generate /isolinux/prompt.cfg
printf "generating %s/iso/isolinux/prompt.cfg\\n" "$work_dir" 2>&1 | log
cat > "$work_dir"/iso/isolinux/prompt.cfg <<EOF
prompt 1
display f1.txt
timeout 0
include menu.cfg
include exithelp.cfg

f1 f1.txt
f2 f2.txt
f3 f3.txt
f4 f4.txt
f5 f5.txt
f6 f6.txt
f7 f7.txt
f8 f8.txt
f9 f9.txt
EOF

# generate /isolinux/stdmenu.cfg
printf "generating %s/iso/isolinux/stdmenu.cfg\\n" "$work_dir" 2>&1 | log
cat > "$work_dir"/iso/isolinux/stdmenu.cfg <<EOF
menu background /isolinux/splash.png
menu color title	* #FFFFFFFF *
menu color border	* #00000000 #00000000 none
menu color sel		* #ffffffff #686373 *
menu color hotsel	1;7;37;40 #dad9dc #686373 *
menu color tabmsg	* #dad9dc #00000000 *
menu color cmdline 0 #dad9dc #00000000
menu color help		37;40 #ffdddd00 #00000000 none
menu vshift 8
menu rows 12
#menu helpmsgrow 15
#menu cmdlinerow 25
#menu timeoutrow 26
#menu tabmsgrow 14
menu tabmsg Press ENTER to boot or TAB to edit a menu entry
EOF

# generate /isolinux/isolinux.cfg
printf "generating %s/iso/isolinux/isolinux.cfg\\n" "$work_dir" 2>&1 | log
cat > "$work_dir"/iso/isolinux/isolinux.cfg <<EOF
include menu.cfg
default /isolinux/vesamenu.c32
prompt 0
timeout 200
EOF

# generate /isolinux/exithelp.cfg
printf "generating %s/iso/isolinux/exithelp.cfg\\n" "$work_dir" 2>&1 | log
cat > "$work_dir"/iso/isolinux/exithelp.cfg <<EOF
label menu
	kernel /isolinux/vesamenu.c32
	config isolinux.cfg
EOF

# copy memtest
if [ -f /boot/memtest86+x64.bin ]; then
  cp -v /boot/memtest86+x64.bin "${work_dir:?}/iso/live/memtest"
fi
}

#==============================================================================#
# Let iso/, vmlinuz and initrd.img get copied, even if work_dir was saved, in case they have changed.
copy_kernel(){
seperator
cp -v "$kernel_image" "$work_dir"/iso/live/ || \
  { printf "Error: could not copy %s" "$kernel_image"; exit 1; }
printf "copied %s to %s\\n" "$kernel_image" "$work_dir/iso/live/" 2>&1 | log

cp -v "$initrd_image" "$work_dir"/iso/live/ || \
  { printf "Error: could not copy init file" 2>&1 | log; exit 1; }
printf "copied %s to %s\\n" "$initrd_image" "$work_dir/iso/live/" 2>&1 | log
}


#==============================================================================#
get_filename(){
# Need to define $filename here (moved up from genisoimage)
# and use it as directory name to identify the build on the cdrom.
# and put package list inside that directory
if [ "$stamp" = 'datetime' ]; then
  # use this variable so iso and sha256 have same time stamp
  filename="$snapshot_basename"-$(date +%Y%m%d_%H%M).iso
elif [ -z "$stamp" ]; then
  n=1
  while [ -f "$snapshot_dir"/snapshot$n.iso ]; do n="$((n+1))"; done
  filename="$snapshot_basename$n.iso"
else
  filename="$snapshot_basename"-"$stamp".iso
fi
}

#==============================================================================#
# create /boot and /efi for uefi.
mkefi(){
uefi_opt="-eltorito-alt-boot -e boot/grub/efiboot.img -isohybrid-gpt-basdat -no-emul-boot"
#################################

# for initial grub.cfg
mkdir -vp "$tempdir"/boot/grub

cat > "$tempdir"/boot/grub/grub.cfg <<EOF
search --file --set=root /isolinux/isolinux.cfg
set prefix=(\$root)/boot/grub
source \$prefix/x86_64-efi/grub.cfg
EOF

[ ! -d "$efi_work" ] && check_mkdir "$efi_work"
# start with empty directories.
check_rm "${efi_work:?}/boot"
check_rm "${efi_work:?}/efi"
check_mkdir "$efi_work/boot/grub/x86_64-efi"
check_mkdir "$efi_work/efi/boot"
check_cd "$efi_work"

# copy splash
if [ -f "$work_dir"/iso/isolinux/splash.png ]; then
  cp "$work_dir"/iso/isolinux/splash.png boot/grub/splash.png
fi

# second grub.cfg file
for i in $(ls /usr/lib/grub/x86_64-efi|grep part_|grep \.mod|sed 's/.mod//'); do
  echo "insmod $i" >> boot/grub/x86_64-efi/grub.cfg
done

# Additional modules so we don't boot in blind mode. I don't know which ones are really needed.
for i in efi_gop efi_uga ieee1275_fb vbe vga video_bochs video_cirrus jpeg png gfxterm ; do
  echo "insmod $i" >> boot/grub/x86_64-efi/grub.cfg
done

echo "source /boot/grub/grub.cfg" >> boot/grub/x86_64-efi/grub.cfg

check_cd "$tempdir"
  # make a tarred "memdisk" to embed in the grub image
  tar -cvf memdisk boot
  # make the grub image
  grub-mkimage -O "x86_64-efi" -m "memdisk" -o "bootx64.efi" -p '(memdisk)/boot/grub' search iso9660 configfile normal memdisk tar cat part_msdos part_gpt fat ext2 ntfs ntfscomp hfsplus chain boot linux
  check_cd "$efi_work"
	# copy the grub image to efi/boot (to go later in the device's root)
	cp "$tempdir"/bootx64.efi efi/boot

	## Do the boot image "boot/grub/efiboot.img"
	dd if=/dev/zero of=boot/grub/efiboot.img bs=1K count=1440
#	/sbin/mkdosfs -F 12 boot/grub/efiboot.img
	mkdosfs -F 12 boot/grub/efiboot.img
	mkdir img-mnt
	mount -o loop boot/grub/efiboot.img img-mnt
	mkdir -p img-mnt/efi/boot
	cp "$tempdir"/bootx64.efi img-mnt/efi/boot/

	# copy modules and font
	cp /usr/lib/grub/x86_64-efi/* boot/grub/x86_64-efi/

	# if this doesn't work try another font from the same place (grub's default, unicode.pf2, is much larger)
	# Either of these will work, and they look the same to me. Unicode seems to work with qemu. -fsr
#	cp /usr/share/grub/ascii.pf2 boot/grub/font.pf2
	cp /usr/share/grub/unicode.pf2 boot/grub/font.pf2

	# doesn't need to be root-owned
	chown -R 1000:1000 "$(pwd)" 2>/dev/null

	# Cleanup efi temps
	umount img-mnt
	check_rmdir img-mnt
  check_cd "$work_dir"

# Copy efi files to iso
rsync -avx "$efi_work"/boot "$work_dir"/iso/
rsync -avx "$efi_work"/efi  "$work_dir"/iso/

# Do the main grub.cfg (which gets loaded last):
cp "$grub_template" "$work_dir"/iso/boot/grub/grub.cfg
}

#==============================================================================#
set_boot_options(){
# modify the boot menu
livecfg_file="${work_dir}/iso/isolinux/${livecfg}"
if [ -f "$livecfg_file" ]; then
  sed -i "s:\${DISTRO}:$DISTRO:g" "$livecfg_file"
  sed -i "s:\${netconfig_opt}:$netconfig_opt:g" "$livecfg_file"
  sed -i "s:\${ifnames_opt}:$ifnames_opt:g" "$livecfg_file"
  sed -i "s:\${username_opt}:$username_opt:g" "$livecfg_file"
fi

if [ "$make_efi" = 'yes' ]; then
  sed -i "s:\${DISTRO}:$DISTRO:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:\${netconfig_opt}:$netconfig_opt:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:\${username_opt}:$username_opt:g" "$work_dir"/iso/boot/grub/grub.cfg
  sed -i "s:\${ifnames_opt}:$ifnames_opt:g" "$work_dir"/iso/boot/grub/grub.cfg
fi
}

#==============================================================================#
edit_boot_menus(){
if [ "$edit_boot_menu" = 'yes' ]; then
  printf "\\nYou may now go to another virtual console to edit any files in\\n"
  printf "the work directory, or hit ENTER and edit the boot menu.\\n"
  read -r ans
  "$TEXT_EDITOR" "$livecfg_file"
  [ "$make_efi" = "yes" ] && "$TEXT_EDITOR" "$work_dir"/iso/boot/grub/grub.cfg
fi
}

#==============================================================================#
squash_filesystem(){
if [ "$WAIT_OPT" = 'yes' ]; then
  unset READY
  while [ "$READY" != '1' ]; do
    TOPTEXT="$(text_box $text_box_opt "Now is your chance to modify $snapshot_dir")"
    PAUSE_MENU="$(awk_menu " > edit new /etc/network/interfaces
 > edit new /etc/rc.local
 > start compressing")"
    unset TOPTEXT
    case "$PAUSE_MENU" in
      '> edit new /etc/network/interfaces') $TEXT_EDITOR "$work_dir"/myfs/etc/network/interfaces;;
      '> edit new /etc/rc.local') $TEXT_EDITOR "$work_dir"/myfs/etc/rc.local;;
      '> start compressing') READY=1;;
    esac
  done
fi

if [ "$use_cpu_limit" = 'yes' ]; then
  cpulimit -e mksquashfs -l "$cpu_limit_percent" &
  pid="$!"
fi

if [ "$BASE_DISTRO_LIKE" = 'hyperbola' ]; then
  printf "\\nCreating container file...\\n" | log
  FS_SIZE="$(( $(du "$work_dir/myfs" | awk 'END{print $1}') + 1024 ))"
  dd if=/dev/zero of="$work_dir/filesystem.fs" bs=1024 count="$FS_SIZE" || space_error
  mkfs.ext4 -L "newroot" -b 1024 -d "$work_dir/myfs" -J size=1 "$work_dir/filesystem.fs"
  check_mkdir "$work_dir/iso/live/x86_64"
  squash_in="$work_dir/filesystem.fs"
  squash_out="live/x86_64/filesystem.fs.sfs"

elif [ "$BASE_DISTRO_LIKE" = 'arch' ]; then
  check_mkdir "$work_dir/iso/live/x86_64"
  squash_in="$work_dir/myfs"
  squash_out="live/x86_64/airootfs.sfs"

else
  squash_in="$work_dir/myfs"
  squash_out="live/filesystem.squashfs"
fi

seperator
printf "# mksquashfs\n" 2>&1 | log
printf "\\nSquashing the filesystem...\\n"

mksquashfs $squash_in $work_dir/iso/$squash_out ${squash_comp} -noappend -info -progress || space_error

if [ "$use_cpu_limit" = 'yes' ] && [ -n "$pid" ]; then
  kill "$pid"
fi

if [ "$save_work" = 'no' ] && [ -d "$work_dir/myfs" ]; then
  rm -vrf "$work_dir/myfs"
fi
}

#==============================================================================#
# use xorriso to build iso file
make_iso_fs(){
printf "\\nCreating CD/DVD image file...\\n"

if [ "$make_isohybrid" = "yes" ]; then
  rsync -av "$isohdpfxbin" "$work_dir"/iso/isolinux
  isohybrid_opt="-isohybrid-mbr $isohdpfxbin"
else
  unset isohybrid_opt
fi

[ -n "$volid" ] || volid='liveiso'
seperator
printf "# xorriso\\n" 2>&1 | log
xorriso -as mkisofs -r -J -joliet-long -l -iso-level 3 ${isohybrid_opt} \
  -partition_offset 16 -V "$volid" \
  -b isolinux/isolinux.bin -c isolinux/boot.cat -no-emul-boot \
  -boot-load-size 4 -boot-info-table ${uefi_opt} \
  -o "$snapshot_dir"/"$filename" "$work_dir"/iso/ | log || check_exit
}

#==============================================================================#
add_grub_pkgs(){
# arch apparently only has one grub package
if [ "$BASE_DISTRO_LIKE" = 'debian' ]; then
# search grub packages in $grub_package_dir
  check_mkdir "$work_dir/myfs/${grub_package_dir##/}"
  GRUB_PKGS='grub-pc grub-pc-bin grub-common grub2-common grub-efi-amd64 grub-efi-amd64-bin'
  unset GRUB_NEED
  for pkg in "$GRUB_PKGS"; do
    if find "${grub_package_dir}/" -maxdepth 1 -iname "$pkg_*.deb"; then
      cp -v "$pkg" "$work_dir/myfs/${grub_package_dir##/}"
    else
      GRUB_NEED="$GRUB_NEED $pkg"
    fi
  done
# download missing packages to $grub_package_dir
  pushd "$work_dir/myfs/${grub_package_dir##/}"
    awk -v l="$GRUB_NEED" 'BEGIN { printf "\033\13332m"
                                 cmd = "apt download "l
                                 while ( ( cmd | getline ans ) > 0 ) {
                                   printf "%s\n",ans
                                 }
                                 close(cmd);
                                 exit
                                }
                            END{printf "\033\1330m"}'

  popd
fi
}

#==============================================================================#
check_snapshot_hostname(){
  if [ "$(cat "$work_dir"/myfs/etc/hostname)" != "$snapshot_hostname" ]; then
           chroot "$work_dir"/myfs hostname "$snapshot_hostname"
           echo "$snapshot_hostname" >"$work_dir"/myfs/etc/hostname
  fi
}

#==============================================================================#
add_extras(){
check_cd "$snapshot_dir"
  [ "$make_sha256sum" = 'yes' ] && sha256sum "$filename" > "$filename".sha256
  [ "$make_pkglist" = 'yes' ] && dpkg -l > "$filename".pkglist
check_cd "$work_dir"
}

#==============================================================================#
cleanup_snapshot(){
if [ "$save_work" = "no" ]; then
  printf "Cleaning...\\n"; cd /
  if [ -d "$work_dir" ]; then
    rm -rdf "$work_dir"
  fi
fi
}

#==============================================================================#
final_message(){
exec 2>&1
final_iso="$snapshot_dir/$filename"
printf "\\nComplete!\\n" | log
printf "%s\\n" "$final_iso" | log
snapshot_size="$(du -h "$final_iso" | awk '{print $1}' | sed 's/G/ GB/')"
snapshot_size_real="$(du -b "$final_iso" | awk '{print $1}')"
awk '{print $1}' "$final_iso".sha256
while [ "$FINAL_MSG" != '> Exit' ]; do
  LINE='9'
  TOPTEXT="$(text_box $text_box_opt " All finished!
 File   = $final_iso
 Size   = $snapshot_size  ($snapshot_size_real bytes)
 Sha256 = $(awk '{print $1}' "$final_iso".sha256)")"
  FINAL_MSG="$(awk_menu "   > test ISO in Virtual Machine
   > burn ISO to USB Device
   > add ISO to multiboot usb
   > Exit")"
  case "$FINAL_MSG" in
    '> test ISO in Virtual Machine')
    qemu_test "$final_iso";;
    '> burn ISO to USB Device')
      WARNING="${c1}WARNING: ALL DATA ON THIS DEVICE WILL BE LOST${c_end}"
      select_usb
      [ ! -f "$final_iso" ] && { echo "Error, ISO file not found."; exit 1; }
      if [ -f "$final_iso" ] && [ -b "$TARGET_DEVICE" ]; then
        printf "\\n SOURCE_FILE=\\033[1;33m %s %s\\033[0m \\n\\n TARGET_DEVICE=\\033[1;33m%s\\033[0m\\n" "$final_iso" "$(du -h "$final_iso"| awk '{print $1}')" "$TARGET_DEVICE"
        lsblk "$TARGET_DEVICE" -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL | awk 'NR == 1 {print}'
        printf "\\033[1;33m %s\\033[0m\\n" "$(lsblk "$TARGET_DEVICE" -S -o NAME,SIZE,MODEL,LABEL,FSTYPE,SERIAL|tail -n 1)"
        printf "\\n Hit Enter to Start or Control+C to Cancel\\033[1;33m \\n (Warning: All Data on %s will be lost!)\\033[0m \\n" "$TARGET_DEVICE"
        read -r "confirm" && printf "\\n This can take a while...\\n"
        dd if=/dev/zero of="$TARGET_DEVICE" bs=512 count=4096 oflag=direct
        dd if="$final_iso" of="$TARGET_DEVICE" bs=4M status=progress && sync
        printf "Done!\\n"
      fi; exit 0;;
    '> add ISO to multiboot usb') multi_usb;;
    '> Exit') exit 0;;
    *) unset FINAL_MSG;;
  esac
done
}

#==============================================================================#
check_mounts
DISTRO="$(awk -F '"' '/^NAME/ {print $2}' /etc/os-release) $(uname -r)"
if [ "$make_efi" = 'yes' ]; then
  uefi_message='uefi enabled'
else
  uefi_message='uefi disabled'
fi
while [ -z "$SNAPSHOT_MENU_EVENT" ]; do

SNAPSHOT_MENU_LIST="${c3}   > ${c4}Options${c_end}
${c3}   > ${c4}Create a snapshot ($uefi_message)${c_end}
${c3}   > ${c4}Re-squash and make iso (no-copy)${c_end}
${c3}   > ${c4}Re-make efi files and iso (no-copy, no-squash)${c_end}
${c3}   > ${c4}Re-run xorriso only. (make iso, no-copy, no-squash)${c_end}
${c3}   > ${c4}Clear Work directory${c_end}
   < Exit"

update_snapshot_opt_menu(){
if [ -x /usr/bin/cpulimit ] || [ -x /usr/local/bin/cpulimit ]; then
  if [ "$use_cpu_limit" = 'yes' ]; then
    CPU_LIMIT_PLACEHOLDER="${c3} > ${c7}Limit CPU usage = ${c4}$use_cpu_limit${c_end}
${c3} > ${c7}CPU limit = ${c4}$cpu_limit_percent"
  else
    CPU_LIMIT_PLACEHOLDER="${c3} > ${c7}Limit CPU usage = ${c4}$use_cpu_limit${c_end}"
  fi
else
  CPU_LIMIT_PLACEHOLDER="${c3} > ${c7}Limit CPU usage  (not available)${c_end}"
  use_cpu_limit='no'
fi

if [ "$BASE_DISTRO_LIKE" = 'debian' ]; then
  KERNEL_PLACEHOLDER="${c3} > ${c7}Select kernel = ${c4}$kernel_image${c_end}
${c3} > ${c7}Select initrd = ${c4}$initrd_image${c_end}"
else
  KERNEL_PLACEHOLDER="${c3} > ${c7}Select kernel = ${c4}$kernel_image${c_end}"
fi
SNAPSHOT_OPTIONS="${c3} > ${c7}Distro name = ${c4}$DISTRO${c_end}
${c3} > ${c7}Set snapshot basename = ${c4}$snapshot_basename${c_end}
${c3} > ${c7}Add stamp to snapshot name = ${c4}$stamp${c_end}
${c3} > ${c7}Snapshot directory = ${c4}$snapshot_dir${c_end}
${c3} > ${c7}Set Work directory = ${c4}$work_dir${c_end}
${c3} > ${c7}Keep Work directory after snapshot has finished = ${c4}$save_work${c_end}
${c3} > ${c7}Edit list of excluded files${c_end}
${c3} > ${c7}Enable UEFI support = ${c4}$make_efi${c_end}
${c3} > ${c7}Enable Isohybrid = ${c4}$make_isohybrid${c_end}
${c3} > ${c7}Squashfs compression = ${c4}$SQUASHFS_COMPRESSION${c_end}
${KERNEL_PLACEHOLDER}
${CPU_LIMIT_PLACEHOLDER}
${c3} > ${c7}Change hostname = ${c4}$snapshot_hostname${c_end}
${c3} > ${c7}Delete wifi passowrds = ${c4}$delete_wifi_passwords${c_end}
${c3} > ${c7}Disable IPv6 = ${c4}$DISABLE_IPV6${c_end}
${c3} > ${c7}Insert pause before creating the iso file = ${c4}$WAIT_OPT${c_end}
< Back"
}
exec 2>&1
if [ -z "$LINE" ]; then
  LINE='1'
fi
TOPTEXT="$(text_box $text_box_opt " Create an iso snapshot of your system")"
SNAPSHOT_MENU_EVENT="$(awk_menu "$SNAPSHOT_MENU_LIST" | strip_color \
                     | awk -F '=' '{print $1}' | awk '{ gsub(/^[ \t]+|[ \t]+$/, ""); print }')"

case "$use_cpu_limit" in
  'yes') EXTRA_LINES='1';;
      *) EXTRA_LINES='0';;
esac
case "$SNAPSHOT_MENU_EVENT" in
  '> Options')
    SNAPSHOT_MENU_OPT='1'
    while [ "$SNAPSHOT_MENU_OPT" ] && [ "$SNAPSHOT_MENU_OPT" != '< Back' ]; do
      update_snapshot_opt_menu
      TOPTEXT="$(text_box $text_box_opt " Snapshot Options:")"
      SNAPSHOT_MENU_OPT="$(awk_menu "$SNAPSHOT_OPTIONS" | strip_color \
                         | awk -F '=' '{print $1}' | awk '{ gsub(/^[ \t]+|[ \t]+$/, ""); print }')"
      case "$SNAPSHOT_MENU_OPT" in
        '> Distro name') LINE='1'
           clear
           DISTRO="$(awk -F '"' '/^NAME/ {print $2}' /etc/os-release) $(uname -r)"
           printf "\\nThis is the name that will appear in the boot menu.\\n"
           printf "Enter name: "
           read -r NEW_STRING
           if [ "$NEW_STRING" ]; then
             string_test; check_chars; check_length_64; no_spaces
             if [ "$STRING_STATUS" = 'GOOD' ]; then
               DISTRO="$NEW_STRING"
               unset NEW_STRING STRING_STATUS
             fi
             unset SNAPSHOT_MENU_EVENT
             check_rm "$grub_template"
             generate_grub_template
           fi
           unset SNAPSHOT_MENU_EVENT;;

         '> Set snapshot basename') LINE='2'
           printf "\\n\\tEnter basename for the iso file: "
           read -r NEW_STRING
           string_test check_chars check_length_64 no_spaces
           if [ "$STRING_STATUS" = 'GOOD' ]; then
             snapshot_basename="$NEW_STRING"
             unset NEW_STRING STRING_STATUS
           fi
           unset SNAPSHOT_MENU_EVENT;;

         '> Add stamp to snapshot name') LINE='3'
           printf "\\nEnter stamp: "
           read -r NEW_STRING
           string_test check_chars check_length_64 no_spaces
           if [ "$STRING_STATUS" = 'GOOD' ]; then
             stamp="$NEW_STRING"
             unset NEW_STRING STRING_STATUS
           fi
           unset SNAPSHOT_MENU_EVENT;;

         '> Snapshot directory') LINE='4'
  	       unset NEW_PATH_STATUS
  	       clear
  	       printf "\\n\\tThe current snapshot directory is %s\\n" "$snapshot_dir"
  	       printf "\\n\\tThis is where the .iso file will be created."
  	       printf "\\n\\t(make sure to have some free space)"
  	       printf "\\n\\tEnter a path for the snapshot directory: "
           while [ "$NEW_PATH_STATUS" != 'OK' ]; do
             read -r "NEW_PATH"
             if [ -n "$NEW_PATH" ]; then
               check_path
             else
               break
             fi
           done
           if [ -n "$NEW_PATH" ] && [ "$NEW_PATH_STATUS" = 'OK' ]; then
             snapshot_dir="/$NEW_PATH"
           fi
           check_mkdir "$snapshot_dir"
           unset SNAPSHOT_MENU_EVENT;;

         '> Set Work directory') LINE='5'
           unset NEW_PATH_STATUS
           printf "\\n\\tThe current working directory is %s" "$work_dir"
           printf "\\n\\t(make sure to have some free space)"
           printf "\\n\\tEnter a path for the working directory: "
           unset NEW_PATH
           while [ "$NEW_PATH_STATUS" != 'OK' ]; do
             read -r "NEW_PATH"
             if [ "$NEW_PATH" ]; then
               check_path
             fi
           done
           if [ "$NEW_PATH" ]; then
             work_dir="/$NEW_PATH"
           fi
           check_mkdir "$work_dir"
           unset SNAPSHOT_MENU_EVENT;;

         '> Keep Work directory after snapshot has finished') LINE='6'
           case "$save_work" in
             'yes') save_work='no';;
                 *) save_work='yes';;
           esac
           unset SNAPSHOT_MENU_EVENT;;

         '> Edit list of excluded files') LINE='7'
           "$TEXT_EDITOR" "$snapshot_excludes"
           unset SNAPSHOT_MENU_EVENT;;

         '> Enable UEFI support') LINE='8'
           case "$make_efi" in
             'yes') make_efi='no'; uefi_message='uefi disabled';;
    	        'no') make_efi='yes'; uefi_message='uefi enabled';;
  	       esac
           unset SNAPSHOT_MENU_EVENT;;

         '> Enable Isohybrid') LINE='9'
           case "$make_isohybrid" in
             'yes') make_isohybrid='no';;
                 *) make_isohybrid='yes';;
           esac
           unset SNAPSHOT_MENU_EVENT;;

         '> Squashfs compression') LINE='10'
           TOPTEXT="Select compression for the squashfs file:"
           ASK_SQUASHFS_COMPRESSION="$(awk_menu "${c3} > ${c4}gzip${c_end}
${c3} > ${c4}lzo${c_end}
${c3} > ${c4}xz${c_end}
${c3} > ${c4}xz-smaller${c_end}
${c3} > ${c4}zstd${c_end}
${c3} < ${c_end}Back" | strip_color | awk '{print $2}')"
           unset TOPTEXT
           if [ "$ASK_SQUASHFS_COMPRESSION" ] && [ "$ASK_SQUASHFS_COMPRESSION" != 'Back' ]; then
             SQUASHFS_COMPRESSION="$ASK_SQUASHFS_COMPRESSION"
           fi
           case "$ASK_SQUASHFS_COMPRESSION" in
                 'gzip') squash_comp='-comp gzip';;
                  'lzo') squash_comp='-comp lzo';;
                   'xz') squash_comp='-comp xz';;
           'xz-smaller') squash_comp='-comp xz -Xbcj x86';;
                 'zstd') squash_comp='-comp zstd -Xcompression-level 20';;
           esac
           unset ASK_SQUASHFS_COMPRESSION
           unset SNAPSHOT_MENU_EVENT;;

         '> Select kernel') LINE='11'
           TOPTEXT="Select kernel"
           KERN="$(awk_menu "$(find / -maxdepth 1 \( -iname '*vmlinuz*' ! -iname '*.old' \); \
                              find /boot -maxdepth 1 -iname '*vmlinuz*' -type f)" )"
           if [ -L "$KERN" ]; then
             KERN="/$(readlink "$KERN")"
           fi
           if [ -f "$KERN" ]; then
             kernel_image="$KERN"
             printf "Kernel image: %s\\n" "$KERN" 2>&1 | log
           else
             printf "Error could not find kernel.\\n" 2>&1 | log
             exit 1
           fi
           unset SNAPSHOT_MENU_EVENT;;

         '> Select initrd') LINE='12'
           TOPTEXT="Select initrd"
           INITRD="$(awk_menu "$(find / -maxdepth 1 \( -iname '*initrd*' ! -iname '*.old' \); \
                              find /boot -maxdepth 1 -iname '*initrd*' -type f; \
                              find /boot -maxdepth 1 -iname '*initramfs*' -type f)" )"
           if [ -L "$INITRD" ]; then
             INITRD="/$(readlink "$INITRD")"
           fi
           if [ -f "$INITRD" ]; then
             initrd_image="$INITRD"
             printf "initrd image: %s\\n" "$INITRD" 2>&1 | log
           else
             printf "Error could not initrd.\\n" 2>&1 | log
             exit 1
           fi
           unset SNAPSHOT_MENU_EVENT;;

         '> Limit CPU usage') LINE='13'
           case "$use_cpu_limit" in
             'yes') EXTRA_LINES='0'; use_cpu_limit='no';;
                 *) EXTRA_LINES='1'; use_cpu_limit='yes';;
           esac
           unset SNAPSHOT_MENU_EVENT;;

         '> CPU limit') LINE="$((13+EXTRA_LINES))"
           TOPTEXT="$(text_box $text_box_opt " Set a limit to how much percent of the CPU you want to use
 to create this snapshot.
 This might be usefull to prevent laptops from overheating.
 But even a slight limit will increase the time a lot.")"
           SET_cpu_limit_percent="$(awk_menu "   95
   90
   85
   80
   75
   70
 < Back")"
           unset TOPTEXT
           if [ "$SET_cpu_limit_percent" -ge '70' ] && [ "$SET_cpu_limit_percent" -le '95' ]; then
             cpu_limit_percent="$SET_cpu_limit_percent"
           fi
           unset SNAPSHOT_MENU_EVENT;;

           '> Change hostname') LINE="$((14+EXTRA_LINES))"
           printf "\\nEnter a new hostname for this iso: "
           read -r NEW_STRING
           string_test check_chars space_to_underscore check_length_64
           if [ "$STRING_STATUS" = 'GOOD' ]; then
             snapshot_hostname="$NEW_STRING"
             unset NEW_STRING STRING_STATUS
           fi
           unset SNAPSHOT_MENU_EVENT;;

         '> Delete wifi passowrds') LINE="$((15+EXTRA_LINES))"
           case "$delete_wifi_passwords" in
             'yes') delete_wifi_passwords='no';;
              'no') delete_wifi_passwords='yes';;
	       esac
           unset SNAPSHOT_MENU_EVENT;;

         '> Disable IPv6') LINE="$((16+EXTRA_LINES))"
           case "$DISABLE_IPV6" in
             'yes') DISABLE_IPV6='no';;
              'no') DISABLE_IPV6='yes';;
	       esac
           unset SNAPSHOT_MENU_EVENT;;

         '> Insert pause before creating the iso file') LINE="$((17+EXTRA_LINES))"
           case "$WAIT_OPT" in
             'yes') WAIT_OPT='no';;
              'no') WAIT_OPT='yes';;
	       esac
           unset SNAPSHOT_MENU_EVENT;;
       esac
       done
       unset SNAPSHOT_MENU_EVENT;;

  "> Create a snapshot ($uefi_message)")
#    exec 2> "$ERROR_LOG"
    #echo "This may take a moment while the program checks for free space."
    cpu_limit_func
    check_snapshot_dirs
    check_disk_space
#    check_initrd
    housekeeping
#    if [ "$initrd_crypt" = "yes" ]; then
#      prepare_initrd_crypt
#    fi
    check_cd "$work_dir"
      copy_filesystem
      edit_system
      extra_steps
      copy_isolinux
      copy_kernel
      add_grub_pkgs
      if [ "$remove_cryptroot" = 'yes' ] || [ "$remove_resume" = 'yes' ]; then
        clean_initrd
      fi
      get_filename
      if [ "$make_efi" = 'yes' ]; then
        mkefi
      fi
      set_boot_options
      if [ "$edit_boot_menu" = 'yes' ]; then
        edit_boot_menus
      fi
      if [ "$snapshot_hostname" ]; then
        check_snapshot_hostname
      fi
      squash_filesystem
      make_iso_fs
      add_extras
      cleanup_snapshot
      final_message
      exit 0;;

  '> Re-squash and make iso (no-copy)')
    if [ "$make_efi" = 'yes' ]; then
      uefi_opt="-eltorito-alt-boot -e boot/grub/efiboot.img -isohybrid-gpt-basdat -no-emul-boot"
    fi
    check_cd "$work_dir"
      cpu_limit_func
      get_filename
      squash_filesystem
      make_iso_fs
      add_extras
      final_message
      exit 0;;

  '> Re-make efi files and iso (no-copy, no-squash)')
    if [ "$make_efi" != 'yes' ]; then
      exit 1
    fi
    check_cd "$work_dir"
      cpu_limit_func
      get_filename
      mkefi
      set_boot_options
      find_editor
      edit_boot_menus
      make_iso_fs
      add_extras
      final_message
      exit 0;;

  '> Re-run xorriso only. (make iso, no-copy, no-squash)')
    if [ "$make_efi" = 'yes' ]; then
      uefi_opt="-eltorito-alt-boot -e boot/grub/efiboot.img -isohybrid-gpt-basdat -no-emul-boot"
    fi
    check_cd "$work_dir"
      cpu_limit_func
      get_filename
      make_iso_fs
      add_extras
      final_message
      exit 0;;

  '> Clear Work directory') LINE='5'
    if [ -d "$work_dir" ]; then
      rm -vrd "${work_dir:?}/"*
    fi
    unset SNAPSHOT_MENU_EVENT;;

  '< Exit')
     check_rm "$LOGFILE_SNAPSHOT"
     exit 0;;

  *) unset SNAPSHOT_MENU_EVENT;;
esac
done

check_rmdir "$tempdir"
exit 0
}
#beginning of multi usb script=================================================#
#          ,      .            ______               _____  _____               
#          |\    /| |   | |      |    |      |   | |       |    \              
#          | \  / | |   | |      |    |  --- |   | |_____  |____/              
#          |  \/  | |   | |      |    |      |   |      |  |    \              
#          |      | |___| |____  |    |      |___| _____|  |____/              
#                                                                              
#
multi_usb(){
# prepare logfile
if [ -n "$SNAPSHOT_MENU_EVENT" ]; then
  LOG="$LOGFILE_SNAPSHOT"
elif [ -n "$MULTIUSB_LOG" ]; then
  LOG="$MULTIUSB_LOG"
  mv "$EARLY_LOG" "$LOG"
else
  LOG="/dev/null"
  check_rm "$EARLY_LOG"
fi
check_dependencies

# make sure mounting directories exist
check_mkdir "$MULTIUSB_DIR"
check_mkdir "$ISO_DIR"

inspect_iso(){
if [ -z "$1" ]; then
  echo "Error, the inspect_iso function is missing an argument."
  exit 1
fi
ISO="$1"

ISO_MNT="$tempdir/mnt/iso"
check_mkdir "$ISO_MNT"
printf "mounting mount %s to %s\\n" "$ISO_FILE" "$ISO_MNT" 2>&1 | log
mount "$ISO_FILE" "$ISO_MNT"

# find squashfs
SFS_PATH="$(find "$ISO_MNT" \( -iname "*.squashfs" -o -iname "*.sfs" \) -printf "%h\n" | awk 'NR == 1 {print}')"
SFS_NAME="$(find "$SFS_PATH" \( -iname "*.squashfs" -o -iname "*.sfs" \) -printf "%f\n" | awk 'NR == 1 {print}')"
SFS_FILE="$(echo "$SFS_PATH/$SFS_NAME" | sed "s:^$ISO_MNT::")"

# find kernel
KERN_PATH="$(find "$ISO_MNT" -name '*vmlinuz*' -printf "%h\n" | awk 'NR == 1 {print}')"
KERN_NAME="$(find "$KERN_PATH" -type f -iname "*vmlinuz*" -printf "%f\n" | awk 'NR == 1 {print}')"
KERN_FILE="$(echo "$KERN_PATH/$KERN_NAME" | sed "s:^$ISO_MNT::")"

# find initird
INIT_PATH="$(find "$ISO_MNT" \( -iname "*initrd*" -o -iname "*initramfs*" \) -printf "%h\n" | awk 'NR == 1 {print}')"
INIT_NAME="$(find "$INIT_PATH" \( -iname "*initrd*" -o -iname "*initramfs*" \) -printf "%f\n" | awk 'NR == 1 {print}')"
INIT_FILE="$(echo "$INIT_PATH/$INIT_NAME" | sed "s:^$ISO_MNT::")"

#==============================================================================#
# check which distro this iso is based on
#unsquashfs -no-wild -follow -dest "$tempdir/squash" "$SFS_FILE" /etc/os-release
# old versions of unsquashfs dont have these options
if [ -f "$ISO_MNT/$SFS_FILE" ]; then
  unsquashfs -dest "$tempdir/squash" "$ISO_MNT/$SFS_FILE" /etc/os-release
  if [ -L "$tempdir/squash/etc/os-release" ]; then
    REAL_FILE="$(readlink "$tempdir/squash/etc/os-release" | sed 's/^..//')"
    check_rm "$tempdir/squash/etc/os-release"
    check_rmdir "$tempdir/squash/etc"
    check_rmdir "$tempdir/squash"
    unsquashfs -dest $tempdir/squash "$ISO_MNT/$SFS_FILE" "$REAL_FILE"
    check_mkdir "$tempdir/squash/etc"
    mv "$tempdir/squash$REAL_FILE" "$tempdir/squash/etc/os-release"
  fi
fi

check_base_distro "$tempdir/squash"

# test if squashfs was detected
if [ ! -f "$ISO_MNT/$SFS_FILE" ]; then
  printf "Error: %s not found." "$SFS_FILE" 2>&1 | log
  exit 1
else
  printf "found squashfs: %s\\n" "$SFS_FILE" 2>&1 | log
fi

# test if kernel was detected
if [ ! -f "$ISO_MNT/$KERN_FILE" ]; then
  printf "Error: %s not found." "$KERN_FILE" 2>&1 | log
  exit 1
else
  printf "found kernel: %s\\n" "$KERN_FILE" 2>&1 | log
fi

# test if initrd was detected
if [ ! -f "$ISO_MNT/$INIT_FILE" ]; then
  printf "Error: init file %s not found." "$INIT_FILE" 2>&1 | log
  exit 1
else
  printf "found init file: %s\\n" "$INIT_FILE" 2>&1 | log
fi

umount "$ISO_MNT"
}

#==============================================================================#
#  █▀▀ █▀▄ █ █ █▀▄
#  █ █ █▀▄ █ █ █▀▄
#  ▀▀▀ ▀ ▀ ▀▀▀ ▀▀
create_new_usb_grub(){
# Exit if there is an unbound variable or an error
#set -o nounset
#set -o errexit
# Defaults
scriptname=$(basename "$0")
hybrid=0
eficonfig=0
interactive=0
data_part=2
data_fmt="ext4"
data_size=""
efi_mnt=""
data_mnt=""
data_subdir="boot"
repo_dir=""
tmp_dir="${TMPDIR-/tmp}"

# Show usage
showUsage(){
cat << EOF
	Script to prepare multiboot USB drive
	Usage: $scriptname [options] device [fs-type] [data-size]

	 device                         Device to modify (e.g. /dev/sdb)
	 fs-type                        Filesystem type for the data partition [ext3|ext4|vfat|ntfs]
	 data-size                      Data partition size (e.g. 5G)
	  -b,  --hybrid                 Create a hybrid MBR
	  -e,  --efi                    Enable EFI compatibility
	  -i,  --interactive            Launch gdisk to create a hybrid MBR
	  -h,  --help                   Display this message
	  -s,  --subdirectory <NAME>    Specify a data subdirectory (default: "boot")

EOF
}

#==============================================================================#
# Clean up when exiting
cleanup_multi_usb(){
  # Change ownership of files
  { [ "$data_mnt" ] && chown -R "$normal_user" "${data_mnt}"/* 2>/dev/null; } \
    || true
  # Unmount everything
  umount -f "$efi_mnt" 2>/dev/null || true
  umount -f "$data_mnt" 2>/dev/null || true
  # Delete mountpoints
  check_rmdir "$efi_mnt"
  check_rmdir "$data_mnt"
  check_rmdir "$repo_dir"
  # Exit
  exit "${1-0}"
}

#==============================================================================#
# Make sure USB drive is not mounted
unmountUSB(){
  umount -f "${1}"* 2>/dev/null || true
}

# Trap kill signals (SIGHUP, SIGINT, SIGTERM) to do some cleanup and exit
trap 'cleanup_multi_usb' 1 2 15

# Show help before checking for root
[ "$#" -eq 0 ] && showUsage && exit 0
case "$1" in
  -h|--help) showUsage; exit 0;;
esac

# Check for root
if [ "$(id -u)" -ne 0 ]; then
	printf "This script must be run as root. Using sudo...\\n" >&2
	exec sudo -k -- /bin/sh "$0" "$@" || cleanup_multi_usb 2
fi

# Get original user
normal_user="${SUDO_USER-$(who -m | awk '{print $1}')}"

# Check arguments
while [ "$#" -gt 0 ]; do
	case "$1" in
		-b|--hybrid) hybrid="1";;
		-e|--efi)	 eficonfig="1"; data_part="3";;
		-i|--interactive)	interactive="1";;
		-s|--subdirectory)	shift && data_subdir="$1";;
		/dev/*)	if [ -b "$1" ]; then
				  TARGET_DEVICE="$1"
				else
				  printf '%s: %s is not a valid device.\n' "$scriptname" "$1" >&2
				  cleanup_multi_usb 1
				fi;;
		[a-z]*)	data_fmt="$1";;
		[0-9]*)	data_size="$1";;
		*)	printf '%s: %s is not a valid argument.\n' "$scriptname" "$1" >&2
			cleanup_multi_usb 1;;
	esac
	shift
done

# Check for required arguments
if [ ! "$TARGET_DEVICE" ]; then
	printf '%s: No device was provided.\n' "$scriptname" >&2
	showUsage
	cleanup_multi_usb 1
fi

# Check for GRUB installation binary
#grub_cmd=$(command -v grub2-install) \
#    || grub_cmd=$(command -v grub-install) \
#    || cleanup_multi_usb 3
grub_cmd=/usr/sbin/grub-install

# Unmount device
unmountUSB "$TARGET_DEVICE"

# Confirm the device
if [ -b "$TARGET_DEVICE" ]; then
  lsblk -dno NAME,MODEL,SIZE "$TARGET_DEVICE"
else
  cleanup_multi_usb; exit
fi

printf 'Are you sure you want to use %s? [y/N] ' "$TARGET_DEVICE"
read -r ans
case "$ans" in
  [yY][eE][sS]|[yY])
    printf 'THIS WILL DELETE ALL DATA ON THE DEVICE. Are you sure? [y/N] '
	read -r answer2
	case $answer2 in
      [yY][eE][sS]|[yY])
     # Print all steps
     set -o verbose
     #set -x

     # Remove partitions
     sgdisk --zap-all "$TARGET_DEVICE"

     # Create GUID Partition Table
     sgdisk --mbrtogpt "$TARGET_DEVICE" || cleanup_multi_usb 10

     # Create BIOS boot partition (1M)
     sgdisk --new 1::+1M --typecode 1:ef02 \
         --change-name 1:"BIOS boot partition" "$TARGET_DEVICE" || cleanup_multi_usb 10

     # Create EFI System partition (50M)
     [ "$eficonfig" -eq 1 ] && \
         { sgdisk --new 2::+50M --typecode 2:ef00 \
         --change-name 2:"EFI System" "$TARGET_DEVICE" || cleanup_multi_usb 10; }

     # Set data partition size
     [ -z "$data_size" ] || data_size="+$data_size"

     # Set data partition information
     case "$data_fmt" in
	     ext[234]) type_code='8300'; part_name='Linux filesystem';;
	     msdos|fat|vfat|ntfs|exfat) type_code='0700'; part_name='Microsoft basic data';;
	     *) printf '%s: %s is an invalid filesystem type.\n' "$scriptname" "$data_fmt" >&2
	        showUsage; cleanup_multi_usb 1;;
     esac

     # Create data partition
     sgdisk --new ${data_part}::"${data_size}": --typecode ${data_part}:"$type_code" \
         --change-name ${data_part}:"$part_name" "$TARGET_DEVICE" || cleanup_multi_usb 10

     # Unmount device
     unmountUSB "$TARGET_DEVICE"

     partprobe "$TARGET_DEVICE"
     sleep 0.1
     # Interactive configuration?
     if [ "$interactive" -eq '1' ]; then
     # Create hybrid MBR manually # https://web.archive.org/web/20170705170024id_/https:/wiki.archlinux.org/index.php/Multiboot_USB_drive#Hybrid_UEFI_GPT_.2B_BIOS_GPT.2FMBR_boot
	   gdisk "$TARGET_DEVICE"
     elif [ "$hybrid" -eq '1' ]; then
	 # Create hybrid MBR
       if [ "$eficonfig" -eq '1' ]; then
         sgdisk --hybrid 1:2:3 "$TARGET_DEVICE" || cleanup_multi_usb 10
	   else
         sgdisk --hybrid 1:2 "$TARGET_DEVICE" || cleanup_multi_usb 10
	   fi
     fi

     # Set bootable flag for data partion
     sgdisk --attributes ${data_part}:set:2 "$TARGET_DEVICE" || cleanup_multi_usb 10

     # Unmount device
     unmountUSB "$TARGET_DEVICE"

     # Wipe BIOS boot partition
     wipefs -af "${TARGET_DEVICE}1" || true

     # Format EFI System partition
     if [ "$eficonfig" -eq 1 ]; then
       wipefs -af "${TARGET_DEVICE}2" || true
       mkfs.vfat -v -F 32 -n EFI "${TARGET_DEVICE}2" || cleanup_multi_usb 10
     fi

     # Wipe data partition
     wipefs -af "${TARGET_DEVICE}${data_part}" || true

     # Format data partition
     if [ "$data_fmt" = "ntfs" ]; then
     # Use mkntfs quick format
       mkfs -t "$data_fmt" -f -L "ISO_PART" "${TARGET_DEVICE}${data_part}" || cleanup_multi_usb 10
     else
       mkfs -t "$data_fmt" -L "ISO_PART" "${TARGET_DEVICE}${data_part}" || cleanup_multi_usb 10
     fi

     # Unmount device
     unmountUSB "$TARGET_DEVICE"

     # Create temporary directories
     efi_mnt=$(mktemp -p "$tmp_dir" -d efi.XXXX)   || cleanup_multi_usb 10
     data_mnt=$(mktemp -p "$tmp_dir" -d data.XXXX) || cleanup_multi_usb 10
     repo_dir=$(mktemp -p "$tmp_dir" -d repo.XXXX) || cleanup_multi_usb 10

     # Mount EFI System partition
     [ "$eficonfig" -eq 1 ] && { mount "${TARGET_DEVICE}2" "$efi_mnt" || cleanup_multi_usb 10; }

     # Mount data partition
     mount "${TARGET_DEVICE}${data_part}" "$data_mnt" || cleanup_multi_usb 10

     # Install GRUB for EFI
     [ "$eficonfig" -eq 1 ] && \
         { $grub_cmd --target=x86_64-efi --efi-directory="$efi_mnt" \
         --boot-directory="${data_mnt}/${data_subdir}" --removable --recheck \
         || cleanup_multi_usb 10; }

     # Install GRUB for BIOS
     $grub_cmd --force --target=i386-pc \
         --boot-directory="${data_mnt}/${data_subdir}" --recheck "$TARGET_DEVICE" \
         || cleanup_multi_usb 10

     # Install fallback GRUB
     $grub_cmd --force --target=i386-pc \
         --boot-directory="${data_mnt}/${data_subdir}" --recheck "${TARGET_DEVICE}${data_part}" \
         || true

     # Create necessary directories
     cat > "${data_mnt}/${data_subdir}/grub/grub.cfg" <<EOF
if loadfont /boot/grub/fonts/unicode.pf2 ; then
  set gfxmode=640x480
#  insmod efi_gop
#  insmod efi_uga
  insmod video_bochs
  insmod video_cirrus
  insmod gfxterm
  insmod jpeg
  insmod png
  terminal_output gfxterm
fi

insmod iso9660
insmod gzio
insmod part_gpt
insmod part_msdos
insmod ntfs
insmod ext2
insmod loopback

background_image /boot/grub/splash.png
set menu_color_normal=white/black
set menu_color_highlight=dark-gray/white
set timeout=15

source /boot/grub/menu.cfg
EOF

     # Clean up and exit
     #cleanup_multi_usb
     partprobe "$TARGET_DEVICE"
     sleep 0.1
     sync
     #scan_usb
     ;;
	 *) cleanup_multi_usb 3; multiusb_menu;;
  esac;;
  *) cleanup_multi_usb 3; multiusb_menu;;
esac

multiusb_menu
}

#==============================================================================#
#  ▀█▀ █▀▀ █▀█ █   ▀█▀ █▀█ █ █ █ █
#   █  ▀▀█ █ █ █    █  █ █ █ █ ▄▀▄
#  ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀▀▀ ▀ ▀ ▀▀▀ ▀ ▀
create_new_usb_isolinux(){
clear
printf "\\n\\n\\tWaring all files on %s will be lost" "$TARGET_DEVICE"
printf "\\n\\tPress enter to continue or Ctrl-C to cancel.\\n"
read -r confirm
for P in $(lsblk -nlo MOUNTPOINT "$TARGET_DEVICE"); do umount "$P"; done
dd if=/dev/zero of="$TARGET_DEVICE" bs=1M count=4
printf "o\\n n\\n p\\n 1\\n \\n \\n t\\n b\\n a\\n p\\n w\\n" | fdisk "$TARGET_DEVICE"
TARGET_PART="$TARGET_DEVICE"1
dd if=/dev/zero of="$TARGET_PART" bs=1M count=4
mkfs.fat -F32 -v -I "$TARGET_PART"
sync
partprobe "$TARGET_DEVICE"
sleep 0.1
syslinux -i "$TARGET_PART"
check_mkdir "$MULTIUSB_DIR"
mount -o rw "$TARGET_PART" "$MULTIUSB_DIR"
if [ -f /usr/lib/syslinux/modules/bios/vesamenu.c32 ]; then
  cp -R -v /usr/lib/syslinux/modules/bios/*.c32 "$MULTIUSB_DIR"
elif [ -f /usr/lib/syslinux/bios/vesamenu.c32 ]; then
  cp -R -v /usr/lib/syslinux/bios/*.c32 "$MULTIUSB_DIR"
fi
if [ -f /usr/lib/syslinux/mbr/mbr.bin ]; then
  dd bs=440 count=1 if=/usr/lib/syslinux/mbr/mbr.bin of="$TARGET_DEVICE"
elif [ -f /usr/lib/syslinux/bios/mbr.bin ]; then
  dd bs=440 count=1 if=/usr/lib/syslinux/bios/mbr.bin of="$TARGET_DEVICE"
fi
if [ -f /usr/share/backgrounds/multiusb_splash.png ]; then
  cp -v /usr/share/backgrounds/multiusb_splash.png "$MULTIUSB_DIR/splash.png"
fi
cat > "$MULTIUSB_DIR"/syslinux.cfg <<EOF
UI vesamenu.c32
PROMPT	1
MENU TITLE Boot Menu
MENU HSHIFT 8
MENU VSHIFT 8
MENU ROWS 7
MENU WIDTH 64
MENU TABMSGROW 29
MENU BACKGROUND splash.png
include menu.cfg
EOF
sync
umount "$TARGET_PART"
}

#==============================================================================#
#  █▀█ █▀▄ █▀▄   █▀▄ ▀█▀ █▀▀ ▀█▀ █▀▄ █▀█
#  █▀█ █ █ █ █   █ █  █  ▀▀█  █  █▀▄ █ █
#  ▀ ▀ ▀▀  ▀▀    ▀▀  ▀▀▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀
add_distro(){
# add space check before copy?
seperator
printf "Add distro\\n" 2>&1 | log
  find_iso_file(){
  unset ISO_PATH ISO_FILE MENU
  if [ "$SEARCH_REALLY_HARD" = '2' ]; then
    ISO_SEARCH="$(find / -iname "*.iso")"
  elif [ "$SEARCH_REALLY_HARD" = '1' ]; then
    ISO_SEARCH="$(find "$USERHOME" -iname "*.iso";
                  find /home/snapshot -maxdepth 3 -iname "*.iso";
                  find /home/*/Downloads -maxdepth 3 -iname "*.iso";
                  find /media -iname "*.iso")"
    SEARCH_REALLY_HARD='2'
  else
    ISO_SEARCH="$(find "$USERHOME" -maxdepth 7 -iname "*.iso";
                  find /home/snapshot -maxdepth 3 -iname "*.iso";
                  find /media -maxdepth 7 -iname "*.iso")"
    SEARCH_REALLY_HARD='1'
  fi

  for x in $ISO_SEARCH; do
    if ! file "$x" | grep "ISO 9660" > /dev/null; then
      ISO_SEARCH="$(echo "$ISO_SEARCH" | grep -sv "$x" | grep -v -e '^$')"
    fi
  done

  while [ -z "$MENU" ]; do
    TOPTEXT="$(text_box $text_box_opt " Select ISO:")"
    LINE='1'
    MENU_TEXT=$(echo "$ISO_SEARCH
 > search harder
 > enter path manually
 < Back")

    MENU="$(awk_menu "$MENU_TEXT")"
    if [ -f "$MENU" ]; then
      ISO_FILE="$MENU"
    elif [ "$MENU" = '> search harder' ]; then
      if [ "$SEARCH_REALLY_HARD" = '1' ]; then
        SEARCH_REALLY_HARD='2'
      else
        SEARCH_REALLY_HARD='1'
      fi
      unset MENU
    elif [ "$MENU" = '> enter path manually' ]; then
        printf "\\nEnter the absolute path of the iso file:\\n"
        read -r "ans"
        if file "$ans" | grep "ISO 9660" > /dev/null; then
          ISO_FILE="$ans"
          break
        else
          printf "Error: Not a valid iso.\\n"
          sleep 2.5
          unset MENU
        fi
    elif [ "$MENU" = '< Back' ]; then
      multiusb_menu
    else
      unset MENU
    fi
  done
  unset MENU TOPTEXT ISO_NAME
  ISO_FILE_USB="$(echo "$ISO_FILE" | awk -F/  '{print $NF; exit}')"
  ISO_TITLE="$(echo "$ISO_FILE_USB" | sed 's/.[Ii][Ss][Oo]$//')"
  }

  find_iso_file

  update_add_iso_menu(){
  ADD_ISO_MENU="
 > Set filename   = $ISO_FILE_USB
 > Set grub title = $ISO_TITLE

 > Continue
 < Back"
  }

  update_add_iso_menu
  set_iso_name(){
    unset ADD_ISO_MENU_EVENT
    while [ -z "$ADD_ISO_MENU_EVENT" ]; do
      if [ -n "$ADD_ISO_MENU_LINE_NR" ]; then
        LINE="$ADD_ISO_MENU_LINE_NR"
      else
        LINE='4'
      fi
      TOPTEXT="$(text_box $text_box_opt " Set ISO name:")"
      ADD_ISO_MENU_EVENT="$(awk_menu "$ADD_ISO_MENU" | awk -F '=' '{print $1}' | awk '{gsub(/^[ \t]+|[ \t]+$/, ""); print }')"
      case "$ADD_ISO_MENU_EVENT" in
        '> Set filename')
          printf "\\nEnter a filename for this iso: "
          read -r NEW_STRING
          string_test check_chars_allow_dots space_to_underscore check_length_255
          if [ -f "$MULTIUSB_DIR/boot/$NEW_STRING" ]; then
            STRING_STATUS='BAD'
          fi
          if [ "$STRING_STATUS" = 'GOOD' ]; then
            ISO_FILE_USB="$NEW_STRING"
            unset NEW_STRING STRING_STATUS
          fi
          unset ADD_ISO_MENU_EVENT
          if ! echo "$ISO_FILE_USB" | grep -i ".iso$"; then
            ISO_FILE_USB="$ISO_FILE_USB.iso"
          fi;;

        '> Set grub title')
          printf "\\nEnter a title for the grub menu entry of this iso.\\n"
          read -r NEW_STRING
          string_test check_chars check_length_80
          if [ "$STRING_STATUS" = "GOOD" ]; then
            ISO_TITLE="$NEW_STRING"
            unset NEW_STRING STRING_STATUS
          fi
          unset ADD_ISO_MENU_EVENT;;

        '< Back')
          find_iso_file
          unset ADD_ISO_MENU_EVENT;;

      esac
      update_add_iso_menu
    done
  }
  set_iso_name

  # check if file already exists
  while [ -f "$MULTIUSB_DIR/boot/$ISO_FILE_USB" ]; do
    set_iso_name
  done

  # copy iso file to multiboot device
  if [ ! -d "$MULTIUSB_DIR/boot" ]; then
    mkdir -p "$MULTIUSB_DIR/boot"
  fi
  cp -v "$ISO_FILE" "$MULTIUSB_DIR/boot/$ISO_FILE_USB" || check_exit

  inspect_iso "$ISO_FILE"

  case "$BASE_DISTRO_LIKE" in
    'hyperbola') GRUB_OPTIONS="img_dev=/dev/disk/by-label/ISO_PART img_loop=/boot/$ISO_FILE_USB hyperisobasedir=live hyperisolabel=liveiso arch=x86_64"
                 toram_opt="copytoram";;
         'arch') GRUB_OPTIONS="img_dev=/dev/disk/by-label/ISO_PART img_loop=/boot/$ISO_FILE_USB archisobasedir=live archisolabel=liveiso arch=x86_64"
                 toram_opt="copytoram";;
     'debian'|*) GRUB_OPTIONS="findiso=/boot/$ISO_FILE_USB"
                 toram_opt="toram";;
  esac

  # generate grub config for this iso
  GRUB_CFG_FILE="$(echo "$ISO_FILE_USB" | sed 's/iso$/cfg/')"
  cat > "$MULTIUSB_DIR"/boot/grub/"$GRUB_CFG_FILE" <<EOF
submenu "$ISO_TITLE" {
  menuentry "$ISO_TITLE (default)" {
    loopback loop (hd0,gpt3)/boot/$ISO_FILE_USB
    set root=(loop)
    linux (loop)$KERN_FILE boot=live ${GRUB_OPTIONS}
    initrd (loop)$INIT_FILE
  }
  menuentry "$ISO_TITLE (persistent)" {
    loopback loop (hd0,gpt3)/boot/$ISO_FILE_USB
    set root=(loop)
    linux (loop)$KERN_FILE boot=live ${GRUB_OPTIONS} persistent
    initrd (loop)$INIT_FILE
  }
  menuentry "$ISO_TITLE (toram)" {
    loopback loop (hd0,gpt3)/boot/$ISO_FILE_USB
    set root=(loop)
    linux (loop)$KERN_FILE boot=live ${GRUB_OPTIONS} ${toram_opt}
    initrd (loop)$INIT_FILE
  }
}
EOF
echo "source /boot/grub/$GRUB_CFG_FILE" >> "$MULTIUSB_DIR"/boot/grub/menu.cfg
}

#==============================================================================#
#  █▀▀ █▀▄ █▀▀ █▀█ ▀█▀ █▀▀   █▀█ █▀▀ █ █   █ █ █▀▀ █▀▄
#  █   █▀▄ █▀▀ █▀█  █  █▀▀   █ █ █▀▀ █▄█   █ █ ▀▀█ █▀▄
#  ▀▀▀ ▀ ▀ ▀▀▀ ▀ ▀  ▀  ▀▀▀   ▀ ▀ ▀▀▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀▀
create_new_usb(){
LINE="3"
TOPTEXT="$(text_box $text_box_opt " Select a bootloader:")"
MENU_TEXT="   > Grub EFI
   > Grub Legacy
   > Isolinux Legacy
   < Back"
LINE='1'
MENU="$(awk_menu "$MENU_TEXT")"
case "$MENU" in
	'> Grub EFI') echo "OK"; create_new_usb_grub -e "$TARGET_DEVICE";;
	'> Grub_Legacy') create_new_usb_grub "$TARGET_DEVICE";;
	'> Isolinux Legacy') create_new_usb_isolinux;;
	*) TARGET_DEVICE=""; select_usb;;
esac
unset MENU TOPTEXT
}

#==============================================================================#
#  █▀▀ █▀▀ █▀█ █▀█   █ █ █▀▀ █▀▄
#  ▀▀█ █   █▀█ █ █   █ █ ▀▀█ █▀▄
#  ▀▀▀ ▀▀▀ ▀ ▀ ▀ ▀   ▀▀▀ ▀▀▀ ▀▀
scan_usb(){
[ "$TARGET_DEVICE" ] || { printf "\\n\\tNo target device set.\\n"; exit 1; }
[ -b "$TARGET_DEVICE" ] || { printf "\\n\\tInvalid target device.\\n"; exit 1; }
[ -b "$TARGET_DEVICE" ] && partprobe "$TARGET_DEVICE"
sleep 0.1
FOUND_MEMTEST='0'
unset BOOT_TYPE

printf "scanning device...\\n" 2>&1 | log
for PART in $(lsblk -nplo NAME "$TARGET_DEVICE" | grep "$TARGET_DEVICE"'[1-9]'); do
#lsblk -nplo MOUNTPOINT "$PART"
  if [ -z  "$(lsblk -nplo MOUNTPOINT "$PART")" ]; then
    printf "mounting %s to %s\\n" "$PART" "$MULTIUSB_DIR" 2>&1 | log
    mount "$PART" "$MULTIUSB_DIR"
  fi
  if [ -f "$MULTIUSB_DIR/EFI/BOOT/BOOTX64.EFI" ]; then
    EFI_PART="$PART"
  fi
  if [ -f "$MULTIUSB_DIR"/boot/memtest86+.bin ] || [ -f "$MULTIUSB_DIR"/boot/memtest86.bin ]; then
    FOUND_MEMTEST="1"
    printf "found memtest86\\n" 2>&1 | log
  fi
  if [ "$(lsblk -no LABEL "$PART")" = "ISO_PART" ]; then
    TARGET_PART="$PART"
  fi
  if [ -f "$MULTIUSB_DIR"/syslinux.cfg ]; then
	[ -z "$BOOT_TYPE" ] && BOOT_TYPE='syslinux'
	DISTRO_LIST=$(grep "include" "$MULTIUSB_DIR"/menu.cfg | sed 's:include menu_::g;s:.cfg::g;s:.txt::g' | grep -v 'memtest')
	printf "found %s on %s\\n" "$DISTRO_LIST" "$PART" 2>&1 | log
    [ -z "$TARGET_PART" ] && TARGET_PART="$PART"
    break
  elif [ -f "$MULTIUSB_DIR"/boot/grub/grub.cfg ]; then
    [ -z "$BOOT_TYPE" ] && BOOT_TYPE='grub'
	DISTRO_LIST=$(grep "source" "$MULTIUSB_DIR"/boot/grub/menu.cfg | sed 's:include menu_::g;s:source /boot/grub/::g;s:.cfg::g;s:.txt::g' | grep -v 'memtest')
	printf "found %s on %s\\n" "$DISTRO_LIST" "$PART" 2>&1 | log
    [ -z "$TARGET_PART" ] && TARGET_PART="$PART"
    break
  else
	printf "nothing found on %s\\n" "$PART" 2>&1 | log
  fi

  printf "unmounting %s\\n" "$PART"
  umount "$PART"
done
if [ -b "$TARGET_PART" ]; then
  mount "$TARGET_PART" "$MULTIUSB_DIR"
else
  create_new_usb
fi
}

#==============================================================================#
#  █▀▄ █▀▀ █▄█ █▀█ █ █ █▀▀   █▀▄ ▀█▀ █▀▀ ▀█▀ █▀▄ █▀█
#  █▀▄ █▀▀ █ █ █ █ ▀▄▀ █▀▀   █ █  █  ▀▀█  █  █▀▄ █ █
#  ▀ ▀ ▀▀▀ ▀ ▀ ▀▀▀  ▀  ▀▀▀   ▀▀  ▀▀▀ ▀▀▀  ▀  ▀ ▀ ▀▀▀
remove_distro(){
if [ -n "$DISTRO_LIST" ]; then
  TOPTEXT="$(text_box $text_box_opt " Remove ISO:")"
  RM_DISTRO="$(awk_menu "$(echo "$DISTRO_LIST" | sed 's/^/ --> /g')
 < Back")"
  if [ "$RM_DISTRO" ] && [ "$RM_DISTRO" != "< Back" ]; then
    RM_DISTRO="$(echo "$RM_DISTRO" | sed 's/--> //g')"
    if [ "$BOOT_TYPE" = 'syslinux' ]; then
      [ -d "$MULTIUSB_DIR/$RM_DISTRO" ] && rm -rd  "${MULTIUSB_DIR:?}/$RM_DISTRO"
      check_rm "$MULTIUSB_DIR"/menu_"$RM_DISTRO".cfg
      sed -i "s:include menu_$RM_DISTRO.cfg::" "$MULTIUSB_DIR"/menu.cfg
    elif [ "$BOOT_TYPE" = 'grub' ]; then
      [ -d "$MULTIUSB_DIR"/boot/isos/"$RM_DISTRO" ] && rm -rd  "$MULTIUSB_DIR"/boot/isos/"$RM_DISTRO"
      [ -f "$MULTIUSB_DIR"/boot/grub/menu_"$RM_DISTRO".cfg ] && rm "$MULTIUSB_DIR"/boot/grub/menu_"$RM_DISTRO".cfg
      sed -i "s:source /boot/grub/menu_$RM_DISTRO.cfg::" "$MULTIUSB_DIR"/boot/grub/menu.cfg
    fi
  fi
  unset TOPTEXT
fi
unset MENU
scan_usb
}

#==============================================================================#
# █▄█ █▀▀ █▄█ ▀█▀ █▀▀ █▀▀ ▀█▀
# █ █ █▀▀ █ █  █  █▀▀ ▀▀█  █
# ▀ ▀ ▀▀▀ ▀ ▀  ▀  ▀▀▀ ▀▀▀  ▀
add_memtest(){
# memtest86+
if [ -f "$MULTIUSB_DIR"/boot/memtest86+.bin ]; then
  printf "memtest86+ is already there, nothing to do..."
else
  if ! [ -f /boot/memtest86+.bin ]; then
    printf "/boot/memtest86+.bin was not found"
    printf "install it now via apt? [Y/n]"
    read -r ans
    case "$ans" in
      N|n) multiusb_menu;;
      Y|y|*) apt install memtest86+;;
    esac
  fi
  if [ -f /boot/memtest86+.bin ]; then
    if [ "$BOOT_TYPE" = "grub" ]; then
      cp /boot/memtest86+.bin "$MULTIUSB_DIR"/boot/memtest86+.bin
    elif [ "$BOOT_TYPE" = "syslinux" ]; then
      cp /boot/memtest86+.bin "$MULTIUSB_DIR"/memtest86+.bin
    fi
  fi
fi
# memtest86
if [ -f "$MULTIUSB_DIR"/boot/memtest86.bin ]; then
  printf "memtest86 is already there, nothing to do..."
else
  if ! [ -f /boot/memtest86.bin ]; then
    printf "/boot/memtest86.bin was not found"
    printf "install it now via apt? [Y/n]"
    read -r ans
    case "$ans" in
      N|n) multiusb_menu;;
      Y|y|*) apt install memtest86;;
    esac
  fi
  if [ -f /boot/memtest86.bin ]; then
    if [ "$BOOT_TYPE" = 'grub' ]; then
      cp /boot/memtest86.bin "$MULTIUSB_DIR"/boot/memtest86.bin
    elif [ "$BOOT_TYPE" = 'syslinux' ]; then
      cp /boot/memtest86.bin "$MULTIUSB_DIR"/memtest86.bin
    fi
  fi
fi
if [ "$BOOT_TYPE" = 'grub' ]; then
  cat > "$MULTIUSB_DIR"/boot/grub/menu_memtest.cfg <<EOF
submenu "Memtest" {
    menuentry Memtest86 {
	linux16   /boot/memtest86.bin
    }
    menuentry Memtest86+ {
	linux16   /boot/memtest86+.bin
    }
}
EOF
  if ! grep -q "menu_memtest.cfg" "$MULTIUSB_DIR"/boot/grub/menu.cfg; then
    echo "source /boot/grub/menu_memtest.cfg" >> "$MULTIUSB_DIR"/boot/grub/menu.cfg
  fi
elif [ "$BOOT_TYPE" = "syslinux" ]; then
  if ! grep -q "memtest86" "$MULTIUSB_DIR"/syslinux.cfg; then
  echo "
LABEL memtest86+
    MENU LABEL Memtest86+
    LINUX memtest86+.bin
LABEL memtest86
    MENU LABEL Memtest86
    LINUX memtest86.bin
" >> "$MULTIUSB_DIR"/syslinux.cfg
  fi
fi
}

remove_memtest(){
  check_rm "${MULTIUSB_DIR:?}/boot/memtest86+.bin"
  check_rm "${MULTIUSB_DIR:?}/boot/memtest86.bin"
  check_rm "${MULTIUSB_DIR:?}/boot/grub/menu_memtest.cfg"
  check_rm "${MULTIUSB_DIR:?}/memtest86+.bin"
  check_rm "${MULTIUSB_DIR:?}/memtest86.bin"
  if [ -f "$MULTIUSB_DIR"/boot/grub/menu.cfg ]; then
    sed -i "s:source /boot/grub/menu_memtest.cfg::" "${MULTIUSB_DIR:?}/boot/grub/menu.cfg"
  fi
}

multiusb_datapart(){
  if [ -n "$(lsblk "$TARGET_PART" -no MOUNTPOINT)" ]; then
    umount "$TARGET_PART"
  fi
  printf "\\nthe script will continue when gparted is closed\\n"
  gparted "$TARGET_DEVICE"
}

multiusb_cleanup(){
  lsblk -dno MOUNTPOINT "$TARGET_PART" | grep "$MULTIUSB_DIR" && umount "$MULTIUSB_DIR";
  lsblk -dno MOUNTPOINT | grep "$ISO_DIR" && umount "$ISO_DIR";
  #[ -d "$MULTIUSB_DIR" ] && rm -rd "$MULTIUSB_DIR";
  #[ -d "$ISO_DIR" ] && rm -rd "$ISO_DIR";
}

#==============================================================================#
#  █▄█ █ █ █  ▀█▀ ▀█▀  █ █ █▀▀ █▀▄   █▄█ █▀▀ █▀█ █ █
#  █ █ █ █ █   █   █   █ █ ▀▀█ █▀▄   █ █ █▀▀ █ █ █ █
#  ▀ ▀ ▀▀▀ ▀▀▀ ▀  ▀▀▀  ▀▀▀ ▀▀▀ ▀▀    ▀ ▀ ▀▀▀ ▀ ▀ ▀▀▀
multiusb_menu(){
scan_usb
if [ "$FOUND_MEMTEST" = '1' ]; then
  MEMTEST_PLACEHOLDER='Remove memtest86+'
else
  MEMTEST_PLACEHOLDER='Add memtest86+'
fi

if [ -b "$TARGET_DEVICE" ]; then
  MENU_DEVICE=" > Selected device = $TARGET_DEVICE
 --> Scan usb device for images
 --> Create new multiboot usb device
 --> Add new image
 --> Remove image
 --> $MEMTEST_PLACEHOLDER"
else
  MENU_DEVICE=" > Select USB device"
fi
MENU_TEXT="$MENU_DEVICE
 > Add data partition
 < Back
 < Exit
\\n\\n\\n
BOOTLOADER: $BOOT_TYPE
IMAGES: $(echo "$DISTRO_LIST" | xargs)"
TOPTEXT="Multiboot_USB_Menu"
[ -z "$MULTIUSB_MENU_LINE" ] && MULTIUSB_MENU_LINE=3
LINE="$MULTIUSB_MENU_LINE"
unset MENU
while [ -z "$MENU" ]; do
  TOPTEXT="$(text_box $text_box_opt " Multi-USB Menu")"
  MENU="$(awk_menu "$MENU_TEXT")"
  case "$MENU" in
          '> Select USB device') MULTIUSB_MENU_LINE='1'; unset WARNING; select_usb;;
        '> Selected device = '*) MULTIUSB_MENU_LINE='1'; unset WARNING; select_usb;;
         '--> Scan usb device'*) MULTIUSB_MENU_LINE='3'; scan_usb;;
    '--> Create new multiboot'*) MULTIUSB_MENU_LINE='4'; create_new_usb;;
            '--> Add new image') MULTIUSB_MENU_LINE='5'; add_distro;;
             '--> Remove image') MULTIUSB_MENU_LINE='6'; remove_distro;;
           '--> Add memtest86+') MULTIUSB_MENU_LINE='7'; add_memtest; scan_usb;;
        '--> Remove memtest86+') MULTIUSB_MENU_LINE='8'; remove_memtest; scan_usb;;
         '> Add data partition') MULTIUSB_MENU_LINE='9'; multiusb_datapart;;
                       '< Back') MULTIUSB_MENU_LINE='3'; start_script;;
                       '< Exit') multiusb_cleanup; exit 0;;
  esac
done
unset TOPTEXT MENU
}
select_usb
scan_usb
while [ ! "$MENU" = '< Back' ]; do
  multiusb_menu
done && unset MENU
}

#==============================================================================#
gen_arch_initcpio_hooks(){
  # use the hooks from arch with slight modifications
  # https://gitlab.archlinux.org/mkinitcpio/mkinitcpio-archiso

  iso_root="$work_dir"/myfs
  check_mkdir "$iso_root"

  if [ ! -f "$iso_root"/etc/initcpio/hooks/archiso ]; then
    printf "generating %s/etc/initcpio/hooks/archiso\\n" "$iso_root" 2>&1 | log
    cat > "$iso_root"/etc/initcpio/hooks/archiso <<'EOF'
#!/bin/ash
#
# SPDX-License-Identifier: GPL-3.0-or-later

# args: source, newroot, mountpoint
_mnt_dmsnapshot() {
    local img="${1}"
    local newroot="${2}"
    local mnt="${3}"
    local img_fullname="${img##*/}"
    local img_name="${img_fullname%%.*}"
    local dm_snap_name="${dm_snap_prefix}_${img_name}"
    local ro_dev ro_dev_size rw_dev

    ro_dev="$(losetup --find --show --read-only -- "${img}")"
    ro_dev_size="$(blockdev --getsz "${ro_dev}")"

    if [ "${cow_persistent}" = "P" ]; then
        if [ -f "/run/archiso/cowspace/${cow_directory}/${img_name}.cow" ]; then
            msg ":: Found '/run/archiso/cowspace/${cow_directory}/${img_name}.cow', using as persistent."
        else
            msg ":: Creating '/run/archiso/cowspace/${cow_directory}/${img_name}.cow' as persistent."
            truncate -s "${cow_spacesize}" "/run/archiso/cowspace/${cow_directory}/${img_name}.cow"
        fi
    else
        if [ -f "/run/archiso/cowspace/${cow_directory}/${img_name}.cow" ]; then
            msg ":: Found '/run/archiso/cowspace/${cow_directory}/${img_name}.cow' but non-persistent requested, removing."
            rm -f "/run/archiso/cowspace/${cow_directory}/${img_name}.cow"
        fi
        msg ":: Creating '/run/archiso/cowspace/${cow_directory}/${img_name}.cow' as non-persistent."
        truncate -s "${cow_spacesize}" "/run/archiso/cowspace/${cow_directory}/${img_name}.cow"
    fi

    rw_dev="$(losetup --find --show "/run/archiso/cowspace/${cow_directory}/${img_name}.cow")"

    dmsetup create "${dm_snap_name}" --table \
        "0 ${ro_dev_size} snapshot ${ro_dev} ${rw_dev} ${cow_persistent} ${cow_chunksize}"

    if [ "${cow_persistent}" != "P" ]; then
        rm -f "/run/archiso/cowspace/${cow_directory}/${img_name}.cow"
    fi

    _mnt_dev "/dev/mapper/${dm_snap_name}" "${newroot}${mnt}" "-w" "defaults"
}

# args: source, newroot, mountpoint
_mnt_overlayfs() {
    local src="${1}"
    local newroot="${2}"
    local mnt="${3}"
    mkdir -p "/run/archiso/cowspace/${cow_directory}/upperdir" "/run/archiso/cowspace/${cow_directory}/workdir"
    mount -t overlay -o \
        "lowerdir=${src},upperdir=/run/archiso/cowspace/${cow_directory}/upperdir,workdir=/run/archiso/cowspace/${cow_directory}/workdir" \
        airootfs "${newroot}${mnt}"
}

# args: /path/to/image_file, mountpoint
_mnt_fs() {
    local img="${1}"
    local mnt="${2}"
    local img_fullname="${img##*/}"
    local img_loopdev

    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    if [ "${copytoram}" = "y" ]; then
        msg -n ":: Copying rootfs image to RAM..."

        # in case we have pv use it to display copy progress feedback otherwise
        # fallback to using plain cp
        if command -v pv >/dev/null 2>&1; then
            echo ""
            (pv "${img}" >"/run/archiso/copytoram/${img_fullname}")
            local rc=$?
        else
            (cp -- "${img}" "/run/archiso/copytoram/${img_fullname}")
            local rc=$?
        fi

        if [ $rc != 0 ]; then
            echo "ERROR: while copy '${img}' to '/run/archiso/copytoram/${img_fullname}'"
            launch_interactive_shell
        fi

        img="/run/archiso/copytoram/${img_fullname}"
        msg "done."
    fi
    img_loopdev="$(losetup --find --show --read-only -- "${img}")"
    _mnt_dev "${img_loopdev}" "${mnt}" "-r" "defaults"
}

# args: device, mountpoint, flags, opts
_mnt_dev() {
    local dev="${1}"
    local mnt="${2}"
    local flg="${3}"
    local opts="${4}"

    msg ":: Mounting '${dev}' to '${mnt}'"

    while ! poll_device "${dev}" 30; do
        echo "ERROR: '${dev}' device did not show up after 30 seconds..."
        echo "   Falling back to interactive prompt"
        echo "   You can try to fix the problem manually, log out when you are finished"
        launch_interactive_shell
    done

    if mount --mkdir -o "${opts}" "${flg}" "${dev}" "${mnt}"; then
        msg ":: Device '${dev}' mounted successfully."
    else
        echo "ERROR; Failed to mount '${dev}'"
        echo "   Falling back to interactive prompt"
        echo "   You can try to fix the problem manually, log out when you are finished"
        launch_interactive_shell
    fi
}

_verify_checksum() {
    local _status
    cd "/run/archiso/bootmnt/${archisobasedir}/${arch}" || exit 1
    sha512sum -c airootfs.sha512 >/tmp/checksum.log 2>&1
    _status=$?
    cd -- "${OLDPWD}" || exit 1
    return "${_status}"
}

_verify_signature() {
    local _status
    local sigfile="${1}"
    cd "/run/archiso/bootmnt/${archisobasedir}/${arch}" || exit 1
    gpg --homedir /gpg --status-fd 1 --verify "${sigfile}" 2>/dev/null | grep -E '^\[GNUPG:\] GOODSIG'
    _status=$?
    cd -- "${OLDPWD}" || exit 1
    return ${_status}
}

run_hook() {
    [ -z "${arch}" ] && arch="$(uname -m)"
    [ -z "${copytoram_size}" ] && copytoram_size="75%"
    [ -z "${archisobasedir}" ] && archisobasedir="arch"
    [ -z "${dm_snap_prefix}" ] && dm_snap_prefix="arch"
    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    [ -z "${archisodevice}" ] && archisodevice="/dev/disk/by-label/${archisolabel}"
    [ -z "${cow_spacesize}" ] && cow_spacesize="256M"
    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    if [ -n "${cow_label}" ]; then
        cow_device="/dev/disk/by-label/${cow_label}"
        [ -z "${cow_persistent}" ] && cow_persistent="P"
    elif [ -n "${cow_device}" ]; then
        [ -z "${cow_persistent}" ] && cow_persistent="P"
    else
        cow_persistent="N"
    fi

    [ -z "${cow_flags}" ] && cow_flags="defaults"
    [ -z "${cow_directory}" ] && cow_directory="persistent_${archisolabel}/${arch}"
    [ -z "${cow_chunksize}" ] && cow_chunksize="8"

    # set mount handler for archiso
    export mount_handler="archiso_mount_handler"
}

# This function is called normally from init script, but it can be called
# as chain from other mount handlers.
# args: /path/to/newroot
archiso_mount_handler() {
    local newroot="${1}"
    local sigfile fs_img

    if ! mountpoint -q "/run/archiso/bootmnt"; then
        _mnt_dev "${archisodevice}" "/run/archiso/bootmnt" "-r" "defaults"
    fi

    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    if [ "${checksum}" = "y" ]; then
        if [ -f "/run/archiso/bootmnt/${archisobasedir}/${arch}/airootfs.sha512" ]; then
            msg ":: Self-test requested, please wait..."
            if _verify_checksum; then
                msg "Checksum is OK, continue booting."
            else
                echo "ERROR: one or more files are corrupted"
                echo "see /tmp/checksum.log for details"
                launch_interactive_shell
            fi
        else
            echo "ERROR: checksum=y option specified but ${archisobasedir}/${arch}/airootfs.sha512 not found"
            launch_interactive_shell
        fi
    fi

    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    if [ "${verify}" = "y" ]; then
        if [ -f "/run/archiso/bootmnt/${archisobasedir}/${arch}/airootfs.sfs.sig" ]; then
            sigfile="airootfs.sfs.sig"
        elif [ -f "/run/archiso/bootmnt/${archisobasedir}/${arch}/airootfs.erofs.sig" ]; then
            sigfile="airootfs.erofs.sig"
        fi
        if [ -n "${sigfile}" ]; then
            msg ":: Signature verification requested, please wait..."
            if _verify_signature "${sigfile}"; then
                msg "Signature is OK, continue booting."
            else
                echo "ERROR: one or more files are corrupted"
                launch_interactive_shell
            fi
        else
            echo "ERROR: verify=y option specified but GPG signature not found in ${archisobasedir}/${arch}/"
            launch_interactive_shell
        fi
    fi

    if [ "${copytoram}" = "y" ]; then
        msg ":: Mounting /run/archiso/copytoram (tmpfs) filesystem, size=${copytoram_size}"
        mount --mkdir -t tmpfs -o "size=${copytoram_size}",mode=0755 copytoram /run/archiso/copytoram
    fi

    if [ -n "${cow_device}" ]; then
        # Mount cow_device read-only at first and remount it read-write right after. This prevents errors when the
        # device is already mounted read-only somewhere else (e.g. if cow_device and archisodevice are the same).
        _mnt_dev "${cow_device}" "/run/archiso/cowspace" "-r" "${cow_flags}"
        mount -o remount,rw "/run/archiso/cowspace"
    else
        msg ":: Mounting /run/archiso/cowspace (tmpfs) filesystem, size=${cow_spacesize}..."
        mount --mkdir -t tmpfs -o "size=${cow_spacesize}",mode=0755 cowspace /run/archiso/cowspace
    fi
    mkdir -p "/run/archiso/cowspace/${cow_directory}"
    chmod 0700 "/run/archiso/cowspace/${cow_directory}"

    if [ -f "/run/archiso/bootmnt/${archisobasedir}/${arch}/airootfs.sfs" ]; then
        fs_img="/run/archiso/bootmnt/${archisobasedir}/${arch}/airootfs.sfs"
    elif [ -f "/run/archiso/bootmnt/${archisobasedir}/${arch}/airootfs.erofs" ]; then
        fs_img="/run/archiso/bootmnt/${archisobasedir}/${arch}/airootfs.erofs"
    elif [ -f "/run/archiso/bootmnt/live/filesystem.squashfs" ]; then
        fs_img="/run/archiso/bootmnt/live/filesystem.squashfs"
    else
        echo "ERROR: no root file system image found"
        launch_interactive_shell
    fi
    _mnt_fs "${fs_img}" "/run/archiso/airootfs"
    if [ -f "/run/archiso/airootfs/airootfs.img" ]; then
        _mnt_dmsnapshot "/run/archiso/airootfs/airootfs.img" "${newroot}" "/"
    else
        _mnt_overlayfs "/run/archiso/airootfs" "${newroot}" "/"
    fi

    if [ "${copytoram}" = "y" ]; then
        umount -d /run/archiso/bootmnt
        rmdir /run/archiso/bootmnt
    fi
}

# vim: set ft=sh:
EOF
  fi

#==============================================================================#
  if [ ! -f "$iso_root"/etc/initcpio/install/archiso ]; then
    printf "generating %s/etc/initcpio/install/archiso\\n" "$iso_root" 2>&1 | log
    cat > "$iso_root"/etc/initcpio/install/archiso <<'EOF'
#!/usr/bin/env bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

build() {
    add_module "cdrom"
    add_module "loop"
    add_module "dm-snapshot"
    add_module "overlay"

    add_runscript

    add_binary /usr/lib/udev/cdrom_id
    add_binary blockdev
    add_binary dmsetup
    add_binary losetup
    add_binary mountpoint
    add_binary truncate
    add_binary gpg
    add_binary grep

    if command -v pv >/dev/null 2>&1; then
        add_binary pv
    else
        warning 'pv not found; falling back to cp for copy to RAM'
    fi

    add_file /usr/lib/udev/rules.d/60-cdrom_id.rules
    add_file /usr/lib/udev/rules.d/10-dm.rules
    add_file /usr/lib/udev/rules.d/95-dm-notify.rules
    add_file /usr/lib/initcpio/udev/11-dm-initramfs.rules /usr/lib/udev/rules.d/11-dm-initramfs.rules
    if [[ $ARCHISO_GNUPG_FD ]]; then
        mkdir -m 0700 -- "$BUILDROOT/gpg"
        gpg --homedir "$BUILDROOT/gpg" --import <&"$ARCHISO_GNUPG_FD"
    fi
}
EOF
  fi

#==============================================================================#
  if [ ! -f "$iso_root"/etc/initcpio/install/archiso_loop_mnt ]; then
    printf "generating %s/etc/initcpio/install/archiso_loop_mnt\\n" "$iso_root" 2>&1 | log
    cat > "$iso_root"/etc/initcpio/install/archiso_loop_mnt <<'EOF'
#!/usr/bin/env bash
#
# SPDX-License-Identifier: GPL-3.0-or-later

build() {
    add_runscript
}

help() {
    cat <<HELPEOF
  This hook loads the necessary modules for boot via loop device.
HELPEOF
}
EOF
  fi

#==============================================================================#
 if [ ! -f "$iso_root"/etc/initcpio/hooks/archiso_loop_mnt ]; then
  printf "generating %s/etc/initcpio/hooks/archiso_loop_mnt\\n" "$iso_root"2>&1 | log
  cat > "$iso_root"/etc/initcpio/hooks/archiso_loop_mnt <<'EOF'
#!/bin/ash
#
# SPDX-License-Identifier: GPL-3.0-or-later

run_hook() {
    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    [ -n "${img_label}" ] && img_dev="/dev/disk/by-label/${img_label}"
    [ -z "${img_flags}" ] && img_flags="defaults"
    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    if [ -n "${img_dev}" ] && [ -n "${img_loop}" ]; then
        export mount_handler="archiso_loop_mount_handler"
    fi
}

archiso_loop_mount_handler() {
    newroot="${1}"

    local _dev_loop

    msg ":: Setup a loop device from ${img_loop} located at device ${img_dev}"
    _mnt_dev "${img_dev}" "/run/archiso/img_dev" "-r" "${img_flags}"

    if _dev_loop=$(losetup --find --show --read-only "/run/archiso/img_dev/${img_loop}"); then
        export archisodevice="${_dev_loop}"
    else
        echo "ERROR: Setting loopback device for file '/run/archiso/img_dev/${img_loop}'"
        launch_interactive_shell
    fi

    archiso_mount_handler "${newroot}"

    # shellcheck disable=SC2154
    # defined via initcpio's parse_cmdline()
    if [ "${copytoram}" = "y" ]; then
        losetup -d "${_dev_loop}" 2>/dev/null
        umount /run/archiso/img_dev
    fi
}

# vim: set ft=sh:
EOF
fi
}

debootstrap_second_stage(){

  TOPTEXT="$(text_box $text_box_opt " WARNING:
  The --debootstrap-second-stage option is only meant to
  be run on a fresh debootstrap installation.
  This will install oprnrc and related packages.")"
  STAGE_MENU="$(awk_menu "< Exit\\n> Continue")"
  case "$STAGE_MENU" in
    '> Continue') unset TOPTEXT;;
      *|'< Exit') exit 0;;
  esac
  # install openrc after minbase is done or minbase will fail
##  chroot '/target' env LC_ALL=C apt install openrc sysvinit-core orphan-sysvinit-scripts systemctl procps elogind libpam-elogind
#  apt-mark hold libsystemd0
  apt install sysvinit-core orphan-sysvinit-scripts openrc

  # configure openrc
  rc_par="$(awk -F\" '/rc_parallel=/{print $2}' /etc/rc.conf)"
  sed -i "s@#*rc_parallel=\"$rc_par\"@rc_parallel=\"YES\"@" /etc/rc.conf
#  rc_iac="$(awk -F\" '/rc_interactive=/{print $2}' /target/etc/rc.conf)"
#  sed -i "s@#*rc_interactive=\"$rc_iac\"@rc_interactive=\"YES\"@" /target/etc/rc.conf
  if [ -x '/usr/bin/zsh' ]; then
    rc_shl="$(awk -F\" '/rc_shell=/{print $2}' /etc/rc.conf)"
    sed -i "s@#*rc_shell=\"$rc_par\"@rc_shell=\"/usr/bin/zsh\"@" /etc/rc.conf
  fi
  service_test="$(rc-status -Cs | awk '/nftables/ && /started/ {print "$1"}')"
  if [ "$service_test" ]; then
    service "$service_test" stop
    update-rc.d "$service_test" disable
  fi

  for file in /etc/rc0.d/K*; do
    s="$(basename "$(readlink "$file")")"
    /etc/init.d/$s stop
  done

#chroot '/target' env LC_ALL=C apt install elogind libpam-elogind orphan-sysvinit-scripts systemctl procps

#chroot /mnt /usr/bin/zsh

}


# ▀█▀ █ █ █▀▀ █▀█ █ █ █▀▀
#  █  █▄█ █▀▀ █▀█ █▀▄ ▀▀█
#  ▀  ▀ ▀ ▀▀▀ ▀ ▀ ▀ ▀ ▀▀▀
udisk_conf='/etc/udisks2/udisks2.conf'
udisks_automount_status(){
  old_defaults="$(grep '^defaults=' "$udisk_conf" | awk -F '=' 'END{printf "%s",$2}')"
  if echo "$old_defaults" | grep -q 'noauto' ; then
    udisks_automount_status="${c2}yes${c_end}"
  else
    udisks_automount_status="${c1}no${c_end}"
  fi
}

disable_udisks_automount_toggle(){
  if [ -f "$udisk_conf" ]; then
    old_defaults="$(grep '^defaults=' "$udisk_conf" | awk -F '=' 'END{printf "%s",$2}')"
    if [ "$old_defaults" = 'noauto' ]; then
      noauto_status='2'
    elif echo "$old_defaults" | grep -q 'noauto' ; then
      noauto_status='1'
      new_defaults="$(echo "$old_defaults" | sed 's/noauto//g;s/^,//;s/,,/,/g;s/,$//g')"
    elif echo "$old_defaults" | grep -q '.' ; then
      noauto_status='0'
      new_defaults="$(echo "$old_defaults" | sed 's/^,//;s/,,/,/g;s/,$//g;s/$/,noauto/;')"
    else
      unset noauto_status
      new_defaults="defaults=noauto"
    fi
    case "$noauto_status" in
      '0'|'1') sed -i "s@^defaults=$old_defaults@#defaults=$old_defaults\ndefaults=$new_defaults@g" "$udisk_conf";;
          '2') sed -i "s@^defaults=$old_defaults@#defaults=$old_defaults@g" "$udisk_conf";;
            *) if awk 'END{print}' /etc/udisks2/udisks2.conf | grep -q "."; then
                 printf "\\n" >> "$udisk_conf"
               fi
               if ! grep -q '\[defaults\]' "$udisk_conf"; then
                 printf "[defaults]\\n%s\\n" "$new_defaults" >> "$udisk_conf"
               else
                 printf "%s\\n" "$new_defaults" >> "$udisk_conf"
               fi;;
    esac
  else
    printf "%s Error: file %s not found\\n" "$0" "$udisk_conf"
    exit 1
  fi
  unset old_defaults new_defaults noauto_status
}

apt_config_menu(){
APT_OPTFILE_RECOMMENDED="$(find /etc/apt/apt.conf.d/*install_recommended \
                           || echo '/etc/apt/apt.conf.d/10install_recommended')"
if [ -f "$APT_OPTFILE_RECOMMENDED" ]; then
  APT_OPT_RECOMMENDED="$(awk -F \" '{print $2}' "$APT_OPTFILE_RECOMMENDED")"
else
  APT_OPT_RECOMMENDED="$(apt-config dump | awk -F \" '/APT::Install-Recommends/ {print $2}')"
fi
APT_OPT_RECOMMENDED="$(echo "$APT_OPT_RECOMMENDED" | check_bool)"

APT_OPTFILE_SUGGENSTIONS="$(find /etc/apt/apt.conf.d/*install_suggestions \
                           || echo '/etc/apt/apt.conf.d/10install_suggestions')"
if [ -f "$APT_OPTFILE_SUGGENSTIONS" ]; then
  APT_OPT_SUGGENSTIONS="$(awk -F \" '{print $2}' "$APT_OPTFILE_SUGGENSTIONS")"
else
  APT_OPT_SUGGENSTIONS="$(apt-config dump | awk -F \" '/APT::Install-Suggests/ {print $2}')"
fi
APT_OPT_SUGGENSTIONS="$(echo "$APT_OPT_SUGGENSTIONS" | check_bool)"


APT_OPTFILE_CERTCHECK="$(find /etc/apt/apt.conf.d/*cert_check \
                           || echo '/etc/apt/apt.conf.d/10cert_check')"
if [ -f "$APT_OPTFILE_CERTCHECK" ]; then
  APT_OPT_CERTCHECK="$(awk -F \" '{print $2; exit}' "$APT_OPTFILE_CERTCHECK")"
else
  APT_OPT_CERTCHECK='?'
fi
APT_OPT_CERTCHECK="$(echo "$APT_OPT_CERTCHECK" | check_bool)"

APT_MENU_TEXT="   install recommended pkgs       ${c_end}[$APT_OPT_RECOMMENDED]
   install suggested pkgs         ${c_end}[$APT_OPT_SUGGENSTIONS]
   require certificate checking   ${c_end}[$APT_OPT_CERTCHECK]
   < Back"
TOPTEXT="$(text_box $text_box_opt " Some configurations for apt:
 (Be carefull with these.)")"
LINE="$APT_MENU_LINE"
APT_MENU="$(awk_menu "$APT_MENU_TEXT")"
case "$APT_MENU" in
                         '< Back') break;;
      'install recommended pkgs'*) APT_MENU_LINE='1'
                                   case $(echo "$APT_OPT_RECOMMENDED" | strip_color) in
                                           '1') cat > "$APT_OPTFILE_RECOMMENDED" <<EOF
APT::Install-Recommends "0";
EOF
;;
                                     '0'|'?'|*) cat > "$APT_OPTFILE_RECOMMENDED" <<EOF
APT::Install-Recommends "1";
EOF
;;

                                   esac;;
        'install suggested pkgs'*) APT_MENU_LINE='2'
                                   case $(echo "$APT_OPT_SUGGENSTIONS" | strip_color) in
                                           '1') cat > "$APT_OPTFILE_SUGGENSTIONS" <<EOF
APT::Install-Suggests "0";
EOF
;;

                                     '0'|'?'|*) cat > "$APT_OPTFILE_SUGGENSTIONS" <<EOF
APT::Install-Suggests "1";
EOF
;;
                                   esac;;
  'require certificate checking'*) APT_MENU_LINE='3'
                                   case $(echo "$APT_OPT_CERTCHECK" | strip_color) in
                                           '1') cat > "$APT_OPTFILE_CERTCHECK" <<EOF
Acquire::https::Verify-Peer "0";
Acquire::https::Verify-Host "0";
EOF
;;
                                     '0'|'?'|*) cat > "$APT_OPTFILE_CERTCHECK" <<EOF
Acquire::https::Verify-Peer "1";
Acquire::https::Verify-Host "1";
EOF
;;
                                   esac;;
esac
}

net_settings_menu(){
NET4_REDIRECTS='/proc/sys/net/ipv4/conf/all/accept_redirects'
NET4_REDIRECT_OPT="$(cat "$NET4_REDIRECTS" | check_bool)"
NET6_REDIRECTS='/proc/sys/net/ipv6/conf/all/accept_redirects'
NET6_REDIRECT_OPT="$(cat "$NET6_REDIRECTS" | check_bool)"
NET_MENU_TEXT="   accept ipv4 redirects  ${c_end}[$NET4_REDIRECT_OPT]
   accept ipv6 redirects  ${c_end}[$NET6_REDIRECT_OPT]
   < Back"
TOPTEXT="$(text_box $text_box_opt " Some network options...")"
LINE="1"
NET_MENU="$(awk_menu "$NET_MENU_TEXT" | strip_color)"
case "$NET_MENU" in
  'accept ipv4 redirects  [0]') echo '1' > "$NET4_REDIRECTS";;
  'accept ipv4 redirects  [1]') echo '0' > "$NET4_REDIRECTS";;
  'accept ipv6 redirects  [0]') echo '1' > "$NET6_REDIRECTS";;
  'accept ipv6 redirects  [1]') echo '0' > "$NET6_REDIRECTS";;
esac
}

tweak_menu(){
udisks_automount_status
TWEAK_MENU_TEXT=" > keyboard settings    ${c3}$(awk -F\" '/XKBLAYOUT/{print $2}' /etc/default/keyboard)${c_end}
 > set time
 > set date
 > apt config
 > net settings
 > enable touchpad tapping
 > disable udisks2 automount ${c_end}[$udisks_automount_status]
 < back"
TOPTEXT="$(text_box $text_box_opt " ${c2}${txt_special} Various system settings ${c_end}\\n
----------------------------------------------
 ${c4}Changes will apply immediately on your system!
 ${c4}Time will be written to ${txt_i}hardware clock!${c_end}
----------------------------------------------
")"
TWEAK="$(awk_menu "$TWEAK_MENU_TEXT")"

case "$TWEAK" in
  '> enable touchpad tapping') line_tweak='6'
                               cat > /etc/X11/xorg.conf.d/30-touchpad.conf <<EOF
Section  "InputClass"
  Identifier  "touchpad overrides"
  Driver "libinput"
  MatchIsTouchpad "on"
  Option "Tapping" "on"
  Option "TappingButtonMap" "lmr"
EndSection
EOF
                               echo "$(text_box $text_box_opt "please logout and login again")"
                               exit 0;;
       '> keyboard settings'*) line_tweak='1'
                               dpkg-reconfigure keyboard-configuration \
                               && service keyboard-setup restart \
                               && udevadm trigger --subsystem-match=input --action=change;;
                 '> set time') line_tweak='2'; set_time;;
                 '> set date') line_tweak='3'; set_date;;
               '> apt config') line_tweak='4'
                               check_base_distro
                               if [ "$BASE_DISTRO_LIKE" != 'debian' ]; then
                                 printf "\\nthis only works on debian based systems...\\n"
                                 sleep 3; return
                               else
                                 while [ "$APT_MENU" != '< Back' ]; do
                                   apt_config_menu
                                 done
                               fi;;
             '> net settings') line_tweak='5'
                               while [ "$NET_MENU" != '< Back' ]; do
                                 net_settings_menu
                               done;;
    '> disable udisks2 automount'*) line_tweak='7'; disable_udisks_automount_toggle;;                  
    '< back'|*) unset TWEAK;;
esac
if [ "$TWEAK" ]; then
  unset TWEAK
  LINE="$line_tweak"
  tweak_menu
fi
}

#    _____ __             __     __  ___
#   / ___// /_____ ______/ /_   /  |/  /__  ____  __  __
#   \__ \/ __/ __ `/ ___/ __/  / /|_/ / _ \/ __ \/ / / /
#  ___/ / /_/ /_/ / /  / /_   / /  / /  __/ / / / /_/ /
# /____/\__/\__,_/_/   \__/  /_/  /_/\___/_/ /_/\__,_/

  TOPTEXT="$(text_box $text_box_opt "Version: $SCRIPT_VERSION" )"

START_MENU_TEXT='          > Install to disk
          > Make new ISO
          > Multiboot USB
          > Tweaks
          > bootstrap a minimal debian system
          > Help
          < Exit
'
START_MENU_TEXT=$(echo "$START_MENU_TEXT" | sed s"/PLACE_HOLDER/$SCRIPT_VERSION/")

HELP_TEXT='
optional parameter:
	-h, --help	show this help text
	-v, --version	display the version information
	-d, --debug	debug mode
	-i, --install	install from live system to disk
	-s, --snapshot	generate a iso file from running system
	-m, --multi	open the multiboot usb menu

 "Install to disk"
   Use this to install from a live system.

 "Make new ISO"
   This will create a bootable ISO file of your running system.
   Needs a lot of free space! (aprox 2.5 times the size of your system)
   First everything will be copied into a new directory, except some excluded files.
   A squashfs file of that directory will be created and a bootloader will be added.
   Those will then compressed into an ISO file.

 "Multiboot USB"
   Setup a multiboot usb device, add and remove ISOs.

 "Tweaks"
   A few random post-install options.
'

start_script(){
LINE='21'
OK='0'
while [ "$OK" = '0' ]; do
  TOPTEXT="$(text_box $text_box_opt " WAHEY:")"
  START_MENU="$(awk_menu "$START_MENU_TEXT")"
  case "$START_MENU" in
    '> Install to disk') OK='1'; make_install;;
       '> Make new ISO') OK='1'; make_snapshot;;
      '> Multiboot USB') OK='1'; multi_usb;;
             '> Tweaks') tweak_menu;;
         '> bootstrap'*) make_install --bootstrap;;
               '> Help') OK='1'; clear
                         printf "\\n%s\\nversion %s\\n%s\\n" "$0" "$SCRIPT_VERSION" "$HELP_TEXT"
                         exit 0;;
               '< Exit') OK='1'; check_rm "$EARLY_LOG"; exit 0;;
  esac
done
}

for PARAMETER in "$@"; do
  case "$PARAMETER" in
	   '--debug'|'-d') set -x; set -o verbose; DEBUG='TRUE';;
	 '--install'|'-i') OPTION='install';;
	   '--multi'|'-m') OPTION='multi'; check_rm "$EARLY_LOG";;
	'--snapshot'|'-s') OPTION='snapshot'; mv -v "$EARLY_LOG" "$LOGFILE_SNAPSHOT";;
         '--set-time') OPTION='set_time';;
         '--set-date') OPTION='set_date';;
     '--version'|'-v') check_rm "$EARLY_LOG"
	                   printf "Script Version %s\\n" "$SCRIPT_VERSION"
	                   printf "Use at your own risk\\n"
	                   exit 0;;
	 '--extract-init') extract_init "$2" "$3"
	                   exit 0;;
	 '--rebuild-init') rebuild_init "$2" "$3" "$4"
	                   exit 0;;
     '--debootstrap-second-stage') debootstrap_second_stage "$2"; exit 0;;
      '-h'|'--help'|*) check_rm "$EARLY_LOG"
	                   printf "%s" "$START_MENU_TEXT\\n" | awk '1;{if (NR==14) exit}'
	                   printf "\\n%s\\n" "$HELP_TEXT"
	                   exit 0;;
  esac
done

case "$OPTION" in
   'install') make_install;;
     'multi') multi_usb;;
  'snapshot') make_snapshot;;
  'set_date') set_date; exit 0;;
  'set_time') set_time; exit 0;;
  *)          start_script;;
esac
exit 0
