#!/bin/sh
# list devices, mount, unmount, format, create filesystems and more
# by Michael Thorberger
version='2024.08'

cachedir="$XDG_CACHE_HOME"
if [ -z "$cachedir" ]; then
  cachedir="$HOME/.cache"
fi

#shellcheck disable=SC2034,SC2154,SC1091
. "$SCRIPTS"/functions/text-box.func || exit 1
. "$SCRIPTS"/functions/awk-menu.func || exit 1
. "$SCRIPTS"/functions/colors.func   || exit 1

if [ ! "$FILEMANAGER" ]; then
  if command -v pcmanfm-qt > /dev/null; then
    FILEMANAGER="pcmanfm-qt"
  elif command -v pcmanfm > /dev/null; then
    FILEMANAGER="pcmanfm"
  fi
fi

text_box_opt="-c clr3 -t $(echo "${clr7}$0${clr_end}" | sed "s:$HOME:~:") -tpos 10"

awk_menu_bmsg="Keys: h=help  r=reload  m=mount  u=unmount"
awk_menu_help="$(text_box $text_box_opt $(echo "\\nPess h to go back\\n") )
$(echo "\\n Keys:\\n
   ${clr7}j${clr_end} - ${clr2}move up${clr_end}
   ${clr7}k${clr_end} - ${clr2}move down${clr_end}
   ${clr7}l${clr_end} - ${clr2}mount/unmount${clr_end}
   ${clr7}m${clr_end} - ${clr2}mount${clr_end}
   ${clr7}M${clr_end} - ${clr2}set mount point + mount${clr_end}
   ${clr7}u${clr_end} - ${clr2}unmount${clr_end}
   ${clr7}r${clr_end} - ${clr2}reload${clr_end}
   ${clr7}o${clr_end} - ${clr2}open encrypted fs${clr_end}
   ${clr7}c${clr_end} - ${clr2}close encrypted fs${clr_end}
   ${clr7}b${clr_end} - ${clr2}open file browser at mountpoint${clr_end}
   ${clr7}B${clr_end} - ${clr2}open file browser at mountpoint (sudo)${clr_end}
   ${clr7}P${clr_end} - ${clr2}partition disk${clr_end}
   ${clr7}F${clr_end} - ${clr2}create new filesystem${clr_end}
   ${clr7}L${clr_end} - ${clr2}change filesystem label${clr_end}
   ${clr7}e${clr_end} - ${clr2}try to boot in qemu${clr_end} (use this on your boot dev or external bootable devices)
   ${clr7}h${clr_end} - ${clr2}show this help${clr_end}
   ${clr7}/${clr_end} - ${clr2}find string${clr_end}
   ${clr7}q${clr_end} - ${clr2}quit${clr_end}")"

change_label(){
  fs_type="$(lsblk -no FSTYPE "$device")"

  while [ -z "$label" ]; do
    echo " ${clr3}$(text_box $text_box_opt "$(echo "  ${clr5}Enter a label for ${clr4}$device${clr_end}")")"
    read -r label
  done

  case "$fs_type" in
    'ext2'|'ext3'|'ext4') sudo e2label "$device" "${label}" || exit 1;;
   'exfat') sudo exfatlabel "$device" "${label}" || exit 1;;
    'vfat') sudo fatlabel "$device" "${label}" || exit 1;;
    'ntfs') sudo ntfslabel "$device" "${label}" || exit 1;;
         *) printf "Error: unrecognized fs_type %s" "$fs_type"; exit 1;;
  esac
}

lsblk_menu() {
  tmp1="$cachedir/mount_menu.tmp1"
  tmp2="$cachedir/mount_menu.tmp2"
  tmp3="$cachedir/mount_menu.tmp3"

  longlen="$(lsblk -npo NAME --tree | sed 's/[├└]/-/g;s/─/->/g' | awk 'length > l {l=length}; END{printf "%s",l}')"
  lsblk -po NAME --tree  \
  | sed 's/├/-/g;s/└/-/g;s/─/->/g' \
  | awk '{printf "%s%*.s\n", $0, 80, " "}' \
  | cut -b "-$((longlen))" > "$tmp1"

  lsblk -po NAME,TYPE,SIZE,FSTYPE,LABEL,MOUNTPOINT \
  | sed 's/├/-/g;s/└/-/g;s/─/->/g' \
  | cut -b "$((longlen+1))-" \
  | sed 's/^ //g' > "$tmp2"

  paste "$tmp1" "$tmp2" \
  | sed 's/\tTYPE/  TYPE/g;s/\tdisk/  disk/g;s/\tpart/  part/g;s/\tcrypt/  crypt/g;' > "$tmp3"

  if [ -f "$tmp1" ]; then
    rm "$tmp1"
  fi

  if [ -f "$tmp2" ]; then
    rm "$tmp2"
  fi

  awk_menu_keys="bBceoFmMPruL"
  awk_menu_tmsg="$(text_box $text_box_opt "${clr5}$(lsblk -dpo NAME,SIZE,VENDOR,MODEL)")
$(awk 'NR==1' "$tmp3")"
  if [ -z "$awk_menu_line_number" ]; then
    awk_menu_line_number="$(($(lsblk -npo NAME | awk END'{print NR}')+1))"
  fi

  input_text="$( awk 'NR > 1 {if ($NF ~ /^\//) print "\033[32m" "\033[1m" $0 "\033[0m"; else print $0}' "$tmp3")
${txt_b}> ${clr4}reload${clr_end}
${txt_b}> ${clr4}sync${clr_end}
${txt_b}> ${clr4}exit${clr_end}"

  awk_menu -q "$input_text"


  device="$(echo "$awk_menu_output" | grep -P -o "/dev/sd[a-z][0-9]?[0-9]?|/dev/mapper/.*|/dev/mmcblk.*" | awk '{print $1}')"
  if [ -b "$device" ]; then
  read device_type subs mountpoint <<EOF
$(lsblk -dno TYPE,SUBSYSTEMS,MOUNTPOINT "$device")
EOF
    #device_type="$(lsblk -dno TYPE "$device")"
    #mountpoint="$(lsblk -no MOUNTPOINT "$device")"
    #subs="$(lsblk -no SUBSYSTEMS "$device")"
    if [ "${subs#*"usb"}" != "$subs" ]; then
      is_usb='1'
    else
      is_usb='0'
    fi
  fi
  if [ "$awk_menu_key_press" ]; then
    case "$awk_menu_key_press" in
    'r') lsblk_menu
       ;;
    'o') if [ ! -b "$device" ]; then
         exit 1
       fi
       do_open_encrypted
       lsblk_menu
       ;;
    'c') check_device
       if lsblk -dnpo NAME | grep -w -q "$device"; then
         if lsblk -no MOUNTPOINT "$device" | grep '/'; then
           parent_device="$device"
           for part in $(lsblk -no MOUNTPOINT "$parent_device" | grep '/'); do
             device="$part"
             do_unmount
           done
         fi
       fi
       do_unmount
       sudo cryptsetup close "$device" || exit 1
       ;;
    'e') if [ -e '/dev/kvm' ]; then
         sudo qemu-system-x86_64 -enable-kvm -cpu host -m 2048 -drive format=raw,file="$device" || exit 1
       else
         sudo qemu-system-x86_64 -m 2048 -drive format=raw,file="$device" || exit 1
       fi
       ;;
    'b') mdir="$(echo "$awk_menu_output" | awk '{print $NF}' | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g")"
       if [ -d "$mdir" ]; then
         "$FILEMANAGER" "$mdir" &
       fi
       ;;
    'B') mdir="$(echo "$awk_menu_output" | awk '{print $NF}' | sed -r "s/\x1B\[([0-9]{1,3}(;[0-9]{1,2};?)?)?[mGK]//g")"
       if [ -d "$mdir" ]; then
         sudo -A "$FILEMANAGER" "$mdir" &
       fi
       ;;
    'm') check_device; do_mount;;
    'M') check_device; read_mount; do_mount;;
    'P') check_device; partition_device;;
    'F') check_device; format_device;;
    'u') check_device; do_unmount;;
    'L') check_device; change_label;;
    esac
  else
    awk_menu_output="$(echo "$awk_menu_output" | strip_color)"
    case "$awk_menu_output" in
      '> reload') lsblk_menu;;
        '> sync') awk_menu_line_number="$(($(lsblk -no NAME | awk END'{print NR}')+2))";sync; lsblk_menu;;
        '> exit') exit 0;;
               *) check_device; do_mount_unmount;;
    esac
  fi
}

check_device() {
  if [ ! -b "$device" ]; then
    exit 1
  fi
}

check_mount() {
  if [ -d "$mountpoint" ]; then
    awk_menu_tmsg=" ${clr3}$(text_box $text_box_opt "$(echo "
  ${clr4}$device ${clr5}is mounted at $mountpoint
  please unmount it first${clr_end}")" )${clr_end}"
    awk_menu_unset_keys
    awk_menu_line_number="1"
    awk_menu -q "  < ${clr3}OK${clr_end}"
    awk_menu_line_number="$old_awk_menu_line_number"
    lsblk_menu
    return
  fi
}

check_device_type_is_disk() {
  if [ "$device_type" = 'part' ]; then
    if [ "${device#*sd}" != "$device" ]; then
      device="$(echo "$device" | tr -d '[:digit:]')"
    elif [ "${device#*mmc}" != "$device" ]; then
      device="${device%%p**}"
    fi
  fi
  if [ "$device_type" != 'disk' ]; then
    exit 1
  fi
  check_device
}

partition_device() {
  old_awk_menu_line_number="$awk_menu_line_number"
  unset awk_menu_output
  awk_menu_tmsg=" ${clr3}$(text_box $text_box_opt "$(echo "
  ${clr5}You are about to partition ${clr4}$device
  All data on ${clr4}$device${clr_end} will be lost!
  are you sure?")" )${clr_end}"
  awk_menu_unset_keys
  awk_menu_line_number="1"
  unset selection
  selection="$(awk_menu "  < ${clr3}Back${clr_end}\\n  > ${clr3}Yes${clr_end}" | strip_color)"
  if [ "$selection" = '< Back' ] || [ -z "$selection" ]; then
    awk_menu_line_number="$old_awk_menu_line_number"
    lsblk_menu
    return
  fi
  check_device_type_is_disk
  check_mount

  unset partition_tools
  for x in gparted fdisk cfdisk gdisk parted; do
    if command -v "$x" >> /dev/null || command -v /sbin/"$x" >> /dev/null ; then
      partition_tools="$partition_tools\\n > $x"
    fi
  done
  partition_tools="$(echo "$partition_tools\\n < Exit" | awk '!/^[[:space:]]*$/ && !/^$/')"
  awk_menu_tmsg=" ${clr3}$(text_box $text_box_opt "$(echo "
  ${clr5}Select a tool to partition ${clr4}$device${clr_end}")")"
  part_tool="$(awk_menu "$partition_tools" | cut -b3-)"

  if [ ! "$part_tool" ]; then
    exit 1
  elif sudo which "$part_tool"; then
    eval sudo "${part_tool} ${device}"
  else
    exit 1
  fi
}

check_device_type_is_part() {
  if [ "$device_type" != 'part' ]; then
    awk_menu_tmsg=" ${clr3}$(text_box $text_box_opt "$(echo "
  ${clr4}$device${clr5} is not a partition
  please select a partition${clr_end}")")"
    awk_menu_line_number="1"
    unset selection
    selection="$(awk_menu "  < ${clr3}OK${clr_end}
  ${clr3}> format the raw disk${clr_end}" | strip_color)"
    if [ "$selection" = '< OK' ] || [ -z "$selection" ]; then
      awk_menu_line_number="$old_awk_menu_line_number"
      lsblk_menu
      return
    fi
  fi
}

format_device() {
  old_awk_menu_line_number="$awk_menu_line_number"
  awk_menu_tmsg=" ${clr3}$(text_box $text_box_opt "$(echo "
  ${clr5}You are about to create a new filesystem on ${clr4}$device
  All data on ${clr4}$device${clr_end} will be lost!
  are you sure?")" )${clr_end}"
  awk_menu_unset_keys
  awk_menu_line_number="1"
  unset selection
  selection="$(awk_menu "  < ${clr3}Back${clr_end}\\n  > ${clr3}Yes${clr_end}" | strip_color)"
  if [ "$selection" = '< Back' ] || [ -z "$selection" ]; then
    awk_menu_line_number="$old_awk_menu_line_number"
    lsblk_menu
    return
  fi
  check_mount

  awk_menu_tmsg=" ${clr3}$(text_box $text_box_opt "$(echo "
  ${clr5}What type of filesystem for ${clr4}$device${clr_end}")")"
  unset new_fs_type
  awk_menu_line_number="7"
  new_fs_type="$(awk_menu "  > ext4
  > ext3
  > ext2
  > exfat
  > vfat
  > ntfs
  > btrfs
  > xfs
  < Back
" | cut -b3-)"
  if [ "$new_fs_type" = 'Back' ] || [ -z "$new_fs_type" ]; then
    awk_menu_line_number="$old_awk_menu_line_number"
    lsblk_menu
    return
  fi

  echo " ${clr3}$(text_box $text_box_opt "$(echo "  ${clr5}Enter a label for ${clr4}$device${clr_end}")")"
  read -r label

  case "$new_fs_type" in
    'ext2') sudo mkfs.ext2 -F -L "${label}" "$device" && sudo tune2fs -r10000 "$device" || exit 1;;
    'ext3') sudo mkfs.ext3 -F -L "${label}" "$device" && sudo tune2fs -r10000 "$device" || exit 1;;
    'ext4') sudo mkfs.ext4 -F -L "${label}" "$device" && sudo tune2fs -r10000 "$device" || exit 1;;
   'exfat') sudo mkfs.exfat -n "${label}" "$device" || exit 1;;
    'vfat') sudo mkfs.vfat -n "${label}" -F 32 "$device" || exit 1;;
    'ntfs') sudo mkfs.ntfs -F -L "${label}" "$device" || exit 1;;
   'btrfs') sudo mkfs.btrfs -L "${label}" "$device" || exit 1;;
     'xfs') sudo mkfs.xfs -L "${label}" "$device" || exit 1;;
         *) awk_menu_line_number="$old_awk_menu_line_number"; lsblk_menu;;
  esac
}

read_mount() {
  alt_mount_point="1"
  printf "Enter full path to a directory:"
  read -r mount_dir
}

check_mount_dir() {
  if [ ! -d "$mount_dir" ]; then
    sudo mkdir -p "$mount_dir" || exit 1
    sudo chown -R "$USER":"$USER" "$mount_dir"
    sudo chmod -R 775 "$mount_dir"
  fi
}

do_mount() {
  if [ -n "$mountpoint" ]; then
    printf "%s ist already mounted!" "$device"
    exit 0
  fi
  devname="$(echo "$device" | awk -F '/' '{print $NF}')"

  if [ "$alt_mount_point" != "1" ] && [ "$is_usb" = '1' ] && pmount "$device"; then
    printf "\033[1;32m %s \033[0m has been mounted to \033[1;34m/media/%s \033[0m\\n" "$device" "$devname"
    if [ "$ask_fm" = "1" ]; then
      printf "do you want to open %s [N/y]?\\n" "$FILEMANAGER"
      read -r ans
      if [ "$ans" = "y" ]; then
        $FILEMANAGER "$mountpoint"
      fi
    fi
  elif [ "$alt_mount_point" = "1" ]; then
    check_mount_dir
    if sudo -p "Enter password:" mount ${FLAG} "$device" "$mount_dir"; then
      printf "\033[1;32m %s \033[0m has been mounted to \033[1;34m/media/%s \033[0m\\n" "$device" "$devname"
    fi
  else
    mount_dir="/media/$devname"
    check_mount_dir
    if sudo -p "Enter password:" mount ${FLAG} "$device" "$mount_dir"; then
      printf "\033[1;32m %s \033[0m has been mounted to \033[1;34m/media/%s \033[0m\\n" "$device" "$devname"
    fi
  fi
  if [ "$do_loop" = "1" ]; then
    sleep 1
  fi
}

do_unmount() {
  printf "please wait...\\n"
  sync
  if pumount "$device"; then
    printf "\033[1;34m %s \033[0m has been unmounted\\n" "$device"
  else
    sudo -p "Enter password:" umount "$device"
  fi
}

do_mount_unmount() {
  if [ -n "$mountpoint" ]; then
    do_unmount
  else
    do_mount
  fi
}

do_open_encrypted() {
  old_awk_menu_output="$awk_menu_output"
  old_awk_menu_line_number="$awk_menu_line_number"
  awk_menu_tmsg=" ${clr3}$(text_box $text_box_opt "$(echo "
  Open encrypted device $old_awk_menu_output\\n
  Select an option:")" )${clr_end}"
  awk_menu_unset_keys
  awk_menu_line_number="4"
  selction="$(awk_menu "  > ${clr7}luks${clr_end}
  > ${clr7}truecrypt${clr_end}
  > ${clr7}veracrypt${clr_end}
  < ${clr7}Back${clr_end}
  < ${clr7}Exit Script${clr_end}" | strip_color)"
  case "$selection" in
    '> luks') ctype="luks";;
    '> truecrypt'|'> veracrypt') ctype="tcrypt";;
    '< Exit Script') exit 0;;
    '< Back'|*) awk_menu_line_number="$old_awk_menu_line_number"; lsblk_menu;;
  esac
  awk_menu_unset_keys
  selection="$(awk_menu "  > ${clr7}Use keyfile${clr_end}
  > ${clr7}Enter key manually${clr_end}
  < ${clr7}Back${clr_end}
  < ${clr7}Exit Script${clr_end}" | strip_color)"
  case "$selection" in
    '> Enter key manually') return;;
    '> Use keyfile') printf "Enter path to key file: "
                     read -r keyfile
                     [ -f "$keyfile" ] || exit 1 ;;
    '< Back') awk_menu_line_number="$old_awk_menu_line_number"; lsblk_menu;;
    '< Exit Script') exit 0;;
    *) if [ -z "$awk_menu_output" ]; then
         exit 0
       else
         exit 1
       fi
  esac

  printf "Enter a name for this device: "
  read -r devname

  if [ -f "$keyfile" ]; then
    cat "$keyfile" | sudo cryptsetup open "$device" --type "$ctype" "$devname"
  else
    sudo cryptsetup open "$device" --type "$ctype" "$devname"
  fi
}
sleep 0.1
while [ "$#" -gt 0 ]; do
  case "$1" in
    '-x')           ask_fm="1"; shift;;
    '-l'|'--loop') do_loop="1"; shift;;
  esac
done

if [ "$do_loop" = "1" ]; then
  while [ "$awk_menu_output" != "> Exit Script" ]; do
    unset device
    lsblk_menu
    awk_menu_line_number="$(echo "$input_text" | grep -nw "$device" | cut -d\: -f1)"
  done
else
  unset device
  lsblk_menu
fi
exit 0
