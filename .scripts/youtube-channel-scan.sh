#!/bin/sh
# slow and painfull script to get information
# on all videos of a given youtube channel

read_url() {
while [ -z "$URL" ]; do
  echo "Paste a link to a Youtube channel:"
  read URL
done
}
[ "$1" ] && URL="$1" || read_url

CHANNEL_ID="$(echo $URL | sed 's;youtube.com/; ;g' | awk '{print $2}' | cut -d '/' -f2)"

youtube-dl --skip-download --youtube-skip-dash-manifest "$URL" | tee ~/.cache/yt-channel-"$CHANNEL_ID".tmp
awk '/^\[youtube\]/ {print $2}' ~/.cache/yt-channel-"$CHANNEL_ID".tmp | sed 's;:$;;g;s;^;https://www.youtube.com/watch?v=;g' > ~/.cache/yt-channel-"$CHANNEL_ID".txt

echo "gathering information..."

COUNTER="0"
LINES="$(wc -l ~/.cache/yt-channel-"$CHANNEL_ID".txt | cut -d ' ' -f1)"
for URL in $(cat ~/.cache/yt-channel-"$CHANNEL_ID".txt); do
  COUNTER="$(($COUNTER+1))"
  echo "$COUNTER / $LINES"
  VIDEO_ID="$(echo "$URL" | cut -d '=' -f 2)"
  VIDEO_TITLE="$(youtube-dl --get-title "$URL")"
  VIDEO_DURATION="$(youtube-dl --get-duration "$URL")"
  VIDEO_DESCRIPTION="$(youtube-dl --get-description "$URL")"
  FILENAME=yt-"$CHANNEL_ID"-scan.txt
  echo "=== $COUNTER === \n$URL\n$VIDEO_TITLE\n$VIDEO_DURATION\n$VIDEO_DESCRIPTION\n\n" >> $FILENAME
done

echo "removing temporary files..."
rm ~/.cache/yt-channel-"$CHANNEL_ID".tmp
rm ~/.cache/yt-channel-"$CHANNEL_ID".txt

echo "Done."
exit 0
